﻿using fernicherLibrary.BizData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fernichercafe
{
    public partial class Website : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            Control ctlHome = LoadControl("~/UserControls/GlobalHeaderWeb.ascx");


            Control ctlFoot = LoadControl("~/UserControls/GlobalFooterWeb.ascx");

            GlobalHeaderContent.Controls.Add(ctlHome);            GlobalFooterContent.Controls.Add(ctlFoot);

            if (this.Page.Request.QueryString.ToString().Length == 0)
            {
                Control ctlAdminDashboard = LoadControl("~/UserControls/IndexWeb.ascx");                MiddleCenterContent.Controls.Add(ctlAdminDashboard);
            }


            if (this.Page.Request.QueryString.Get("Action").ToString().Equals("ProductListWeb"))            {                Control ctlAdminDashboard = LoadControl("~/UserControls/ProductListWeb.ascx");                MiddleCenterContent.Controls.Add(ctlAdminDashboard);            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("IndexWeb"))            {                Control ctlAdminDashboard = LoadControl("~/UserControls/IndexWeb.ascx");                MiddleCenterContent.Controls.Add(ctlAdminDashboard);            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("SubCategoryWeb"))            {                Control ctlAdminDashboard = LoadControl("~/UserControls/SubCategoryWeb.ascx");                MiddleCenterContent.Controls.Add(ctlAdminDashboard);            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("ManageDetailsWeb"))            {                Control ctlAdminDashboard = LoadControl("~/UserControls/ManageDetailsWeb.ascx");                MiddleCenterContent.Controls.Add(ctlAdminDashboard);            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("About-UsWeb"))            {                Control ctlAdminDashboard = LoadControl("~/UserControls/About-UsWeb.ascx");                MiddleCenterContent.Controls.Add(ctlAdminDashboard);            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("Contact-UsWeb"))            {                Control ctlAdminDashboard = LoadControl("~/UserControls/Contact-UsWeb.ascx");                MiddleCenterContent.Controls.Add(ctlAdminDashboard);            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("BlogsWeb"))            {                Control ctlAdminDashboard = LoadControl("~/UserControls/BlogsWeb.ascx");                MiddleCenterContent.Controls.Add(ctlAdminDashboard);            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("BrandsWeb"))            {                Control ctlAdminDashboard = LoadControl("~/UserControls/BrandsWeb.ascx");                MiddleCenterContent.Controls.Add(ctlAdminDashboard);            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("privacypolicyWeb"))            {                Control ctlAdminDashboard = LoadControl("~/UserControls/privacypolicyWeb.ascx");                MiddleCenterContent.Controls.Add(ctlAdminDashboard);            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("termsandconditionsWeb"))            {                Control ctlAdminDashboard = LoadControl("~/UserControls/termsandconditionsWeb.ascx");                MiddleCenterContent.Controls.Add(ctlAdminDashboard);            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("searchlist"))            {
                Control ctlUserDetails = LoadControl("~/UserControls/searchlist.ascx");                MiddleCenterContent.Controls.Add(ctlUserDetails);            }


            buildhtml();
        }

        private void buildhtml()
        {
            BizDataUser obj = new BizDataUser();
            DataTable dt = obj.GetCompanyInfo();
            if (dt.Rows.Count > 0)
            {
                string html = "",html2="";
                html += "<p class='postal-address-footer'>" + dt.Rows[0]["address"].ToString() + "</p>";
                phldfooteraddress.Controls.Add(new Literal { Text = html.ToString() });
                html2 += dt.Rows[0]["WorkingHours"].ToString();
                html2 += " | Sun: Holiday";
                phldfootertiming.InnerText = html2;
                phldmobile.InnerText = dt.Rows[0]["mobileNo"].ToString();

                maplocation.Src = dt.Rows[0]["MapLocation"].ToString();

               
            }
            
        }
    }
}