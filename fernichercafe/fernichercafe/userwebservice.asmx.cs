﻿using fernicherLibrary.BizData;
using fernicherLibrary.Entity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fernichercafe
{
    /// <summary>
    /// Summary description for userwebservice
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [ScriptService]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class userwebservice : System.Web.Services.WebService
    {

        [WebMethod]
        public string AddRating(string name,string categoryname, string email, string message, string rating, string productid)
        {
            if (name != "" || message != "" || productid != "")
            {
                float ratings = 0.00F;

                if (rating != "")
                {
                    ratings = float.Parse(rating);
                }
                
                GeneralEntitie ge = new GeneralEntitie();
                ge.productid = productid;
                ge.enquiryname = name;
                ge.enquiryemail = email;
                ge.enquirycomments = message;
                ge.rating = ratings;
                ge.categoryname = categoryname;
                BizDataUser biz = new BizDataUser();
                biz.AddRating(ge);
            }
            return "Success";

        }
        [WebMethod]
        public string CallBack(string name, string email, string message, string mobile)
        {
            if (name.Trim() == "" || email.Trim() == "" || mobile.Trim() == "") { }
            else
            {
                BizDataUser biz = new BizDataUser();
                GeneralEntitie ge = new GeneralEntitie();
                ge.enquiryname = name;
                ge.enquirymobile = mobile;
                ge.enquiryemail = email;
                ge.enquirycomments = message;
                biz.InsertCallBack(ge);

            }
            return "success";
        }
        [WebMethod]
        public string getEnquiryImage(string obj)
        {
            if (obj != "null" && obj != "")
            {
                RootObject root = new RootObject();
                var result = JsonConvert.DeserializeObject<RootObject>(obj);
                BizDataUser biz = new BizDataUser();
                root.ProductList = biz.getEnquiryImages(result);
                var final = root.ProductList.ToArray();
                var resultfinal = JsonConvert.SerializeObject(final);
                return resultfinal;
            }
            else
            {
                return "Failed";
            }
        }

        [WebMethod]
        public string MultipleProductEnquiry(string Name, string Email, string Number, string Comments, string obj)
        {
            BizDataUser biz = new BizDataUser();
            GeneralEntitie ge = new GeneralEntitie();
            RootObject root = new RootObject();
            var result = JsonConvert.DeserializeObject<RootObject>(obj);
            if (result != null)
            {
                if (result.ProductList.Count > 0)
                {
                    for (int i = 0; i < result.ProductList.Count; i++)
                    {
                        ge.productid = result.ProductList[i].productid.ToString();
                        ge.subcategoryid = result.ProductList[i].subcategoryid.ToString();
                        ge.enquiryname = Name;
                        ge.categoryname = "";
                        ge.enquirymobile = Number;
                        ge.enquiryemail = Email;
                        ge.enquirycomments = Comments;
                        biz.InsertEnquiry(ge);
                    }
                }
            }
            else
            {
                ge.enquiryname = Name;
                ge.categoryname = "";
                ge.enquirymobile = Number;
                ge.enquiryemail = Email;
                ge.enquirycomments = Comments;
                biz.InsertEnquiry(ge);
            }
            return "Success";
        }

        [WebMethod]
        public string AddEnquiry(string obj)
        {
            BizDataUser biz = new BizDataUser();
            GeneralEntitie ge = new GeneralEntitie();
            RootObject root = new RootObject();
            var result = JsonConvert.DeserializeObject<RootObject>(obj);
            if (result != null)
            {
                if (result.ProductList.Count > 0)
                {
                    for (int i = 0; i < result.ProductList.Count; i++)
                    {
                        ge.productid = result.ProductList[i].productid.ToString();
                        ge.subcategoryid = "0";
                        ge.enquiryname = result.ProductList[i].name.ToString();
                        ge.categoryname = result.ProductList[i].categoryname.ToString();
                        ge.enquirymobile = result.ProductList[i].mobile.ToString();
                        ge.enquiryemail = result.ProductList[i].email.ToString();
                        ge.enquirycomments = result.ProductList[i].comments.ToString();
                        biz.InsertEnquiry(ge);

                    }
                }
            }
            return "Success";
        }

        [WebMethod]
        public List<ProductEntities> populateproductdropdown(string id)
        {
            BizDataProducts user = new BizDataProducts();
            DataSet ds = user.GetAllKidProductsfordropdown(Convert.ToInt32(id), "Kid");
            DataTable dt = ds.Tables[0];
            List<ProductEntities> productlist = new List<ProductEntities>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ProductEntities product = new ProductEntities();
                product.productid = Convert.ToInt32(dt.Rows[i]["productkidid"]);
                product.productname = dt.Rows[i]["productname"].ToString();

                productlist.Add(product);
            }

            return productlist;
        }

        [WebMethod]
        public List<ProductEntities> populatemapleveldropdown(string id,string idname)
        {
            BizDataProducts user = new BizDataProducts();
            DataSet ds = user.GetAllMaplevelfordropdown(Convert.ToInt32(id),Convert.ToInt32(idname));
            DataTable dt = ds.Tables[0];
            List<ProductEntities> productlist = new List<ProductEntities>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ProductEntities product = new ProductEntities();
                product.productid = Convert.ToInt32(dt.Rows[i]["mapid"]);
                product.productname = dt.Rows[i]["mapname"].ToString();

                productlist.Add(product);
            }

            return productlist;
        }

        [WebMethod]
        public List<ProductEntities> populatesubcategorydropdown(string id)
        {
            BizDataProducts user = new BizDataProducts();
            DataSet ds = user.fillsubcategorybycategoryid(id);
            DataTable dt = ds.Tables[0];
            List<ProductEntities> productlist = new List<ProductEntities>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ProductEntities product = new ProductEntities();
                product.productid = Convert.ToInt32(dt.Rows[i]["subcategoryid"]);
                product.productname = dt.Rows[i]["subcategoryname"].ToString();

                productlist.Add(product);
            }

            return productlist;
        }
        //populateproductdropdown
        [WebMethod]
        public List<ProductEntities> populateproductdropdown2(string id,string  idname)
        {
            BizDataProducts user = new BizDataProducts();
            DataSet ds = user.GetAllKidProductsN(Convert.ToInt32(id), idname);
            DataTable dt = ds.Tables[0];
            List<ProductEntities> productlist = new List<ProductEntities>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ProductEntities product = new ProductEntities();
                product.productid = Convert.ToInt32(dt.Rows[i]["productkidid"]);
                product.productname = dt.Rows[i]["productname"].ToString();

                productlist.Add(product);
            }

            return productlist;
        }

        [WebMethod]
        public List<ProductEntities> populateproductdropdownmap(string id, string idname)
        {
            BizDataProducts user = new BizDataProducts();
            DataSet ds = user.GetAllKidProductsmap(Convert.ToInt32(id), idname);
            DataTable dt = ds.Tables[0];
            List<ProductEntities> productlist = new List<ProductEntities>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ProductEntities product = new ProductEntities();
                product.productid = Convert.ToInt32(dt.Rows[i]["productkidid"]);
                product.productname = dt.Rows[i]["productname"].ToString();

                productlist.Add(product);
            }

            return productlist;
        }

        [WebMethod]
        public string Addsubscription(string obj)
        {
            string success = "Enter Email";
            if (obj != "")
            {
                BizDataUser biz = new BizDataUser();
                string email = obj;
                if (biz.checksubscribedUser(email) == 0)
                {
                    biz.AddSubscription(email);
                    success = "Subscribed Successfully";
                }
                else
                {
                    success = "Email Already subscribed";
                }
            }
            return success;
        }

        [WebMethod]
        public string deletemaplevel(string productid,string subcategoryid,string categoryid)
        {
            BizDataProducts biz = new BizDataProducts();
            biz.deletemaplevel(Convert.ToInt32(productid), Convert.ToInt32(subcategoryid), Convert.ToInt32(categoryid));
            return "success";
        }

        [WebMethod]
        public string InsertIpAddress(string ip)
        {

            var hostName = Dns.GetHostEntry(Dns.GetHostName());
            string ipv4 = hostName.AddressList[0].ToString();
            string _value = GetValue(ipv4 + "TestWeb");
            if (!string.IsNullOrEmpty(_value))
            {
                //Response.Write(_value);
            }
            else
            {
                SetValue(ipv4 + "TestWeb", Guid.NewGuid().ToString().GetHashCode().ToString("x"));
                //Response.Write(GetValue(ipv4 + "TestWeb"));
            }

            GetAddresses(GetValue(ipv4 + "TestWeb"),ip);
            return "success";
        }

        //Add Value in cokies    
        public void SetValue(string key, string value)
        {
            HttpContext.Current.Response.Cookies[key].Value = value;
            HttpContext.Current.Response.Cookies[key].Expires = DateTime.Now.AddDays(1); // ad    
        }

        //Get value from cokkie    
        public string GetValue(string _str)
        {
            if (HttpContext.Current.Request.Cookies[_str] != null)
                return HttpContext.Current.Request.Cookies[_str].Value;
            else
                return "";
        }

        public static IEnumerable<string> GetAddresses(string session,string ipp)
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            Ping ping = new Ping();
            var replay = ping.Send(Dns.GetHostName());
            var ipv4 = ipp;
            var ipv6 = (from ip in host.AddressList where ip.AddressFamily == AddressFamily.InterNetworkV6 select ip.ToString()).ToList();
            BizDataUser biz = new BizDataUser();

            if (ipv4.Length > 0)
            {
                ipv4 = "0";
            }
            switch (ipv6.Count)
            {
                case 0:
                    ipv6.Add("0"); ipv6.Add("0"); break;
                case 1:
                    ipv6.Add("0"); break;
            }
            biz.InsertVisitorDetails(ipp, ipv6[0].ToString(), ipv6[1].ToString(), session);
            return ipv6;
        }



    }
}
