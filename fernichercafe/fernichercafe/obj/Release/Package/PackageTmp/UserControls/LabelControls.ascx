﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LabelControls.ascx.cs" Inherits="fernichercafe.UserControls.LabelControls" %>

<div id="main">
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s10 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0">Label Controls</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="container">
                <div class="section section-data-tables">
                    <form runat="server">
                        <div class="row">
                            <div class="col s12">
                                <div class="card">
                                    <div class="card-content">

                                        
                                        <div class="row">
                                            
                                            <div class="input-field col x12 s6 m6 l6">
                                                <input id="txtaboutusheader" runat="server" type="text" class="validate">
                                                <label for="MiddleCenterContent_ctl01_txtaboutusheader">About us Header</label>
                                            </div>
                                            <div class="input-field col x12 s6 m6 l6">
                                                <input id="txtaboutussubheader" runat="server" type="text" class="validate">
                                                <label for="MiddleCenterContent_ctl01_txtaboutussubheader">About us Sub Header</label>
                                            </div>


                                            <div class="input-field col x12 s6 m6 l6">
                                                <input id="txtproductcategoriesheader" runat="server" type="text" class="validate">
                                                <label for="MiddleCenterContent_ctl01_txtproductcategoriesheader">Product Categories Title</label>
                                            </div>
                                            <div class="input-field col x12 s6 m6 l6">
                                                <textarea id="txtproductcategoriessubheader" runat="server" class="materialize-textarea"></textarea>
                                                <label for="MiddleCenterContent_ctl01_txtproductcategoriessubheader">Product Categories Description</label>
                                            </div>
                                             <div class="input-field col x12 s6 m6 l6">
                                                <input id="txtofferheader" runat="server" type="text" class="validate">
                                                <label for="MiddleCenterContent_ctl01_txtofferheader">Offer Title</label>
                                            </div>
                                            <div class="input-field col x12 s6 m6 l6">
                                                <textarea id="txtoffersubheader" runat="server" class="materialize-textarea"></textarea>
                                                <label for="MiddleCenterContent_ctl01_txtoffersubheader">Offer Description</label>
                                            </div>
                                             <div class="input-field col x12 s6 m6 l6">
                                                <input id="txtconceptsheader" runat="server" type="text" class="validate">
                                                <label for="MiddleCenterContent_ctl01_txtconceptsheader">Product Concepts Title</label>
                                            </div>
                                            <div class="input-field col x12 s6 m6 l6">
                                                <textarea id="txtconceptssubheader" runat="server" class="materialize-textarea"></textarea>
                                                <label for="MiddleCenterContent_ctl01_txtconceptssubheader">Product Concepts Description</label>
                                            </div>
                                 
                                        </div>

                                        <br>
                                        <div class="divider"></div>

                                        <div class="row">
                                            <div class="input-field col s12">
                                                <asp:Button type="button" ID="btnsave" runat="server" OnClick="btnsave_Click" Text="save" class="modal-action waves-effect waves-green btn btn-info"></asp:Button>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>



            </div>
            <!-- START RIGHT SIDEBAR NAV -->
        </div>
    </div>
</div>


<!-- END: Page Main-->

<!-- Theme Customizer -->
<!-- BEGIN: Footer-->

<footer class="page-footer footer footer-static footer-dark gradient-45deg-indigo-purple gradient-shadow navbar-border navbar-shadow">
    <div class="footer-copyright">
        <div class="container"><span>&copy; 2019</span></div>
    </div>
</footer>

