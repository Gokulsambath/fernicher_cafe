﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="productsAccessory.ascx.cs" Inherits="fernichercafe.UserControls.productsAccessory" %>
<style>
    label.radio-inline label {
        padding-left: 20px;
        display: inline-block;
    }
    .select-dropdown ,svg {
            display: none !important;
    }
    
    select {
            display: block !important;
    }
</style>
<form id="Form1" runat="server">

    <div id="main">
        <div class="row">
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">
                    <div class="row">
                        <div class="col s10 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0">Product Accessories</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <div class="section section-data-tables">

                        <div class="row">
                            <div class="col s12">
                                <div class="card">
                                    <div class="card-content">
                                        <div class="input-field col s6 col m4 col l4">
                                            <label class="active">Sub Category</label>
                                            <asp:DropDownList class="form-control p-0" AutoPostBack="true" ID="ddlSubCategory1" OnSelectedIndexChanged="ddlSubCategory1_SelectedIndexChanged" runat="server" >
                                            </asp:DropDownList>
                                        </div>
                                        <div class="input-field col s6 col m4 col l4">
                                            <label class="active">Product Name</label>
                                            <asp:DropDownList class="form-control p-0" AutoPostBack="true" ID="ddlProduct" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="row">
                                            <div class="col s12">
                                                <table id="page-length-option" class="display wrap" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>sub category</th>
                                                            <th>Product Name</th>
                                                            <th>Accessory Name</th>                                                            
                                                            <th>Height</th>
                                                            <th>width</th>
                                                            <th>Depth</th>
                                                            <th>status </th>
                                                            <th>Image </th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <asp:PlaceHolder ID="pldProductAccessoriestable" runat="server"></asp:PlaceHolder>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- Scroll - vertical, dynamic height -->



                    <!-- Scroll - Vertical and Horizontal -->



                    <!-- Multi Select -->


                </div>
                <!-- START RIGHT SIDEBAR NAV -->

                <!-- END RIGHT SIDEBAR NAV -->
                <div style="bottom: 50px; right: 19px;" id="adddproducts" runat="server" class="fixed-action-btn direction-top">
                    <a href="#add_accessory" class="btn-floating btn-large gradient-45deg-light-blue-cyan gradient-shadow modal-trigger mb-2 mr-1">
                        <i class="material-icons">add</i>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <!-- END: Page Main-->


    <div id="add_accessory" class="modal">
        <div class="modal-content">
            <span class="modal-header right modal-close">
                <i class="material-icons right-align">clear</i>
            </span>
            <h4 runat="server" id="myModalLabel">Add Accessory</h4>
            <div class="divider"></div>
            <div class="modal-body">

                <div class="row">
                    <input type="hidden" id="hiddenid" runat="server" />
                    <div class="input-field col s6 col m6 col l6">
                        <label class="active">Sub Category Name <span class="red-text">*</span></label>
                        <select class="form-control p-0" id="ddlSubCategory" runat="server">
                        </select>
                        <span class="highlight"></span><span class="bar"></span>

                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                            ControlToValidate="ddlSubCategory" display="dynamic"
                            ErrorMessage="Please Select" InitialValue="0" Text="Please select" ForeColor="Red"
                            runat="server" />--%>
                    </div>
                    <div class="input-field col s6 col m6 col l6" id="productnamediv">
                        <label class="active">Product Name <span class="red-text">*</span></label>
                        <select class="form-control p-0" id="ddlproductname" runat="server">
                        </select>
                        <input type="hidden" id="ddlproductnamehidden" runat="server" />
                        <span class="highlight"></span><span class="bar"></span>

                       
                    </div>
                    <div class="input-field col s6 col m6 col l6">
                        <label for="txtaccessoryname">Accessory Name</label>
                        <input type="text" class="form-control" id="txtaccessoryname" runat="server" /><span class="highlight"></span> <span class="bar"></span>

                    </div>
                
                    <div class="input-field col s6 col m6 col l6">
                        <label for="txtmodelname">Model</label>
                        <input type="text" class="form-control" id="txtmodelname" runat="server"><span class="highlight"></span> <span class="bar"></span>

                    </div>
                    <div class="input-field col s6 col m6 col l6">
                        <label for="txtwidth">Width</label>
                        <input type="text" class="form-control" id="txtwidth" runat="server"><span class="highlight"></span> <span class="bar"></span>

                    </div>
                    <div class="input-field col s6 col m6 col l6">
                        <label for="txtHeight">Height</label>
                        <input type="text" class="form-control" id="txtHeight" runat="server"><span class="highlight"></span> <span class="bar"></span>

                    </div>
                     <div class="input-field col s6 col m6 col l6">
                        <label for="txtDepth">Depth</label>
                        <input type="text" class="form-control" id="txtDepth" runat="server"><span class="highlight"></span> <span class="bar"></span>

                    </div>
                </div>
                <div id="file-upload" class="section">
                    <div class="row section">
                        <div class="col s12 m4 l3">
                            <p>Upload Photo</p>
                        </div>
                        <div class="col s12 m8 l9">
                            <img id="image_upload_preview" />
                            <label class="floatlabels" id="filenamelabel" runat="server"></label>
                            <input type="hidden" id="filepathedit" runat="server" />
                            <asp:FileUpload ID="inputFile" runat="server" />
                        </div>
                    </div>
                </div>
            
                <div class="col s12 m12 l12 row" id="view-options">
                    <label style="font-size: 1rem">Status :&nbsp;</label>
                    <label class="radio-inline p-0" style="top: 20px; margin-left: 60px">

                        <asp:RadioButton GroupName="radio" ID="active" Text="Active" runat="server" />

                    </label>
                    <label class="radio-inline" style="width: 21%; margin-left: 130px; top: 20px">

                        <asp:RadioButton GroupName="radio" Text="In-Active" ID="inactive" runat="server" />

                    </label>
                </div>                
                <br />
                <div class="divider"></div>

                <div class="modal-footer">
                    <asp:HyperLink href="Data.aspx?Action=productsAccessory" runat="server" class="modal-action modal-close waves-effect waves-red btn btn-danger ">Cancel</asp:HyperLink>
                    <asp:Button type="button" ID="btnsave" runat="server" OnClick="btnsave_Click" Text="save" class="modal-action waves-effect waves-green btn btn-info"></asp:Button>

                </div>
            </div>

        </div>
    </div>


</form>

<!-- Theme Customizer -->
<!-- BEGIN: Footer-->

<footer class="page-footer footer footer-static footer-dark gradient-45deg-indigo-purple gradient-shadow navbar-border navbar-shadow">
    <div class="footer-copyright">
        <div class="container"><span>&copy; Fernicher Cafe 2019</span></div>
    </div>
</footer>

<script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputFile").change(function () {
        readURL(this);
    });


    $("#MiddleCenterContent_ctl01_ddlproductname").change(function () {

        var id = $(this).find(":selected").val();
        //console.log(id);
        $("#MiddleCenterContent_ctl01_ddlproductnamehidden").val(id);
    });
     $("#MiddleCenterContent_ctl01_ddlSubCategory").change(function () {
        
         var id = $(this).find(":selected").val();

         $("#MiddleCenterContent_ctl01_ddlproductname").html("");
         if (id != 0) {
             $.ajax({
                 type: 'POST',
                 url: "userwebservice.asmx/populateproductdropdown",
                 data: "{ 'id': '" + id + "' }",
                 async: false,
                 contentType: 'application/json; charset-utf-8',

                 success: function (response) {
                     

                     var html = "<option value=0>choose your option</option>";
                     for (var i = 0; i < response.d.length; i++) {
                         html += "<option value=" + response.d[i].productid + ">" + response.d[i].productname + "</option>";
                     }
                     $("#MiddleCenterContent_ctl01_ddlproductname").append(html);
                     

                 }
             
             });
         }
    });

</script>
