﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IndexWeb.ascx.cs" Inherits="fernichercafeWebsite.UserControls.Index" %>


<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<link media="screen" rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>

<style type="text/css">
  @media only screen and (min-width: 320px) and (max-width:600px)        {             .owl-icons-wrapper .owl-icons {                position: relative;                padding-top: 50px;        }        .owl-icons-wrapper .owl-icons figure .f-icon         {            font-size: 30px;            -moz-transition: all 0.5s;            -o-transition: all 0.5s;            -webkit-transition: all 0.5s;            transition: all 0.5s;        }        .sticky-container        {            padding:0px;            margin:-13px;            position:fixed;            right:-130px;            top:130px;            width:240px;            z-index: 1100;        }        .sticky-container1        {            padding:0px;            margin:0px;            position:fixed;            right:-130px;            top:182px;            width:240px;            z-index: 1100;        }        .sticky1 li        {            list-style-type:none;            background:linear-gradient(to right,#FF7B02,#B42868);            color:#efefef;            height:50px;            padding:5px;            margin:0px 0px 1px 18px;            cursor:pointer;            border-radius: 10px 0px 0px 10px;        }        .sticky1 li img        {            width: 10%;            margin: 2px 0 0 13px;        }        .gift-icon        {            font-size: 30px;        }               .sticky li        {            list-style-type:none;            background:linear-gradient(to right,#FF7B02,#B42868);            color:#efefef;            height:54px;            padding:0px;            margin:0px 0px 1px 4px;            cursor:pointer;            border-radius: 10px 0px 0px 10px;        }        .sticky li img        {            float:left;            /*margin:5px 4px;            margin-right:5px;*/            margin: 2px 0 0 13px;        }        .sticky li p        {            padding:1px;            margin: 8px;            line-height: 16px;            font-size: 10px;        }        .request-icon        {            font-size: 15px;            margin: 0px 0 0 3px;        }        .ongoing-text        {            font-size: 10px;            margin: -3px 0 0 4px;        }        .offers-text        {            font-size: 10px;            margin: 2px 0 0 10px;        }}
</style>
<style type="text/css">
    @media only screen and (min-width: 768px) and (max-width: 1400px) {
        #requestcallback {
            width: 400px;
        }

        .req-title-text {
            font-size: 23px;
        }

        .req-small-text {
            font-size: 13px;
        }
    }

    @media only screen and (min-width: 320px) and (max-width: 640px) {
        .req-title-text {
            font-size: 18px;
        }

        .req-small-text {
            font-size: 11px;
        }
    }
</style>
<style type="text/css">
    @media only screen and (min-width: 320px) and (max-width: 600px) {
        #myModal {
            width: 100%;
        }

        .SignUpModal {
            margin-top: -79px;
        }

        .searchmodal {
            margin-top: -79px;
        }
    }
</style>
<script>
    async function msg(msg, title) {
        const msgss = await showpop(msg, title);
        window.history.replaceState('', '', window.location.href)

    }
    function showpop(msg, title) {
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "12000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        // toastr['success'](msg, title);
        var d = Date();
        if (title != 'success') {
            title = '';
            toastr.error(msg, title);
        }
        else {
            title = '';
            toastr.success(msg, title);
        }
        return false;
    }
</script>

<section class="header-content">

    <div class="owl-slider">
        <asp:PlaceHolder ID="phldhomebanner" runat="server"></asp:PlaceHolder>
        <!-- === slide item === -->

    </div>

    <!--/owl-slider-->
</section>

<!-- ========================  Icons slider ======================== -->

<section class="owl-icons-wrapper owl-icons-frontpage">

    <!-- === header === -->

    <header class="hidden">
        <h2>Product categories</h2>
    </header>

    <div class="container">

        <div class="owl-icons">

            <!-- === icon item === -->

            <a href="DataWeb.aspx?Action=searchlist&id=sofa">
                <figure class="figure">
                    <i class="f-icon f-icon-sofa"></i>
                    <figcaption>Sofa</figcaption>
                </figure>
            </a>

            <!-- === icon item === -->

            <a href="DataWeb.aspx?Action=searchlist&id=armchair">
                <figure class="figure">
                    <i class="f-icon f-icon-armchair"></i>
                    <figcaption>Armchairs</figcaption>
                </figure>
            </a>



            <a href="DataWeb.aspx?Action=ProductListWeb&category=Livingroom&id=10006&maplevel=null">
                <figure class="figure">
                    <i class="f-icon f-icon-table"></i>
                    <figcaption>Tables</figcaption>
                </figure>
            </a>

            <!-- === icon item === -->

            <a href="DataWeb.aspx?Action=searchlist&id=bookcase">
                <figure class="figure">
                    <i class="f-icon f-icon-bookcase"></i>
                    <figcaption>Bookcase</figcaption>
                </figure>
            </a>

            <!-- === icon item === -->

            <a href="DataWeb.aspx?Action=searchlist&id=bed">
                <figure class="figure">
                    <i class="f-icon f-icon-bedroom"></i>
                    <figcaption>Bed</figcaption>
                </figure>
            </a>


            <a href="DataWeb.aspx?Action=SubCategoryWeb&id=1">
                <figure class="figure">
                    <i class="f-icon f-icon-children-room"></i>
                    <figcaption>Children room</figcaption>
                </figure>
            </a>

            <a href="DataWeb.aspx?Action=searchlist&id=wardrobe">
                <figure class="figure">
                    <i class="f-icon f-icon-wardrobe"></i>
                    <figcaption>Wardrobe</figcaption>
                </figure>
            </a>


            <a href="DataWeb.aspx?Action=ProductListWeb&category=Livingroom&id=10007&maplevel=null">
                <figure class="figure">
                    <i class="f-icon f-icon-accessories"></i>
                    <figcaption>Accessories</figcaption>
                </figure>
            </a>

        </div>
        <!--/owl-icons-->
    </div>
    <!--/container-->
</section>
<div class="sticky-container">
    <ul class="sticky">
        <li><a href="#requestcallback" class="mfp-open" style="color: #fff">
            <p>
                Request<br>
                &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-phone-square fa-2x" aria-hidden="true"></i><br>
                Call Back
            </p>
        </a>
            <a href="#modalsearchlist" id="searchpopup" class="mfp-open" style="color: #fff; display: none"></a>
        </li>
    </ul>
</div>

<div class="sticky-container1">
    <ul class="sticky1">
        <li>
            <p class="ongoing-text">Ongoing</p>
            <a href="#offer-section">
                <img src="ContentWebsite/assets/discount.png" class="faa-vertical animated offers-img" class="text-center"></a>
            <p class="offers-text">Offers</p>
        </li>
    </ul>
</div>


<section class="about-us">
    <div class="container">
        <header>
            <div class="row">
                <div class="col-md-offset-2 col-md-8 text-center">
                    <h1 class="h2 title" id="lblaboutheader" runat="server"></h1>
                    <div class="text">
                        <p id="lblaboutsubheader" runat="server">Shopping Pleasure at FɘrniCHɘr Café</p>
                    </div>
                    <br>
                    <div class="text">
                        <p id="phldfooteraboutus" runat="server">
                        </p>
                    </div>
                </div>
            </div>
        </header>
    </div>
</section>

<section class="products">

    <div class="container">

        <!-- === header title === -->

        <header>
            <div class="row">
                <div class="col-md-offset-2 col-md-8 text-center">
                    <h2 class="title" id="lblproductcategoryheader" runat="server"></h2>
                    <div class="text">
                        <p id="lblproductcategorysubheader" runat="server"></p>
                    </div>
                </div>
            </div>
        </header>

        <div class="row">


            <asp:PlaceHolder ID="phldcategorylist" runat="server"></asp:PlaceHolder>

        </div>


    </div>
    <!--/container-->
</section>

<section class="offers" id="offer-section">

    <div class="container">
        <header>
            <div class="row">
                <div class="col-md-offset-2 col-md-8 text-center">
                    <h2 class="title" id="lblofferheader" runat="server"></h2>
                    <div class="text">
                        <p id="lbloffersubheader" runat="server"></p>
                    </div>
                </div>
            </div>
        </header>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-centered">

                <div id="carousel" class="carousel slide" data-ride="carousel" data-type="multi" data-interval="3500">
                    <div class="carousel-inner open-popup-gallery">
                        <asp:PlaceHolder ID="offerplace" runat="server"></asp:PlaceHolder>



                    </div>

                    <!-- Controls -->
                    <div class="left carousel-control">
                        <a href="#carousel" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </div>
                    <div class="right carousel-control">
                        <a href="#carousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<section class="stretcher-wrapper">

    <!-- === stretcher header === -->

    <header class="">
        <!--remove class 'hidden'' to show section header -->
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 text-center">
                    <h1 class="h2 title" id="lblproductconceptheader" runat="server"></h1>
                    <div class="text">
                        <p id="lblproductconceptsubheader" runat="server">
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <ul class="stretcher">
        <asp:PlaceHolder ID="phldhomeproductconcepts1" runat="server"></asp:PlaceHolder>

    </ul>

</section>



<section class="stretcher-wrapper">

    <ul class="stretcher">
        <asp:PlaceHolder ID="phldhomeproductconcepts2" runat="server"></asp:PlaceHolder>

    </ul>

</section>
<!-- ========================  Blog Block ======================== -->

<section class="blog blog-block">
</section>


<section class="blog">
</section>

<!-- ========================  Instagram ======================== -->

<section class="instagram">

    <!-- === instagram header === -->


</section>

<!-- ========================  Request Call Back info popup - quick view ======================== -->
<div class="popup-main mfp-hide" id="requestcallback">
    <!-- === product popup === -->
    <div class="product">
        <!-- === popup-title === -->
        <div class="popup-title">
            <div class="h1 title req-title-text">
                Request a CallBack<br>
            </div>
            <small class="req-small-text">Fill in the details and we will get back to you shortly.</small>
        </div>
        <!-- === product-popup-info === -->
        <div class="popup-content">
            <div class="product-info-wrapper">
                <div class="row">
                    <div class="col-sm-12">

                        <!-- <p style="text-align: center;font-size: 20px;color: #1d2228;font-weight: 400;">Request a CallBack</p>
                                    <p style="text-align: center;">Fill in the details and we will get back to you shortly.</p><br> -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" id="callbackname" class="form-control" runat="server" placeholder="Name">
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                            ControlToValidate="callbackname"
                                            ErrorMessage="Name Required" Display="dynamic" Text="Name Required" ForeColor="Red"
                                            runat="server" />--%>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" id="callbackmobile" class="form-control" runat="server" placeholder="Mobile No.">
                                    <%--  <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Enter valid Phone number" ForeColor="Red"
                                            ControlToValidate="callbackmobile" Display="Dynamic" ValidationExpression="^[0-9]{10}$"></asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                            ControlToValidate="callbackmobile"
                                            ErrorMessage="Mobile Required" Display="dynamic" Text="Mobile Required" ForeColor="Red"
                                            runat="server" />--%>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="email" id="callbackemail" class="form-control" runat="server" placeholder="Email">
                            <%-- <asp:RegularExpressionValidator ID="regexEmailValid" Display="dynamic" runat="server" ForeColor="Red" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="callbackemail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                    ControlToValidate="callbackemail"
                                    ErrorMessage="Email Required" Display="dynamic" Text="Email Required" ForeColor="Red"
                                    runat="server" />--%>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" id="callbackcomments" rows="3" runat="server" placeholder="Message"></textarea>
                            <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                                    ControlToValidate="callbackcomments"
                                    ErrorMessage="comments Required" Display="dynamic" Text="comments Required" ForeColor="Red"
                                    runat="server" />--%>
                        </div>
                        <%--<asp:Button runat="server" ID="callbackbtn" class="btn btn-block btn-main callbackbtn" OnClick="callbackbtn_Click" Text="SUBMIT" />--%>
                        <input type="button" id="callbackbtn" class="btn btn-block btn-main callbackbtn" onclick="callback()" value="SUBMIT" />
                    </div>
                </div>
            </div>
            <!--/row-->
        </div>
        <!--/product-info-wrapper-->
    </div>
    <!--/popup-content-->
    <!-- === product-popup-footer === -->
</div>
<!--/product-->


<div class="modalbox SignUpModal" id="myModal">
    <div class="box">
        <div class="content">
            <div class="row">
                <div class="col-md-6 col-lg-6 hidden-xs hidden-sm">
                    <div style="background-image: url('ContentWebsite/assets/signup.jpg'); width: 100%" class="signup-banner">
                        <img src="ContentWebsite/assets/signup-logo-final.png" class="sign-img">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="form-div">
                        <a class="close" title="Close (Esc)">X</a>
                        <p class="title">Sign Up With Us</p>
                        <hr class="line">
                        <small style="text-align: center; margin-left: 10px;">Get notified on exclusive discounts, newsletters and more</small>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="sr-only">Name</label>
                                    <input type="text" class="form-control" runat="server" id="signupname" placeholder="Name" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5"
                                        ControlToValidate="signupname"
                                        ErrorMessage="Name Required" Display="dynamic" Text="Name Required" ForeColor="Red"
                                        runat="server" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="mobileno" class="sr-only">Mobile No.</label>
                                    <input type="text" class="form-control" runat="server" id="signupmobileno" placeholder="Mobile No." />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Enter valid Phone number" ForeColor="Red"
                                        ControlToValidate="signupmobileno" Display="Dynamic" ValidationExpression="^[0-9]{10}$"></asp:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                        ControlToValidate="signupmobileno"
                                        ErrorMessage="Mobile Required" Display="dynamic" Text="Mobile Required" ForeColor="Red"
                                        runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="sr-only">Email</label>
                            <input type="email" class="form-control" runat="server" id="signupemail" placeholder="Email" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="dynamic" runat="server" ForeColor="Red" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="signupemail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7"
                                ControlToValidate="signupemail"
                                ErrorMessage="Email Required" Display="dynamic" Text="Email Required" ForeColor="Red"
                                runat="server" />
                        </div>
                        <div class="form-group">
                            <label for="message" class="sr-only">Message</label>
                            <textarea class="form-control" rows="3" id="signupmessage" runat="server" placeholder="Message"></textarea>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8"
                                ControlToValidate="signupmessage"
                                ErrorMessage="comments Required" Display="dynamic" Text="comments Required" ForeColor="Red"
                                runat="server" />
                        </div>
                        <asp:Button runat="server" ID="btnsignup" class="btn btn-info btn-block" OnClick="btnsignup_Click" Style="background: linear-gradient(to right,#FF7B02,#B42868); border: 0px;" Text="SUBMIT" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="popup-main mfp-hide" id="modalsearchlist">

    <div class="product">

        <div class="popup-title">
            <div class="h1 title req-title-text">
                Search Items<br>
            </div>
        </div>

        <div class="popup-content">
            <div class="product-info-wrapper">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p>No Item Found</p>
                    </div>

                </div>
            </div>

        </div>

    </div>

</div>


<script>
    setTimeout(function () {
        if (localStorage.getItem('popState') != 'shown') {
            $('#myModal').modal({ backdrop: 'static', keyboard: false });
            localStorage.setItem('popState', 'shown')
        }
    }, 15000);
    $(document).ready(function () {
        $.getJSON('https://ipapi.co/json/', function(data) {
            var response = JSON.stringify(data, null, 2);
            console.log(response);
            response = JSON.parse(response);            
            $.ajax({
                type: "POST",
                url: "userwebservice.asmx/InsertIpAddress",
                data: "{ 'ip': '" + response["ip"] + "' }",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                   

                }
            });
        });
        localStorage.removeItem('popState');
        $('.close').click(function (e) // You are clicking the close button
        {
            $('#myModal').modal('toggle'); // Now the pop up is hiden.
        });
        var searchlist = getUrlVars()["searchlist"];
        if (searchlist != undefined) {
            if (searchlist == 0) {


                jQuery.magnificPopup.open({
                    items: { src: '#modalsearchlist' }, type: 'inline'
                }, 0);

            }
        }
    });

    function callback() {

        if ($('#callbackname').val() != undefined ||
            $('#callbackmobile').val() != undefined ||
            $('#callbackemail').val() != undefined ||
            $('#callbackcomments').val() != "" ||
            $('#callbackname').val() != "" ||
            $('#callbackmobile').val() != "") {
            var obj = {};
            obj.name = $('#MiddleCenterContent_ctl00_callbackname').val();
            obj.message = $('#MiddleCenterContent_ctl00_callbackcomments').val();
            obj.email = $('#MiddleCenterContent_ctl00_callbackemail').val();
            obj.mobile = $('#MiddleCenterContent_ctl00_callbackmobile').val();

            $.ajax({
                type: "POST",
                url: "userwebservice.asmx/CallBack",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    toastr.success("Requested Call");
                    location.reload();
                    window.history.replaceState('', '', window.location.href)

                }
            });
        } else {
            alert('Fill All Fields');
        }
    }

    function getUrlVars() {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }

    $('.carousel[data-type="multi"] .item').each(function () {
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        for (var i = 0; i < 2; i++) {
            next = next.next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }

            next.children(':first-child').clone().appendTo($(this));
        }
    });
</script>




<!--End of Tawk.to Script for Message box-->
<script src="https://apps.elfsight.com/p/platform.js" defer></script>
<div class="elfsight-app-76e2c451-a762-4e41-9f22-7d9363b84648"></div>
