﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductsMapping.ascx.cs" Inherits="fernichercafe.UserControls.ProductsMapping" %>



    <!-- /#wrapper -->
      <div id="main">
      <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
          <!-- Search for small screen-->
          <div class="container">
            <div class="row">
              <div class="col s10 m6 l6">
                <h5 class="breadcrumbs-title mt-0 mb-0">Products Mapping</h5>
              </div>
            </div>
          </div>
        </div>
        <div class="col s12">
          <div class="container">
            <div class="section section-data-tables">

        <div class="row">
              <div class="col s12 m12 l12">
      <div id="basic-tabs" class="card card card-default scrollspy">
        <div class="card-content">
          <div class="col s12">
            <div class="row" id="main-view">
              <div class="col s12">
                <ul class="tabs tab-demo ">
                  <li class="tab col m3"><a class="active" href="#">Add Map Level</a>
                  </li>
                  <li class="tab col m3"><a target="_self" href="Data.aspx?Action=mapproducts">Map Products</a>
                  </li>
                </ul>
              </div>
              <div class="col s12">
                <div id="category" class="col s12" style="margin-top:1rem;">
                  <div class="row">
            <div class="col s12">
              <table id="page-length-option" class="display" width="100%">
                <thead>
                  <tr>
                      
                    <th width="30%">Level Name</th>
                    <th  width="30%">Sub Category</th>                      
                    <th width="20%" >Category</th>
                    <th width="20%" >Image</th>
                    <th width="20%">Status</th>
                    <th width="20%">Action</th>
                  </tr>
                </thead>
                <tbody>
              <asp:PlaceHolder ID="pldMapLevelTable" runat="server"></asp:PlaceHolder>
               
                </tbody>
                
              </table>
            </div>
          </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>



</div>
          
          </div>
        </div>
      </div>
    </div>


   <div style="bottom: 50px; right: 19px;" id="adddmaplevel" runat="server" class="fixed-action-btn direction-top"><a class="btn-floating btn-large gradient-45deg-light-blue-cyan gradient-shadow modal-trigger mb-2 mr-1" href="#add_maplevel"><i class="material-icons">add</i></a>
       </div>

    
<div id="add_maplevel" class="modal">
        <div class="modal-content">
            <span class="modal-header right modal-close">
                  <i class="material-icons right-align">clear</i>
                </span>
            <h4 id="myModalLabel" runat="server">Map Name</h4>
            <div class="divider"></div>
            <div class="modal-body">
                <form id="categoryform" runat="server">
                    <div class="row">
                    <input type="hidden" id="hiddenid" runat="server" />

                    <div class="input-field col s6 col m6 col l6">
                        <label class="active">Category Name <span class="red-text">*</span></label>
                        <select class="form-control p-0" id="ddlcat" runat="server">
                        </select>
                        <span class="highlight"></span><span class="bar"></span>


                    </div>
                    <div class="input-field col s6 col m6 col l6">
                        <label class="active">Sub Category Name <span class="red-text">*</span></label>
                        <select class="form-control p-0" id="ddlSubCategory" runat="server">
                        </select>
                        <input type="hidden" id="hidsub" runat="server" />
                        <span class="highlight"></span><span class="bar"></span>


                    </div>
                  </div>
                    <div class="row">
                        <label for="txtmap">Mapping Name <span class="red-text">*</span></label>
                        <input type="text" id="txtmap" runat="server" />
                    </div>
                      <div class="input-field col s12 col m12 col l12">
                        <div id="file-upload" class="section">
                            <div class="row section">
                                <div class="col s12 m4 l3">
                                    <p>Upload Photo</p>
                                </div>
                                <div class="col s6 m6 l6">
                                    <img id="image_upload_preview" />
                                    <label class="floatlabels" id="filenamelabel" runat="server"></label>
                                    <input type="hidden" id="filepathedit" runat="server" />
                                    <asp:FileUpload ID="inputFile" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m12 l12" id="view-options">
                       
                        <label class="radio-inline p-0" style="top:18px"> Status </label>
                        <label class="radio-inline p-0" style="top:0px;margin-left:60px">
                         
                                    <asp:RadioButton GroupName="radio" CssClass="with-gap" ID="active" Text="Active" runat="server" />                                         
                                                        
                           
                        </label>
                        <label class="radio-inline" style="width: 21%;margin-left:50px;top:0px">
                           
                                <asp:RadioButton GroupName="radio" CssClass="with-gap" Text="In-Active" ID="inactive" runat="server" />
                                          
                        </label>
                    </div>
                    <br />
                <div class="divider"></div>
                <div class="modal-footer">
                    <asp:HyperLink href="Data.aspx?Action=ProductsMapping" runat="server" class="modal-action modal-close waves-effect waves-red btn btn-danger">Cancel</asp:HyperLink>
                          <asp:button type="button" id="btnsave" runat="server" OnClick="btnsave_Click" Text="save" class="modal-action modal-close waves-effect waves-green btn btn-info "></asp:button>

                </div>
            </form>
            </div>
        </div> 

 </div>


    <footer class="page-footer footer footer-static footer-dark gradient-45deg-indigo-purple gradient-shadow navbar-border navbar-shadow">
      <div class="footer-copyright">
        <div class="container"><span>&copy; Fernicher Cafe 2019</span></div>
      </div>
    </footer> 


<script>

     $("#MiddleCenterContent_ctl01_ddlcat").change(function () {

        var id = $(this).find(":selected").val();

        $("#MiddleCenterContent_ctl01_ddlSubCategory").html("");
        if (id != 0) {
            $.ajax({
                type: 'POST',
                url: "userwebservice.asmx/populatesubcategorydropdown",
                data: "{ 'id': '" + id + "' }",
                async: false,
                contentType: 'application/json; charset-utf-8',

                success: function (response) {


                    var html = "<option value=0>choose your option</option>";
                    for (var i = 0; i < response.d.length; i++) {
                        html += "<option value=" + response.d[i].productid + ">" + response.d[i].productname + "</option>";
                    }
                    $("#MiddleCenterContent_ctl01_ddlSubCategory").append(html);


                }

            });
        }
    });


    $("#MiddleCenterContent_ctl01_ddlSubCategory").change(function () {

        var id = $(this).find(":selected").val();
        $("#MiddleCenterContent_ctl01_hidsub").val(id);
       

    });
</script>