﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="brands.ascx.cs" Inherits="fernichercafe.UserControls.brands" %>
<style>
    label.radio-inline label {
        padding-left: 20px;
        display: inline-block;
    }
</style>
  <!-- BEGIN: Page Main-->
    <div id="main">
        <div class="row">
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">
                    <div class="row">
                        <div class="col s10 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0">Our Brand Partners</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <div class="section section-data-tables">

                        <div class="row">
                            <div class="col s12 m12 l12">
            <div class="card card card-default scrollspy">
                <div class="card-content">
                    <div class="col s12">
                        <div class="row" id="main-view">
                           
            <table id="page-length-option" class="display" width="100%">
                <thead>
                    <tr>
                        <th width="25%">Partner Name</th>
                        <th width="25%">Image </th>
                        <th width="40%">Official link</th>
                        <th width="40%">Status</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>
                <tbody>
                  <asp:PlaceHolder ID="phldbrandtable" runat="server"></asp:PlaceHolder>

                </tbody>

            </table>

                </div>
            </div>
        </div>
    </div>
</div>
</div>


                        <!-- Scroll - vertical, dynamic height -->



                        <!-- Scroll - Vertical and Horizontal -->



                        <!-- Multi Select -->


                    </div><!-- START RIGHT SIDEBAR NAV -->

                    <!-- END RIGHT SIDEBAR NAV -->
                    <div style="bottom: 50px; right: 19px;" class="fixed-action-btn direction-top"><a class="btn-floating btn-large gradient-45deg-light-blue-cyan gradient-shadow modal-trigger mb-2 mr-1" href="#add_brand_partner"><i class="material-icons">add</i></a>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Page Main-->

    <!-- Theme Customizer -->
    <!-- BEGIN: Footer-->

<footer class="page-footer footer footer-static footer-dark gradient-45deg-indigo-purple gradient-shadow navbar-border navbar-shadow">
    <div class="footer-copyright">
        <div class="container"><span>&copy; Fernicher Cafe 2019</span></div>
    </div>
</footer>

    <div id="add_brand_partner" class="modal">
        <div class="modal-content">
            <span class="modal-header right modal-close">
                  <i class="material-icons right-align">clear</i>
                </span>
            <h4 id="myModalLabel" runat="server"></h4>
             <div class="divider"></div>
            <div class="modal-body">
                <form id="subcategoryform" runat="server">
                    <div class="row">
                        
                        <div class="input-field col s6 col m6 col l6">
                             <input type="hidden" id="hiddenid" runat="server" />
                            <input type="text" class="validate" id="brandname" runat="server" >
                            <asp:RequiredFieldValidator ID="reqname"
                                  ControlToValidate="brandname"
                                   ErrorMessage="Please Select" display="dynamic" Text="Please Enter Name" ForeColor="Red"
                                                        runat="server" />
                            <label for="partnername">Brand Name</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" class="validate" id="sitelink" runat="server">
                            <asp:RequiredFieldValidator ID="reqlink"
                                  ControlToValidate="sitelink"
                                   ErrorMessage="Please Select" display="dynamic" Text="Please Enter Link" ForeColor="Red"
                                                        runat="server" />
                            <label for="sitelink">Official Link</label>
                        </div>
                    </div>
                    <div id="file-upload" class="section">
                        <div class="row section">
                            <div class="col s12 m4 l3">
                                <p>Upload Photo</p>
                            </div>
                            <div class="col s12 m8 l9">
                                
                                <img id="image_upload_preview"  />
                                <label class="floatlabels" id="filenamelabel" runat="server"></label>
                                   <input type="hidden" id="filepathedit" runat="server" />
                                <asp:FileUpload ID="inputFile" runat="server" />
                            </div>
                        </div>
                    </div>
                    
                    <div class="col s12 m12 l12" id="view-options">
                       
                        <label class="radio-inline p-0" style="top:18px"> Status </label>
                        <label class="radio-inline p-0" style="top:0px;margin-left:60px">
                         
                                    <asp:RadioButton GroupName="radio" CssClass="with-gap" ID="active" Text="Active" runat="server" />                                         
                                                        
                           
                        </label>
                        <label class="radio-inline" style="width: 21%;margin-left:50px;top:0px">
                           
                                <asp:RadioButton GroupName="radio" CssClass="with-gap" Text="In-Active" ID="inactive" runat="server" />
                                          
                        </label>
                    </div>
                 <br>
                   <div class="divider"></div>
                                <div class="modal-footer">
                    <asp:HyperLink href="Data.aspx?Action=Brands" runat="server" class="modal-action waves-effect waves-red btn btn-danger">Cancel</asp:HyperLink>
                          <asp:button type="button" id="btnsave" runat="server" OnClick="btnsave_Click" Text="save" class="modal-action waves-effect waves-green btn btn-info "></asp:button>

                </div>
            </form>
            </div>
        </div> 

     </div>


