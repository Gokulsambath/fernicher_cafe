﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GlobalHeaderLoggedIn.ascx.cs" Inherits="fernichercafe.UserControls.GlobalHeaderLoggedIn" %>

<style>
.count {animation: shake 0.8s;animation-iteration-count: infinite;}@keyframes shake {  0% { transform: translate(1px, 1px) rotate(0deg); }  10% { transform: translate(-1px, -2px) rotate(-1deg); }  20% { transform: translate(-3px, 0px) rotate(1deg); }  30% { transform: translate(3px, 2px) rotate(0deg); }  40% { transform: translate(1px, -1px) rotate(1deg); }  50% { transform: translate(-1px, 2px) rotate(-1deg); }  60% { transform: translate(-3px, 1px) rotate(0deg); }  70% { transform: translate(3px, 1px) rotate(-1deg); }  80% { transform: translate(-1px, -1px) rotate(1deg); }  90% { transform: translate(1px, 2px) rotate(0deg); }  100% { transform: translate(1px, -2px) rotate(-1deg); }}
</style>
<div class="navbar navbar-fixed">
    <nav class="navbar-main navbar-color nav-collapsible sideNav-lock navbar-dark gradient-45deg-indigo-purple no-shadow">
        <div class="nav-wrapper">
            <!--  <div class="header-search-wrapper hide-on-med-and-down"><i class="material-icons">search</i>
              <input class="header-search-input z-depth-2" type="text" name="Search" placeholder="Explore Materialize">
            </div> -->
            <div class="header-search-wrapper hide-on-med-and-down">
                <div class="row">
                    <div class="col s12 m6 l6 xl6 right-align"></div>
                    <div class="col s12 m3 l3 xl3 right-align">
                       
                        <h6 id="todaycount" runat="server" class="count pb-2" style="color:red;margin-top:-3px"></h6>
                    </div>
                    <div class="col s12 m3 l3 xl3 right-align">
                        <h6 class="count" id="totalcount" runat="server" style="color:red;  margin-top:-3px"></h6>
                    </div>
                </div>
            </div>
            <ul class="navbar-list right">
                <li><a class="waves-effect waves-block waves-light profile-button" href="javascript:void(0);" data-target="profile-dropdown"><span class="avatar-status avatar-online">
                    <img src="Images/admin.jpg" alt="avatar"><i></i></span></a></li>
            </ul>


            <!-- profile-dropdown-->
            <ul class="dropdown-content" id="profile-dropdown">

                <li><a class="grey-text text-darken-1" href="Data.aspx?Action=Logout"><i class="material-icons">keyboard_tab</i> Logout</a></li>
            </ul>
        </div>

    </nav>
</div>


