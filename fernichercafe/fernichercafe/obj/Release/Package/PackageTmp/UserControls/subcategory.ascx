﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="subcategory.ascx.cs" Inherits="fernichercafe.UserControls.subcategory" %>

<style>
    label.radio-inline label {
        padding-left: 20px;
        display: inline-block;
    }
</style>
<!-- Page Content -->
<!-- ============================================================== -->
<!-- editmodal -->


<!-- BEGIN: Page Main-->
<div id="main">
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s10 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0">Sub Category</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="container">
                <div class="section section-data-tables">

                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div id="basic-tabs" class="card card card-default scrollspy">
                                <div class="card-content">
                                    <div class="col s12">
                                        <div class="row" id="main-view">
                                            <div class="col s12">
                                                <ul class="tabs tab-demo ">
                                                    <li class="tab col m3"><a target="_self" href="Data.aspx?Action=Category">Category</a>
                                                    </li>
                                                    <li class="tab col m3"><a class="active" href="#">Sub Category</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col s12">
                                                <div id="category" class="col s12" style="margin-top: 1rem;">
                                                    <div class="row">
                                                        <div class="col s12">
                                                            <table id="page-length-option" class="display" width="100%">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Banner Image</th>
                                                                        <th>Category Name</th>
                                                                        <th>Sub Category Name</th>
                                                                        <th>Product Image </th>
                                                                        <th>Status</th>
                                                                        <th>Action</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>

                                                                    <asp:PlaceHolder ID="pldsubCategorytable" runat="server"></asp:PlaceHolder>

                                                                </tbody>

                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div id="adddsubcategory" runat="server" class="fixed-action-btn direction-top">
                        <a class="btn-floating btn-large gradient-45deg-light-blue-cyan gradient-shadow modal-trigger mb-2 mr-1" href="#add_sub_category"><i class="material-icons">add</i></a>
                    </div>


                </div>

            </div>
        </div>
    </div>
</div>
<!-- END: Page Main-->
<div id="add_sub_category" class="modal">
    <div class="modal-content">
        <span class="modal-header right modal-close">
            <i class="material-icons right-align">clear</i>
        </span>
        <h4 id="myModalLabel" runat="server">Sub Category</h4>
        <div class="divider"></div>
        <div class="modal-body">
            <form id="subcategoryform" runat="server">
                <div class="row">
                    <div class="input-field col s6 col m6 col l6">
                        <input type="hidden" id="hiddenid" runat="server" />

                        <select id="ddlCategory" runat="server" required>
                        </select>
                        <label for="ddlCategory">Category Name</label>
                        <asp:RequiredFieldValidator ID="reqyear"
                            ControlToValidate="ddlCategory"
                            ErrorMessage="Please Select" Display="dynamic" InitialValue="0" Text="Please select" ForeColor="Red"
                            runat="server" />
                    </div>
                    <div class="input-field col s6 col m6 col l6">
                        <input type="text" class="validate" id="subCategoryname" runat="server" required="">
                        <label for="subCategoryname">Sub Category Name</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <textarea id="txtdesc" runat="server" class="materialize-textarea" data-length="120"></textarea>
                        <label for="txtdesc">Description</label>
                    </div>
                </div>
                <div id="file-upload" class="section">
                    <div class="row section">
                        <div class="col s12 m4 l3">
                            <p>Upload Photo</p>
                        </div>
                        <div class="col s12 m8 l9">

                            <img id="image_upload_preview" />
                            <label class="floatlabels" id="filenamelabel" runat="server"></label>
                            <input type="hidden" id="filepathedit" runat="server" />
                            <asp:FileUpload ID="inputFile" runat="server" />
                        </div>
                    </div>
                </div>
                <div id="banner-file-upload" class="section">
                    <div class="row section">
                        <div class="col s12 m4 l3">
                            <p>Upload Banner Image</p>
                        </div>
                        <div class="col s12 m8 l9">

                            <img id="banner-image_upload_preview" />
                            <label class="floatlabels" id="bannername" runat="server"></label>
                            <input type="hidden" id="bannerpathedit" runat="server" />
                            <asp:FileUpload ID="inputfile1" runat="server" />
                        </div>
                    </div>
                </div>
                <div class="col s12 m12 l12" id="view-options">
                    <div class="row">
                       <div class="col s6 m6 l6">
                        <label class="radio-inline p-0" style="top: 18px">Status </label>
                        <label class="radio-inline p-0" style="top: 0px; margin-left: 60px">

                            <asp:RadioButton GroupName="radio" CssClass="with-gap" ID="active" Text="Active" runat="server" />


                        </label>
                        <label class="radio-inline" style="width: 21%; margin-left: 50px; top: 0px">

                            <asp:RadioButton GroupName="radio" CssClass="with-gap" Text="In-Active" ID="inactive" runat="server" />

                        </label>
                    </div>
                    <div class="input-field col s6 m6 l6" style="margin-top:-15px;">
                        <label for="sortby">Sort By</label>
                        <input type="number" id="sortby" runat="server" />

                    </div>

                    </div>

                </div>
                <br>
                <div class="divider"></div>
                <div class="modal-footer">
                    <asp:HyperLink href="Data.aspx?Action=Subcategory" runat="server" class="modal-action modal-close waves-effect waves-red btn btn-danger">Cancel</asp:HyperLink>
                    <asp:Button type="button" ID="btnsave" runat="server" OnClick="btnsave_Click" Text="save" class="modal-action modal-close waves-effect waves-green btn btn-info "></asp:Button>

                </div>
            </form>
        </div>
    </div>

</div>

<footer class="page-footer footer footer-static footer-dark gradient-45deg-indigo-purple gradient-shadow navbar-border navbar-shadow">
    <div class="footer-copyright">
        <div class="container"><span>&copy; Fernicher Cafe 2019</span></div>
    </div>
</footer>


