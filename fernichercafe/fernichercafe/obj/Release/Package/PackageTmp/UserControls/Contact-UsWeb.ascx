﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Contact-UsWeb.ascx.cs" Inherits="fernichercafe.UserControls.Contact_UsWeb" %>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<link media="screen" rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>

<style type="text/css">
    .img-thumbnail {
        width: 200px;
        height: 200px;
        padding: 8px
    }

    .close {
        float: right;
        font-size: 25px;
        color: #dc3545;
        margin-top: -8px;
        margin-left: -15px;
        position: relative;
        margin-right: 15px;
        padding-left: 3px;
        /*color: #fff;*/
    }

    .row .col {
        padding: 0 .5rem;
    }

    input[type="file"] {
        position: absolute;
        left: 0;
        opacity: 0;
        top: 0;
        bottom: 0;
        width: 100%;
    }

    .upload {
        align-items: center;
        justify-content: center;
        border-radius: 10px;
        display: inline-block;
        position: relative;
        padding: 3px 3px;
        margin-bottom: 0;
        font-size: 14px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
    }

    tr {
        border-bottom: none;
    }

    td,
    th {
        padding: 0px 5px;
    }


    .card-panel {
        box-shadow: 0 2px 48px 0 rgba(0, 0, 0, 0.06);
        background: #fff;
        text-align: center;
        padding: 20px 9px;
        position: relative;
        z-index: 1;
        border-radius: 15px 5px 5px 5px;
        margin-top: -1.5rem;
        margin-bottom: 40px;
        -moz-transition: all 0.3s ease 0s;
        -o-transition: all 0.3s ease 0s;
        -ms-transition: all 0.3s ease 0s;
        -webkit-transition: all 0.3s ease 0s;
        transition: all 0.3s ease 0s;
    }

    .contact-info .card-panel-sec h4 {
        display: initial;
        text-transform: capitalize;
        font-weight: 500;
        font-size: 18px;
        padding-left: 5px;
        color: #ED7745;
    }

    .contact-info {
        margin-top: 30px;
    }

        .contact-info .card-panel-sec {
            box-shadow: 0 2px 48px 0 rgba(0, 0, 0, 0.06);
            background: #fff;
            text-align: center;
            padding: 20px 9px;
            position: relative;
            z-index: 1;
            border-radius: 15px 5px 5px 5px;
            margin-bottom: 40px;
            -moz-transition: all 0.3s ease 0s;
            -o-transition: all 0.3s ease 0s;
            -ms-transition: all 0.3s ease 0s;
            -webkit-transition: all 0.3s ease 0s;
            transition: all 0.3s ease 0s;
        }

    .contact-map .card-panel-sec-map {
        box-shadow: 0 2px 48px 0 rgba(0, 0, 0, 0.06);
        background: #fff;
        text-align: center;
        padding: 20px 9px;
        position: relative;
        z-index: 1;
        border-radius: 15px 5px 5px 5px;
        margin-bottom: 40px;
        -moz-transition: all 0.3s ease 0s;
        -o-transition: all 0.3s ease 0s;
        -ms-transition: all 0.3s ease 0s;
        -webkit-transition: all 0.3s ease 0s;
        transition: all 0.3s ease 0s;
    }

    .card-panel-sec:hover {
        -webkit-transform: translateY(-10px);
        transform: translateY(-10px);
    }

    .card-panel-sec::before {
        content: '';
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        border-radius: 15px 5px 5px 5px;
        background: #ED7745;
        color: #fff;
        opacity: 0;
        visibility: hidden;
        z-index: -1;
        -moz-transition: all 0.3s ease 0s;
        -o-transition: all 0.3s ease 0s;
        -ms-transition: all 0.3s ease 0s;
        -webkit-transition: all 0.3s ease 0s;
        transition: all 0.3s ease 0s;
    }

    .card-panel-sec:hover::before {
        opacity: 1;
        visibility: visible;
    }


    .card-panel-sec:hover h4, .card-panel-sec:hover p {
        color: #fff;
    }

    .contact-info .card-panel-sec p {
        padding-top: 10px;
    }

    .contact-info .card-panel-sec .icon {
        font-size: 18px;
        color: #ED7745;
    }

    .contact-info ul {
        list-style-type: none;
        display: -webkit-inline-box;
    }

        .contact-info ul li {
            padding: 0px 10px;
        }

            .contact-info ul li a i {
                font-size: 23px;
                color: #fff;
                padding: 3px;
                border-radius: 10%
            }

        .contact-info ul .twitter a i {
            background: #00acee;
        }

        .contact-info ul .instagram a i {
            background: radial-gradient(circle at 30% 107%, #fdf497 0%, #fdf497 5%, #fd5949 45%,#d6249f 60%,#285AEB 90%);
        }

        .contact-info ul .youtube a i {
            background: #c4302b;
        }

    .email {
        margin-top: 15px;
    }

    .subject {
        margin-top: -15px;
    }

    @media(min-width: 992px) {
        .contact-info .card-panel-sec {
            height: 200px;
            font-size: 13px;
        }

        .contact header .location {
            padding-top: 10px;
        }

        .contact-info ul {
            margin-left: -44px;
        }
    }

    @media(min-width: 1200px) {
        .contact-info .card-panel-sec {
            height: 212px;
            font-size: 16px;
        }
    }

    @media only screen and (min-width: 320px) and (max-width:768px) {
        .subject {
            margin-top: 0px;
        }
    }

    @media only screen and (min-width: 320px) and (max-width:640px) {
        .main-header {
            padding-top: 25px;
            height: 40vh;
            /*display:none;*/
        }

        .card-panel {
            margin-top: 50px;
        }
    }

    @media only screen and (max-width: 768px) {
        .main-header {
            padding-top: 182px;
            padding-bottom: 63px;
            height: 45vh;
        }

        .card-panel {
            margin-top: -6px;
        }
    }

    @media only screen and (min-width: 992px){
        .main-header {
            padding-top: 100px;
            padding-bottom: 160px;
            height: 80vh;
        }
        .card-panel {
            margin-top: -0.5rem;
        }
    }
</style>


<a href="DataWeb?Action=ManageDetailsWeb&category=Kid&id=1"><section class="main-header" style="background-image: url(ContentWebsite/assets/banner-romanticproduct.jpg)">
    <header>
        <div class="container text-center">
    
        </div>
    </header>
</section>
    </a>

<!-- ========================  Contact ======================== -->

<section class="contact">

    <div class="container">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="card-panel">
                    <header>
                        <div class="" style="text-align: center;">
                            <h2 class="title message">Send Us A Message</h2>
                        </div>
                    </header>

                    <div class="contact-form">
                        <form>
                            <div class="row">
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <input id="Name" name="Name" type="text" value="" class="form-control" placeholder="Your Name" required>
                                    </div>
                                </div>


                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group email">
                                        <input id="Email" name="Email" type="email" value="" class="form-control" placeholder="Your Email" required>
                                    </div>
                                </div>

                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <input id="phonenumber" name="phonenumber" type="tel" value="" class="form-control" placeholder="Phone Number" maxlength="10" minlength="10" required>
                                    </div>
                                </div>
                                 <div class="col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <input id="subject" name="subject" type="tel" value="" class="form-control" placeholder="Subject">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea id="Comment" name="Comment" class="form-control" placeholder="Your message" rows="5"></textarea>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="productenquiryimages">
                                </div>
                                <div class="col-md-12 text-center">
                                    <input type="button" class="btn btn-clean" onclick="submitform()" value="Send message" />
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="contact-info row">
        <div class=" col-md-8 col-lg-8 col-md-offset-2">

            <div class="row">

                <div class="col-md-4 col-lg-4">
                    <div class="card-panel-sec">
                        <h4>Our Address</h4>
                        <p id="phldfooteraddress" runat="server"><span class="icon icon-map-marker"></span>&nbsp;</p>
                    </div>
                </div>

                <div class="col-md-4 col-lg-4">
                    <div class="card-panel-sec">
                        <h4>Get In Touch </h4>
                        <p id="phldfooteremail" runat="server"><span class="icon icon-envelope"></span>&nbsp;</p>
                        <p style="padding-top: 0px" id="phldmobile" runat="server"><span class="icon icon-phone"></span>&nbsp;</p>
                        <div class="text-center" style="margin-top: 15px;">
                            <ul>
                                <li class="instagram"><a href="https://www.instagram.com/fernichercafe/" target="_blank" title="Instagram"><i class="fa fa-instagram"></i></a></li>
                                <li class="twitter"><a href="https://twitter.com/search?q=fernicher%20cafe&src=typed_query" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                <li class="youtube"><a href="https://www.youtube.com/channel/UC14qmcaHxiJFvvsysiN7Ihg" target="_blank" title="Youtube"><i class="fa fa-youtube"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-lg-4">
                    <div class="card-panel-sec">
                        <h4>Working Hours</h4>
                        <p id="phldfootertiming" runat="server"><span class="icon icon-clock"></span>&nbsp;</p>
                        <p style="padding-top: 0px"><span class="icon icon-clock" style="visibility: hidden;"></span>&nbsp;Sunday: Holiday</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="contact-map ">
        <div class="row card-panel-sec-map">
            <!--  <header>
                      <div class="col-md-offset-1 col-xs-offset-1">
                            <h2 class="title location">Our Location</h2>
                                </div>
                        </header> -->
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                <iframe id="maplocation" runat="server"  width="100%" height="450" frameborder="0" style="border: 0;" allowfullscreen=""></iframe>
            </div>
        </div>
    </div>

    </div><!-- container -->
</section>
<!-- container -->



<script>
    var obj = localStorage.getItem('Enquirystorage');


    if (JSON.stringify(obj) != "" && JSON.stringify(obj) != null) {
        $.ajax({
            type: "POST",
            url: "userwebservice.asmx/getEnquiryImage",
            data: "{ 'obj': '" + obj + "' }",
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response == "Failed") {

                }
                var jsonresponse = JSON.parse(response.d);
                if (jsonresponse.length > 0) {
                    var html = "";
                    for (var i = 0; i < jsonresponse.length; i++) {
                        html += "<div class='col-xs-6 col-sm-4 col-md-3 col-lg-3 localdiv'><a href='#!' type='button' onclick=deletelocalimage(this," + jsonresponse[i].productid + "," + jsonresponse[i].subcategoryid + ") class='close deletelocalimage btn-floating mb-1 waves-effect waves-light' aria-label='Close'>×</a><img class='img-thumbnail' src='" + jsonresponse[i].Image + "' style='height:100px; width:150px; margin-bottom:20px;' /></div>";
                    }
                    $('#productenquiryimages').append(html);
                }
            },
            error: function (req, status, error) {
                alert("Error try again");
            }
        });
    }

    function deletelocalimage(elem, productid, subcategoryid) {
        var result1 = confirm("Are you sure want to delete?");
        if (result1) {
            var original = JSON.parse(localStorage.getItem('Enquirystorage'));
            var setValue = original["ProductList"];

            const index = setValue.findIndex(x => x.subcategoryid === productid && x.productid === subcategoryid);

            if (index !== undefined) setValue.splice(index, 1);
            localStorage.setItem('Enquirystorage', JSON.stringify(original));
            showpop("Item removed", "Success");

            $(elem).closest(".localdiv").html('');
        }
        return false;
    }
    function showpop(msg, title) {
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "200",
            "hideDuration": "1000",
            "timeOut": "7000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        // toastr['success'](msg, title);
        
        toastr.success(msg);
        return false;
    }

    function submitform() {
        var name = $("#Name").val();
        var email = $("#Email").val();
        var number = $("#phonenumber").val();
        var comments = $("#Comment").val();
        var jsonstr = localStorage.getItem('Enquirystorage');
        
        if (email != "" || name != "") {
            if (comments != "" || number != "") {
                $.ajax({
                    type: "POST",
                    url: "userwebservice.asmx/MultipleProductEnquiry",
                    data: "{'Name':'" + name + "','Email':'" + email + "','Number':'" + number + "','Comments':'" + comments + "', 'obj': '" + jsonstr + "' }",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        
                        if (response.d == "Success") {
                            localStorage.removeItem('Enquirystorage');
                            window.location.reload();
                        }
                    }
                });
            } else {
                alert("Fill other fields");
            }
        } else {
            alert("Fill other fields");
        }

    }
</script>
