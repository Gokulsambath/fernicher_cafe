﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="privacypolicyWeb.ascx.cs" Inherits="fernichercafe.UserControls.privacypolicyWeb" %>

<style type="text/css">
    .privacy_policy_section .privacy_policy_outer ul {
        list-style-type: upper-roman;
    }

    .privacy_policy_section ul li h5 {
        margin-bottom: 5px;
        color: #ED7745;
        /*text-transform: capitalize;*/
    }

    .privacy_policy_section ul li {
        padding-left: 10px
    }

        .privacy_policy_section ul li p {
            text-align: justify;
            padding-bottom: 10px;
        }

    @media only screen and (min-width: 320px) and (max-width:640px) {
        .main-header {
            padding-top: 25px;
            height: 40vh;
            /*display:none;*/
        }

    }

    @media only screen and (max-width: 768px) {
        .main-header {
            padding-top: 182px;
            padding-bottom: 63px;
            height: 45vh;
        }

    }

    @media only screen and (min-width: 992px){
        .main-header.main-header-blog {
            padding-top: 100px;
            padding-bottom: 160px;
            height: 70vh;
        }

    }
</style>
<!-- ========================  Main header ======================== -->

<a href="DataWeb?Action=ManageDetailsWeb&category=Kid&id=4"><section class="main-header main-header-blog" style="background-image: url(ContentWebsite/assets/policy.jpg)">
    <header>
        <div class="container text-center">
            <!-- <h2 class="h2 title">Contact</h2> -->
            <!-- <ol class="breadcrumb breadcrumb-inverted">
                        <li><a href="index.html"><span class="icon icon-home"></span></a></li>
                        <li><a class="active" href="contact.html">Contact</a></li>
                    </ol>
 -->
        </div>
    </header>
</section>
    </a>



<!-- ========================  Band Partner ======================== -->
<section class="terms_conditions">
    <div class="container">

        <!-- === History header === -->

        <header>
            <div class="row">
                <div class="col-md-offset-2 col-md-8 text-center">
                    <h1 class="h2 title">Privacy Policy</h1>
                </div>
            </div>
        </header>

        <!-- === row item === -->

        <div class="privacy_policy_section">
            <div class="row">
                <div class="privacy_policy_outer">

                    <ul>
                        <li>
                            <h5><strong>ADVANCE PAYMENT</strong></h5>
                            <p></p>
                        </li>
                        <li>
                            <h5><strong>MATERIALS ONCE DELIVERD WILL NOT BE EXCHANGED</strong></h5>
                            <p></p>
                        </li>
                        <li>
                            <h5><strong>PRICES ARE SUBJECT TO CHANGED WITHOUT PRIOR NOTICE</strong></h5>
                            <p></p>
                        </li>
                        <li>
                            <h5><strong>SUBJECT TO BANGALORE JURISDICTION ONLY</strong>
                            </h5>
                            <p></p>
                        </li>
                        <li>
                            <h5><strong>TRANSPORT WILL BE EXTRA.</strong></h5>
                            <p></p>
                        </li>
                    </ul>
                </div>
                <!-- terms_conditions_outer -->
            </div>
            <!-- row -->
        </div>
        <!-- terms_conditions_section -->




    </div>
</section>

