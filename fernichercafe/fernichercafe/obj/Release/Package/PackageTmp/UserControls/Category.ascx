﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Category.ascx.cs" Inherits="fernichercafe.UserControls.Category" %>



    <!-- /#wrapper -->
      <div id="main">
      <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
          <!-- Search for small screen-->
          <div class="container">
            <div class="row">
              <div class="col s10 m6 l6">
                <h5 class="breadcrumbs-title mt-0 mb-0">Category</h5>
              </div>
            </div>
          </div>
        </div>
        <div class="col s12">
          <div class="container">
            <div class="section section-data-tables">

        <div class="row">
              <div class="col s12 m12 l12">
      <div id="basic-tabs" class="card card card-default scrollspy">
        <div class="card-content">
          <div class="col s12">
            <div class="row" id="main-view">
              <div class="col s12">
                <ul class="tabs tab-demo ">
                  <li class="tab col m3"><a class="active" href="#">Category</a>
                  </li>
                  <li class="tab col m3"><a target="_self" href="Data.aspx?Action=Subcategory">Sub Category</a>
                  </li>
                </ul>
              </div>
              <div class="col s12">
                <div id="category" class="col s12" style="margin-top:1rem;">
                  <div class="row">
            <div class="col s12">
              <table id="page-length-option" class="display" width="100%">
                <thead>
                  <tr>
                    <%--<th width="30%">Category Name</th>--%>
                      <th width="30%">Category Name</th>
                    <th  width="30%">Image </th>
                    <th width="20%">Status</th>                    
                      <th width="20%">Action</th>
                  </tr>
                </thead>
                <tbody>
              <asp:PlaceHolder ID="pldCategorytable" runat="server"></asp:PlaceHolder>
               
                </tbody>
                
              </table>
            </div>
          </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- Scroll - vertical, dynamic height -->

  

  <!-- Scroll - Vertical and Horizontal -->

 

  <!-- Multi Select -->


</div><!-- START RIGHT SIDEBAR NAV -->

<!-- END RIGHT SIDEBAR NAV -->
          
          </div>
        </div>
      </div>
    </div>


 <%--  <div style="bottom: 50px; right: 19px;" id="adddsubcategory" runat="server" class="fixed-action-btn direction-top"><a class="btn-floating btn-large gradient-45deg-light-blue-cyan gradient-shadow modal-trigger mb-2 mr-1" href="#add_category"><i class="material-icons">add</i></a>
       </div>--%>

    <!-- END: Page Main-->
<div id="add_category" class="modal">
        <div class="modal-content">
            <span class="modal-header right modal-close">
                  <i class="material-icons right-align">clear</i>
                </span>
            <h4 id="myModalLabel" runat="server"> Category</h4>
            <div class="divider"></div>
            <div class="modal-body">
                <form id="categoryform" runat="server">
                    <div class="row">
                       
                        <%--<div class="input-field col s6 col m6 col l6">                            
                            <input type="text" class="validate" id="Categoryname" runat="server" required="" >
                            <label for="Categoryname">Category Name</label>
                        </div>--%>
                        <div class="input-field col s6 col m6 col l6">
                            <input type="hidden" id="hiddenid" runat="server" />
                            <input type="text" class="validate" id="displayname" runat="server" required="" >
                            <label for="displayname">Category Name</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <textarea id="txtdesc" runat="server" class="materialize-textarea" data-length="120"></textarea>
                            <label for="txtdesc">Description</label>
                        </div>
                    </div>
                    <div id="file-upload" class="section">
                        <div class="row section">
                            <div class="col s12 m4 l3">
                                <p>Upload Photo</p>
                            </div>
                            <div class="col s12 m8 l9">
                                
                                <img id="image_upload_preview"  /><label class="floatlabels" id="filenamelabel" runat="server"></label>
                                   <input type="hidden" id="filepathedit" runat="server" />
                                <asp:FileUpload ID="inputFile" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m12 l12" id="view-options">
                       
                        <label class="radio-inline p-0" style="top:18px"> Status </label>
                        <label class="radio-inline p-0" style="top:0px;margin-left:60px">
                         
                                    <asp:RadioButton GroupName="radio" CssClass="with-gap" ID="active" Text="Active" runat="server" />                                         
                                                        
                           
                        </label>
                        <label class="radio-inline" style="width: 21%;margin-left:50px;top:0px">
                           
                                <asp:RadioButton GroupName="radio" CssClass="with-gap" Text="In-Active" ID="inactive" runat="server" />
                                          
                        </label>
                    </div>
                    <br />
                <div class="divider"></div>
                <div class="modal-footer">
                    <asp:HyperLink href="Data.aspx?Action=Category" runat="server" class="modal-action modal-close waves-effect waves-red btn btn-danger">Cancel</asp:HyperLink>
                          <asp:button type="button" id="btnsave" runat="server" OnClick="btnsave_Click" Text="save" class="modal-action modal-close waves-effect waves-green btn btn-info "></asp:button>

                </div>
            </form>
            </div>
        </div> 

 </div>
    <!-- Theme Customizer -->    
    <!-- BEGIN: Footer-->

    <footer class="page-footer footer footer-static footer-dark gradient-45deg-indigo-purple gradient-shadow navbar-border navbar-shadow">
      <div class="footer-copyright">
        <div class="container"><span>&copy; Fernicher Cafe 2019</span></div>
      </div>
    </footer> 

