﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Blog.ascx.cs" Inherits="fernichercafe.UserControls.Blog" %>


    <!-- BEGIN: Page Main-->
    <div id="main">
        <div class="row">
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">
                    <div class="row">
                        <div class="col s10 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0">Newsletter</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <div class="section section-data-tables">

                        <div class="row">
                            <div class="col s12 m12 l12">
                                <div class="card card card-default scrollspy">
                                    <div class="card-content">
                                        <div class="col s12">
                                            <div class="row" id="main-view">


                                                <table id="page-length-option" class="display" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="20%">Title</th>
                                                            <th width="17%">Image </th>
                                                            <th width="15%">Uplaoded on</th>
                                                            <th width="38%">Description</th>
                                                            <th width="5%">Status</th>
                                                            <th width="5%">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit,....</td>


                                                            <td><img src="images/fimg.jpg" alt="furniture" style="width: 50px;height: 50px"></td>
                                                            <td>19-11-2019</td>
                                                            <td style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua......</td>
                                                            <td>Active</td>
                                                            <td><a class="modal-trigger" href="#add_sub_category" style="padding-right: 10px"><i class="fa fa-edit"></i></a>
                                                                <a href="#!" style="padding-left:10px"><i class="fa fa-trash"></i></a>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td>Kid Furniture</td>


                                                            <td><img src="images/fimg.jpg" alt="furniture" style="width: 50px;height: 50px"></td>
                                                            <td>19-11-2019</td>
                                                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua....</td>

                                                            <td>Active</td>
                                                            <td><a class="modal-trigger" href="#add_sub_category" style="padding-right: 10px"><i class="fa fa-edit"></i></a>
                                                                <a href="#!" style="padding-left:10px"><i class="fa fa-trash"></i></a>
                                                            </td>

                                                        </tr>


                                                    </tbody>

                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>

                    <!-- END RIGHT SIDEBAR NAV -->
                    <div style="bottom: 50px; right: 19px;" class="fixed-action-btn direction-top"><a class="btn-floating btn-large gradient-45deg-light-blue-cyan gradient-shadow modal-trigger mb-2 mr-1" href="#add_sub_category"><i class="material-icons">add</i></a>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Page Main-->
 <div id="add_sub_category" class="modal">
        <div class="modal-content">
            <h4> Add Newsletter</h4>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <div class="input-field col m9 l9 s12">
                            <label for="title">Title </label>
                            <input type="text" id="title" name="title" class="validate" aria-required="true">
                        </div>
                        <div class="input-field col m3 l3 s12">
                            <label for="uploadedon">Uploaded on</label>
                            <input type="text" class="datepicker" id="uploadedon">
                        </div>

                    </div>
                    <div class="row">
                        <div class=" col s12">
                            <label for="textarea1">Description</label>
                            <textarea id="textarea1" style="height: 7rem"></textarea>

                        </div>
                    </div>
                    <div id="file-upload" class="section">
                        <div class="row section">
                            <div class="col s12 m4 l3">
                                <p>Upload Photo</p>
                            </div>
                            <div class="col s12 m8 l9">
                                <input type="file" id="input-file-now" class="dropify" data-default-file="" />
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m12 l12" id="view-options">
                        <label style="font-size: 1rem">Status :&nbsp;</label>
                        <label>
                            <input class="with-gap" name="group4" type="radio" checked />
                            <span>Active</span>
                        </label>
                        <label>
                            <input class="with-gap" name="group4" type="radio" />
                            <span>In-Active</span>
                        </label>
                    </div>

                </form>
                <div class="modal-footer">
                    <a href="#!" class="modal-action modal-close waves-effect waves-red btn btn-danger ">Cancel</a>
                    <a href="#!" class="modal-action modal-close waves-effect waves-green btn btn-info ">Submit</a>
                </div>
            </div>

        </div>

     </div>