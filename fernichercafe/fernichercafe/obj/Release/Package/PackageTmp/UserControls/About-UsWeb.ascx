﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="About-UsWeb.ascx.cs" Inherits="fernichercafe.UserControls.About_Us" %>
<style type="text/css">
    html {
        scroll-behavior: smooth;
    }
</style>
<style type="text/css">
   
    @media only screen and (min-width: 320px) and (max-width:640px) {
        .main-header {
            padding-top: 200px;
        }
        header{
            margin-bottom:20px;
        }
    }
    @media only screen and (min-width: 768px) {
        .main-header {
            padding-top: 350px;
            
        }
    }
   
    @media only screen and (min-width: 320px) and (max-width:640px) {
        .image-responsive {
            height: 360px;
        }

        .team {
            padding: 20px 0;
        }
    }

    @media only screen and (min-width: 641px) and (max-width:768px) {

        .image-responsive {
            height: 189px;
        }

        .team {
            padding: 20px 0;
        }
    }

    @media only screen (max-width:992px) {

        .image-responsive {
            height: 240px;
        }

        .team {
            padding: 20px 0;
        }
    }

    @media only screen and (min-width: 993px) and (max-width:1200px) {

        .image-responsive {
            height: 280px;
        }
    }

    @media only screen and (min-width: 992px) and (max-width:1024px) {
        .history .row-block > * {
            padding: 0px;
        }

        .history .history-title:before {
            opacity: 0.6;
        }

        .history-desc1 {
            padding: 2px;
        }
    }
</style>
<style>
   
    .amit-profile {
        margin-top: 50px;
    }

    .bikash-profile {
        margin-top: 100px;
    }

    .bikash-text-div {
        padding: 50px;
    }

    .bikash-comment, .amit-comment, .radhe-text, .fernicher-text {
        text-align: justify;
        line-height: 24px;
    }

    .amit-text-div {
        padding: 50px;
    }

    @media only screen and (min-width: 320px) and (max-width:640px) {
        .bikash-profile {
            margin-top: 30px;
            display: flex;
            flex-direction: column-reverse;
        }

        .bikash-text-div {
            padding: 0px;
            margin-top: 50px;
        }

        .amit-text-div {
            padding: 0px;
            margin-top: 50px;
        }

        .amit-profile {
            margin-top: 10px;
        }

        .bikash-comment, .amit-comment, .radhe-text, .fernicher-text {
            text-align: justify;
            line-height: 20px;
        }
    }

    @media only screen and (max-width:768px) {
        .bikash-text-div {
            padding: 0px;
            margin-top: 20px;
        }

        .amit-text-div {
            padding: 0px;
            margin-left: 20px;
        }
    }
    

</style>
 
<section class="main-header main-header-blog" id="aboutusheaderbanner" runat="server">
    <header>
        <div class="container text-center">
            <a href="#ourstory">
                <h2 class="h2 title" id="bannerheader" runat="server"></h2>
                <p class="subtitle" id="bannersubheader" runat="server" style="color: white; text-shadow: 1px 1px 5px black, 0 0 30px #ED7745, 0 0 20px #ED7745; text-transform: none;"></p>
            </a>
        </div>
    </header>
</section>


<section class="history" id="ourstory">
    <div class="container">

        <!-- === History header === -->

        <header>
            <div class="row">
                <div class="col-md-offset-2 col-md-8 text-center">
                    <h1 class="h2 title" id="historyheader" runat="server"></h1>
                    <div class="text">
                        <p id="historysubheader" runat="server"></p>
                    </div>
                </div>
            </div>
        </header>

        <!-- === row item === -->

        <a href="http://www.radheshyamlaminates.com/" target="_blank">
            <div class="row row-block">
                <div class="col-md-6  col-sm-5 col-lg-5 history-image1" id="title1photo" runat="server">
                    <div class="history-title">
                        <h2 class="title">2000</h2>
                        <p>The begining</p>
                    </div>
                </div>
                <div class="col-md-6  col-sm-7 col-lg-7 history-desc">
                    <div class="history-desc1">
                        <div class="h5 title" id="title1" runat="server"></div>
                        <p class="radhe-text" id="title1desc" runat="server">
                        </p>
                    </div>
                </div>
            </div>
        </a>



        <!-- === row item === -->
        <div class="row row-block">
            <div class="col-md-5 col-sm-5 col-lg-5 history-image" id="title2photo" runat="server">
                <div class="history-title">
                    <h2 class="title">2019</h2>
                    <p>The begining</p>
                </div>
            </div>
            <div class="col-md-7 col-sm-7 col-lg-7 history-desc">
                <div class="history-desc1">
                    <div class="h5 title" id="title2" runat="server"></div>
                    <p class="fernicher-text" id="title2desc" runat="server">
                     
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ================== Intro section default ================== -->

<section class="our-team">
    <div class="container">

        <!-- === Our team header === -->

        <div class="row">
            <div class="col-md-offset-2 col-md-8 text-center">
                <h1 class="h2 title" id="directorheader" runat="server"></h1>
                <div class="text">
                    <p id="directordesc" runat="server"></p>
                </div>
            </div>
        </div>

        <!-- === Our team === -->

        <div class="team">
            <div class="row amit-profile">
               
                <div class="col-md-5 col-sm-5 col-lg-5" id="director1photo" runat="server">
                    
                </div>
                <div class="col-md-7 col-sm-7 col-lg-7 history-desc">
                    <div class="bikash-text-div">
                        <div class="h5 title" style="text-transform: none;" id="director1" runat="server"></div>
                        <p class="amit-comment" id="director1desc" runat="server">
                            
                        </p>
                    </div>
                </div>
            </div>
            <!--/row-->
            <div class="row bikash-profile">
                <div class="col-xs-12 col-md-7 col-sm-7 col-lg-7 history-desc">
                    <div class="bikash-text-div">
                        <div class="h5 title" style="text-transform: none;" id="director2" runat="server"></div>
                        <p class="bikash-comment" id="director2desc" runat="server">
                           
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-md-5 col-sm-5 col-lg-5" id="director2photo" runat="server">
                    
                </div>
            </div>
           
        </div>
        
    </div>
</section>



<section class="banner mission-vission" id="missionbannerphoto" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8 text-center">
                <h1 class="h2 title" style="color: white; text-shadow: 1px 1px 5px black, 0 0 30px #ED7745, 0 0 20px #ED7745; text-transform: none;" id="missionheader" runat="server"></h1>
                <div class="text">
                    <p style="color: white; text-shadow: 1px 1px 5px black, 0 0 30px #ED7745, 0 0 20px #ED7745; text-transform: none;" id="missiondesc" runat="server">
                       </p>
                </div>
            </div>
        </div>
    </div>
</section>



