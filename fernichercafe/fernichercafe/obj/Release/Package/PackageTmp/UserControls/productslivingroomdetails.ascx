﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="productslivingroomdetails.ascx.cs" Inherits="fernichercafe.UserControls.productslivingroomdetails" %>
<style type="text/css">
    .img-thumbnail {
        width: 200px;
        height: 200px;
        display: block;
        padding: 8px
    }

    .close {
        float: right;
        font-size: 25px;
        color: #dc3545;
        margin-top: -8px;
        margin-left: -15px;
        position: relative;
        margin-right: 15px;
        padding-left: 3px;
        color: #fff;
    }

    .row .col {
        padding: 0 .5rem;
    }

    input[type="file"] {
        position: absolute;
        left: 0;
        opacity: 0;
        top: 0;
        bottom: 0;
        width: 100%;
    }

    .upload {
        align-items: center;
        justify-content: center;
        border-radius: 10px;
        display: inline-block;
        position: relative;
        padding: 3px 3px;
        margin-bottom: 0;
        font-size: 14px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
    }

    tr {
        border-bottom: none;
    }

    td,
    th {
        padding: 0px 5px;
    }
</style>
<script>
    $(document).ready(function () {
        col1 = $("table tr th:nth-child(1), table tr td:nth-child(1)");
        col3 = $("table tr th:nth-child(3), table tr td:nth-child(3)");
        col4 = $("table tr th:nth-child(4), table tr td:nth-child(4)");
        col1.hide();        
        if ($('#MiddleCenterContent_ctl01_hiddensubcategoryid').val() != "2") {
            col3.hide(); col4.hide();
        }
    });
</script>
<form id="formdetails" runat="server">

    <!-- BEGIN: Page Main-->
    <div id="main">
        <div class="row">
            <input type="hidden" id="hiddensubcategoryid" runat="server" />
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">
                    <div class="row">
                        <div class="col s10 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0">Living Room Furniture</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <div class="section section-form-wizard">


                        <!-- Horizontal Stepper -->

                        <div class="row">
                            <div class="col s12">
                                <div class="card">
                                    <div class="card-content pb-0">


                                        <ul class="stepper horizontal" id="horizStepper">
                                            <li class="step active">
                                                <div class="step-title waves-effect">Product Details</div>
                                                <div class="step-content">
                                                    <div class="row">
                                                        <div class="input-field col m4 l4 s12">
                                                            <label for="productname">Product Name </label>
                                                            <input type="text" id="productname" name="firstName" runat="server" class="validate"
                                                                aria-required="true">
                                                        </div>
                                                        <div class="input-field col m4 l4 s12">
                                                            <label for="targetgroup">Model Name</label>
                                                            <input type="text" id="modelname" class="validate" runat="server" aria-required="true"
                                                                name="modelname">
                                                        </div>
                                                        <%-- <div class="input-field col m4 l4 s12">
                                                            <label for="segment">Segment </label>
                                                            <input type="text" id="segment" class="validate" runat="server" aria-required="true"
                                                                name="">
                                                        </div>--%>
                                                    </div>
                                                    <%--  <div class="row">
                                                        <div class="input-field col m4 l4 s12">
                                                            <label for="agegroup">Age Group </label>
                                                            <input type="text" id="agegroup" name="firstName" runat="server" class="validate"
                                                                aria-required="true">
                                                        </div>
                                                        <div class="input-field col m4 l4 s12">
                                                            <label for="theme">Theme </label>
                                                            <input type="text" id="theme" name="" runat="server" class="validate"
                                                                aria-required="true">
                                                        </div>
                                                        <div class="input-field col m4 l4 s12">
                                                            <label for="certificates">Certificates </label>
                                                            <input type="text" id="certificates" runat="server" name="" class="validate"
                                                                aria-required="true">
                                                        </div>
                                                    </div>--%>
                                                    <div class="row">
                                                        <div class="input-field col m4 l4 s12">
                                                            <label for="manufacture">Manufacture </label>
                                                            <input type="text" id="manufacture" runat="server" name="firstName" class="validate"
                                                                aria-required="true">
                                                        </div>
                                                        <div class="input-field col m4 l4 s12">
                                                            <label for="matirials">Matirials </label>
                                                            <input type="text" id="matirials" runat="server" name="" class="validate"
                                                                aria-required="true">
                                                        </div>
                                                        <div class="input-field col m4 l4 s12">
                                                            <label>
                                                                <input type="checkbox" id="chkavail" runat="server" />
                                                                <span>Avaliblility</span>
                                                            </label>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="input-field col m12 l12 s12">
                                                            <label for="feature">Product Description </label>
                                                            <input type="text" id="feature" runat="server" name="" class="validate"
                                                                aria-required="true">
                                                        </div>
                                                    </div>
                                                    <div class="" style="float: right;">
                                                        <div class="row">



                                                            <button class="waves-effect waves dark btn btn-primary next-step"
                                                                type="submit">
                                                                Continue
                                           
                                                            </button>

                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="step">
                                                <div class="step-title waves-effect">Technical Details </div>
                                                <div class="step-content">

                                                    <asp:PlaceHolder ID="dynamictextbox" runat="server"></asp:PlaceHolder>
                                                    <table class="mytable4" id="mytable" runat="server">
                                                        <tbody>
                                                            <tr>
                                                                <td></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                    <input type="hidden" runat="server" id="hiddendynamictextboxCount" value="0" />
                                                    <div class="row">

                                                        <a href="#!" class="add4"><i class="fa fa-plus"></i>&nbsp;Add row</a>
                                                    </div>
                                                    <br>


                                                    <div class="" style="float: right;">
                                                        <div class="row">


                                                            <button class="btn  btn-light previous-step">
                                                                Previous
                                           
                                                            </button>


                                                            <button class="waves-effect waves dark btn btn-primary next-step"
                                                                type="submit">
                                                                Continue
                                           
                                                            </button>

                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="step">
                                                <div class="step-title waves-effect">Upload Images</div>
                                                <div class="step-content">
                                                    <div class="row">
                                                        <div class=" btn btn-warning upload " style="float: right;">
                                                            <span><i class="fa fa-plus" style="color: #fff"></i>&nbsp;Add Livingroom Furniture Images</span>
                                                            <%--<input type="file" class=""  name="photo" id="photo" accept=".png, .jpg, .jpeg" onchange="readFile(this);" multiple>--%>
                                                            <asp:FileUpload ID="photo" Multiple="Multiple" onchange="readFile(this)" accept=".png,.jpg,.jpeg" runat="server" />
                                                        </div>
                                                        <br>
                                                        <br>
                                                        <div id="status"></div>
                                                        <div id="photosection" runat="server" class="row"></div>
                                                    </div>
                                                    <div class="row">
                                                        <asp:GridView runat="server" ID="datagridview" PageSize="8" CssClass="mydatagrid" AutoGenerateColumns="false" AllowPaging="True"
                                                            CellPadding="4" ForeColor="#333333" GridLines="None" BorderColor="#003300" BorderStyle="Solid"
                                                            BorderWidth="1px" Font-Size="11pt" Font-Names="Century" DataKeyNames="ID" OnRowEditing="datagridview_RowEditing" OnRowCancelingEdit="datagridview_RowCancelingEdit" OnRowUpdating="datagridview_RowUpdating"
                                                            OnRowDeleting="datagridview_RowDeleting">
                                                            <RowStyle BackColor="#EFF3FB" BorderColor="Black" BorderWidth="1px" />
                                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                            <EditRowStyle BackColor="#2461BF" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                                            <AlternatingRowStyle BackColor="White" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                                            <Columns>
                                                                
                                                                <asp:BoundField HeaderText="ID" DataField="ID" />
                                                                <asp:TemplateField HeaderText="Images">
                                                                    <ItemTemplate>
                                                                        <asp:Literal ID="Literal2" runat="server" Text='<%# Eval ("Image") %>'></asp:Literal>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField HeaderText="colorcode" DataField="colorcode" />
                                                               
                                                                <asp:CommandField ShowEditButton="true" HeaderText="Edit" CausesValidation="false" />
                                                                <asp:CommandField ShowDeleteButton="true" HeaderText="Delete" /> 
                                                            </Columns>

                                                        </asp:GridView>
                                                    </div>
                                                    <div class="" style="float: right;">
                                                        <div class="row">
                                                            <button class="btn  btn-light previous-step">
                                                                Previous
                                           
                                                            </button>


                                                            <asp:Button CssClass="waves-effect waves-dark btn btn-info"
                                                                Text="Submit" runat="server" OnClick="submit_Click" ID="submit"></asp:Button>



                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--don't remove this code -->

                        <div class="row" style="display: none">
                            <div class="col s12">
                                <div class="card">
                                    <div class="card-content">
                                        <div class="card-header">
                                            <h4 class="card-title">Linear Stepper</h4>
                                        </div>
                                        <ul class="stepper linear" id="linearStepper">
                                            <li class="step active">
                                                <div class="step-title waves-effect">Step 1</div>
                                                <div class="step-content">
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Non Linear Stepper -->





                    </div>
                    <!-- START RIGHT SIDEBAR NAV -->

                    <!-- END RIGHT SIDEBAR NAV -->

                </div>
            </div>
        </div>
    </div>
    <!-- END: Page Main-->
</form>

<footer class="page-footer footer footer-static footer-dark gradient-45deg-indigo-purple gradient-shadow navbar-border navbar-shadow">
    <div class="footer-copyright">
        <div class="container"><span>&copy; Fernicher Cafe 2019</span></div>
    </div>
</footer>



<script>
    function readFile(input) {
        $("#status").html('Processing...');
        counter = input.files.length;
        for (x = 0; x < counter; x++) {
            if (input.files && input.files[x]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $("#MiddleCenterContent_ctl01_photosection").append('<div class="col m3 col 3 cols 12 localdiv" > <a href="#!" type="button" class="close deletelocalimage btn-floating mb-1 waves-effect waves-light"  aria-label="Close">&times;</a><img src="' + e.target.result + '" name="multipleimageupload" class="img-thumbnail"></div>');
                };

                reader.readAsDataURL(input.files[x]);
            }
        }
        if (counter == x) {
            $("#status").html('');
        }
    }

    $(document).on('click', ".deletelocalimage", function () {
        var result1 = confirm("Are you sure want to delete?");
        if (result1) {
            $(this).closest(".localdiv").remove();
        }

        return false
    });

    $(".add4").click(function () {
        $(".mytable4").each(function () {

            var x = document.getElementById('<%=hiddendynamictextboxCount.ClientID%>').value;
            x = parseInt(x) + 1;
            var html = '<div class=row><div class=input-field col s12 style=margin-bottom:0rem><input id=technicaldetails' + x + ' name=technicaldetails type=text data-length=10><label for=technicaldetails>Technical details</label></div></div>';
            var tds = '<tr>';
            jQuery.each($('tr:last td', this), function () {
                tds += '<td>' + html + '</td>';
            });
            tds += '</tr>';
            if ($('tbody', this).length > 0) {
                $('tbody', this).append(tds);
            } else {
                $(this).append(tds);
            }
            document.getElementById('<%=hiddendynamictextboxCount.ClientID%>').value = x;
        });
    });
    </script>
