﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fernichercafe
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Control ctlHeader = LoadControl("~/UserControls/GlobalHeaderLoggedIn.ascx");                        Control ctlHome = LoadControl("~/UserControls/GlobalHeader.ascx");            

            Control ctlFoot = LoadControl("~/UserControls/GlobalFooter.ascx");

            GlobalHeaderContent.Controls.Add(ctlHeader);            MiddleCenterContent.Controls.Add(ctlHome);            GlobalFooterContent.Controls.Add(ctlFoot);

            
            //MiddleContent



            if (this.Page.Request.QueryString.Get("Action").ToString().Equals("AdminDashboard"))            {                Control ctlAdminDashboard = LoadControl("~/UserControls/AdminDashboard.ascx");                MiddleCenterContent.Controls.Add(ctlAdminDashboard);            }
            
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("Category"))            {                Control ctlApproverDashboard = LoadControl("~/UserControls/Category.ascx");                MiddleCenterContent.Controls.Add(ctlApproverDashboard);            }

            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("productkiddetails"))            {                Control ctlUserDetails = LoadControl("~/UserControls/productkiddetails.ascx");                MiddleCenterContent.Controls.Add(ctlUserDetails);            }

            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("Subcategory"))            {                Control ctlApproverDashboard = LoadControl("~/UserControls/subcategory.ascx");                MiddleCenterContent.Controls.Add(ctlApproverDashboard);            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("productEnquiry"))            {                Control ctlApproverDashboard = LoadControl("~/UserControls/productEnquiry.ascx");                MiddleCenterContent.Controls.Add(ctlApproverDashboard);            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("CallBack"))            {                Control ctlApproverDashboard = LoadControl("~/UserControls/RequestedCallBack.ascx");                MiddleCenterContent.Controls.Add(ctlApproverDashboard);            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("Brandpage"))            {                Control ctlApproverDashboard = LoadControl("~/UserControls/Brandpage.ascx");                MiddleCenterContent.Controls.Add(ctlApproverDashboard);            }
            
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("productskid"))            {                Control ctlUserDetails = LoadControl("~/UserControls/ProductsKid.ascx");                MiddleCenterContent.Controls.Add(ctlUserDetails);            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("productsgreenery"))            {                Control ctlUserDetails = LoadControl("~/UserControls/ProductsGreenery.ascx");                MiddleCenterContent.Controls.Add(ctlUserDetails);            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("productslivingroom"))            {                Control ctlUserDetails = LoadControl("~/UserControls/ProductsLivingroom.ascx");                MiddleCenterContent.Controls.Add(ctlUserDetails);            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("Logout"))            {
                Session.Abandon();
                Session.Clear(); Session.RemoveAll();                Response.Redirect("Login.aspx");            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("Blog"))            {
                Control ctlUserDetails = LoadControl("~/UserControls/Blog.ascx");                MiddleCenterContent.Controls.Add(ctlUserDetails);            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("productslivingroomdetails"))            {
                Control ctlUserDetails = LoadControl("~/UserControls/productslivingroomdetails.ascx");                MiddleCenterContent.Controls.Add(ctlUserDetails);            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("productgreenerydetails"))            {
                Control ctlUserDetails = LoadControl("~/UserControls/productgreenerydetails.ascx");                MiddleCenterContent.Controls.Add(ctlUserDetails);            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("Brands"))            {
                Control ctlUserDetails = LoadControl("~/UserControls/brands.ascx");                MiddleCenterContent.Controls.Add(ctlUserDetails);            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("productsAccessory"))            {
                Control ctlUserDetails = LoadControl("~/UserControls/productsAccessory.ascx");                MiddleCenterContent.Controls.Add(ctlUserDetails);            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("Subscription"))
            {
                Control ctlUserDetails = LoadControl("~/UserControls/Subscription.ascx");
                MiddleCenterContent.Controls.Add(ctlUserDetails);
            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("productsoffer"))
            {
                Control ctlUserDetails = LoadControl("~/UserControls/productsoffer.ascx");
                MiddleCenterContent.Controls.Add(ctlUserDetails);
            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("HomeBanner"))            {
                Control ctlUserDetails = LoadControl("~/UserControls/HomeBanner.ascx");                MiddleCenterContent.Controls.Add(ctlUserDetails);            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("HomePageProductConcept"))            {
                Control ctlUserDetails = LoadControl("~/UserControls/HomePageProductConcept.ascx");                MiddleCenterContent.Controls.Add(ctlUserDetails);            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("UserProfile"))            {
                Control ctlUserDetails = LoadControl("~/UserControls/UserProfile.ascx");                MiddleCenterContent.Controls.Add(ctlUserDetails);            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("AboutUs"))            {
                Control ctlUserDetails = LoadControl("~/UserControls/AboutUs.ascx");                MiddleCenterContent.Controls.Add(ctlUserDetails);            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("ProductsMapping"))            {
                Control ctlUserDetails = LoadControl("~/UserControls/ProductsMapping.ascx");                MiddleCenterContent.Controls.Add(ctlUserDetails);            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("mapproducts"))            {
                Control ctlUserDetails = LoadControl("~/UserControls/ProductsMappingLevel.ascx");                MiddleCenterContent.Controls.Add(ctlUserDetails);            }
            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("LabelControls"))            {
                Control ctlUserDetails = LoadControl("~/UserControls/LabelControls.ascx");                MiddleCenterContent.Controls.Add(ctlUserDetails);            }
        }
    }
}