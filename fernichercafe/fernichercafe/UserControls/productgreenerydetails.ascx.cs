﻿using fernicherLibrary.BizData;
using fernicherLibrary.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fernichercafe.UserControls
{
    public partial class productgreenerydetails : System.Web.UI.UserControl
    {
        protected string Values;

        protected void Page_Load(object sender, EventArgs e)
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {
                Response.Redirect("Login.aspx");
            }
            string id = Request.QueryString["productgreeneryid"];
            string sometin = Request.QueryString["name"];
            productname.Value = sometin;
            productname.Disabled = true;
            GetAllProdcutDetails(Convert.ToInt32(id));
        }

        private void GetAllProdcutDetails(int id)
        {
            BizDataProducts sa = new BizDataProducts();
            DataSet sss = sa.GetAllProductDetails("Greenery", id);
            if (sss.Tables[0].Rows.Count > 0)
            {
                //targetgroup.Value = sss.Tables[0].Rows[0]["targetgroup"].ToString();
                //theme.Value = sss.Tables[0].Rows[0]["theme"].ToString();
                //segment.Value = sss.Tables[0].Rows[0]["segment"].ToString();
                //agegroup.Value = sss.Tables[0].Rows[0]["agegroup"].ToString();
                //certificates.Value = sss.Tables[0].Rows[0]["certificates"].ToString();
                //feature.Value = sss.Tables[0].Rows[0]["features"].ToString();

                //manufacture.Value = sss.Tables[0].Rows[0]["manufacture"].ToString();
                //matirials.Value = sss.Tables[0].Rows[0]["materials"].ToString();

                //chkavail.Checked = sss.Tables[0].Rows[0]["availability"].ToString() == "True" ? true : false;
                txtarea.Value= sss.Tables[0].Rows[0]["area"].ToString();
                txtsize.Value= sss.Tables[0].Rows[0]["size"].ToString();
                txtmodel.Value= sss.Tables[0].Rows[0]["modelname"].ToString();
            }
            if (sss.Tables[1].Rows.Count > 0)
            {
                int count = sss.Tables[1].Rows.Count; var html = "";
                for (int i = 1; i <= count; i++)
                {
                    html += "<table  class='mytable2' id='mytable' runat='server'><tbody><tr><td><div class=row><div class=input-field col s12 style=margin-bottom:0rem><input id=technicaldetails" + i + " name=technicaldetails value='" + sss.Tables[1].Rows[i - 1]["name"].ToString() + "' type=text data-length=10><label for=technicaldetails>Technical details</label></div></div></td></tr></tbody></table>";
                }
                hiddendynamictextboxCount.Value = count.ToString();
                dynamictextbox.Controls.Add(new LiteralControl(html));

            }
            if (sss.Tables[2].Rows.Count > 0)
            {
                int count = sss.Tables[2].Rows.Count;
                DataTable dt = new DataTable();
                dt.Columns.Add("ID", typeof(string));
                dt.Columns.Add("Image", typeof(string));
                for (int i = 1; i <= count; i++)
                {
                    string yourString = sss.Tables[2].Rows[i - 1]["imagename"].ToString();
                    string imageid = sss.Tables[2].Rows[i - 1]["productgreeneryimagesid"].ToString();
                    String result = yourString.Replace("~", "");
                    if (result != "")
                    {
                        string result1 = result.Replace("\\", "/");
                        string result2 = result1.Remove(0, 1);
                        result2 = result2.Replace(" ", "%20");
                        string table = "<img src=" + result2 + "  name='multipleimageupload' class='listimg'>";
                        //html += "<div class='col m3 col 3 cols 12 localdiv'> <img src=" + result2 + "  name='multipleimageupload' class='img-thumbnail'></div>";
                        dt.Rows.Add(imageid, table);
                    }
                }

                datagridview.DataSource = dt;
                datagridview.DataBind();

                //photosection.InnerHtml = html;
            }
            else
            {
                photosection.InnerHtml = "";
            }
        }

        protected void datagridview_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                BizDataProducts biz = new BizDataProducts();
                var row = datagridview.Rows[e.RowIndex];
                long imageid = Int64.Parse(datagridview.DataKeys[e.RowIndex].Values["ID"].ToString());
                string imagepath = biz.getimagepath(imageid,"Greenery");

                File.Delete(Server.MapPath(imagepath));
                biz.deleteimagepath(imageid, "Greenery");
                Response.Redirect(Request.RawUrl);
            }
        }

        protected void submit_Click(object sender, EventArgs e)
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                BizDataProducts biz = new BizDataProducts();
                ProductEntities pe = new ProductEntities();
                pe.photourlarray = new List<string>();
                pe.photonamearray = new List<string>();
                pe.Technicaldetails = new List<string>();
                pe.productgreeneryid = Convert.ToInt32(Request.QueryString["productgreeneryid"].ToString());
                string fileName = "";
                pe.area = txtarea.Value;
                pe.modelname = txtmodel.Value;
                pe.size = txtsize.Value;
                //pe.targetgroup = targetgroup.Value;
                //pe.segment = segment.Value;
                //pe.agegroup = agegroup.Value;
                //pe.theme = theme.Value;
                //pe.certificates = certificates.Value;
                //pe.manufacture = manufacture.Value;
                //pe.materials = matirials.Value;
                //pe.availability = chkavail.Checked == true ? true : false;
                //pe.features = feature.Value;
                if (photosection.InnerText != "")
                {

                }
                if (photo.HasFiles)
                {
                    for (int i = 0; i < photo.PostedFiles.Count; i++)
                    {
                        pe.photo = photo.PostedFiles[i];

                        string FileName = Path.GetFileNameWithoutExtension(photo.FileName);
                        string FileExtension = Path.GetExtension(photo.FileName);
                        var guid = Guid.NewGuid().ToString();
                        FileName = DateTime.Now.ToString("yyyyMMdd") + "-" + guid + "-" + FileName.Trim() + FileExtension;


                        string UploadPath = ConfigurationManager.AppSettings["UserImagePath"].ToString();


                        fileName = UploadPath + FileName;



                        pe.photo.SaveAs(Server.MapPath(fileName));
                        pe.photourlarray.Add(fileName);
                        pe.photonamearray.Add(FileName);

                    }
                }
                string[] textboxValues = Request.Form.GetValues("technicaldetails");
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                this.Values = serializer.Serialize(textboxValues);
                if (textboxValues != null)
                {
                    foreach (string textboxValue in textboxValues)
                    {
                        if (textboxValue != "")
                        {
                            pe.Technicaldetails.Add(textboxValue.Trim());
                        }
                    }
                }
                biz.InsertGreeneryProductdetails(pe);
                Response.Redirect("Data.aspx?Action=productsgreenery");
            }
        }
    }
}