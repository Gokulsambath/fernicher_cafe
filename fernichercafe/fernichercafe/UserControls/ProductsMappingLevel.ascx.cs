﻿using fernicherLibrary.BizData;
using fernicherLibrary.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fernichercafe.UserControls
{
    public partial class ProductsMappingLevel : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sessionUserId = Session["UserID"] as string;
            if (string.IsNullOrEmpty(sessionUserId))
            {

                Response.Redirect("Login.aspx");
            }
            if (!IsPostBack)
            {
                btnsave.Text = "Add";
                myModalLabel.InnerText = "Add Map";
                BizDataCategory bdl = new BizDataCategory();
                DataSet dsrole = bdl.GetAllCategory();

                FillDropDowns(ddlcat, dsrole.Tables[0], "Choose your option");
                BuildHTMLTable();
                string sometin = Request.QueryString["id"];
                if (!(string.IsNullOrEmpty(sometin)))
                {

                    EditMappingDetails();
                }

            }
        }


        private void EditMappingDetails()
        {
            BizDataProducts user = new BizDataProducts();
            string sometin = Request.QueryString["id"];
            int iddd = Convert.ToInt32(sometin);

            if (sometin != null)
            {

                DataSet dsCategoryid = user.EditMapping(iddd);
                if (dsCategoryid.Tables[0].Rows.Count > 0)
                {
                    string ss = "";
                    hiddenid.Value = dsCategoryid.Tables[0].Rows[0]["mapid"].ToString();
                    BizDataCategory bdl = new BizDataCategory();
                    DataSet dsrole = bdl.GetAllCategory();
                    fillddlcat(dsrole.Tables[0]);

                    string value = dsCategoryid.Tables[0].Rows[0]["flgActive"].ToString();
                    
                    ddlcat.Value = dsCategoryid.Tables[0].Rows[0]["categoryid"].ToString();                    
                    fillsubctegory(Convert.ToInt32(ddlcat.Value));
                    ddlSubCategory.Value = dsCategoryid.Tables[0].Rows[0]["subcategoryid"].ToString();
                    hidsub.Value = dsCategoryid.Tables[0].Rows[0]["subcategoryid"].ToString();
                    string catname = dsCategoryid.Tables[0].Rows[0]["CategoryName"].ToString();
                    BizDataProducts userd = new BizDataProducts();
                    btnsave.Text = "Update";
                    myModalLabel.InnerText = "Edit Map";
                    hiddenid.Value = iddd.ToString();

                    ss = "<script language=JavaScript>$(document).ready(function(){ setTimeout(function () {   $('#add_maplevel').modal('open');}, 200);});</script>";

                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CreatePopupOpen", ss);


                }
                btnsave.Text = "Update";
                myModalLabel.InnerText = "Edit Map";

            }
        }

        private void fillsubctegory(int v)
        {
            
            BizDataProducts user = new BizDataProducts();
            ddlSubCategory.Items.Clear();

            DataSet ds = user.fillsubcategorybycategoryid(v.ToString());
            populateproductdropdownsedit(ds.Tables[0]);
            
        }

        private void populateproductdropdownsedit(DataTable dataTable)
        {
            ddlSubCategory.Items.Clear();
            ListItem lstItem = new ListItem();
            lstItem.Value = "0";
            lstItem.Text = "Choose your option";
            //ddlSubCategory1.Items.Add(lstItem);
            for (int intCount1 = 0; intCount1 < dataTable.Rows.Count; intCount1++)
            {
                lstItem = new ListItem();
                lstItem.Value = dataTable.Rows[intCount1][0].ToString().Trim();
                lstItem.Text = dataTable.Rows[intCount1][1].ToString().Trim();
                ddlSubCategory.Items.Add(lstItem);
            }
        }

        private void fillddlcat(DataTable dataTable)
        {
            // throw new NotImplementedException();
            ddlcat.Items.Clear();
            ListItem lstItem = new ListItem();
            lstItem.Value = "0";
            lstItem.Text = "Choose your option";
            //ddlCategory1.Items.Add(lstItem);
            for (int intCount1 = 0; intCount1 < dataTable.Rows.Count; intCount1++)
            {
                lstItem = new ListItem();
                lstItem.Value = dataTable.Rows[intCount1][0].ToString().Trim();
                lstItem.Text = dataTable.Rows[intCount1][1].ToString().Trim();
                ddlcat.Items.Add(lstItem);
            }
        }

        private void BuildHTMLTable()
        {
            pldMappedTable.Controls.Clear();
            BizDataProducts objBizDataUser = new BizDataProducts();
            DataSet dsUser = objBizDataUser.GetAllMappedItems();


            StringBuilder strAccount = new StringBuilder();
            if (dsUser.Tables[0].Rows.Count > 0)
            {
                for (int intCount = 0; intCount < dsUser.Tables[0].Rows.Count; intCount++)
                {                   
                    strAccount.Append("<tr>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["displayname"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["SubCategoryName"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["productname"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["mapname"].ToString());
                    strAccount.Append("</td>");

                    strAccount.Append("<td class='center-align'>");
                    
                    strAccount.Append("<a href='#' onclick=deletemaplevel("+ dsUser.Tables[0].Rows[intCount]["productid"].ToString() + ","+ dsUser.Tables[0].Rows[intCount]["SubCategoryId"].ToString() + ","+ dsUser.Tables[0].Rows[intCount]["CategoryId"].ToString() + ") style='padding-left:10px;padding-right: 10px'><i class='fa fa-trash'></i></a>");
                    strAccount.Append("</td>");


                    strAccount.Append("</tr>");

                }

                pldMappedTable.Controls.Add(new Literal { Text = strAccount.ToString() });
            }
        }

        private void FillDropDowns(System.Web.UI.HtmlControls.HtmlSelect ddlcat, DataTable dataTable, string v)
        {
            ddlcat.Items.Clear();
            ListItem lstItem = new ListItem();
            lstItem.Value = "0";
            lstItem.Text = v;
            ddlcat.Items.Add(lstItem);
            for (int intCount1 = 0; intCount1 < dataTable.Rows.Count; intCount1++)
            {
                lstItem = new ListItem();
                lstItem.Value = dataTable.Rows[intCount1][0].ToString().Trim();
                lstItem.Attributes.Add("id",dataTable.Rows[intCount1][2].ToString().Trim());
                lstItem.Text = dataTable.Rows[intCount1][1].ToString().Trim();
                ddlcat.Items.Add(lstItem);
            }

        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                if (btnsave.Text == "Update")
                {
                    UpdateMapping();
                }
                else
                {
                    if (hidmap.Value != "0" || hidmap.Value != "" || hidproduct.Value != "0" || hidproduct.Value !="" || hidsub.Value != "0" || hidsub.Value != "")
                    {
                        MappingEntity ent = new MappingEntity();
                        ent.categoryid = Convert.ToInt32(ddlcat.Value);
                        ent.subcategoryid = Convert.ToInt32(hidsub.Value);
                        ent.productid = Convert.ToInt32(hidproduct.Value);
                        ent.mapid = Convert.ToInt32(hidmap.Value);
                        //ent.flgActive = active.Checked == true ? "1" : "0";
                        BizDataProducts obj = new BizDataProducts();
                        obj.AddMappingLevel(ent);

                    }
                    Response.Redirect("Data.aspx?Action=mapproducts");
                }
            }
        }

        private void UpdateMapping()
        {

            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                if (hidsub.Value != "0" || hidsub.Value != "")
                {
                    MappingEntity ent = new MappingEntity();

                    ent.categoryid = Convert.ToInt32(ddlcat.Value);
                    ent.subcategoryid = Convert.ToInt32(hidsub.Value);
                    ent.productid = Convert.ToInt32(hidproduct.Value);
                    ent.mapid = Convert.ToInt32(hidmap.Value);                    
                    
                    BizDataProducts obj = new BizDataProducts();
                    obj.UpdateMappingLevel(ent);

                }
                Response.Redirect("Data.aspx?Action=mapproducts");
            }
        }
    }
}