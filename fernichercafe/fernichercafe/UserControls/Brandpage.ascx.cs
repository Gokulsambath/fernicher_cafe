﻿using DairymanLibrary.Entity;
using fernicherLibrary.BizData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fernichercafe.UserControls
{
    public partial class Brandpage : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {

                Response.Redirect("Login.aspx");
            }
            if (!IsPostBack)
            {

                FillProfileValues();
            }

        }


        private void FillProfileValues()
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                BizDataUser obj = new BizDataUser();
                DataTable dt = obj.Getbrandpage();
                if (dt.Rows.Count > 0)
                {
                    txtbrandheader.Value  = dt.Rows[0]["brandheader"].ToString();
                   txtbrandsubheader.Value = dt.Rows[0]["brandsubheader"].ToString();

                }

            }
        }


        protected void btnsave_Click(object sender, EventArgs e)
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {
                Response.Redirect("Login.aspx");
            }
            else
            {

                UserEntities sourcelead = new UserEntities();
                sourcelead.brandpageheader = txtbrandheader.Value;
                sourcelead.brandpagesubheader = txtbrandsubheader.Value;
                sourcelead.userid = Convert.ToInt32(sessionUserId);
                BizDataUser obj = new BizDataUser();
                obj.InsertBrandpage(sourcelead);
                Response.Redirect("Data.aspx?Action=Brandpage");

                Response.Redirect(Request.Url.AbsoluteUri);


            }
        }
    }
}