﻿using DairymanLibrary.Entity;
using fernicherLibrary.BizData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fernichercafe.UserControls
{
    public partial class UserProfile : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {

                Response.Redirect("Login.aspx");
            }
            if (!IsPostBack)
            {

                FillProfileValues();
            }
            
        }

        private void FillProfileValues()
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                BizDataUser obj = new BizDataUser();
                DataTable dt = obj.GetCompanyInfo();
                if (dt.Rows.Count > 0)
                {
                    txtusername.Value = dt.Rows[0]["Name"].ToString();
                    txtemail.Value = dt.Rows[0]["email"].ToString();
                    txtmobile.Value = dt.Rows[0]["mobileNo"].ToString();
                    txtworkhrs.Value = dt.Rows[0]["WorkingHours"].ToString();
                    txtaddress.Value = dt.Rows[0]["address"].ToString();
                    txtlocation.Value = dt.Rows[0]["MapLocation"].ToString();
                    txtintro.Value = dt.Rows[0]["IntroductoryLine"].ToString();
                    
                }
               
            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                
                UserEntities sourcelead = new UserEntities();
                sourcelead.firstName = txtusername.Value;
                sourcelead.Email = txtemail.Value;
                sourcelead.mobileNo = txtmobile.Value;
                sourcelead.workinghours = txtworkhrs.Value;
                sourcelead.address = txtaddress.Value;
                sourcelead.LocationEmbed = txtlocation.Value;
                sourcelead.Intro = txtintro.Value;
                sourcelead.userid = Convert.ToInt32(sessionUserId);
                BizDataUser obj = new BizDataUser();
                obj.InsertAboutCompany(sourcelead);
                Response.Redirect("Data.aspx?Action=UserProfile");

                Response.Redirect(Request.Url.AbsoluteUri);


            }
        }
    }
}