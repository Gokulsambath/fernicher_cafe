﻿using fernicherLibrary.BizData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fernichercafe.UserControls
{
    public partial class About_Us : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
                BuildHtmlPage();
            
        }

        private void BuildHtmlPage()
        {
            BizDataUser obj = new BizDataUser();
            DataTable dt = obj.GetAboutUs();
            if (dt.Rows.Count > 0)
            {
                string html = "", html2 = "";
                string yourString = dt.Rows[0]["Bannerphoto"].ToString();
                String result = yourString.Replace("~", "");
                if (result != "")
                {
                    html = result.Replace("\\", "/");
                    html2 = html.Remove(0, 1);
                }
                aboutusheaderbanner.Attributes.Add("style", "background-image: url('"+ html2 + "');");
                bannerheader.InnerText = dt.Rows[0]["BannerHeader"].ToString();
                bannersubheader.InnerText = dt.Rows[0]["BannerSubHeader"].ToString(); 
                historyheader.InnerText = dt.Rows[0]["HistoryHeader"].ToString();
                historysubheader.InnerText = dt.Rows[0]["HsistorySubHeader"].ToString();
                string html3 = "", html4 = "";
                string yourString1 = dt.Rows[0]["Title1photo"].ToString();
                String result1 = yourString1.Replace("~", "");
                if (result1 != "")
                {
                    html3 = result1.Replace("\\", "/");
                    html4 = html3.Remove(0, 1);
                }
                title1photo.Attributes.Add("style", "background-image: url('" + html4 + "');");
                title1.InnerText = dt.Rows[0]["Title1"].ToString();
                title1desc.InnerText = dt.Rows[0]["Title1Description"].ToString();
                string html5 = "", html6 = "";
                string yourString2 = dt.Rows[0]["Title2photo"].ToString();
                String result2 = yourString2.Replace("~", "");
                if (result2 != "")
                {
                    html5 = result2.Replace("\\", "/");
                    html6 = html5.Remove(0, 1);
                }
                title2photo.Attributes.Add("style", "background-image: url('" + html6 + "');");
                title2.InnerText = dt.Rows[0]["Title2"].ToString();
                title2desc.InnerText = dt.Rows[0]["Title2Description"].ToString();
                directorheader.InnerText = dt.Rows[0]["DirectorHeader"].ToString();
                directordesc.InnerText = dt.Rows[0]["DirectorHeaderDescription"].ToString();
                string hhtml1 = "", hhtml2 = "";
                string yourString3 = dt.Rows[0]["Director1photo"].ToString();
                String resuls1 = yourString3.Replace("~", "");
                if (resuls1 != "")
                {
                    hhtml1 = resuls1.Replace("\\", "/");
                    hhtml2 = hhtml1.Remove(0, 1);
                }                
                director1.InnerText = dt.Rows[0]["Director1"].ToString();
                director1desc.InnerText = dt.Rows[0]["Director1Description"].ToString();
                director2.InnerText = dt.Rows[0]["Director2"].ToString();
                director2desc.InnerText = dt.Rows[0]["Director2Description"].ToString();
                string hhh = "", hhh1 = "";
                string yous1 = dt.Rows[0]["Director2photo"].ToString();
                String hhhs = yous1.Replace("~", "");
                if (hhhs != "")
                {
                    hhh = hhhs.Replace("\\", "/");
                    hhh1 = hhh.Remove(0, 1);
                }               
                
                missionheader.InnerText = dt.Rows[0]["MissionHeader"].ToString();
                missiondesc.InnerText = dt.Rows[0]["MissionDescription"].ToString();
                string ssss = "", ddd = "";
                string asd = dt.Rows[0]["Missionphoto"].ToString();
                String dsa = asd.Replace("~", "");
                if (dsa != "")
                {
                    ssss = dsa.Replace("\\", "/");
                    ddd = ssss.Remove(0, 1);
                }
                director2photo.InnerHtml = "<img  alt='' src=" + hhh1 + " width='100%' class='im' />";
                director1photo.InnerHtml = "<img  alt='' src=" + hhtml2 + " width='100%' class='im' />";
                missionbannerphoto.Attributes.Add("style", "background-image: url('" + ddd + "');");

            }
        }
    }
}