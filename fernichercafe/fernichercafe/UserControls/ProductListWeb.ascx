﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductListWeb.ascx.cs" Inherits="fernichercafeWebsite.UserControls.ProductList" %>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twbs-pagination/1.4.2/jquery.twbsPagination.min.js"></script>


<link media="screen" rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>

<style>
    .page {
        display: none;
    }

    .page-active {
        display: block;
    }
    @media only screen and (min-width: 320px) and (max-width:640px) {
        .btn.btn-buy {
            color: white;
            background-color:#ED7745;
            position: absolute;
            padding: 0;
            right: 3px;
            top: 3px;
            width: 30px;
            height: 30px;
            font-size: 16px;
            line-height: 28px;
            text-align: center;
            overflow: hidden;
            -moz-border-radius: 100%;
            -webkit-border-radius: 100%;
            border-radius: 100%;
            z-index: 20;
        }
        .btn.btn-buy:before{
            font-size:20px;
            line-height: 28px;
        }
        .btn.btn-buy:after{
            font-size:20px;
            line-height: 28px;
        }
    }

</style>
<style>
      @media only screen and (min-width: 320px) and (max-width:640px) {
        .main-header {
            padding-top: 25px;
            
            height: 37vh;
        }

        header {
            margin-top: 160px;
        }
    }

    @media only screen and (min-width: 768px) and (max-width: 991px) {
        .main-header {
            padding-top: 130px;
            height: 50vh;
        }

        header {
            margin-top: 300px;
        }
    }


    @media only screen and (min-width: 992px) and (max-width: 1199px) {
        .main-header {
            padding-top: 100px;
            height: 70vh;
        }

        header {
            margin-top: 270px;
        }
    }

    @media only screen and (min-width: 1200px) and (max-width: 1400px) {
        .main-header {
            padding-top: 100px;
            margin-top: 0px;
            height: 70vh;
        }

        header {
            margin-top: 280px;
        }
    }
</style>

<section class="main-header" id="headerbanner" runat="server">
    <header>
        <div class="container">
            <h1 class="h2 title" id="titleh1" runat="server"></h1>
            <ol class="breadcrumb breadcrumb-inverted">
                <li><a href="DataWeb.aspx?Action=IndexWeb"><span class="icon icon-home"></span></a></li>

                <li><a id="navtitle1" runat="server"></a></li>
                <li><a class="active" id="navtitle2" runat="server"></a></li>
            </ol>
        </div>
    </header>
</section>

<!-- ========================  Icons slider ======================== -->

<section class="owl-icons-wrapper">

    <!-- === header === -->

    <header class="hidden">
        <h2>Product categories</h2>
    </header>

    <div class="container">

    
    </div>
    <!--/container-->
</section>

<!-- ======================== Products ======================== -->

<section class="products">

    <div class="container">

        <header class="hidden">
            <h3 class="h3 title">Product category list</h3>
        </header>

        <div class="row">

            <!-- === product-filters === -->

          

            <!-- === product items === -->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <div class="sort-bar clearfix hidden">
                    <div class="sort-results pull-left">
                        <!--Showing result per page-->
                        <select>
                            <option value="1">10</option>
                            <option value="2">50</option>
                            <option value="3">100</option>
                            <option value="4">All</option>
                        </select>
                        <!--Items counter-->
                        <span>Showing all <strong>50</strong> of <strong>3,250</strong> items</span>
                    </div>
                    <div class="sort-options pull-right">
                        <span class="hidden-xs">Sort by</span>
                        <!--Sort options-->
                        <select>
                            <option value="1">Name</option>
                            <option value="2">Popular</option>
                            <option value="3">Price: lowest</option>
                            <option value="4">Price: highest</option>
                        </select>
                        <!--Grid-list view-->
                        <span class="grid-list">
                            <a href="products-grid.html"><i class="fa fa-th-large"></i></a>
                            <a href="products-list.html"><i class="fa fa-align-justify"></i></a>
                            <a href="javascript:void(0);" class="toggle-filters-mobile"><i class="fa fa-search"></i></a>
                        </span>
                    </div>
                </div>

                <div class="row">

                    <asp:PlaceHolder ID="phldProductlist" runat="server"></asp:PlaceHolder>
                    <!-- === product-item === -->






                </div>
                <!--/row-->
                <!--Pagination-->
                <div class="pagination-wrapper">
                    <ul id="pagination-demo" class="pagination-lg"></ul>
                </div>

                <input type="hidden" runat="server" id="hiddenpaginationcount" />

            </div>
            <!--/product items-->

        </div>
        <!--/row-->

    </div>
    <!--/container-->
</section>
<script>
     var $content = $('.content');
    var $childs = $content.children();
    var items_per_page = 5;
    function hide_all() {
        $childs.removeClass('active');   
    }
    function show_page(num, items_per_page) {
        hide_all();
        $childs.slice((num-1)*items_per_page, num*items_per_page).addClass('active');
    }
    $('#pagination-demo').twbsPagination({
        totalPages: $('#MiddleCenterContent_ctl00_hiddenpaginationcount').val(),
        // the current page that show on start
        startPage: 1,

        // maximum visible pages
        visiblePages: 1,

        initiateStartPageClick: true,

        // template for pagination links
        href: false,

        // variable name in href template for page number
        hrefVariable: '{{number}}',

        // Text labels
        //first: 'First',
        prev: 'Previous',
        next: 'Next',
        //last: 'Last',

        // carousel-style pagination
        loop: false,

        // callback function
        onPageClick: function (event, page) {
            $('.page-active').removeClass('page-active');
            $('.page' + page).addClass('page-active');
        },

        // pagination Classes
        paginationClass: 'pagination',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'hide',
        firstClass: 'hide',
        pageClass: 'page',
        activeClass: 'active',
        disabledClass: 'disabled'

    });

    var jsonStr = '{"ProductList":[]}';
    var storage = localStorage.getItem('Enquirystorage');
    var obj = JSON.parse(storage);
    if (obj == null) {
        localStorage.setItem('Enquirystorage', jsonStr);
    }
    function getenquiryproducts(elem, id, sub) {

        var jsonStr = localStorage.getItem('Enquirystorage');
        if (jsonStr == "") {
            var jsonStr1 = '{"ProductList":[]}';
            localStorage.setItem('Enquirystorage', jsonStr1);
            jsonStr = localStorage.getItem('Enquirystorage');
        }
        var obj = JSON.parse(jsonStr);

        var hasMatch = false;
        for (var index = 0; index < obj.ProductList.length; ++index) {

            var animal = obj.ProductList[index];

            if (animal.subcategoryid == sub && animal.productid == id) {
                hasMatch = true;
                break;
            }
        }
        if (hasMatch) {
            var aTags = document.getElementsByTagName("a");
            var searchText = "Contact Us";
            var found;

            for (var i = 0; i < aTags.length; i++) {
                if (aTags[i].textContent == searchText) {
                    found = aTags[i];                    
                    window.location = found;
                    break;
                }
            }
        } else {
            obj['ProductList'].push({ "subcategoryid": sub, "productid": id });
        }
        jsonStr = JSON.stringify(obj);

        localStorage.setItem('Enquirystorage', jsonStr);
        showpop("Product Added to Wish List","Success");
    }
    function showpop(msg, title) {
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "200",
            "hideDuration": "1000",
            "timeOut": "7000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        // toastr['success'](msg, title);
        
        toastr.success(msg);
        return false;
    }
    
</script>
