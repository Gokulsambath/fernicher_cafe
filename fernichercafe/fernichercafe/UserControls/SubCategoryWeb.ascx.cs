﻿using fernicherLibrary.BizData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fernichercafeWebsite.UserControls
{
    public partial class SubCategory : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BuildHTMLSubCategory();
        }

        private void BuildHTMLSubCategory()
        {            
           string  sometin = Request.QueryString["id"];
            BizDataCategory biz = new BizDataCategory();
            DataTable dt = biz.GetAllSubCategoryById(sometin);
            if (dt.Rows.Count > 0)
            {
                string html = "";
                titleh1.InnerText = dt.Rows[0]["displayname"].ToString();
                navcategory.InnerText = dt.Rows[0]["displayname"].ToString();
                if (dt.Rows[0]["CategoryName"].ToString().ToLower() == "kid")
                {
                    mainheader.Attributes.Add("style", "background-image: url('ContentWebsite/assets/banner-3.jpg');");
                }
                else if (dt.Rows[0]["CategoryName"].ToString().ToLower() == "livingroom")
                {
                    mainheader.Attributes.Add("style", "background-image: url('ContentWebsite/assets/banner-4.jpg');");
                }
                else if (dt.Rows[0]["CategoryName"].ToString().ToLower() == "greenery")
                {
                    mainheader.Attributes.Add("style", "background-image: url('ContentWebsite/assets/banner-5.jpg');");
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {                    
                    string yourString = dt.Rows[i]["photourl"].ToString();
                    String result = yourString.Replace("~", "");
                    if (result != "")
                    {
                        string result1 = result.Replace("\\", "/");
                        string result2 = result1.Remove(0, 1);
                        result2 = result2.Replace(" ", "%20");
                        if (dt.Rows[0]["CategoryName"].ToString().ToLower() == "livingroom")
                        {
                            html += "<div class='col-xs-12 col-sm-6 col-md-4 col-lg-4'><article class='card'><div class='info'><span class='add-favorite added'></span><span></span></div><a href='DataWeb.aspx?Action=ProductListWeb&category=" + dt.Rows[i]["CategoryName"].ToString() + "&id=" + dt.Rows[i]["subcategoryid"].ToString() + "&maplevel=null'><div class='btn btn-add'><i class='icon icon-eye'></i></div></a><div class='figure-grid outer-div'><div class='image inner-div'><a href='DataWeb.aspx?Action=ProductListWeb&category=" + dt.Rows[i]["CategoryName"].ToString() + "&id=" + dt.Rows[i]["subcategoryid"].ToString() + "&maplevel=null'><img src=" + result2 + " alt='' width='360' height='241' class='img-hover-zoom--colorize' /></a></div><div class='text'><h2 class='title h4'><a href='DataWeb.aspx?Action=ProductListWeb&category=" + dt.Rows[i]["CategoryName"].ToString() + "&id=" + dt.Rows[i]["subcategoryid"].ToString() + "&maplevel=null' >" + dt.Rows[i]["subcategoryname"].ToString() + "</a></h2><span class='description clearfix'>Gubergren amet dolor ea diam takimata consetetur facilisis blandit et aliquyam lorem ea duo labore diam sit et consetetur nulla</span></div></div></article></div>";
                        }
                        else {
                            html += "<div class='col-xs-12 col-sm-6 col-md-4 col-lg-4'><article class='card'><div class='info'><span class='add-favorite added'></span><span></span></div><a href='DataWeb.aspx?Action=ProductListWeb&category=" + dt.Rows[i]["CategoryName"].ToString() + "&id=" + dt.Rows[i]["subcategoryid"].ToString() + "&maplevel=other'><div class='btn btn-add'><i class='icon icon-eye'></i></div></a><div class='figure-grid outer-div'><div class='image inner-div'><a href='DataWeb.aspx?Action=ProductListWeb&category=" + dt.Rows[i]["CategoryName"].ToString() + "&id=" + dt.Rows[i]["subcategoryid"].ToString() + "&maplevel=other'><img src=" + result2 + " alt='' width='360' height='241' class='img-hover-zoom--colorize' /></a></div><div class='text'><h2 class='title h4'><a href='DataWeb.aspx?Action=ProductListWeb&category=" + dt.Rows[i]["CategoryName"].ToString() + "&id=" + dt.Rows[i]["subcategoryid"].ToString() + "&maplevel=other' >" + dt.Rows[i]["subcategoryname"].ToString() + "</a></h2><span class='description clearfix'>Gubergren amet dolor ea diam takimata consetetur facilisis blandit et aliquyam lorem ea duo labore diam sit et consetetur nulla</span></div></div></article></div>";
                        }
                       // html += "<div class='col-xs-12 col-sm-6 col-md-4 col-lg-4'><article class='card'><div class='info'><span class='add-favorite added'></span><span></span></div><a href='DataWeb.aspx?Action=ProductListWeb&category="+ dt.Rows[i]["CategoryName"].ToString()  + "&id=" + dt.Rows[i]["subcategoryid"].ToString() + "&maplevel=null'><div class='btn btn-add'><i class='icon icon-eye'></i></div></a><div class='figure-grid outer-div'><div class='image inner-div'><a href='DataWeb.aspx?Action=ProductListWeb&category=" + dt.Rows[i]["CategoryName"].ToString() + "&id=" + dt.Rows[i]["subcategoryid"].ToString() + "&maplevel=null'><img src=" + result2 + " alt='' width='360' height='241' class='img-hover-zoom--colorize' /></a></div><div class='text'><h2 class='title h4'><a href='DataWeb.aspx?Action=ProductListWeb&category=" + dt.Rows[i]["CategoryName"].ToString() + "&id=" + dt.Rows[i]["subcategoryid"].ToString() + "&maplevel=null' >" + dt.Rows[i]["subcategoryname"].ToString() + "</a></h2><span class='description clearfix'>Gubergren amet dolor ea diam takimata consetetur facilisis blandit et aliquyam lorem ea duo labore diam sit et consetetur nulla</span></div></div></article></div>";
                    }
                }
                phldSubcategorylist.Controls.Add(new Literal { Text = html.ToString() });
            }
        }
    }
}