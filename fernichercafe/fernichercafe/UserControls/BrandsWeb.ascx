﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BrandsWeb.ascx.cs" Inherits="fernichercafe.UserControls.BrandsWeb" %>
<style type="text/css">
    .brand_partner .brand_outer_section .brand_inner_section img {
        width: 100%;
        height: auto;
    }

    .brand_partner .brand_outer_section {
        margin-top: 35px;
        /*border: 1px solid #f7f1f1;*/
        /*padding: 10px;*/
        border-radius: 2px;
        -webkit-box-shadow: -2px 10px 22px -16px rgba(0,0,0,.75);
        -moz-box-shadow: -2px 10px 22px -16px rgba(0,0,0,.75);
        /*box-shadow: -2px 10px 22px -16px rgba(0,0,0,.75);*/
        box-shadow: 0px 5px 15px rgba(0, 0, 0, 0.2);
        /*min-height: 189px;*/
    }

        .brand_partner .brand_outer_section .brand_inner_section {
            padding: 10px;
        }

    .brand_partner a:hover {
        /*box-shadow: 0px 5px 15px rgba(0, 0, 0, 0.2);*/
    }

    .brand_partner .brand_outer_section :hover {
        border: 2px solid #ED7745;
    }

    .brand_partner .brand_outer_section .brand_inner_section img:hover {
        border: none;
    }

    @media only screen and (min-width: 320px) and (max-width:640px) {
        .main-header {
            padding-top: 25px;
            height: 45vh;
            /*display:none;*/
        }

        .brand {
            margin-top: 50px;
        }
    }

    @media only screen and (min-width: 768px) {
        .main-header {
            padding-top: 182px;
            padding-bottom: 63px;
            height: 45vh;
        }

        .brand {
            margin-top: 40px;
        }
    }

    @media only screen and (min-width: 992px){
        .main-header.main-header-blog {
            padding-top: 100px;
            padding-bottom: 160px;
            height: 80vh;
        }
        .brand {
            margin-top: 20px;
        }
    }
</style>

<!-- ========================  Main header ======================== -->

<a href="DataWeb?Action=ManageDetailsWeb&category=Livingroom&id=2"><section class="main-header main-header-blog" style="background-image: url(ContentWebsite/assets/slider/slider-2.png)">
    <header>
        <div class="container text-center">
         
        </div>
    </header>

</section>
</a>




<section class="our-team" style="padding-bottom:30px">
    <div class="container">

        <!-- === History header === -->

        <header class="brand">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 text-center">
                    <h1 class="h2 title" id="txtbrandheader" runat="server"></h1>
                    <div class="text">
                        <p  id="txtbrandsubheader" runat="server"></p>
                    </div>
                </div>
            </div>
        </header>

        <!-- === row item === -->

        <div class="brand_partner">
            <div class="row">

                <asp:PlaceHolder ID="phldbrands" runat="server"></asp:PlaceHolder>

            </div>
            <!-- row -->
        </div>
        




    </div>
</section>

