﻿using fernicherLibrary.BizData;
using fernicherLibrary.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fernichercafe.UserControls
{
    public partial class productsoffer : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sessionUserId = Session["UserID"] as string;
            if (string.IsNullOrEmpty(sessionUserId))
            {

                Response.Redirect("Login.aspx");
            }
            if(!IsPostBack)
            {
               // BuildHTMLTable(0, 0);
                btnsave.Text = "Add";
                myModalLabel.InnerText = "Add Offer";
                BizDataCategory bdl = new BizDataCategory();
                DataSet dsrole = bdl.GetAllCategory();
                FillDropDown(ddlCategory1, dsrole.Tables[0], "Choose your option");
                FillDropDowns(ddlcat, dsrole.Tables[0], "Choose your option");
                fillsubctegory();
                BuildHTMLTable(Convert.ToInt32(ddlCategory1.SelectedItem.Value),Convert.ToInt32(ddlSubCategory1.SelectedItem.Value));
                string sometin = Request.QueryString["id"];
                if (!(string.IsNullOrEmpty(sometin)))
                {

                    EditAccessoryDetails();
                }

            }

        }
        private void EditAccessoryDetails()
        {
            BizDataProducts user = new BizDataProducts();
            string result1 = "", result2 = "", sometin = Request.QueryString["id"];
            int iddd = Convert.ToInt32(sometin);

            if (sometin != null)
            {

                DataSet dsCategoryid = user.EditOffers(iddd);
                if (dsCategoryid.Tables[0].Rows.Count > 0)
                {
                    string ss = "";
                    hiddenid.Value = dsCategoryid.Tables[0].Rows[0]["offerid"].ToString();
                    BizDataCategory bdl = new BizDataCategory();
                    DataSet dsrole = bdl.GetAllCategory();
                    fillddlcat(dsrole.Tables[0]);
                    ddlcat.Value= dsCategoryid.Tables[0].Rows[0]["categoryid"].ToString();
                    fillsubctegory(Convert.ToInt32(ddlcat.Value));
                    ddlSubCategory.Value= dsCategoryid.Tables[0].Rows[0]["subcategoryid"].ToString();
                    hidsub.Value= dsCategoryid.Tables[0].Rows[0]["subcategoryid"].ToString();
                    string catname= dsCategoryid.Tables[0].Rows[0]["CategoryName"].ToString();
                    BizDataProducts userd = new BizDataProducts();
                    string yourString = dsCategoryid.Tables[0].Rows[0]["photourl"].ToString();
                    String result = yourString.Replace("~", "");
                    if (result != "")
                    {
                        result1 = result.Replace("\\", "/");
                        result2 = result1.Remove(0, 1);
                    }
                    inputFile.Attributes.Add("value", dsCategoryid.Tables[0].Rows[0]["photourl"].ToString());
                    filepathedit.Value = dsCategoryid.Tables[0].Rows[0]["photourl"].ToString();
                    filenamelabel.InnerText = result2;
                    DataSet ds = userd.GetAllKidProducts(Convert.ToInt32(ddlSubCategory.Value), catname);
                    populateproductdropdownspro(ds.Tables[0]);
                    ddlproductname.Value= dsCategoryid.Tables[0].Rows[0]["productid"].ToString();
                    hidproduct.Value= dsCategoryid.Tables[0].Rows[0]["productid"].ToString();
                    //txtoffer.Value= dsCategoryid.Tables[0].Rows[0]["offername"].ToString();
                    btnsave.Text = "Update";
                    myModalLabel.InnerText = "Edit Offer";
                    hiddenid.Value = iddd.ToString();

                    ss = "<script language=JavaScript>$(document).ready(function(){ setTimeout(function () {   $('#add_offer').modal('open');}, 200);});</script>";

                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CreatePopupOpen", ss);


                }
                btnsave.Text = "Update";
                myModalLabel.InnerText = "Edit Offer";

            }
        }

        

        private void fillddlcat(DataTable dataTable)
        {
            // throw new NotImplementedException();
            ddlcat.Items.Clear();
            ListItem lstItem = new ListItem();
            lstItem.Value = "0";
            lstItem.Text ="Choose your option";
            //ddlCategory1.Items.Add(lstItem);
            for (int intCount1 = 0; intCount1 < dataTable.Rows.Count; intCount1++)
            {
                lstItem = new ListItem();
                lstItem.Value = dataTable.Rows[intCount1][0].ToString().Trim();
                lstItem.Text = dataTable.Rows[intCount1][1].ToString().Trim();
                lstItem.Attributes.Add("id", dataTable.Rows[intCount1][2].ToString().Trim());
                ddlcat.Items.Add(lstItem);
            }
        }

        private void BuildHTMLTable(int categoryid, int subcategoryid)
        {
            pldProductAccessoriestable.Controls.Clear();
            BizDataProducts objBizDataUser = new BizDataProducts();
            DataSet dsUser = objBizDataUser.GetAllProducts(categoryid, subcategoryid);


            StringBuilder strAccount = new StringBuilder();
            if (dsUser.Tables[0].Rows.Count > 0)
            {
                for (int intCount = 0; intCount < dsUser.Tables[0].Rows.Count; intCount++)
                {
                    string id = dsUser.Tables[0].Rows[intCount]["offerid"].ToString();
                    //string name = dsUser.Tables[0].Rows[intCount]["AccessoryName"].ToString();
                    strAccount.Append("<tr>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["name"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["offername"].ToString());
                    strAccount.Append("</td>");
                    
                    
                    strAccount.Append("<td><img class=listimg src='");
                    string yourString = dsUser.Tables[0].Rows[intCount]["photourl"].ToString();
                    String result = yourString.Replace("~", "");
                    if (result != "")
                    {
                        string result1 = result.Replace("\\", "/");
                        string result2 = result1.Remove(0, 1);
                        strAccount.Append(result2);
                    }
                    strAccount.Append("' /></td>");

                    strAccount.Append("<td class='center-align'>");
                    strAccount.Append("<a href='Data.aspx?Action=productsoffer&id=" + id + "' style='padding-left:10px;padding-right: 10px'><i class='fa fa-edit'></i></a>");
                    strAccount.Append("</td>");


                    strAccount.Append("</tr>");

                }

                pldProductAccessoriestable.Controls.Add(new Literal { Text = strAccount.ToString() });
            }
        }
        private void FillDropDown(DropDownList ddlCategory1, DataTable dataTable, string v)
        {
            ddlCategory1.Items.Clear();
            ListItem lstItem = new ListItem();
            //lstItem.Value = "0";
            //lstItem.Text = v;
            //ddlCategory1.Items.Add(lstItem);
            for (int intCount1 = 0; intCount1 < dataTable.Rows.Count; intCount1++)
            {
                lstItem = new ListItem();
                lstItem.Value = dataTable.Rows[intCount1][0].ToString().Trim();
                lstItem.Text = dataTable.Rows[intCount1][1].ToString().Trim();
                lstItem.Attributes.Add("id", dataTable.Rows[intCount1][2].ToString().Trim());
                ddlCategory1.Items.Add(lstItem);
            }
        }
        private void FillDropDowns(System.Web.UI.HtmlControls.HtmlSelect ddlcat, DataTable dataTable, string v)
        {
            ddlcat.Items.Clear();
            ListItem lstItem = new ListItem();
            lstItem.Value = "0";
            lstItem.Text = v;
            ddlcat.Items.Add(lstItem);
            for (int intCount1 = 0; intCount1 < dataTable.Rows.Count; intCount1++)
            {
                lstItem = new ListItem();
                lstItem.Value = dataTable.Rows[intCount1][0].ToString().Trim();
                lstItem.Text = dataTable.Rows[intCount1][1].ToString().Trim();
                lstItem.Attributes.Add("id", dataTable.Rows[intCount1][2].ToString().Trim());
                ddlcat.Items.Add(lstItem);
            }

        }

        protected void ddlSubCategory1_SelectedIndexChanged(object sender, EventArgs e)
        {

            BuildHTMLTable(Convert.ToInt32(ddlCategory1.SelectedItem.Value), Convert.ToInt32(ddlSubCategory1.SelectedItem.Value));
        }
        private void populateproductdropdowns(DataTable dataTable)
        {
            ddlSubCategory1.Items.Clear();
            ListItem lstItem = new ListItem();
            //lstItem.Value = "0";
            //lstItem.Text = "Choose your option";
            //ddlSubCategory1.Items.Add(lstItem);
            for (int intCount1 = 0; intCount1 < dataTable.Rows.Count; intCount1++)
            {
                lstItem = new ListItem();
                lstItem.Value = dataTable.Rows[intCount1][0].ToString().Trim();
                lstItem.Text = dataTable.Rows[intCount1][1].ToString().Trim();
                ddlSubCategory1.Items.Add(lstItem);
            }
        }
        private void populateproductdropdownsedit(DataTable dataTable)
        {
            ddlSubCategory.Items.Clear();
            ListItem lstItem = new ListItem();
            lstItem.Value = "0";
            lstItem.Text = "Choose your option";
            //ddlSubCategory1.Items.Add(lstItem);
            for (int intCount1 = 0; intCount1 < dataTable.Rows.Count; intCount1++)
            {
                lstItem = new ListItem();
                lstItem.Value = dataTable.Rows[intCount1][0].ToString().Trim();
                lstItem.Text = dataTable.Rows[intCount1][1].ToString().Trim();
                ddlSubCategory.Items.Add(lstItem);
            }
        }
        private void populateproductdropdownspro(DataTable dataTable)
        {
            ddlproductname.Items.Clear();
            ListItem lstItem = new ListItem();
            lstItem.Value = "0";
            lstItem.Text = "Choose your option";
            //ddlSubCategory1.Items.Add(lstItem);
            for (int intCount1 = 0; intCount1 < dataTable.Rows.Count; intCount1++)
            {
                lstItem = new ListItem();
                lstItem.Value = dataTable.Rows[intCount1][0].ToString().Trim();
                lstItem.Text = dataTable.Rows[intCount1][1].ToString().Trim();
                ddlproductname.Items.Add(lstItem);
            }
        }
        

        protected void btnsave_Click(object sender, EventArgs e)
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                if (btnsave.Text == "Update")
                {
                    UpdateAccessory();
                }
                else
                {
                    if (hidsub.Value != "0" || hidsub.Value != "" || hidproduct.Value != "" || hidproduct.Value != "0")
                    {
                        OfferEntity ent = new OfferEntity();
                        ent.offername = "0";
                        ent.categoryid = Convert.ToInt32(ddlcat.Value);
                        ent.subcategoryid = Convert.ToInt32(hidsub.Value);
                        ent.productid = Convert.ToInt32(hidproduct.Value);
                        string fileName = "";
                        if (inputFile.HasFile)
                        {
                            ent.photo = inputFile.PostedFile;

                            string FileName = Path.GetFileNameWithoutExtension(inputFile.FileName);
                            string FileExtension = Path.GetExtension(inputFile.FileName);
                            var guid = Guid.NewGuid().ToString();
                            FileName = DateTime.Now.ToString("yyyyMMdd") + "-" + guid + "-" + FileName.Trim() + FileExtension;


                            string UploadPath = ConfigurationManager.AppSettings["UserImagePath"].ToString();


                            fileName = UploadPath + FileName;


                            ent.photo.SaveAs(Server.MapPath(fileName));

                        }
                        ent.photoname = fileName;
                        BizDataProducts obj = new BizDataProducts();
                        obj.AddOffer(ent);
                        
                    }
                    Response.Redirect("Data.aspx?Action=productsoffer");
                }
            }
        }
        private void UpdateAccessory()
        {

            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                if (hidsub.Value != "0" || hidsub.Value != "" || hidproduct.Value != "" || hidproduct.Value != "0")
                {
                    OfferEntity ent = new OfferEntity();
                    ent.offername = "0";
                    ent.categoryid = Convert.ToInt32(ddlcat.Value);
                    ent.subcategoryid = Convert.ToInt32(hidsub.Value);
                    ent.productid = Convert.ToInt32(hidproduct.Value);
                    ent.offerid = Convert.ToInt32(hiddenid.Value);
                    string fileName = "";
                    if (inputFile.HasFile)
                    {
                        ent.photo = inputFile.PostedFile;

                        string FileName = Path.GetFileNameWithoutExtension(inputFile.FileName);
                        string FileExtension = Path.GetExtension(inputFile.FileName);
                        var guid = Guid.NewGuid().ToString();
                        FileName = DateTime.Now.ToString("yyyyMMdd") + "-" + guid + "-" + FileName.Trim() + FileExtension;


                        string UploadPath = ConfigurationManager.AppSettings["UserImagePath"].ToString();


                        fileName = UploadPath + FileName;


                        ent.photo.SaveAs(Server.MapPath(fileName));

                    }
                    else
                    {
                        fileName = filenamelabel.InnerText;
                        fileName = "/" + fileName;
                    }
                    ent.photoname = fileName;
                    BizDataProducts obj = new BizDataProducts();
                    obj.UpdateOffer(ent);                   

                }
                Response.Redirect("Data.aspx?Action=productsoffer");
            }
        }

        protected void ddlCategory1_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillsubctegory();
            BuildHTMLTable(Convert.ToInt32(ddlCategory1.SelectedItem.Value), Convert.ToInt32(ddlSubCategory1.SelectedItem.Value));
        }

        private void fillsubctegory()
        {
            //throw new NotImplementedException();
            BizDataProducts user = new BizDataProducts();
            ddlSubCategory1.Items.Clear();
            string id = ddlCategory1.SelectedItem.Value;
            if (id != "0")
            {
                DataSet ds = user.fillsubcategorybycategoryid(id);
                populateproductdropdowns(ds.Tables[0]);
            }
        }
        private void fillsubctegory(int v)
        {
            //throw new NotImplementedException();
            BizDataProducts user = new BizDataProducts();
            ddlSubCategory.Items.Clear();
            
                DataSet ds = user.fillsubcategorybycategoryid(v.ToString());
                populateproductdropdownsedit(ds.Tables[0]);
            //}
        }
    }
}