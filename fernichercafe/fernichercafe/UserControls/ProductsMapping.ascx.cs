﻿using fernicherLibrary.BizData;
using fernicherLibrary.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fernichercafe.UserControls
{
    public partial class ProductsMapping : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sessionUserId = Session["UserID"] as string;
            if (string.IsNullOrEmpty(sessionUserId))
            {

                Response.Redirect("Login.aspx");
            }
            if (!IsPostBack)
            {
                btnsave.Text = "Add";
                myModalLabel.InnerText = "Add Map";
                BizDataCategory bdl = new BizDataCategory();
                DataSet dsrole = bdl.GetAllCategory();
                
                FillDropDowns(ddlcat, dsrole.Tables[0], "Choose your option");                
                BuildHTMLTable();
                string sometin = Request.QueryString["id"];
                if (!(string.IsNullOrEmpty(sometin)))
                {

                    EditMappingDetails();
                }

            }
        }

        private void EditMappingDetails()
        {
            BizDataProducts user = new BizDataProducts();
            string sometin = Request.QueryString["id"];
            int iddd = Convert.ToInt32(sometin);

            if (sometin != null)
            {

                DataSet dsCategoryid = user.EditMapping(iddd);
                if (dsCategoryid.Tables[0].Rows.Count > 0)
                {
                    string ss = "",result1="",result2="";
                    hiddenid.Value = dsCategoryid.Tables[0].Rows[0]["mapid"].ToString();
                    BizDataCategory bdl = new BizDataCategory();
                    DataSet dsrole = bdl.GetAllCategory();
                    fillddlcat(dsrole.Tables[0]);
                    string yourString = dsCategoryid.Tables[0].Rows[0]["photourl"].ToString();
                    String result = yourString.Replace("~", "");
                    if (result != "")
                    {
                        result1 = result.Replace("\\", "/");
                        result2 = result1.Remove(0, 1);
                    }
                    inputFile.Attributes.Add("value", dsCategoryid.Tables[0].Rows[0]["photourl"].ToString());
                    filepathedit.Value = dsCategoryid.Tables[0].Rows[0]["photourl"].ToString();
                    filenamelabel.InnerText = result2;
                    string value = dsCategoryid.Tables[0].Rows[0]["flgActive"].ToString();
                    if (value == "1") { active.Checked = true; } else { inactive.Checked = true; }
                    ddlcat.Value = dsCategoryid.Tables[0].Rows[0]["categoryid"].ToString();
                    txtmap.Value = dsCategoryid.Tables[0].Rows[0]["mapname"].ToString();
                    fillsubctegory(Convert.ToInt32(ddlcat.Value));
                    ddlSubCategory.Value = dsCategoryid.Tables[0].Rows[0]["subcategoryid"].ToString();
                    hidsub.Value = dsCategoryid.Tables[0].Rows[0]["subcategoryid"].ToString();
                    string catname = dsCategoryid.Tables[0].Rows[0]["CategoryName"].ToString();
                    BizDataProducts userd = new BizDataProducts();
                   btnsave.Text = "Update";
                    myModalLabel.InnerText = "Edit Map";
                    hiddenid.Value = iddd.ToString();

                    ss = "<script language=JavaScript>$(document).ready(function(){ setTimeout(function () {   $('#add_maplevel').modal('open');}, 200);});</script>";

                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CreatePopupOpen", ss);


                }
                btnsave.Text = "Update";
                myModalLabel.InnerText = "Edit Map";

            }
        }

        private void fillsubctegory(int v)
        {
            //throw new NotImplementedException();
            BizDataProducts user = new BizDataProducts();
            ddlSubCategory.Items.Clear();

            DataSet ds = user.fillsubcategorybycategoryid(v.ToString());
            populateproductdropdownsedit(ds.Tables[0]);
            //}
        }

        private void populateproductdropdownsedit(DataTable dataTable)
        {
            ddlSubCategory.Items.Clear();
            ListItem lstItem = new ListItem();
            lstItem.Value = "0";
            lstItem.Text = "Choose your option";
            //ddlSubCategory1.Items.Add(lstItem);
            for (int intCount1 = 0; intCount1 < dataTable.Rows.Count; intCount1++)
            {
                lstItem = new ListItem();
                lstItem.Value = dataTable.Rows[intCount1][0].ToString().Trim();
                lstItem.Text = dataTable.Rows[intCount1][1].ToString().Trim();
                ddlSubCategory.Items.Add(lstItem);
            }
        }

        private void fillddlcat(DataTable dataTable)
        {
            // throw new NotImplementedException();
            ddlcat.Items.Clear();
            ListItem lstItem = new ListItem();
            lstItem.Value = "0";
            lstItem.Text = "Choose your option";
            //ddlCategory1.Items.Add(lstItem);
            for (int intCount1 = 0; intCount1 < dataTable.Rows.Count; intCount1++)
            {
                lstItem = new ListItem();
                lstItem.Value = dataTable.Rows[intCount1][0].ToString().Trim();
                lstItem.Text = dataTable.Rows[intCount1][1].ToString().Trim();
                lstItem.Attributes.Add("id", dataTable.Rows[intCount1][2].ToString().Trim());
                ddlcat.Items.Add(lstItem);
            }
        }

        private void BuildHTMLTable()
        {
            pldMapLevelTable.Controls.Clear();
            BizDataProducts objBizDataUser = new BizDataProducts();
            DataSet dsUser = objBizDataUser.GetAllMappingLevel();


            StringBuilder strAccount = new StringBuilder();
            if (dsUser.Tables[0].Rows.Count > 0)
            {
                for (int intCount = 0; intCount < dsUser.Tables[0].Rows.Count; intCount++)
                {
                    string id = dsUser.Tables[0].Rows[intCount]["mapid"].ToString();
                    
                    strAccount.Append("<tr>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["mapname"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["subcategoryname"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["displayname"].ToString());
                    strAccount.Append("</td>");
                    


                    strAccount.Append("<td><img class=listimg src='");
                    string yourString = dsUser.Tables[0].Rows[intCount]["photourl"].ToString();
                    String result = yourString.Replace("~", "");
                    if (result != "")
                    {
                        string result1 = result.Replace("\\", "/");
                        string result2 = result1.Remove(0, 1);
                        strAccount.Append(result2);
                    }
                    strAccount.Append("' /></td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["flgActive"].ToString() == "1" ? "Active":"In-Active");
                    strAccount.Append("</td>");

                    strAccount.Append("<td class='center-align'>");
                    strAccount.Append("<a href='Data.aspx?Action=ProductsMapping&id=" + id + "' style='padding-left:10px;padding-right: 10px'><i class='fa fa-edit'></i></a>");
                    strAccount.Append("</td>");


                    strAccount.Append("</tr>");

                }

                pldMapLevelTable.Controls.Add(new Literal { Text = strAccount.ToString() });
            }
        }

        private void FillDropDowns(System.Web.UI.HtmlControls.HtmlSelect ddlcat, DataTable dataTable, string v)
        {
            ddlcat.Items.Clear();
            ListItem lstItem = new ListItem();
            lstItem.Value = "0";
            lstItem.Text = v;
            ddlcat.Items.Add(lstItem);
            for (int intCount1 = 0; intCount1 < dataTable.Rows.Count; intCount1++)
            {
                lstItem = new ListItem();
                lstItem.Value = dataTable.Rows[intCount1][0].ToString().Trim();
                lstItem.Text = dataTable.Rows[intCount1][1].ToString().Trim();
                lstItem.Attributes.Add("id", dataTable.Rows[intCount1][2].ToString().Trim());
                ddlcat.Items.Add(lstItem);
            }

        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                if (btnsave.Text == "Update")
                {
                    UpdateMapping();
                }
                else
                {
                    if (hidsub.Value != "0" || hidsub.Value != "")
                    {
                        MappingEntity ent = new MappingEntity();
                        ent.categoryid = Convert.ToInt32(ddlcat.Value);
                        ent.subcategoryid = Convert.ToInt32(hidsub.Value);
                        ent.mapname = txtmap.Value;
                        string fileName = "";
                        if (inputFile.HasFile)
                        {
                            ent.photo = inputFile.PostedFile;

                            string FileName = Path.GetFileNameWithoutExtension(inputFile.FileName);
                            string FileExtension = Path.GetExtension(inputFile.FileName);
                            var guid = Guid.NewGuid().ToString();
                            FileName = DateTime.Now.ToString("yyyyMMdd") + "-" + guid + "-" + FileName.Trim() + FileExtension;


                            string UploadPath = ConfigurationManager.AppSettings["UserImagePath"].ToString();


                            fileName = UploadPath + FileName;


                            ent.photo.SaveAs(Server.MapPath(fileName));

                        }
                        ent.photoname = fileName;
                        ent.flgActive = active.Checked == true ? "1" : "0";
                        BizDataProducts obj = new BizDataProducts();
                        obj.AddMapping(ent);

                    }
                    Response.Redirect("Data.aspx?Action=ProductsMapping");
                }
            }
        }

        private void UpdateMapping()
        {

            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                if (hidsub.Value != "0" || hidsub.Value != "")
                {
                    MappingEntity ent = new MappingEntity();
                    
                    ent.categoryid = Convert.ToInt32(ddlcat.Value);
                    ent.subcategoryid = Convert.ToInt32(hidsub.Value);
                    ent.mapid = Convert.ToInt32(hiddenid.Value);
                    ent.mapname = txtmap.Value;
                    string fileName = "";
                    if (inputFile.HasFile)
                    {
                        ent.photo = inputFile.PostedFile;

                        string FileName = Path.GetFileNameWithoutExtension(inputFile.FileName);
                        string FileExtension = Path.GetExtension(inputFile.FileName);
                        var guid = Guid.NewGuid().ToString();
                        FileName = DateTime.Now.ToString("yyyyMMdd") + "-" + guid + "-" + FileName.Trim() + FileExtension;


                        string UploadPath = ConfigurationManager.AppSettings["UserImagePath"].ToString();


                        fileName = UploadPath + FileName;


                        ent.photo.SaveAs(Server.MapPath(fileName));

                    }
                    else
                    {
                        fileName = filenamelabel.InnerText;
                        fileName = "/" + fileName;
                    }
                    ent.photoname = fileName;
                    ent.flgActive = active.Checked == true ? "1" : "0";
                    BizDataProducts obj = new BizDataProducts();
                    obj.UpdateMapping(ent);

                }
                Response.Redirect("Data.aspx?Action=ProductsMapping");
            }
        }
    }
}