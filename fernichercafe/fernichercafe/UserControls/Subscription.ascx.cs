﻿using fernicherLibrary.BizData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fernichercafe.UserControls
{
    public partial class Subscription : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BuildHTMLTable();
        }

        private void BuildHTMLTable()
        {
            plhdtblsubscription.Controls.Clear();
            BizDataUser objBizDataUser = new BizDataUser();
            DataSet dsUser = objBizDataUser.GetAllNewsLetter();


            StringBuilder strAccount = new StringBuilder();
            if (dsUser.Tables[0].Rows.Count > 0)
            {
                for (int intCount = 0; intCount < dsUser.Tables[0].Rows.Count; intCount++)
                {
                    string id = dsUser.Tables[0].Rows[intCount]["newsletterid"].ToString();
                    strAccount.Append("<tr>");

                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["emailid"].ToString());
                    strAccount.Append("</td>");

                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["flgActive"].ToString() == "1" ?"Active" :"In-Active");
                    strAccount.Append("</td>");

                    strAccount.Append("<td>");
                    strAccount.Append(Convert.ToDateTime(dsUser.Tables[0].Rows[intCount]["CreatedOn"].ToString()).ToString("dd-MMM-yyyy HH:mm",CultureInfo.InvariantCulture));
                    strAccount.Append("</td>");

                    strAccount.Append("</tr>");


                }

                plhdtblsubscription.Controls.Add(new Literal { Text = strAccount.ToString() });
            }
        }

    }
}