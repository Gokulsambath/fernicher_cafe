﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductsMappingLevel.ascx.cs" Inherits="fernichercafe.UserControls.ProductsMappingLevel" %>


<style>
    label.radio-inline label {
        padding-left: 20px;
        display: inline-block;
    }
</style>
<!-- Page Content -->
<!-- ============================================================== -->
<!-- editmodal -->


<!-- BEGIN: Page Main-->
<div id="main">
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s10 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0">Map Level</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="container">
                <div class="section section-data-tables">

                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div id="basic-tabs" class="card card card-default scrollspy">
                                <div class="card-content">
                                    <div class="col s12">
                                        <div class="row" id="main-view">
                                            <div class="col s12">
                                                <ul class="tabs tab-demo ">
                                                    <li class="tab col m3"><a target="_self" href="Data.aspx?Action=ProductsMapping">ADD Map Level</a>
                                                    </li>
                                                    <li class="tab col m3"><a class="active" href="#">Map Level</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col s12">
                                                <div id="category" class="col s12" style="margin-top: 1rem;">
                                                    <div class="row">
                                                        <div class="col s12">
                                                            <table id="page-length-option" class="display" width="100%">
                                                                <thead>
                                                                    <tr>
                                                                        <th width="20%">Category</th>
                                                                        <th width="30%">Sub Category</th>
                                                                        <th width="30%">Product Name</th>
                                                                        <th width="30%">Level Name</th>
                                                                        <th width="20%">Action</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <asp:PlaceHolder ID="pldMappedTable" runat="server"></asp:PlaceHolder>

                                                                </tbody>

                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div id="adddsubcategory" runat="server" class="fixed-action-btn direction-top">
                        <a class="btn-floating btn-large gradient-45deg-light-blue-cyan gradient-shadow modal-trigger mb-2 mr-1" href="#add_maplevel"><i class="material-icons">add</i></a>
                    </div>


                </div>

            </div>
        </div>
    </div>
</div>

<div id="add_maplevel" class="modal">
    <div class="modal-content">
        <span class="modal-header right modal-close">
            <i class="material-icons right-align">clear</i>
        </span>
        <h4 id="myModalLabel" runat="server">Map Name</h4>
        <div class="divider"></div>
        <div class="modal-body">
            <form id="categoryform" runat="server">
                <div class="row">
                    <input type="hidden" id="hiddenid" runat="server" />

                    <div class="input-field col s6 col m6 col l6">
                        <label class="active">Category Name <span class="red-text">*</span></label>
                        <select class="form-control p-0" id="ddlcat" runat="server">
                        </select>
                        <span class="highlight"></span><span class="bar"></span>


                    </div>
                    <div class="input-field col s6 col m6 col l6">
                        <label class="active">Sub Category Name <span class="red-text">*</span></label>
                        <select class="form-control p-0" id="ddlSubCategory" runat="server">
                        </select>
                        <input type="hidden" id="hidsub" runat="server" />
                        <span class="highlight"></span><span class="bar"></span>


                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6 col m6 col l6">
                        <label class="active">Product Name <span class="red-text">*</span></label>
                        <select class="form-control p-0" id="ddlproductname" runat="server">
                        </select>
                        <input type="hidden" id="hidproduct" runat="server" />
                        <span class="highlight"></span><span class="bar"></span>


                    </div>
                    <div class="input-field col s6 col m6 col l6">
                        <label class="active">Mapping To<span class="red-text">*</span></label>
                        <select class="form-control p-0" id="ddlmaplevel" runat="server">
                        </select>
                        <input type="hidden" id="hidmap" runat="server" />
                        <span class="highlight"></span><span class="bar"></span>


                    </div>
                </div>
               
                <br />
                <div class="divider"></div>
                <div class="modal-footer">
                    <asp:HyperLink href="Data.aspx?Action=mapproducts" runat="server" class="modal-action modal-close waves-effect waves-red btn btn-danger">Cancel</asp:HyperLink>
                    <asp:Button type="button" ID="btnsave" runat="server" OnClick="btnsave_Click" Text="save" class="modal-action modal-close waves-effect waves-green btn btn-info "></asp:Button>

                </div>
            </form>
        </div>
    </div>

</div>



<footer class="page-footer footer footer-static footer-dark gradient-45deg-indigo-purple gradient-shadow navbar-border navbar-shadow">
    <div class="footer-copyright">
        <div class="container"><span>&copy; Fernicher Cafe 2019</span></div>
    </div>
</footer>

<script>
    
    $("#MiddleCenterContent_ctl01_ddlproductname").change(function () {

        var id = $(this).find(":selected").val();
        
        $("#MiddleCenterContent_ctl01_hidproduct").val(id);
    });
    $("#MiddleCenterContent_ctl01_ddlmaplevel").change(function () {

        var id = $(this).find(":selected").val();
        
        $("#MiddleCenterContent_ctl01_hidmap").val(id);
    });
    $("#MiddleCenterContent_ctl01_ddlcat").change(function () {

        var id = $(this).find(":selected").val();

        $("#MiddleCenterContent_ctl01_ddlSubCategory").html("");
        if (id != 0) {
            $.ajax({
                type: 'POST',
                url: "userwebservice.asmx/populatesubcategorydropdown",
                data: "{ 'id': '" + id + "' }",
                async: false,
                contentType: 'application/json; charset-utf-8',

                success: function (response) {


                    var html = "<option value=0>choose your option</option>";
                    for (var i = 0; i < response.d.length; i++) {
                        html += "<option value=" + response.d[i].productid + ">" + response.d[i].productname + "</option>";
                    }
                    $("#MiddleCenterContent_ctl01_ddlSubCategory").append(html);


                }

            });
        }
    });


    $("#MiddleCenterContent_ctl01_ddlSubCategory").change(function () {

        var id = $(this).find(":selected").val();
        var selectedText = $("#MiddleCenterContent_ctl01_ddlcat option:selected").attr("id");
        $("#MiddleCenterContent_ctl01_hidsub").val(id);
        

        $("#MiddleCenterContent_ctl01_ddlproductname").html("");
        if (id != 0) {
            $.ajax({
                type: 'POST',
                url: "userwebservice.asmx/populateproductdropdownmap",
                data: "{ 'id': '" + id + "','idname':'" + selectedText + "' }",
                async: false,
                contentType: 'application/json; charset-utf-8',

                success: function (response) {


                    var html = "<option value=0>choose your option</option>";
                    for (var i = 0; i < response.d.length; i++) {
                        html += "<option value=" + response.d[i].productid + ">" + response.d[i].productname + "</option>";
                    }
                    $("#MiddleCenterContent_ctl01_ddlproductname").append(html);


                }

            });
        }
        var catid = $("#MiddleCenterContent_ctl01_ddlcat").find('option:selected').val();
         $("#MiddleCenterContent_ctl01_ddlmaplevel").html("");
        if (id != 0) {
            $.ajax({
                type: 'POST',
                url: "userwebservice.asmx/populatemapleveldropdown",
                data: "{ 'id': '" + id + "','idname':'" + catid + "' }",
                async: false,
                contentType: 'application/json; charset-utf-8',

                success: function (response) {


                    var html = "<option value=0>choose your option</option>";
                    for (var i = 0; i < response.d.length; i++) {
                        html += "<option value=" + response.d[i].productid + ">" + response.d[i].productname + "</option>";
                    }
                    $("#MiddleCenterContent_ctl01_ddlmaplevel").append(html);


                }

            });
        }
    });


    function deletemaplevel(productid, subcategoryid, categoryid) {
        console.log(productid, subcategoryid, categoryid);
        confirm('Are you sure to delete?')
        {
            $.ajax({
                type: 'POST',
                url: "userwebservice.asmx/deletemaplevel",
                data: "{ 'productid': '" + productid + "','subcategoryid':'" + subcategoryid + "','categoryid':'" + categoryid + "' }",
                async: false,
                contentType: 'application/json; charset-utf-8',

                success: function (response) {


                    if (response.d == "success") {
                        location.reload();
                    }


                }

            });
        }
    }
</script>