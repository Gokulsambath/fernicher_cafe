﻿using fernicherLibrary.BizData;
using fernicherLibrary.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace fernichercafe.UserControls
{
    public partial class Category : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {
                Response.Redirect("Login.aspx");
            }
            BuildHTMLTable();
            if (!IsPostBack)
            {

                btnsave.Text = "Add";
                myModalLabel.InnerText = "Add Category";
                BizDataCategory bdl = new BizDataCategory();
                DataSet dsrole = bdl.GetAllCategory();
                // FillDropDown(ddlCategory, dsrole.Tables[0], "Choose your option");

                EditCategoryDetails();



            }
        }
        

        private void EditCategoryDetails()
        {

            BizDataCategory user = new BizDataCategory();
            string result1 = "", result2 = "", sometin = Request.QueryString["id"];
            int iddd = Convert.ToInt32(sometin);

            if (sometin != null)
            {

                DataSet dsCategoryid = user.EditCategoryDetails(iddd);
                if (dsCategoryid.Tables[0].Rows.Count > 0)
                {
                    string ss = ""; string value;
                    hiddenid.Value = dsCategoryid.Tables[0].Rows[0]["Categoryid"].ToString();
                    string yourString = dsCategoryid.Tables[0].Rows[0]["photourl"].ToString();
                    String result = yourString.Replace("~", "");
                    if (result != "")
                    {
                        result1 = result.Replace("\\", "/");
                        result2 = result1.Remove(0, 1);
                    }
                    inputFile.Attributes.Add("value", dsCategoryid.Tables[0].Rows[0]["photourl"].ToString());
                    filepathedit.Value = dsCategoryid.Tables[0].Rows[0]["photourl"].ToString();
                    filenamelabel.InnerText = result2;
                    value = dsCategoryid.Tables[0].Rows[0]["status"].ToString();
                    if (value == "1") { active.Checked = true; } else { inactive.Checked = true; }

                    //Categoryname.Value = dsCategoryid.Tables[0].Rows[0]["CategoryName"].ToString();
                    displayname.Value = dsCategoryid.Tables[0].Rows[0]["displayname"].ToString();

                    btnsave.Text = "Update";
                    myModalLabel.InnerText = "Edit Category";

                    ss = "<script language=JavaScript>$(document).ready(function(){ setTimeout(function () {   $('#add_category').modal('open');}, 200);});</script>";

                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CreatePopupOpen", ss);


                }
                btnsave.Text = "Update";
                myModalLabel.InnerText = "Edit Category";

            }

        }

        private void BuildHTMLTable()
        {
            pldCategorytable.Controls.Clear();
            BizDataCategory objBizDataUser = new BizDataCategory();
            DataSet dsUser = objBizDataUser.GetAllDetailsAdmin();


            StringBuilder strAccount = new StringBuilder();
            if (dsUser.Tables[0].Rows.Count > 0)
            {
                for (int intCount = 0; intCount < dsUser.Tables[0].Rows.Count; intCount++)
                {
                    string id = dsUser.Tables[0].Rows[intCount]["CategoryId"].ToString();
                    strAccount.Append("<tr>");
                    //strAccount.Append("<td>");
                    //strAccount.Append(dsUser.Tables[0].Rows[intCount]["CategoryName"].ToString());
                    //strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["displayname"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td><img class=listimg src='");
                    string yourString = dsUser.Tables[0].Rows[intCount]["photourl"].ToString();
                    String result = yourString.Replace("~", "");
                    if (result != "")
                    {
                        string result1 = result.Replace("\\", "/");
                        string result2 = result1.Remove(0, 1);
                        strAccount.Append(result2);
                    }
                    strAccount.Append("' /></td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["status"].ToString() == "1" ? "Active" : "In-Active");
                    strAccount.Append("</td>");

                    strAccount.Append("<td class='center-align'>");
                    strAccount.Append("<a href='Data.aspx?Action=Category&id=" + id + "' style='padding-right: 10px'><i class='fa fa-edit'></i></a>");
                    strAccount.Append("</td>");


                    strAccount.Append("</tr>");


                }

                pldCategorytable.Controls.Add(new Literal { Text = strAccount.ToString() });
            }
        }

       
        private void UpdateCategories()
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                string fileName = "";
                CategoryEntitie sourcelead = new CategoryEntitie();
                //sourcelead.CategoryName = Categoryname.Value;
                sourcelead.displayname = displayname.Value;
                sourcelead.status = active.Checked == true ? "1" : "0";
                if (inputFile.HasFile)
                {
                    sourcelead.photo = inputFile.PostedFile;
                
                    string FileName = Path.GetFileNameWithoutExtension(inputFile.FileName);
                    string FileExtension = Path.GetExtension(inputFile.FileName);
                    var guid = Guid.NewGuid().ToString();
                    FileName = DateTime.Now.ToString("yyyyMMdd") + "-" + guid + "-" + FileName.Trim() + FileExtension;


                string UploadPath = ConfigurationManager.AppSettings["UserImagePath"].ToString();


                    fileName = UploadPath + FileName;


                    sourcelead.photo.SaveAs(Server.MapPath(fileName));

                }
                else
                {
                    fileName = filenamelabel.InnerText;
                    fileName = "/" + fileName;
                }
                 sourcelead.photourl = fileName;
                sourcelead.CategoryId = Convert.ToInt32(hiddenid.Value);
                BizDataCategory obj = new BizDataCategory();
                obj.UpdateCategory(sourcelead);
                Response.Redirect("Data.aspx?Action=Category");

                Response.Redirect(Request.Url.AbsoluteUri);


            }
        }

         protected void btnsave_Click(object sender, EventArgs e)
         {
            if (btnsave.Text == "Update")
            {
                UpdateCategories();
            }
            else
            {

                string sessionUserId = Session["UserID"] as string;

                if (string.IsNullOrEmpty(sessionUserId))
                {
                    Response.Redirect("Login.aspx");
                }
                else
                {
                    string fileName="";
                    CategoryEntitie sourcelead = new CategoryEntitie();
                    sourcelead.displayname = displayname.Value;
                    //sourcelead.CategoryName = Categoryname.Value;
                    sourcelead.status = active.Checked == true ? "1" : "0";
                    if (inputFile.HasFile)
                    {
                        sourcelead.photo = inputFile.PostedFile;

                        string FileName = Path.GetFileNameWithoutExtension(inputFile.FileName);
                        string FileExtension = Path.GetExtension(inputFile.FileName);
                        FileName = DateTime.Now.ToString() + "-" + FileName.Trim() + FileExtension;

                        string UploadPath = ConfigurationManager.AppSettings["UserImagePath"].ToString();


                        fileName = UploadPath + FileName;


                        sourcelead.photo.SaveAs(Server.MapPath(fileName));

                    }

                    sourcelead.photourl = fileName;

                     BizDataCategory obj = new BizDataCategory();
                    obj.AddCategory(sourcelead);
                    Response.Redirect("Data.aspx?Action=Category");

                    Response.Redirect(Request.Url.AbsoluteUri);


                }
            }
        }

    }
}