﻿using fernicherLibrary.BizData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fernichercafe.UserControls
{
    public partial class BrandsWeb : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BuildHTMLTable();
            FillProfileValues();
        }

        private void FillProfileValues()
        {
            
                BizDataUser obj = new BizDataUser();
                DataTable dt = obj.Getbrandpage();
                if (dt.Rows.Count > 0)
                {
                    txtbrandheader.InnerText = dt.Rows[0]["brandheader"].ToString();
                    txtbrandsubheader.InnerText = dt.Rows[0]["brandsubheader"].ToString();

                }

            
        }

        private void BuildHTMLTable()
        {
            phldbrands.Controls.Clear();
            BizDataUser objBizDataUser = new BizDataUser();
            DataSet dsUser = objBizDataUser.GetAllActiveBrands();


            StringBuilder strAccount = new StringBuilder();
            if (dsUser.Tables[0].Rows.Count > 0)
            {

                string html = "",results="";
                
                for (int intCount = 0; intCount < dsUser.Tables[0].Rows.Count; intCount++)
                {
                    string yourString = dsUser.Tables[0].Rows[intCount]["photourl"].ToString();
                    String result = yourString.Replace("~", "");
                    if (result != "")
                    {
                        string result1 = result.Replace("\\", "/");
                        string result2 = result1.Remove(0, 1);
                        results = result2;
                    }
                    html += "<div class='col-md-3 col-lg-3 col-xs-12 col-sm-6'><div class='brand_outer_section'><div class='brand_inner_section'><img src='" + results + "' class='img-responsive'></div></div></div>";
                    if((intCount +1) % 4 == 0)
                    {
                        html += "<div class='clearfix'></div>";
                    }
                }

                phldbrands.Controls.Add(new Literal { Text = html.ToString() });
            }
        }
    }
}