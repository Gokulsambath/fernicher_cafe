﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductsGreenery.ascx.cs" Inherits="fernichercafe.UserControls.ProductsGreenery" %>
<style>
    label.radio-inline label{
        padding-left:20px;
        display:inline-block;
    }
   
</style>
 <form id="Form1" runat="server">

       <div id="main">
        <div class="row">
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">
                    <div class="row">
                        <div class="col s10 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0">Greenery Furniture</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <div class="section section-data-tables">

                        <div class="row">
                            <div class="col s12">
                                <div class="card">
                                    <div class="card-content">
                                        <div class="input-field col s6 col m4 col l4">
                                            <label class="active">Sub Category</label>
                                             <asp:DropDownList class="form-control p-0" AutoPostBack = "true" id="ddlSubCategory1" OnSelectedIndexChanged="ddlSubCategory1_SelectedIndexChanged" runat="server" required>
                                               
                                            </asp:DropDownList>
                                        </div>
                                        <div class="row">
                                            <div class="col s12">
                                                <table id="page-length-option" class="display wrap" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Banner url</th>
                                                            <th>Category Name</th>
                                                            <th>Product Name</th>
                                                            <th>Sub Category Name</th>
                                                            <th>Status</th>
                                                            <th>Image </th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                       
                                                         <asp:PlaceHolder ID="pldProductstable" runat="server"></asp:PlaceHolder>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- Scroll - vertical, dynamic height -->



                    <!-- Scroll - Vertical and Horizontal -->



                    <!-- Multi Select -->


                </div><!-- START RIGHT SIDEBAR NAV -->

                <!-- END RIGHT SIDEBAR NAV -->
                <div style="bottom: 50px; right: 19px;" id="adddproducts" runat="server" class="fixed-action-btn direction-top">
                    <a  href="#add_products" class="btn-floating btn-large gradient-45deg-light-blue-cyan gradient-shadow modal-trigger mb-2 mr-1">
                        <i class="material-icons">add</i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    
    <!-- END: Page Main-->


    <div id="add_products" class="modal">
        <div class="modal-content">
            <span class="modal-header right modal-close">
                  <i class="material-icons right-align">clear</i>
                </span>
            <h4 runat="server" id="myModalLabel">Add Products</h4>
            <div class="divider"></div>
            <div class="modal-body">
                
                    <div class="row">
                        <input type="hidden" id="hiddenid" runat="server" />
                        <div class="input-field col s6 col m6 col l6">
                             <label class="active">Sub Category Name <span class="red-text">*</span></label>
                                            <select class="form-control p-0" id="ddlSubCategory" runat="server" required>
                                              
                                            </select>
                                        <span class="highlight"></span> <span class="bar"></span>
                                             
                                       
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                        ControlToValidate="ddlSubCategory"
                                                        ErrorMessage="Please Select" display="dynamic" InitialValue="0" Text="Please select" ForeColor="Red"
                                                        runat="server" />
                        </div>
                        <div class="input-field col s6 col m6 col l6">
                           <label for="productname">Product Name</label>
                          <input type="text" class="form-control" id="productname" runat="server" required="" ><span class="highlight" ></span> <span class="bar"></span>
                                            
                        </div>
                    </div>                    
                    <div class="row">
                        <div class="input-field col s12">
                            <textarea id="txtdesc" runat="server" class="materialize-textarea" data-length="120"></textarea>
                            <label for="txtdesc">Description</label>
                        </div>
                    </div>
                    <div id="file-upload" class="section">
                        <div class="row section">
                            <div class="col s12 m4 l3">
                                <p>Upload Photo</p>
                            </div>
                            <div class="col s12 m8 l9">
                               <img id="image_upload_preview"  /> <label class="floatlabels" id="filenamelabel" runat="server"></label>
                                   <input type="hidden" id="filepathedit" runat="server" />
                                <asp:FileUpload ID="inputFile" runat="server" />
                            </div>
                        </div>
                    </div>
                  <div id="banner-file-upload" class="section">
                    <div class="row section">
                        <div class="col s12 m4 l3">
                            <p>Upload Banner Photo</p>
                        </div>
                        <div class="col s12 m8 l9">
                            <img id="banner_image_upload_preview" />
                            <label class="floatlabels" id="bannername" runat="server"></label>
                            <input type="hidden" id="bannerpathedit" runat="server" />
                            <asp:FileUpload ID="inputfile1" runat="server" />
                        </div>
                    </div>
                </div>
                    <div class="col s12 m12 l12 row" id="view-options">
                        <label style="font-size: 1rem">Status :&nbsp;</label>
                        <label class="radio-inline p-0" style="top:20px;margin-left:60px">
                            
                                    <asp:RadioButton GroupName="radio" ID="active" Text="Active" runat="server" />                                         
                                  
                        </label>
                        <label class="radio-inline" style="width: 21%;margin-left:130px;top:20px">
                           
                                <asp:RadioButton GroupName="radio" Text="In-Active" ID="inactive" runat="server" />
                                   
                        </label>
                    </div>
                 <div class="row">
                    <div class="input-field col s12" >
                        <textarea id="txtvideo" runat="server" class="materialize-textarea" data-length="120"></textarea>
                        <label for="txtvideo">Video Link</label>
                    </div>
                </div>
                <br />
                <div class="divider"></div>
                <div class="modal-footer">
                    <asp:HyperLink href="Data.aspx?Action=productsgreenery" runat="server" class="modal-action modal-close waves-effect waves-red btn btn-danger ">Cancel</asp:HyperLink>
                   <asp:button type="button" id="btnsave" runat="server" OnClick="btnsave_Click" Text="save" class="modal-action waves-effect waves-green btn btn-info"></asp:button>

                </div>
            </div>

        </div>
    </div>


</form>  

    <!-- Theme Customizer -->
    <!-- BEGIN: Footer-->

    <footer class="page-footer footer footer-static footer-dark gradient-45deg-indigo-purple gradient-shadow navbar-border navbar-shadow">
        <div class="footer-copyright">
            <div class="container"><span>&copy; Fernicher Cafe 2019</span></div>
        </div>
    </footer>

    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript">
   function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputFile").change(function () {
        readURL(this);
    });
</script>