﻿using fernicherLibrary.BizData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fernichercafe.UserControls
{
    public partial class Contact_UsWeb : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
                buildhtml();
           
        }
        private void buildhtml()
        {
            BizDataUser obj = new BizDataUser();
            DataTable dt = obj.GetCompanyInfo();
            if (dt.Rows.Count > 0)
            {
                string html = "", html2 = "";
                html += dt.Rows[0]["address"].ToString();
                phldfooteraddress.Controls.Add(new Literal { Text = html.ToString() });
                html2 += "<span class='icon icon-clock'></span>&nbsp;" + dt.Rows[0]["WorkingHours"].ToString();
                phldfootertiming.InnerHtml = html2;
                phldmobile.InnerHtml = "<span class='icon icon-phone'></span>&nbsp;" + dt.Rows[0]["mobileNo"].ToString();
                phldfooteremail.InnerHtml = "<span class='icon icon-envelope'></span>&nbsp;" + dt.Rows[0]["email"].ToString();
                maplocation.Src = dt.Rows[0]["MapLocation"].ToString();

                
            }
        }
    }
}