﻿using fernicherLibrary.BizData;
using fernicherLibrary.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fernichercafe.UserControls
{
    public partial class LabelControls : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {

                Response.Redirect("Login.aspx");
            }
            if (!IsPostBack)
            {

                FilllabelValues();
            }

        }

        private void FilllabelValues()
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                BizDataUser obj = new BizDataUser();
                DataTable dt = obj.GetLabelControls();
                if (dt.Rows.Count > 0)
                {
                    txtaboutusheader.Value = dt.Rows[0]["aboutusheader"].ToString();
                    txtaboutussubheader.Value = dt.Rows[0]["aboutussubheader"].ToString();
                    txtconceptsheader.Value = dt.Rows[0]["productconceptheader"].ToString();
                    txtconceptssubheader.Value = dt.Rows[0]["productconceptsubheader"].ToString();
                    txtofferheader.Value = dt.Rows[0]["offerheader"].ToString();
                    txtoffersubheader.Value = dt.Rows[0]["offersubheader"].ToString();
                    txtproductcategoriesheader.Value = dt.Rows[0]["productcategoriesheader"].ToString();
                    txtproductcategoriessubheader.Value = dt.Rows[0]["productcategoriessubheader"].ToString();
                    
                }

            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {
                Response.Redirect("Login.aspx");
            }
            else
            {

                GeneralEntitie sourcelead = new GeneralEntitie();
                sourcelead.lblaboutusheader = txtaboutusheader.Value;
                sourcelead.lblaboutusdesc = txtaboutussubheader.Value;
                sourcelead.lblproductcategoriesheader = txtproductcategoriesheader.Value;
                sourcelead.lblproductcategoriesdesc = txtproductcategoriessubheader.Value;
                sourcelead.lblofferheader = txtofferheader.Value;
                sourcelead.lblofferdesc = txtoffersubheader.Value;
                sourcelead.lblproductconceptheader = txtconceptsheader.Value;
                sourcelead.lblproductconceptdesc = txtconceptssubheader.Value;


                sourcelead.userid = Convert.ToInt32(sessionUserId);
                BizDataUser obj = new BizDataUser();
                obj.InsertLabelControls(sourcelead);
                Response.Redirect("Data.aspx?Action=LabelControls");

                Response.Redirect(Request.Url.AbsoluteUri);


            }
        }
    }
}