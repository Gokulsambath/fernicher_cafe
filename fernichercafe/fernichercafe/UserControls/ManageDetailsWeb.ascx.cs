﻿using fernicherLibrary.BizData;
using fernicherLibrary.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fernichercafeWebsite.UserControls
{
    public partial class ManageDetails : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BuildHTMLDetails();
            BuildRating();
        }

        private void BuildRating()
        {
            BizDataUser biz = new BizDataUser();
            string id = Request.QueryString["id"];
            string category = Request.QueryString["category"];
            DataTable dt = biz.GetAllRating(id,category);
            if (dt.Rows.Count > 0)
            {
                string html = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string starrating = Getstar(dt.Rows[i]["ratingstar"].ToString());                    
                    html += "<div class='comment-wrapper'><div class='comment-block' style='padding-top: 20px; padding-bottom: 10px;'><div class='comment-user'><div><img src='ContentWebsite/assets/images/user-2.jpg' alt='Alternate Text' width='70' /></div><div><h5><span>" + dt.Rows[i]["username"].ToString() + "</span><span class='pull-right'>" + starrating + "</span><small>" + Convert.ToDateTime(dt.Rows[i]["commenttime"]).ToString("dd-M-yyyy") + "</small></h5></div></div><div class='comment-desc'><p>" + dt.Rows[i]["comments"].ToString() + "</p></div></div></div><hr>";
                }
                ratingsdone.Controls.Add(new Literal { Text = html.ToString() });
            }
        }

        private string Getstar(string v)
        {
            string star = "";
            switch (v)
            {
                case "0.5":
                    star = "<span class='fa fa-star-half-o checked'></span>";
                    break;
                case "1":
                    star = "<span class='fa fa-star checked'></span>";
                    break;
                case "1.5":
                    star = "<span class='fa fa-star checked'></span><span class='fa fa-star-half-o checked'></span>";
                    break;
                case "2":
                    star = "<span class='fa fa-star checked'></span><span class='fa fa-star checked'></span>";
                    break;
                case "2.5":
                    star = "<span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star-half-o checked'></span>";
                    break;
                case "3":
                    star = "<span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star checked'></span>";
                    break;
                case "3.5":
                    star = "<span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star-half-o checked'></span>";
                    break;
                case "4":
                    star = "<span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star checked'></span>";
                    break;
                case "4.5":
                    star = "<span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star-half-o checked'></span>";
                    break;
                case "5":
                    star = "<span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star checked'></span>";
                    break;
            }
            return star;
        }

        private void BuildHTMLDetails()
        {
            BizDataProducts biz = new BizDataProducts();
            string id = Request.QueryString["id"];
            string category = Request.QueryString["category"];
            
            
            DataSet sss = biz.GetAllProductDetails(category, Convert.ToInt32(id));
            if (sss.Tables[0].Rows.Count > 0)
            {
                navtitle.Attributes["href"] = "~/DataWeb.aspx?Action=SubCategoryWeb&id=" + sss.Tables[0].Rows[0]["categoryid"].ToString() + "";
                if(category.ToLower() == "livingroom")
                {
                    navtitle1.Attributes["href"] = "~/DataWeb?Action=ProductListWeb&category=" + category + "&id=" + sss.Tables[0].Rows[0]["SubCategoryId"].ToString() + "&maplevel=null";
                }
                else
                {
                    navtitle1.Attributes["href"] = "~/DataWeb?Action=ProductListWeb&category=" + category + "&id=" + sss.Tables[0].Rows[0]["SubCategoryId"].ToString() + "&maplevel=other";
                }
                
                string yourstring = sss.Tables[0].Rows[0]["Bannerurl"].ToString();
                String resulttt = yourstring.Replace("~", "");
                if (resulttt != "")
                {
                    string result1 = resulttt.Replace("\\", "/");
                    string result2 = result1.Remove(0, 1);
                    result2 = result2.Replace(" ", "%20");
                    headerbanner.Attributes.CssStyle.Add("background-image", result2);
                }
                titleh1.InnerText = sss.Tables[0].Rows[0]["subcategoryname"].ToString();
                videodiv.Visible = false;
                if (!string.IsNullOrEmpty(sss.Tables[0].Rows[0]["videolink"].ToString()))
                {
                    videodiv.Visible = true;
                    
                    videolink.Src = sss.Tables[0].Rows[0]["videolink"].ToString().Replace("/watch?v=","/embed/");
                }
                navtitle.InnerText = sss.Tables[0].Rows[0]["displayname"].ToString();
                navtitle1.InnerText = sss.Tables[0].Rows[0]["subcategoryname"].ToString();
                navtitle2.InnerText = sss.Tables[0].Rows[0]["name"].ToString();
                var html = "";var html1 = ""; var html2 = "";var html3 = "";var html4 = "";
                sizediv.Visible = false;
                areadiv.Visible = false;
                manufacturediv.Visible = false;materialsdiv.Visible = false;availablediv.Visible = false;
                enqheader.InnerText = "Enquiry - " + sss.Tables[0].Rows[0]["name"].ToString() + "";
                if (category == "Kid")
                {
                    manufacturediv.Visible = true; materialsdiv.Visible = true; availablediv.Visible = true;
                    manufacturelbl.InnerText = sss.Tables[0].Rows[0]["manufacture"].ToString();
                    materialslbl.InnerText = sss.Tables[0].Rows[0]["materials"].ToString();
                    availimg.InnerHtml = sss.Tables[0].Rows[0]["availability"].ToString() == "True" ? "<i class='fa fa-check-square-o'></i> In stock" : "<i class='fa fa-truck'></i> Out of stock";
                    html2 += "<span class='h5'>Age Group: " + sss.Tables[0].Rows[0]["agegroup"].ToString() + " Years Old<br><small style='margin-left:0px'>Target Group :" + sss.Tables[0].Rows[0]["targetgroup"].ToString() + "</small><br><small style='margin-left:0px'>Segment: " + sss.Tables[0].Rows[0]["segment"].ToString() + "</small></span>";
                    html3 += "<div class='info-box'><span><strong>Certificates:</strong></span><span>" + sss.Tables[0].Rows[0]["certificates"].ToString() + "</span></div>";
                    modelnamediv.Visible = false;
                    for (int i = 0;i < sss.Tables[3].Rows.Count; i++) {
                        string yourString = sss.Tables[3].Rows[i]["photourl"].ToString();
                        String result = yourString.Replace("~", "");string result2 = "";
                        if (result != "")
                        {
                            string result1 = result.Replace("\\", "/");
                            result2 = result1.Remove(0, 1);                            
                        }
                        html1 += "<div class='col-xs-12 col-sm-4 col-md-4 col-lg-4'><div class='accessory-card card'><div class='accessory-wrapper'><div class='row'>" +
                                 "<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center zoom-accessory'><img class='' src='" + result2+"' alt='Alternate Text' width='80%' style='height: 217px;' />" +
                                 "</div><div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'><div class='accessory-text'><p class='ass-product-name'>" + sss.Tables[3].Rows[i]["AccessoryName"].ToString() + "</p>" +
                                 "<small class='ass-modal-name'>"+sss.Tables[3].Rows[i]["Model"].ToString()+ "</small></div><hr></div><div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>" +
                                 "<div class='row'><div class='col-xs-4 col-sm-4 col-md-4 col-lg-4 ass-width-div text-center'><p class='ass-width'>Width: " + sss.Tables[3].Rows[i]["width"].ToString() + " cm</p></div>" +
                                 "<div class='col-xs-4 col-sm-4 col-md-4 col-lg-4 ass-depth-div text-center'><p class='ass-depth'>Depth: " + sss.Tables[3].Rows[i]["Depth"].ToString() + " cm</p></div>" +
                                 "<div class='col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center'><p class='ass-height'>Height: " + sss.Tables[3].Rows[i]["height"].ToString() + " cm</p></div></div></div></div></div></div></div>";

                        //< div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>" +
                        //         "<div class='row'><div class='col-xs-4 col-sm-4 col-md-4 col-lg-4 ass-width-div text-center'><p class='ass-width'>Width: " + sss.Tables[3].Rows[i]["width"].ToString() + " cm</p></div>" +
                        //         "<div class='col-xs-4 col-sm-4 col-md-4 col-lg-4 ass-depth-div text-center'><p class='ass-depth'>Depth: " + sss.Tables[3].Rows[i]["Depth"].ToString() + " cm</p></div>" +
                        //         "<div class='col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center'><p class='ass-height'>Height: " + sss.Tables[3].Rows[i]["height"].ToString() + " cm</p></div></div></div>
                    }
                    html += "<h1 class='title'>" + sss.Tables[0].Rows[0]["name"].ToString() + "<small id='themelbl'>Theme: " + sss.Tables[0].Rows[0]["theme"].ToString() + "</small><small style='margin-top:10px;line-height:17px' id='featureslbl'>Features: " + sss.Tables[0].Rows[0]["features"].ToString() + "</small></h1>";
                }
                else if(category == "Livingroom")
                {
                    manufacturediv.Visible = true; materialsdiv.Visible = true; availablediv.Visible = true;
                    manufacturelbl.InnerText = sss.Tables[0].Rows[0]["manufacture"].ToString();
                    materialslbl.InnerText = sss.Tables[0].Rows[0]["materials"].ToString();
                    availimg.InnerHtml = sss.Tables[0].Rows[0]["availability"].ToString() == "True" ? "<i class='fa fa-check-square-o'></i> In stock" : "<i class='fa fa-truck'></i> Out of stock";

                    string availablecolors = "";
                    List<string> str = new List<string>();
                    for(int i = 0; i < sss.Tables[2].Rows.Count; i++)
                    {
                        if (i == 0)
                        {
                            str.Add(sss.Tables[2].Rows[i]["colorcode"].ToString());
                        }
                        else
                        {
                            if (!str.Contains(sss.Tables[2].Rows[i]["colorcode"].ToString()) && sss.Tables[2].Rows[i]["colorcode"].ToString() != "")
                            {
                                str.Add(sss.Tables[2].Rows[i]["colorcode"].ToString());
                            }
                        }
                    }
                    for (int i = 0; i < str.Count; i++)
                    {
                        availablecolors += "<span class='color-btn' style=background-color:" + str[i] + "></span>";
                    }
                    modelnamediv.Visible = true;
                    html4 += "<div class='info-box'><span><strong> Available Colors</strong></span><div class='product-colors clearfix'>"+availablecolors+"</div></div>";
                    modelnamelbl.InnerText = sss.Tables[0].Rows[0]["modelname"].ToString();
                    html += "<h1 class='title'>" + sss.Tables[0].Rows[0]["name"].ToString() + "<small style='margin-top:10px;line-height:17px;' id='featureslbl'>Description: " + sss.Tables[0].Rows[0]["features"].ToString() + "</small></h1>";
                }
                else if(category == "Greenery")
                {
                    manufacturediv.Visible = false;
                    materialsdiv.Visible = false; availablediv.Visible = false;
                    modelnamediv.Visible = true;sizediv.Visible = true;areadiv.Visible = true;
                    modelnamelbl.InnerText= sss.Tables[0].Rows[0]["modelname"].ToString();
                    sizelbl.InnerText = ""+sss.Tables[0].Rows[0]["size"].ToString()+"";
                    arealbl.InnerText = ""+sss.Tables[0].Rows[0]["area"].ToString()+" Sq.Ft";
                    //manufacturelbl.InnerText = sss.Tables[0].Rows[0]["manufacture"].ToString();
                    //materialslbl.InnerText = sss.Tables[0].Rows[0]["materials"].ToString();
                    //availimg.InnerHtml = sss.Tables[0].Rows[0]["availability"].ToString() == "True" ? "<i class='fa fa-check-square-o'></i> In stock" : "<i class='fa fa-truck'></i> Out of stock";
                }
                titlelbl.Controls.Add(new Literal { Text = html.ToString() });

                phldaccessory.Controls.Add(new Literal { Text = html1.ToString() });
                phldavailablecolors.Controls.Add(new Literal { Text = html4.ToString() });
                priceplaceholder.Controls.Add(new Literal { Text = html2.ToString() });
                phldcertificate.Controls.Add(new Literal { Text = html3.ToString() });
                

            }
            if (sss.Tables[1].Rows.Count > 0)
            {
                var html = "";
                DataTable dt = sss.Tables[1];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string yourString = dt.Rows[i]["name"].ToString();
                    html += "<li>" + yourString + "</li>";
                }
                technicaldetailslist.Controls.Add(new Literal { Text = html.ToString() });
            }
            if (sss.Tables[2].Rows.Count > 0)
            {
                var html = "";
                DataTable dt = sss.Tables[2];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string yourString = dt.Rows[i]["imagename"].ToString();
                    String result = yourString.Replace("~", "");
                    if (result != "")
                    {
                        string result1 = result.Replace("\\", "/");
                        string result2 = result1.Remove(0, 1);
                        result2 = result2.Replace(" ", "%20");
                        html += "<a href=" + result2 + "><img src=" + result2 + " alt='' height='500' /></a>";
                    }
                }
                imagesplaceholder.Controls.Add(new Literal { Text = html.ToString() });
            }
        }

        //protected void enquirybtn_Click(object sender, EventArgs e)
        //{
        //    if (enqname.Value.Trim() == "" || enqemail.Value.Trim() == "" || enqmobile.Value.Trim() == "") { }
        //    else
        //    {
        //        BizDataUser biz = new BizDataUser();
        //        GeneralEntitie ge = new GeneralEntitie();
        //        ge.productid = Request.QueryString["id"];
        //        ge.categoryname = Request.QueryString["category"];
        //        ge.enquiryname = enqname.Value;
        //        ge.enquirymobile = enqmobile.Value;
        //        ge.enquiryemail = enqemail.Value;
        //        ge.enquirycomments = enqcomments.Value;
        //        biz.InsertEnquiry(ge);
        //        enqname.Value = "";
        //        enqmobile.Value = "";
        //        enqemail.Value = "";
        //        enqcomments.Value = "";               
        //        ScriptManager.RegisterStartupScript(this, this.GetType(),"alert","msg('Enquiry sent sucessfully','success');",true);

        //    }
        //}

       
    }
}