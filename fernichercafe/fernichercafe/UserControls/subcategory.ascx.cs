﻿using fernicherLibrary.BizData;
using fernicherLibrary.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace fernichercafe.UserControls
{
    public partial class subcategory : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {
                Response.Redirect("Login.aspx");
            }
            BuildHTMLTable();
            if (!IsPostBack)
            {

                btnsave.Text = "Add";
                myModalLabel.InnerText = "Add Sub Category";
                BizDataCategory bdl = new BizDataCategory();
                DataSet dsrole = bdl.GetAllCategory();
                FillDropDown(ddlCategory, dsrole.Tables[0], "Choose your option");

                EditCategoryDetails();



            }
        }

        private void FillDropDown(HtmlSelect ddlCategory, DataTable dataTable, string v)
        {
            ddlCategory.Items.Clear();            ListItem lstItem = new ListItem();            lstItem.Value = "0";            lstItem.Text = v;            ddlCategory.Items.Add(lstItem);            for (int intCount1 = 0; intCount1 < dataTable.Rows.Count; intCount1++)            {                lstItem = new ListItem();                lstItem.Value = dataTable.Rows[intCount1][0].ToString().Trim();                lstItem.Text = dataTable.Rows[intCount1][1].ToString().Trim();                lstItem.Attributes.Add("id", dataTable.Rows[intCount1][2].ToString().Trim());                ddlCategory.Items.Add(lstItem);            }
        }

        private void EditCategoryDetails()
        {

            BizDataCategory user = new BizDataCategory();
            string result1 = "", result2 = "", result4 = "", result5 = "", sometin = Request.QueryString["id"];
            int iddd = Convert.ToInt32(sometin);

            if (sometin != null)
            {

                DataSet dsCategoryid = user.EditsubCategoryDetails(iddd);
                if (dsCategoryid.Tables[0].Rows.Count > 0)
                {
                    string ss = ""; string value;
                    hiddenid.Value = dsCategoryid.Tables[0].Rows[0]["subcategoryid"].ToString();
                    string yourString = dsCategoryid.Tables[0].Rows[0]["photourl"].ToString();
                    String result = yourString.Replace("~", "");
                    if (result != "")
                    {
                        result1 = result.Replace("\\", "/");
                        result2 = result1.Remove(0, 1);
                    }
                    inputFile.Attributes.Add("value", dsCategoryid.Tables[0].Rows[0]["photourl"].ToString());
                    filepathedit.Value = dsCategoryid.Tables[0].Rows[0]["photourl"].ToString();
                    filenamelabel.InnerText = result2;
                    string yourString1 = dsCategoryid.Tables[0].Rows[0]["bannerurl"].ToString();
                    String result3 = yourString1.Replace("~", "");
                    if (result3 != "")
                    {
                        result4 = result3.Replace("\\", "/");
                        result5 = result4.Remove(0, 1);
                    }
                    inputfile1.Attributes.Add("value", dsCategoryid.Tables[0].Rows[0]["bannerurl"].ToString());
                    bannerpathedit.Value = dsCategoryid.Tables[0].Rows[0]["bannerurl"].ToString();
                    bannername.InnerText = result5;
                    value = dsCategoryid.Tables[0].Rows[0]["status"].ToString();
                    if (value == "1") { active.Checked = true; } else { inactive.Checked = true; }
                    txtdesc.Value = dsCategoryid.Tables[0].Rows[0]["description"].ToString();
                    sortby.Value = dsCategoryid.Tables[0].Rows[0]["sortby"].ToString();
                    subCategoryname.Value = dsCategoryid.Tables[0].Rows[0]["subcategoryname"].ToString();
                    ddlCategory.Value = dsCategoryid.Tables[0].Rows[0]["CategoryId"].ToString();

                    btnsave.Text = "Update";
                    myModalLabel.InnerText = "Edit Sub Category";

                    ss = "<script language=JavaScript>$(document).ready(function(){ setTimeout(function () {   $('#add_sub_category').modal('open');}, 200);});</script>";

                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CreatePopupOpen", ss);


                }
                btnsave.Text = "Update";
                myModalLabel.InnerText = "Edit Sub  Category";

            }

        }

        private void BuildHTMLTable()
        {
            pldsubCategorytable.Controls.Clear();
            BizDataCategory objBizDataUser = new BizDataCategory();
            DataSet dsUser = objBizDataUser.GetAllSubCategory();


            StringBuilder strAccount = new StringBuilder();
            if (dsUser.Tables[0].Rows.Count > 0)
            {
                for (int intCount = 0; intCount < dsUser.Tables[0].Rows.Count; intCount++)
                {
                    string id = dsUser.Tables[0].Rows[intCount]["subCategoryId"].ToString();
                    strAccount.Append("<tr>");
                    strAccount.Append("<td><img class=listimg src='");
                    string yourString1 = dsUser.Tables[0].Rows[intCount]["bannerurl"].ToString();
                    String results1 = yourString1.Replace("~", "");
                    if (results1 != "")
                    {
                        string result2 = results1.Replace("\\", "/");
                        string result3 = result2.Remove(0, 1);
                        strAccount.Append(result3);
                    }
                    strAccount.Append("' /></td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["displayname"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["subCategoryName"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td><img class=listimg src='");
                    string yourString = dsUser.Tables[0].Rows[intCount]["photourl"].ToString();
                    String result = yourString.Replace("~", "");
                    if (result != "")
                    {
                        string result1 = result.Replace("\\", "/");
                        string result2 = result1.Remove(0, 1);
                        strAccount.Append(result2);
                    }
                    strAccount.Append("' /></td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["status"].ToString() == "1" ? "Active" : "In-Active");
                    strAccount.Append("</td>");

                    strAccount.Append("<td class='center-align'>");
                    strAccount.Append("<a class='modal-trigger' href='Data.aspx?Action=Subcategory&id=" + id + "' style='padding-right: 10px'><i class='fa fa-edit'></i></a>");
                    strAccount.Append("</td>");


                    strAccount.Append("</tr>");


                }

                pldsubCategorytable.Controls.Add(new Literal { Text = strAccount.ToString() });
            }
        }


        private void UpdateCategories()
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                string fileName = "", fileName1 = "";
                CategoryEntitie sourcelead = new CategoryEntitie();
                sourcelead.SubCategoryName = subCategoryname.Value;
                sourcelead.CategoryId = Convert.ToInt32(ddlCategory.Value);
                sourcelead.status = active.Checked == true ? "1" : "0";
                if (inputFile.HasFile)
                {
                    sourcelead.photo = inputFile.PostedFile;

                    string FileName = Path.GetFileNameWithoutExtension(inputFile.FileName);
                    string FileExtension = Path.GetExtension(inputFile.FileName);
                    var guid = Guid.NewGuid().ToString();
                    FileName = DateTime.Now.ToString("yyyyMMdd") + "-" + guid + "-" + FileName.Trim() + FileExtension;


                    string UploadPath = ConfigurationManager.AppSettings["UserImagePath"].ToString();


                    fileName = UploadPath + FileName;


                    sourcelead.photo.SaveAs(Server.MapPath(fileName));

                }
                else
                {
                    fileName = filenamelabel.InnerText;
                    fileName = "/" + fileName;
                }
                if (inputfile1.HasFile)
                {
                    sourcelead.photo = inputfile1.PostedFile;

                    string FileName = Path.GetFileNameWithoutExtension(inputfile1.FileName);
                    string FileExtension = Path.GetExtension(inputfile1.FileName);

                    var guid = Guid.NewGuid().ToString();
                    FileName = DateTime.Now.ToString("yyyyMMdd") + "-" + guid + "-" + FileName.Trim() + FileExtension;

                    string UploadPath = ConfigurationManager.AppSettings["UserImagePath"].ToString();


                    fileName1 = UploadPath + FileName;


                    sourcelead.photo.SaveAs(Server.MapPath(fileName1));

                }
                else
                {
                    fileName1 = bannername.InnerText;
                    fileName1 = "/" + fileName1;
                }
                sourcelead.description = txtdesc.Value;
                sourcelead.bannerurl = fileName1;
                sourcelead.sortby = Convert.ToInt32(sortby.Value);
                sourcelead.photourl = fileName;
                sourcelead.SubCategoryId = Convert.ToInt32(hiddenid.Value);
                BizDataCategory obj = new BizDataCategory();
                obj.UpdatesubCategory(sourcelead);
                Response.Redirect("Data.aspx?Action=Subcategory");

                Response.Redirect(Request.Url.AbsoluteUri);


            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (btnsave.Text == "Update")
            {
                UpdateCategories();
            }
            else
            {

                string sessionUserId = Session["UserID"] as string;

                if (string.IsNullOrEmpty(sessionUserId))
                {
                    Response.Redirect("Login.aspx");
                }
                else
                {
                    string fileName = "", fileName1 = "";
                    CategoryEntitie sourcelead = new CategoryEntitie();
                    sourcelead.SubCategoryName = subCategoryname.Value;
                    sourcelead.CategoryId = Convert.ToInt32(ddlCategory.Value);
                    sourcelead.status = active.Checked == true ? "1" : "0";
                    if (inputFile.HasFile)
                    {
                        sourcelead.photo = inputFile.PostedFile;

                        string FileName = Path.GetFileNameWithoutExtension(inputFile.FileName);
                        string FileExtension = Path.GetExtension(inputFile.FileName);

                        var guid = Guid.NewGuid().ToString();
                        FileName = DateTime.Now.ToString("yyyyMMdd") + "-" + guid + "-" + FileName.Trim() + FileExtension;

                        string UploadPath = ConfigurationManager.AppSettings["UserImagePath"].ToString();


                        fileName = UploadPath + FileName;


                        sourcelead.photo.SaveAs(Server.MapPath(fileName));

                    }
                    if (inputfile1.HasFile)
                    {
                        sourcelead.photo = inputfile1.PostedFile;

                        string FileName = Path.GetFileNameWithoutExtension(inputfile1.FileName);
                        string FileExtension = Path.GetExtension(inputfile1.FileName);

                        var guid = Guid.NewGuid().ToString();
                        FileName = DateTime.Now.ToString("yyyyMMdd") + "-" + guid + "-" + FileName.Trim() + FileExtension;

                        string UploadPath = ConfigurationManager.AppSettings["UserImagePath"].ToString();


                        fileName1 = UploadPath + FileName;


                        sourcelead.photo.SaveAs(Server.MapPath(fileName1));

                    }
                    sourcelead.sortby = Convert.ToInt32(sortby.Value);
                    sourcelead.photourl = fileName;
                    sourcelead.bannerurl = fileName1;
                    sourcelead.description = txtdesc.Value;
                    BizDataCategory obj = new BizDataCategory();
                    obj.AddsubCategory(sourcelead);
                    Response.Redirect("Data.aspx?Action=Subcategory");

                    Response.Redirect(Request.Url.AbsoluteUri);


                }
            }

        }
    }
}