﻿using fernicherLibrary.BizData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fernichercafeWebsite.UserControls
{
    public partial class GlobalHeader : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BuildQuickLinks();
        }

        private void BuildQuickLinks()
        {
            plhdnavbarquicklinks.Controls.Clear();
            BizDataCategory biz = new BizDataCategory();
            DataSet ds = biz.GetAllDetails();
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {
                string html = "",html1="", html2 = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    html1 = ""; html2 = "";
                    html1 = "<li class='label'><a href='DataWeb.aspx?Action=SubCategoryWeb&id=" + dt.Rows[i]["CategoryId"].ToString() + "'>" + dt.Rows[i]["displayname"].ToString() + "</a></li>";
                    string sometin = dt.Rows[i]["CategoryId"].ToString();
                    BizDataCategory bizsub = new BizDataCategory();
                    DataTable dtsub = biz.GetAllSubCategoryById(sometin);
                    if (dtsub.Rows.Count > 0)
                    {                        
                        for (int j = 0; j < dtsub.Rows.Count; j++)
                        {
                            if (dtsub.Rows[j]["CategoryName"].ToString().ToLower() == "livingroom")
                            {
                                html2 += "<li><a href='DataWeb.aspx?Action=ProductListWeb&category=" + dtsub.Rows[j]["CategoryName"].ToString() + "&id=" + dtsub.Rows[j]["subcategoryid"].ToString() + "&maplevel=null'>" + dtsub.Rows[j]["subcategoryname"].ToString() + "</a></li>";
                            }
                            else
                            {
                                html2 += "<li><a href='DataWeb.aspx?Action=ProductListWeb&category=" + dtsub.Rows[j]["CategoryName"].ToString() + "&id=" + dtsub.Rows[j]["subcategoryid"].ToString() + "&maplevel=other'>" + dtsub.Rows[j]["subcategoryname"].ToString() + "</a></li>";
                            }
                        }
                    }
                    html += "<div class='col-md-3'><ul>"+html1+""+html2+"</ul></div>";
                }
                plhdnavbarquicklinks.Controls.Add(new Literal { Text = html.ToString() });
            }
        }

    }
}