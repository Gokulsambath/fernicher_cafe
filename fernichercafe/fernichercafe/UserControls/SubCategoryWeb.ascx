﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SubCategoryWeb.ascx.cs" Inherits="fernichercafeWebsite.UserControls.SubCategory" %>

<style type="text/css">
    @media only screen and (min-width: 320px) and (max-width:640px) {
        .main-header {
            padding-top: 25px;
            
            height: 37vh;
        }

        header {
            margin-top: 160px;
        }
    }

    @media only screen and (min-width: 768px) and (max-width: 991px) {
        .main-header {
            padding-top: 130px;
            height: 50vh;
        }

        header {
            margin-top: 300px;
        }
    }


    @media only screen and (min-width: 992px) and (max-width: 1199px) {
        .main-header {
            padding-top: 100px;
            height: 70vh;
        }

        header {
            margin-top: 270px;
        }
    }

    @media only screen and (min-width: 1200px) and (max-width: 1400px) {
        .main-header {
            padding-top: 100px;
            margin-top: 0px;
            height: 70vh;
        }

        header {
            margin-top: 280px;
        }
    }
</style>
<section class="main-header" id="mainheader" runat="server">
    <header>
        <div class="container">
            <h1 class="h2 title" id="titleh1" runat="server"></h1>
            <ol class="breadcrumb breadcrumb-inverted">
                <li><a href="DataWeb.aspx?Action=IndexWeb"><span class="icon icon-home"></span></a></li>

                <li><a class="active" id="navcategory" runat="server"></a></li>
            </ol>
        </div>
    </header>
</section>

<!-- ========================  Icons slider ======================== -->

<section class="owl-icons-wrapper">

    <!-- === header === -->

    <header class="hidden">
        <h2>Product categories</h2>
    </header>

    <div class="container">
    </div>
    <!--/container-->
</section>

<!-- ======================== Products ======================== -->

<section class="products">
    <div class="container">

        <header class="hidden">
            <h3 class="h3 title">Product category grid</h3>
        </header>

        <div class="row">


            <asp:PlaceHolder ID="phldSubcategorylist" runat="server"></asp:PlaceHolder>

        </div>
        <!--/row-->

    </div>
    <!--/product items-->

  
</section>
