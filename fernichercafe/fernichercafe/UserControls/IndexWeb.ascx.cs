﻿using fernicherLibrary.BizData;
using fernicherLibrary.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using System.Net.Http.Headers;

namespace fernichercafeWebsite.UserControls
{
    public partial class Index : System.Web.UI.UserControl
    {        
        public List<OfferEntity> ent = new List<OfferEntity>();
        public class DataObject
        {
            public string Name { get; set; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            BuildHTMLCategory();
            BuildHTMLOffers();
            BuildHTMLBanners();
            BuildHTMLProductConcepts();
            BuildHTMLAbout();
            BuildHTMLLabels();

            var hostName = Dns.GetHostEntry(Dns.GetHostName());
            string ipv4 = hostName.AddressList[0].ToString();
            string _value = GetValue(ipv4 + "TestWeb");
            if (!string.IsNullOrEmpty(_value))
            {
                //Response.Write(_value);
            }
            else
            {
                SetValue(ipv4 + "TestWeb", Session.SessionID);
                //Response.Write(GetValue(ipv4 + "TestWeb"));
            }
           
        }

        //Add Value in cokies    
        public void SetValue(string key, string value)
        {
            Response.Cookies[key].Value = value;
            Response.Cookies[key].Expires = DateTime.Now.AddDays(1); // ad    
        }

        //Get value from cokkie    
        public string GetValue(string _str)
        {
            if (Request.Cookies[_str] != null)
                return Request.Cookies[_str].Value;
            else
                return "";
        }

    

        private void BuildHTMLLabels()
        {

            BizDataUser obj = new BizDataUser();
            DataTable dt = obj.GetLabelControls();
            if (dt.Rows.Count > 0)
            {
                lblaboutheader.InnerText = dt.Rows[0]["aboutusheader"].ToString();
                lblaboutsubheader.InnerText = dt.Rows[0]["aboutussubheader"].ToString();
                lblproductconceptheader.InnerText = dt.Rows[0]["productconceptheader"].ToString();
                lblproductconceptsubheader.InnerText = dt.Rows[0]["productconceptsubheader"].ToString();
                lblofferheader.InnerText = dt.Rows[0]["offerheader"].ToString();
                lbloffersubheader.InnerText = dt.Rows[0]["offersubheader"].ToString();
                lblproductcategoryheader.InnerText = dt.Rows[0]["productcategoriesheader"].ToString();
                lblproductcategorysubheader.InnerText = dt.Rows[0]["productcategoriessubheader"].ToString();

            }

        }

        private void BuildHTMLAbout()
        {
            BizDataUser obj = new BizDataUser();
            DataTable dt = obj.GetCompanyInfo();
            if (dt.Rows.Count > 0)
            {

                phldfooteraboutus.InnerHtml = dt.Rows[0]["IntroductoryLine"].ToString();

            }

        }

        private void BuildHTMLProductConcepts()
        {
            BizDataCategory biz = new BizDataCategory();
            DataSet ds = biz.GetAllActiveProductConcepts();
            DataTable dt = ds.Tables[0];

            if (dt.Rows.Count > 0)
            {
                string html = "", html2 = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (i < 4)
                    {
                        string yourString = dt.Rows[i]["photourl"].ToString();
                        String result = yourString.Replace("~", "");
                        if (result != "")
                        {
                            string result1 = result.Replace("\\", "/");
                            string result2 = result1.Remove(0, 1);
                            result2 = result2.Replace(" ", "%20");



                            html += "<li class='stretcher-item' style='background-image: url(" + result2 + ");'><div class='stretcher-logo'><div class='text'><a href='DataWeb.aspx?Action=ManageDetailsWeb&category=" + dt.Rows[i]["CategoryName"].ToString() + "&id=" + dt.Rows[i]["productid"].ToString() + "'><span class='text-intro'>" + dt.Rows[i]["name"].ToString() + "</span></a></div></div><a href='DataWeb.aspx?Action=ManageDetailsWeb&category=" + dt.Rows[i]["CategoryName"].ToString() + "&id=" + dt.Rows[i]["productid"].ToString() + "'><figure><h4>" + dt.Rows[i]["name"].ToString() + "</h4></figure></a></li>";


                        }
                    }
                    if (i >= 4 && i < 8)
                    {
                        string yourString = dt.Rows[i]["photourl"].ToString();
                        String result = yourString.Replace("~", "");
                        if (result != "")
                        {
                            string result1 = result.Replace("\\", "/");
                            string result2 = result1.Remove(0, 1);
                            result2 = result2.Replace(" ", "%20");



                            html2 += "<li class='stretcher-item' style='background-image: url(" + result2 + ");'><div class='stretcher-logo'><div class='text'><a href='DataWeb.aspx?Action=ManageDetailsWeb&category=" + dt.Rows[i]["CategoryName"].ToString() + "&id=" + dt.Rows[i]["productid"].ToString() + "'><span class='text-intro'>" + dt.Rows[i]["name"].ToString() + "</span></a></div></div><a href='DataWeb.aspx?Action=ManageDetailsWeb&category=" + dt.Rows[i]["CategoryName"].ToString() + "&id=" + dt.Rows[i]["productid"].ToString() + "'><figure><h4>" + dt.Rows[i]["name"].ToString() + "</h4></figure></a></li>";


                        }
                    }
                }
                phldhomeproductconcepts1.Controls.Add(new Literal { Text = html.ToString() });
                phldhomeproductconcepts2.Controls.Add(new Literal { Text = html2.ToString() });
            }
        }

        private void BuildHTMLBanners()
        {
            BizDataCategory biz = new BizDataCategory();
            DataSet ds = biz.GetAllActiveBanners();
            DataTable dt = ds.Tables[0];

            if (dt.Rows.Count > 0)
            {
                string html = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    string yourString = dt.Rows[i]["photourl"].ToString();
                    String result = yourString.Replace("~", "");
                    if (result != "")
                    {
                        string result1 = result.Replace("\\", "/");
                        string result2 = result1.Remove(0, 1);
                        result2 = result2.Replace(" ", "%20");
                        dt.Rows[i]["bannerheader"].ToString();
                        dt.Rows[i]["bannersubheader"].ToString();


                        if (i == 0)
                        {
                            html += "<a href=DataWeb.aspx?Action=ManageDetailsWeb&category=" + dt.Rows[i]["CategoryName"].ToString() + "&id=" + dt.Rows[i]["productid"].ToString() + "><div class='item' style=background-image:url(" + result2 + ")><div class='box'><div class='container'><h2 class='title animated h1' data-animation='fadeInDown' style='color:#fff;'>" + dt.Rows[i]["bannerheader"].ToString() + "</h2><div class='animated' data-animation='fadeInUp' style='color: #fff;'>" + dt.Rows[i]["bannersubheader"].ToString() + "</div></div></div></div></a>";
                        }
                        else
                        {
                            html += "<a href=DataWeb.aspx?Action=ManageDetailsWeb&category=" + dt.Rows[i]["CategoryName"].ToString() + "&id=" + dt.Rows[i]["productid"].ToString() + "><div class='item' style=background-image:url(" + result2 + ")><div class='box'><div class='container'><h2 class='title animated h1' data-animation='fadeInDown' style='color:#fff;'>" + dt.Rows[i]["bannerheader"].ToString() + "</h2><div class='animated' data-animation='fadeInUp' style='color: #fff;'>" + dt.Rows[i]["bannersubheader"].ToString() + "</div></div></div></div></a>";


                        }

                    }
                }
                phldhomebanner.Controls.Add(new Literal { Text = html.ToString() });
            }

        }

        private void BuildHTMLOffers()
        {
            // throw new NotImplementedException();
            BizDataCategory biz = new BizDataCategory();
            DataSet ds = biz.GetAllDetailsOffers();
            DataTable dt = ds.Tables[0];

            if (dt.Rows.Count > 0)
            {
                string html2 = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OfferEntity enteach = new OfferEntity();
                    string yourString = dt.Rows[i]["photourl"].ToString();
                    String result = yourString.Replace("~", "");
                    if (result != "")
                    {
                        string result1 = result.Replace("\\", "/");
                        string result2 = result1.Remove(0, 1);
                        result2 = result2.Replace(" ", "%20");

                        string offerpercent = dt.Rows[i]["offername"].ToString() + "%";
                        if (i == 0)
                        {
                            //for active status in div
                            html2 += "<div class='item active'><div class='carousel-col'><a href=" + result2 + "><img src=" + result2 + " class='offer-img'></a></div></div>";
                        }
                        //"<div class='item active'><div class='carousel-col'><div class='outer-div'><div class='inner-div'><div class='inner-div'><a href='#'><img src='" + result2 + "' alt='' class='offer-img'></a></div></div></div></div>";
                        else
                        {
                            html2 += "<div class='item'><div class='carousel-col'><a href=" + result2 + "><img src=" + result2 + " class='offer-img'></a></div></div>";
                        }
                    }
                }
                //string html = "<div class='item active'>"+html2+"</div>";
                offerplace.Controls.Add(new Literal { Text = html2.ToString() });
            }
        }

        private void BuildHTMLCategory()
        {
            BizDataCategory biz = new BizDataCategory();
            DataSet ds = biz.GetAllDetails();
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {
                string html = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string yourString = dt.Rows[i]["photourl"].ToString();
                    String result = yourString.Replace("~", "");
                    if (result != "")
                    {
                        string result1 = result.Replace("\\", "/");
                        string result2 = result1.Remove(0, 1);
                        result2 = result2.Replace(" ", "%20");
                        html += "<div class='col-xs-12 col-sm-6 col-md-4 col-lg-4'><article class='card'><div class='info'><span class='add-favorite added'></span><span></span></div><a href='DataWeb.aspx?Action=SubCategoryWeb&id=" + dt.Rows[i]["CategoryId"].ToString() + "'><div class='btn btn-add'><i class='icon icon-eye'></i></div></a><div class='figure-grid outer-div'><div class='image inner-div'><a href='DataWeb.aspx?Action=SubCategoryWeb&id=" + dt.Rows[i]["CategoryId"].ToString() + "'><img src=" + result2 + " alt='' width='360' height='241' class='img-hover-zoom--colorize' /></a></div><div class='text'><h2 class='title h4'><a href='DataWeb.aspx?Action=SubCategoryWeb&id=" + dt.Rows[i]["CategoryId"].ToString() + "'>" + dt.Rows[i]["displayname"].ToString() + "</a></h2><span class='description clearfix'>Gubergren amet dolor ea diam takimata consetetur facilisis blandit et aliquyam lorem ea duo labore diam sit et consetetur nulla</span></div></div></article></div>";
                    }
                }
                phldcategorylist.Controls.Add(new Literal { Text = html.ToString() });
            }
        }

        protected void callbackbtn_Click(object sender, EventArgs e)
        {
            if (callbackname.Value.Trim() == "" || callbackemail.Value.Trim() == "" || callbackmobile.Value.Trim() == "") { }
            else
            {
                BizDataUser biz = new BizDataUser();
                GeneralEntitie ge = new GeneralEntitie();
                ge.enquiryname = callbackname.Value;
                ge.enquirymobile = callbackmobile.Value;
                ge.enquiryemail = callbackemail.Value;
                ge.enquirycomments = callbackcomments.Value;
                biz.InsertCallBack(ge);
                callbackname.Value = "";
                callbackmobile.Value = "";
                callbackemail.Value = "";
                callbackcomments.Value = "";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "msg('Call Back sent sucessfully','success');", true);

            }
        }

        protected void btnsignup_Click(object sender, EventArgs e)
        {
            if (signupname.Value.Trim() == "" || signupemail.Value.Trim() == "" || signupmobileno.Value.Trim() == "") { }
            else
            {
                BizDataUser biz = new BizDataUser();
                GeneralEntitie ge = new GeneralEntitie();
                ge.enquiryname = signupname.Value;
                ge.enquirymobile = signupmobileno.Value;
                ge.enquiryemail = signupemail.Value;
                ge.enquirycomments = signupmessage.Value;
                biz.InsertCallBack(ge);
                signupname.Value = "";
                signupmobileno.Value = "";
                signupemail.Value = "";
                signupmessage.Value = "";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "msg('Sign up sent sucessfully','success');", true);

            }
        }
    }
}