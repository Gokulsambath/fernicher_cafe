﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="searchlist.ascx.cs" Inherits="fernichercafe.UserControls.searchlist" %>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twbs-pagination/1.4.2/jquery.twbsPagination.min.js"></script>

<style>
    .page {
        display: none;
    }

    .page-active {
        display: block;
    }
        @media only screen and (min-width: 320px) and (max-width:640px) {
        .btn.btn-buy {
        background-color: #ED7745;
        color: white;
        position: absolute;
        padding: 0;
        right: 3px;
        top: 3px;
        width: 40px;
        height: 40px;
        font-size: 20px;
        line-height: 39px;
        text-align: center;
        overflow: hidden;
        -moz-border-radius: 100%;
        -webkit-border-radius: 100%;
        border-radius: 100%;
        z-index: 20;
        }
        .btn.btn-buy:before{
            font-size:28px;
            line-height: 37px;
        }
        .btn.btn-buy:after{
            font-size:28px;
            line-height: 37px;
        }
    }
</style>
<style type="text/css">
    @media only screen and (min-width: 320px) and (max-width:640px) {
        .main-header {
            padding-top: 25px;
            
            height: 37vh;
        }

        header {
            margin-top: 160px;
        }
    }

    @media only screen and (min-width: 768px) and (max-width: 991px) {
        .main-header {
            padding-top: 130px;
            height: 50vh;
        }

        header {
            margin-top: 300px;
        }
    }


    @media only screen and (min-width: 992px) and (max-width: 1199px) {
        .main-header {
            padding-top: 100px;
            height: 70vh;
        }

        header {
            margin-top: 270px;
        }
    }

    @media only screen and (min-width: 1200px) and (max-width: 1400px) {
        .main-header {
            padding-top: 100px;
            margin-top: 0px;
            height: 70vh;
        }

        header {
            margin-top: 280px;
        }
    }
</style>

<section class="main-header" id="headerbanner" runat="server">
    <header>
        <div class="container">
            <h1 class="h2 title">Search Result</h1>
            <ol class="breadcrumb breadcrumb-inverted">
                <li><a href="DataWeb.aspx?Action=IndexWeb"><span class="icon icon-home"></span></a></li>

                <li><a class="active"></a>Search List</li>
            </ol>
        </div>
    </header>
</section>

<!-- ========================  Icons slider ======================== -->

<section class="owl-icons-wrapper">

    <!-- === header === -->

    <header class="hidden">
        <h2></h2>
    </header>

    <div class="container">

    
    </div>
    <!--/container-->
</section>

<!-- ======================== Products ======================== -->

<section class="products">

    <div class="container">

        <header class="hidden">
            <h3 class="h3 title">Search list</h3>
        </header>

        <div class="row">

            <!-- === product-filters === -->

          

            <!-- === product items === -->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

               

                <div class="row">

                    <asp:PlaceHolder ID="phldSearchlist" runat="server"></asp:PlaceHolder>
                    <!-- === product-item === -->






                </div>
                <!--/row-->
                <!--Pagination-->
                <div class="pagination-wrapper">
                    <ul id="pagination-demo" class="pagination-lg"></ul>
                </div>

                <input type="hidden" runat="server" id="hiddenpaginationcount" />
                <input type="hidden" runat="server" id="hiddensearchvalue" />
            </div>
            <!--/product items-->

        </div>
        <!--/row-->

    </div>
    <!--/container-->
</section>


<script>
   $('#pagination-demo').twbsPagination({
        totalPages: $('#MiddleCenterContent_ctl00_hiddenpaginationcount').val(),
        // the current page that show on start
        startPage: 1,

        // maximum visible pages
        //visiblePages: 2,

        initiateStartPageClick: true,

        // template for pagination links
        href: false,

        // variable name in href template for page number
        hrefVariable: '{{number}}',

        // Text labels
        //first: 'First',
        prev: 'Previous',
        next: 'Next',
        //last: 'Last',

        // carousel-style pagination
        loop: false,

        // callback function
        onPageClick: function (event, page) {
            $('.page-active').removeClass('page-active');
            $('.page' + page).addClass('page-active');
        },

        // pagination Classes
        paginationClass: 'pagination',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'hide',
        firstClass: 'hide',
        pageClass: 'page',
        activeClass: 'active',
        disabledClass: 'disabled'

    });
    var searchvalue = localStorage.getItem('searchvalue');
    $("#MiddleCenterContent_ctl00_hiddensearchvalue").val(searchvalue);
    var jsonStr = '{"ProductList":[]}';
    var storage = localStorage.getItem('Enquirystorage');
    var obj = JSON.parse(storage);
    if (obj == null) {
        localStorage.setItem('Enquirystorage', jsonStr);
    }
    function getenquiryproducts(elem, id, sub) {

        var jsonStr = localStorage.getItem('Enquirystorage');
        if (jsonStr == "") {
            var jsonStr1 = '{"ProductList":[]}';
            localStorage.setItem('Enquirystorage', jsonStr1);
            jsonStr = localStorage.getItem('Enquirystorage');
        }
        var obj = JSON.parse(jsonStr);

        var hasMatch = false;
        for (var index = 0; index < obj.ProductList.length; ++index) {

            var animal = obj.ProductList[index];

            if (animal.subcategoryid == sub && animal.productid == id) {
                hasMatch = true;
                break;
            }
        }
        if (hasMatch) {
            var aTags = document.getElementsByTagName("a");
            var searchText = "Contact Us";
            var found;

            for (var i = 0; i < aTags.length; i++) {
                if (aTags[i].textContent == searchText) {
                    found = aTags[i];                    
                    window.location = found;
                    break;
                }
            }
        } else {
            obj['ProductList'].push({ "subcategoryid": sub, "productid": id });
        }
        jsonStr = JSON.stringify(obj);

        localStorage.setItem('Enquirystorage', jsonStr);
        //console.log(localStorage.getItem('Enquirystorage'));
    }
    
</script>