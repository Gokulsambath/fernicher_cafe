﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GlobalHeaderWeb.ascx.cs" Inherits="fernichercafeWebsite.UserControls.GlobalHeader" %>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>

<nav class="navbar-fixed">

    <div class="container-fluid" style="padding-left: 0px; padding-right: 0px;">

        <!-- ==========  Top navigation ========== -->

        <div class="navigation navigation-top clearfix">
            <ul>
                <!--add active class for current page-->

                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="https://www.instagram.com/fernichercafe/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                <li><a href="https://twitter.com/search?q=fernicher%20cafe&src=typed_query" target="_blank"><i class="fa fa-twitter"></i></a></li>
                <li><a href="https://www.youtube.com/channel/UC14qmcaHxiJFvvsysiN7Ihg" target="_blank"><i class="fa fa-youtube"></i></a></li>


                <%--<li><a href="javascript:void(0);" class="open-login"><i class="icon icon-user"></i></a></li>--%>
                <li><a href="javascript:void(0);" class="open-search"><i class="icon icon-magnifier"></i></a></li>
                <!-- <li><a href="javascript:void(0);" class="open-cart"><i class="icon icon-cart"></i> <span>3</span></a></li> -->
            </ul>
        </div>
        <!--/navigation-top-->

        <!-- ==========  Main navigation ========== -->

        <div class="navigation navigation-main" style="padding-left: 0px; padding-right: 0px; color: black;">

            <!-- Setup your logo here-->

            <a href="DataWeb.aspx?Action=IndexWeb" style="padding: 0px 12px;">
                <img class="logo1" src="ContentWebsite/assets/logo7.png" alt="" width="25%" style="margin-bottom: 20px;" /></a>


            <!-- Mobile toggle menu -->

            <a href="#" class="open-menu"><i class="icon icon-menu"></i></a>

            <!-- Convertible menu (mobile/desktop)-->

            <div class="floating-menu floating-menu1">

                <!-- Mobile toggle menu trigger-->

                <div class="close-menu-wrapper" style="margin-top: 40px">
                    <span class="close-menu"><i class="icon icon-cross"></i></span>
                </div>

                <ul>
                    <li><a href="DataWeb.aspx?Action=IndexWeb">Home</a></li>

                    <!-- Multi-content dropdown -->

                    <li>
                        <a href="DataWeb.aspx?Action=About-UsWeb">About Us
                            <!-- <span class="open-dropdown"><i class="fa fa-angle-down"></i></span> -->
                        </a>

                    </li>

                    <!-- Mega menu dropdown -->

                    <li>
                        <a>Products<span class="open-dropdown"><i class="fa fa-angle-down"></i></span></a>
                        <div class="navbar-dropdown">
                            <div class="navbar-box">
                                <div class="box-2">
                                    <div class="box clearfix">
                                        <div class="row">
                                            <div class="clearfix">

                                                <asp:PlaceHolder ID="plhdnavbarquicklinks" runat="server"></asp:PlaceHolder>
                                            </div>
                                            <div class="clearfix">
                                            </div>
                                        </div>
                                    </div>
                                    <!--/box-->
                                </div>
                                <!--/box-2-->
                            </div>
                            <!--/navbar-box-->
                        </div>
                        <!--/navbar-dropdown-->
                    </li>
                    <!-- Single dropdown-->



                    <!-- Furniture icons in dropdown-->

                    <li>
                        <a href="DataWeb.aspx?Action=BrandsWeb">Brands</a>

                    </li>



                    <!-- Simple menu link-->

                    <li><a href="DataWeb.aspx?Action=Contact-UsWeb">Contact Us</a></li>
                </ul>
            </div>
            <!--/floating-menu-->
        </div>
        <!--/navigation-main-->

        <!-- ==========  Search wrapper ========== -->

        <div class="search-wrapper">

            <!-- Search form -->
            <input class="form-control" id="txtsearch" runat="server" placeholder="Sofa, Armchair, Berjers, Bookcase, Table, Coat hanger, Mirror, Plants, Grass, Greenwalls..." />
            
            <a class="btn btn-main btn-search" id="btnsearch">Search!</a>

        </div>



        <!-- ==========  Cart wrapper ========== -->
    </div>
    <!--/container-->
</nav>

<script>
    $(document).ready(function () {
        $(document).ajaxStart(function () {
            $(".page-loader").show();
        });

        $(document).ajaxStop(function () {
            $(".page-loader").hide();
        });
        $("#btnsearch").on('click', function () {
            var id = $("#GlobalHeaderContent_ctl00_txtsearch").val();
            var href = "DataWeb.aspx?Action=searchlist&id=";
            href = href.concat(id);         
             
            $(this).attr("href", href);
            $(this).attr("onclick", "").unbind("click");
            $("#GlobalHeaderContent_ctl00_txtsearch").val("");
            
            $(this).trigger('click');

        });

    });
    
   
</script>
