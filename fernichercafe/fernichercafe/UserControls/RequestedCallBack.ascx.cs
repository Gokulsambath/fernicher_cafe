﻿using fernicherLibrary.BizData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fernichercafe.UserControls
{
    public partial class RequestedCallBack : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BuildHTMLTable();
        }
        private void BuildHTMLTable()
        {
            plhdtblcallback.Controls.Clear();
            BizDataUser objBizDataUser = new BizDataUser();
            DataSet dsUser = objBizDataUser.GetAllcallback();


            StringBuilder strAccount = new StringBuilder();
            if (dsUser.Tables[0].Rows.Count > 0)
            {
                for (int intCount = 0; intCount < dsUser.Tables[0].Rows.Count; intCount++)
                {
                    
                    strAccount.Append("<tr>");

                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["name"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["email"].ToString());
                    strAccount.Append("</td>");

                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["mobile"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["comments"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(Convert.ToDateTime(dsUser.Tables[0].Rows[intCount]["CreatedOn"].ToString()).ToString("dd-MMM-yyyy HH:mm", CultureInfo.InvariantCulture));
                    strAccount.Append("</td>");
                    strAccount.Append("</tr>");


                }

                plhdtblcallback.Controls.Add(new Literal { Text = strAccount.ToString() });
            }
        }

    }
}