﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="productEnquiry.ascx.cs" Inherits="fernichercafe.UserControls.productEnquiry" %>

  <!-- BEGIN: Page Main-->
    <div id="main">
      <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
          <!-- Search for small screen-->
          <div class="container">
            <div class="row">
              <div class="col s10 m6 l6">
                <h5 class="breadcrumbs-title mt-0 mb-0">Feedback Inbox</h5>
              </div>
            </div>
          </div>
        </div>
        <div class="col s12">
          <div class="container">
            <div class="section section-data-tables">

<div class="row">
    <div class="col s12 m12 l12">
      <div  class="card card card-default scrollspy">
        <div class="card-content">
          <div class="col s12">
           
              
               
                  <div class="row">
            <div class="col s12">
              <table id="page-length-option" class="display">
                <thead>
                  <tr>
                    <th width="20%">Product Name</th>
                    <th width="20%">User Name</th>
                    <th  width="15%">Email</th>
                    <th width="15%">Phone Number</th>
                    <th  width="50%">Comments</th>
                      <th  width="50%">Date</th>
                  </tr>
                </thead>
                <tbody>
                 <asp:PlaceHolder ID="plhdtblenquiry" runat="server"></asp:PlaceHolder>
               
                </tbody>
                
              </table>
            </div>
          </div>
               
                        
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- Scroll - vertical, dynamic height -->

  

  <!-- Scroll - Vertical and Horizontal -->

 

  <!-- Multi Select -->


</div><!-- START RIGHT SIDEBAR NAV -->

<!-- END RIGHT SIDEBAR NAV -->
          
          </div>
        </div>
      </div>
    </div>
    <!-- END: Page Main-->

<footer class="page-footer footer footer-static footer-dark gradient-45deg-indigo-purple gradient-shadow navbar-border navbar-shadow">
    <div class="footer-copyright">
        <div class="container"><span>&copy; Fernicher Cafe 2019</span></div>
    </div>
</footer>
