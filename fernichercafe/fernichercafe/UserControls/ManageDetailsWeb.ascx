﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManageDetailsWeb.ascx.cs" Inherits="fernichercafeWebsite.UserControls.ManageDetails" %>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<link media="screen" rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
<style>
    .checked {
        color: orange;
    }

    .video-floating-btn {
        position: fixed;
        width: 60px;
        height: 60px;
        bottom: 19px;
        left: 27px;
        background: linear-gradient(to right,#FF7B02,#B42868);
        color: #FFF;
        border-radius: 50px;
        text-align: center;
        z-index: 999;
        /*box-shadow: 2px 2px 3px #ED7745;*/
    }

    .video-floating-icon {
        margin-top: 22px;
    }

    .video-popup {
        display: none;
        position: fixed;
        bottom: 0;
        left: 15px;
        /*border: 3px solid #f1f1f1;*/
        z-index: 9;
    }

    .video-container {
        max-width: 500px;
        padding: 10px;
        background-color: white;
        z-index: 999;
        margin-bottom: 76px;
    }

    @media only screen and (min-width: 320px) and (max-width: 640px) {
        .video-container {
            width: 322px;
            padding: 10px;
            background-color: white;
            margin-bottom: 76px;
        }

        .video-popup {
            display: none;
            position: fixed;
            bottom: 0;
            left: 21px;
            z-index: 9;
        }

        iframe {
            width: 300px;
            height: 200px;
        }
    }
</style>
<style>
    /* Kid Accessory CSS Start*/
    .accessory-card {
        border: 1px solid #ED7745;
        margin-bottom: 20px;
        position: relative;
        top: 0px;
        -webkit-transition: -webkit-box-shadow .25s;
        -moz-transition: box-shadow .25s;
        -o-transition: box-shadow .25s;
        transition: -webkit-box-shadow .25s;
        transition: box-shadow .25s;
        transition: box-shadow .25s, -webkit-box-shadow .25s;
        border-radius: 2px;
        /*background-color: #fff;*/
        -webkit-box-shadow: 0 2px 12px 0 rgba(0,0,0,0.17);
        box-shadow: 0 2px 12px 0 rgba(0,0,0,0.17);
    }

        .accessory-card:hover {
            transition: top ease 0.5s;
            top: -10px;
            border: 1px solid #B42868;
        }

    .accessory-wrapper {
        margin: 10px;
    }

    .accessory-text {
        margin-top: 20px;
    }

    .ass-product-name {
        font-size: 1.7rem;
        margin-bottom: 0px;
        font-weight: 500;
    }

    .ass-modal-name {
        font-size: 1.5rem;
    }

    .ass-width {
        font-size: 1.2rem;
        margin-bottom: 0px;
    }

    .ass-depth {
        font-size: 1.2rem;
        margin-bottom: 0px;
    }

    .ass-height {
        font-size: 1.2rem;
        margin-bottom: 0px;
    }

    .ass-width-div {
        border-right: 1px solid #ED7745;
    }

    .ass-depth-div {
        border-right: 1px solid #ED7745;
    }

    

    /* Kid Accessory CSS End*/

</style>
<style>
      @media only screen and (min-width: 320px) and (max-width:640px) {
        .main-header {
            padding-top: 25px;
            
            height: 37vh;
        }

        header {
            margin-top: 160px;
        }
    }

    @media only screen and (min-width: 768px) and (max-width: 991px) {
        .main-header {
            padding-top: 130px;
            height: 50vh;
        }

        header {
            margin-top: 300px;
        }
    }


    @media only screen and (min-width: 992px) and (max-width: 1199px) {
        .main-header {
            padding-top: 100px;
            height: 70vh;
        }

        header {
            margin-top: 270px;
        }
    }

    @media only screen and (min-width: 1200px) and (max-width: 1400px) {
        .main-header {
            padding-top: 100px;
            margin-top: 0px;
            height: 70vh;
        }

        header {
            margin-top: 280px;
        }
    }
</style>
<script>
    async function msg(msg, title) {
        const msgss = await showpop(msg, title);
        window.history.replaceState('', '', window.location.href)

    }
    function showpop(msg, title) {
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "12000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        // toastr['success'](msg, title);
        var d = Date();
        if (title != 'success') {
            title = '';
            toastr.error(msg, title);
        }
        else {
            title = '';
            toastr.success(msg, title);
        }
        return false;
    }
</script>


<section class="main-header" id="headerbanner" runat="server">
    <header>
        <div class="container">
            <h1 class="h2 title" id="titleh1" runat="server"></h1>
            <ol class="breadcrumb breadcrumb-inverted">
                <li><a href="DataWeb.aspx?Action=IndexWeb"><span class="icon icon-home"></span></a></li>
                <li><a id="navtitle" runat="server"></a></li>
                <li><a id="navtitle1" runat="server"></a></li>
                <li><a class="active" id="navtitle2" runat="server"></a></li>
            </ol>
        </div>
    </header>
</section>

<!-- ========================  Product ======================== -->

<section class="product">
    <div class="main">
        <div class="container">
            <div class="row product-flex">

                <!-- product flex is used only for mobile order -->
                <!-- on mobile 'product-flex-info' goes bellow gallery 'product-flex-gallery' -->

                <div class="col-md-4 col-sm-12 product-flex-info">
                    <div class="clearfix">

                        <!-- === product-title === -->


                        <asp:PlaceHolder ID="titlelbl" runat="server"></asp:PlaceHolder>

                        <div class="clearfix">

                            <!-- === price wrapper === -->

                            <div class="price">
                                <asp:PlaceHolder ID="priceplaceholder" runat="server"></asp:PlaceHolder>

                            </div>
                            <hr />

                            <!-- === info-box === -->

                            <div class="info-box" id="manufacturediv" runat="server">
                                <span><strong>Manufacture</strong></span>
                                <span id="manufacturelbl" runat="server"></span>
                            </div>
                            <!-- === info-box === -->

                            <div class="info-box" id="modelnamediv" runat="server">
                                <span><strong>Model Name</strong></span>
                                <span id="modelnamelbl" runat="server"></span>
                            </div>

                            <!-- === info-box === -->

                            <div class="info-box" id="sizediv" runat="server">
                                <span><strong>Size</strong></span>
                                <span id="sizelbl" runat="server"></span>
                            </div>

                            <!-- === info-box === -->

                            <div class="info-box" id="areadiv" runat="server">
                                <span><strong>Area</strong></span>
                                <span id="arealbl" runat="server"></span>
                            </div>


                            <!-- === info-box === -->

                            <div class="info-box" id="materialsdiv" runat="server">
                                <span><strong>Materials</strong></span>
                                <span id="materialslbl" runat="server"></span>
                            </div>

                            <!-- === info-box === -->

                            <div class="info-box" id="availablediv" runat="server">
                                <span><strong>Availability</strong></span>
                                <span id="availimg" runat="server"></span>

                            </div>

                            <hr />

                           
                            <asp:PlaceHolder ID="phldavailablecolors" runat="server"></asp:PlaceHolder>
                            <asp:PlaceHolder ID="phldcertificate" runat="server"></asp:PlaceHolder>

                        </div>
                        <!--/clearfix-->
                    </div>
                    <!--/product-info-wrapper-->
                </div>
                <!--/col-md-4-->
                <!-- === product item gallery === -->

                <div class="col-md-8 col-sm-12 product-flex-gallery">

                    <!-- === add to cart === -->


                    <a href="#enquiry" class="btn btn-buy-details mfp-open" data-text="Enquiry "></a>


                    <!-- === product gallery === -->

                    <div class="owl-product-gallery open-popup-gallery">

                        <asp:PlaceHolder ID="imagesplaceholder" runat="server"></asp:PlaceHolder>


                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- === product-info === -->

    <div class="info">
        <div class="container">
            <div class="row">



                <div class="col-md-12">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation">
                            <a href="#specification" aria-controls="specification" role="tab" data-toggle="tab">
                                <i class="icon icon-sort-alpha-asc"></i>
                                <span>Specification</span>
                            </a>
                        </li>
                        <li role="presentation" class="active" id="accessorydiv">
                            <a href="#accessory" aria-controls="accessory" role="tab" data-toggle="tab">
                                <i class="icon icon-user"></i>
                                <span>Accessory</span>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#rating" aria-controls="rating" role="tab" data-toggle="tab">
                                <i class="icon icon-thumbs-up"></i>
                                <span>Rating</span>
                            </a>
                        </li>
                    </ul>

                    <!-- === tab-panes === -->

                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane" id="specification">
                            <div class="content">

                                <!-- === designer collection title === -->

                                <h3>Technical Details</h3>

                                <div class="products">
                                    <div class="row">

                                        <!-- === product-item === -->

                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <article>
                                                <div class="figure-grid">
                                                    <div class="image">
                                                        <div class="technical-details">
                                                            <ul>
                                                                <asp:PlaceHolder ID="technicaldetailslist" runat="server"></asp:PlaceHolder>

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>

                                        <!-- === product-item === -->

                                    </div>
                                    <!--/row-->
                                </div>
                                <!--/products-->
                            </div>
                            <!--/content-->
                        </div>
                        <!--/tab-pane-->


                        <!-- ============ tab #2 ============ -->

                        <div role="tabpanel" class="tab-pane active" id="accessory">
                            <div class="content">
                                <div class="row">
                                    <h3>Furniture & Accessory</h3>

                                    <asp:PlaceHolder ID="phldaccessory" runat="server"></asp:PlaceHolder>
                                </div>
                                <!--/row-->
                            </div>
                            <!--/content-->
                        </div>
                        <!--/tab-pane-->
                        <!-- ============ tab #3 ============ -->

                        <div role="tabpanel" class="tab-pane" id="rating">

                            <!-- ============ ratings ============ -->

                            <div class="content">
                                <h3>Rating</h3>

                                <div class="row">
                                    <!-- === comments === -->
                                    <div class="col-md-12">
                                        <div class="comments">
                                            <asp:PlaceHolder ID="ratingsdone" runat="server"></asp:PlaceHolder>

                                            <div class="comment-header">
                                                <!--  <a href="#" class="btn btn-clean-dark">12 comments</a> -->
                                            </div>
                                            <!--/comment-header-->
                                            <!-- === add comment === -->
                                            <div class="comment-add">
                                                <div class="comment-reply-message">
                                                    <div class="h3 title">Leave a Reply </div>
                                                    <p>Your email address will not be published.</p>
                                                </div>

                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" runat="server" id="ratingname" name="name" value="" placeholder="Your Name" />

                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group" style="margin-top: 15px">
                                                            <input type="email" class="form-control" runat="server" id="ratingemail" name="email" value="" placeholder="Your Email" />

                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="form-group">
                                                            <textarea rows="3" runat="server" id="ratingcomment" class="form-control" placeholder="Your comment"></textarea>

                                                        </div>
                                                    </div>
                                                </div>

                                                <input type="hidden" runat="server" id="ratingstarvalue" />
                                                <fieldset class="rating">
                                                    <input type="radio" id="star5" name="rating" value="5" /><label class="full" for="star5" title="Awesome - 5 stars"></label>
                                                    <input type="radio" id="star4half" name="rating" value="4.5" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                                                    <input type="radio" id="star4" name="rating" value="4" /><label class="full" for="star4" title="Pretty good - 4 stars"></label>
                                                    <input type="radio" id="star3half" name="rating" value="3.5" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                                                    <input type="radio" id="star3" name="rating" value="3" /><label class="full" for="star3" title="Meh - 3 stars"></label>
                                                    <input type="radio" id="star2half" name="rating" value="2.5" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                                                    <input type="radio" id="star2" name="rating" value="2" /><label class="full" for="star2" title="Kinda bad - 2 stars"></label>
                                                    <input type="radio" id="star1half" name="rating" value="1.5" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                                                    <input type="radio" id="star1" name="rating" value="1" /><label class="full" for="star1" title="Sucks big time - 1 star"></label>
                                                    <input type="radio" id="starhalf" name="rating" value="0.5" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
                                                </fieldset>
                                                <div class="clearfix text-center">
                                                    <input type="button" id="btnrating" value="Add Comment" class="btn btn-main" onclick="submitrating()" />
                                                    <%--<asp:Button runat="server" ID="btnrating" class="btn btn-main" UseSubmitBehavior="false" OnClick="btnrating_Click" Text="Add Comment" />--%>
                                                </div>
                                            </div>
                                            <!--/comment-add-->
                                        </div>
                                        <!--/comments-->
                                    </div>
                                </div>
                                <!--/row-->
                                <!--/row-->
                            </div>
                            <!--/content-->
                        </div>
                        <!--/tab-pane-->
                    </div>
                    <!--/tab-content-->
                </div>
            </div>
            <!--/row-->
        </div>
        <!--/container-->
    </div>
    <!--/info-->
</section>



<!-- ========================  Enquiry popup - quick view ======================== -->
<div class="popup-main mfp-hide" id="enquiry">
    <!-- === product popup === -->
    <div class="product">

        <!-- === popup-title === -->
        <div class="popup-title">
            <div class="h1 title" id="enqheader" runat="server"></div>
        </div>
        <!-- === product-popup-info === -->
        <div class="popup-content">
            <div class="product-info-wrapper">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="form-group">
                            <input type="text" class="form-control" id="enqname" name="enqname" value="" placeholder="Your Name" />
                            
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="enqemail" id="enqemail" value="" placeholder="Your E-mail" />
                            
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="enqmobile" id="enqmobile" value="" placeholder="Your Mobile No" />
                         
                        </div>
                        <div class="form-group">
                            <textarea rows="6" class="form-control" id="enqcomments" name="enqcomments" placeholder="Your comments"></textarea>
                           
                        </div>
                        <div class="clearfix text-center">
                            <input type="button" ID="Button1" class="btn btn-main" onclick="addenquiry()" value="Add enquiry" />
                            
                        </div>

                    </div>
                </div>
                <!--/row-->
            </div>
            <!--/product-info-wrapper-->
        </div>
        <!--/popup-content-->
        <!-- === product-popup-footer === -->

    </div>
    <!--/product-->
</div>
<!--popup-main-->

<!--------------Floating Button---->
<a class="video-floating-btn" id="videodiv" runat="server" onclick="openForm()"><i class="fa fa-video-camera video-floating-icon"></i></a>
<!-- ========================  Video popup - quick view ======================== -->
<div class="video-popup" id="videopopup" runat="server">
    <div class="video-container">
        <iframe width="480" height="315" id="videolink" runat="server" src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        <button type="button" class="btn btn-block" onclick="closeForm()" style="background-color: #ff7b02; color: #ffffff;">Close</button>
    </div>
</div>

<script>
    $('#btnrating').attr('disabled', true);
    if (getQueryString('category') != "Kid") {
        $("#accessory").css("display", "none");
        $("#accessorydiv").css("display", "none");
    }
    $('input[name=rating]').click(function () {
        var value = $(this).val();
        $('#MiddleCenterContent_ctl00_ratingstarvalue').val(value);
        $('#btnrating').attr('disabled', false);
    });
    function getQueryString(field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    function submitrating() {
        //var productids = parseURLParams(id);
        var url_string = window.location;
        var url = new URL(url_string);
        var categoryname = url.searchParams.get("category");
        var tvid = url.searchParams.get("id");

        if ($('#ratingname').val() != undefined ||
            $('#ratingemail').val() != undefined ||
            $('#ratingcomment').val() != undefined ||
            $('#ratingname').val() != "" ||
            $('#ratingemail').val() != "" ||
            $('#ratingcomment').val() != "") {
            var obj = {};
            obj.name = $('#MiddleCenterContent_ctl00_ratingname').val();
            obj.message = $('#MiddleCenterContent_ctl00_ratingcomment').val();
            obj.email = $('#MiddleCenterContent_ctl00_ratingemail').val();
            obj.rating = $('#MiddleCenterContent_ctl00_ratingstarvalue').val();
            obj.categoryname = categoryname;
            obj.productid = tvid;
            $.ajax({
                type: "POST",
                url: "userwebservice.asmx/AddRating",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                   
                    toastr.success('Rating Added sucessfully', 'success');
                    location.reload();
                    window.history.replaceState('', '', window.location.href)

                }
            });
        } else {
            alert('Fill All Fields');
        }
    }

     function addenquiry() {
        
        var url_string = window.location;
        var url = new URL(url_string);
        var categoryname = url.searchParams.get("category");
        var tvid = url.searchParams.get("id");

        if ($('#enqname').val() != undefined ||
            $('#enqemail').val() != undefined ||
            $('#enqcomments').val() != undefined || $('#enqmobile').val() != undefined ||
            $('#enqname').val() != "" ||
            $('#enqemail').val() != "" || $('#enqmobile').val() != "" ||
            $('#enqcomments').val() != "") {
            var obj = {};
            obj.name = $('#enqname').val();
            obj.comments = $('#enqcomments').val();
            obj.email = $('#enqemail').val();
            obj.mobile = $('#enqmobile').val();
            obj.categoryname = categoryname;
            obj.productid = tvid;
            var ProductList = "ProductList";
            $.ajax({
                type: "POST",
                url: "userwebservice.asmx/AddEnquiry",
                data: "{'obj':'{"+ProductList+": [" + JSON.stringify(obj) + "] }'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                   
                    toastr.success('Enquiry sent sucessfully', 'success');
                    location.reload();
                    window.history.replaceState('', '', window.location.href)

                }
            });
        } else {
            alert('Fill All Fields');
        }
    }
    function openForm() {
        //document.getElementById("videopopup").style.display = "block";
        $('#MiddleCenterContent_ctl00_videopopup').css('display', 'block');
    }

    function closeForm() {
        //document.getElementById("videopopup").style.display = "none";
        $('#MiddleCenterContent_ctl00_videopopup').css('display', 'none');
    }
</script>
