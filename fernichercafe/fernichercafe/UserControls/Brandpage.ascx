﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Brandpage.ascx.cs" Inherits="fernichercafe.UserControls.Brandpage" %>


<div id="main">
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s10 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0">Brands Page</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12">
            <form runat="server">
                <div class="container">
                    <div class="section section-data-tables">

                        <div class="row">
                            <div class="col s12 m12 l12">
                                <div id="basic-tabs" class="card card card-default scrollspy">
                                    <div class="card-content">
                                        <div class="col s12">
                                            <div class="row" id="main-view">
                                                <div class="col s12">
                                                    <div id="category" class="col s12" style="margin-top: 1rem;">
                                                        <div class="row">
                                                            <div class="input-field col s6 m6 l6">
                                                                <label>Brand Header</label>
                                                                <input type="text" class="validate" id="txtbrandheader" runat="server">
                                                            </div>
                                                            <div class="input-field col s6 m6 l6">
                                                                <label>Brand Sub Header</label>
                                                                <input type="text" class="validate" id="txtbrandsubheader" runat="server">
                                                            </div>
                                                            <div class="input-field col s12">
                                                                <asp:Button type="button" ID="btnsave" runat="server" OnClick="btnsave_Click" Text="save" class="modal-action waves-effect waves-green btn btn-info"></asp:Button>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
            </form>
        </div>
    </div>
</div>


<footer class="page-footer footer footer-static footer-dark gradient-45deg-indigo-purple gradient-shadow navbar-border navbar-shadow">
    <div class="footer-copyright">
        <div class="container"><span>&copy; Fernicher Cafe 2019</span></div>
    </div>
</footer>

