﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GlobalHeader.ascx.cs" Inherits="fernichercafe.UserControls.GlobalHeader" %>

<style>
    .sidenav li a.active {
        color: #fff;
        background: -webkit-linear-gradient(45deg, #303f9f, #7b1fa2) !important;
        background: -moz- oldlinear-gradient(45deg, #303f9f, #7b1fa2) !important;
        background: -o-linear-gradient(45deg, #303f9f, #7b1fa2) !important;
        background: linear-gradient(45deg, #303f9f, #7b1fa2) !important;
        background: linear-gradient(45deg, #303f9f, #7b1fa2) !important;
        -webkit-box-shadow: 3px 3px 20px 0 rgba(123, 31, 162, .5);
        box-shadow: 3px 3px 20px 0 rgba(123, 31, 162, .5);
    }
</style>

<script>    $(document).ready(function () {
        var me = "Data?Action=" + getUrlVars()["Action"] + "";
        if (me.endsWith("#")) {
            me = me.slice(0, -1);
        }
        var anchors = $('li.status a');
        for (var i = 0, l = $('li.status a').length; i < l; i++) {

            $(anchors[i]).removeClass("active");
            var orig = "Data?Action=" + geturl(anchors[i].href)["Action"] + "";
            if (me == "Data?Action=Subcategory") {
                $(anchors[0]).addClass("active");
                break;
            }
            if (me == orig) {
                $(anchors[i]).addClass("active");
                $(anchors[i]).parents('li.bold.headstatus').addClass('active');
                break;
            }
            
        }

    });

    //get param for passed url
    function geturl(actionsss) {
        var vars = [], hash;
        var hashes = actionsss.slice(actionsss.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }

    //get param for current url
    function getUrlVars() {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }
</script>

<aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light sidenav-active-square">
    <div class="brand-sidebar">
        <h1 class="logo-wrapper"><a class="brand-logo darken-1" href="#"><span class="logo-text hide-on-med-and-down"></span>Fernicher Cafe</a><a class="navbar-toggler" href="#"><i class="material-icons">radio_button_checked</i></a></h1>
    </div>
    <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">

        <li class="bold status"><a href="Data.aspx?Action=Category" class="waves-effect"><i class="material-icons">view_comfy</i><span class="hide-menu">Manage Categories</span></a>
        </li>
        <li class="bold headstatus"><a class="collapsible-header waves-effect waves-cyan " href="#"><i class="material-icons">local_parking</i><span class="menu-title" data-i18n="">Products</span></a>
            <div class="collapsible-body">
                <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                    <li class="bold status"><a href="Data.aspx?Action=productskid" class="waves-effect"><i class="material-icons">child_care</i><span class="hide-menu">Kid Products</span></a>
                    </li>
                    <li class="bold status"><a href="Data.aspx?Action=productslivingroom" class="waves-effect"><i class="material-icons">spa</i><span class="hide-menu">Living Room Products</span></a>
                    </li>
                    <li class="bold status"><a href="Data.aspx?Action=productsgreenery" class="waves-effect"><i class="material-icons">nature</i><span class="hide-menu">Greenery Products</span></a>
                    </li>

                    <li class="bold status"><a href="Data.aspx?Action=productsAccessory" class="waves-effect"><i class="material-icons">weekend</i><span class="hide-menu">Accessories</span></a>
                    </li>

                    <li class="bold status"><a href="Data.aspx?Action=ProductsMapping" class="waves-effect"><i class="material-icons">share</i><span class="hide-menu">Map Products</span></a>
                    </li>
                </ul>
            </div>
        </li>
       


        <li class="bold headstatus"><a class="collapsible-header waves-effect waves-cyan" href="#"><i class="material-icons">border_color</i><span class="menu-title" data-i18n="">Customize Pages</span></a>
            <div class="collapsible-body">
                <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                    <li class="bold status"><a href="Data.aspx?Action=UserProfile" class="waves-effect waves-cyan"><i class="material-icons">radio_button_unchecked</i><span class="hide-menu">Profile</span></a>
                    </li>
                    <li class="bold status"><a href="Data.aspx?Action=HomeBanner" class="waves-effect"><i class="material-icons">radio_button_unchecked</i><span class="hide-menu">Website Home Page</span></a>
                    </li>
                    <li class="bold status"><a href="Data.aspx?Action=HomePageProductConcept" class="waves-effect"><i class="material-icons">radio_button_unchecked</i><span class="hide-menu">Website Product Concept</span></a>
                    </li>
                    <li class="bold status"><a href="Data.aspx?Action=AboutUs" class="waves-effect waves-cyan"><i class="material-icons">radio_button_unchecked</i><span class="hide-menu">About Us</span></a>
                    </li>
                     <li class="bold status"><a href="Data.aspx?Action=LabelControls" class="waves-effect waves-cyan"><i class="material-icons">radio_button_unchecked</i><span class="hide-menu">Label Controls</span></a>
                    </li>
                     <li class="bold status"><a href="Data.aspx?Action=Brandpage" class="waves-effect waves-cyan"><i class="material-icons">radio_button_unchecked</i><span class="hide-menu">Brand Page</span></a>
                    </li>
                    
                </ul>
            </div>
        </li>
           <li class="bold headstatus"><a class="collapsible-header waves-effect waves-cyan" href="#"><i class="material-icons">local_offer</i><span class="menu-title" data-i18n="">Brands & Offers</span></a>
            <div class="collapsible-body">
                <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                   
                    <li class="bold status"><a href="Data.aspx?Action=productsoffer" class="waves-effect"><i class="material-icons">radio_button_unchecked</i><span class="hide-menu">Product offers</span></a>
                    </li>
                   

                    <li class="bold status"><a href="Data.aspx?Action=Brands" class="waves-effect"><i class="material-icons">radio_button_unchecked</i><span class="hide-menu">Brands</span></a>
                    </li>
                    
                </ul>
            </div>
        </li>
         <li class="bold headstatus"><a class="collapsible-header waves-effect waves-cyan" href="#"><i class="material-icons">contact_phone</i><span class="menu-title" data-i18n="">Contact Users</span></a>
            <div class="collapsible-body">
                <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                    <li class="bold status"><a href="Data.aspx?Action=productEnquiry" class="waves-effect"><i class="material-icons">radio_button_unchecked</i><span class="hide-menu">Enquiries</span></a>
                    </li>
                    <li class="bold status"><a href="Data.aspx?Action=CallBack" class="waves-effect"><i class="material-icons">radio_button_unchecked</i><span class="hide-menu">Requested Call Back's</span></a>
                    </li>
                    <li class="bold status"><a href="Data.aspx?Action=Subscription" class="waves-effect"><i class="material-icons">radio_button_unchecked</i><span class="hide-menu">Subscribe Users</span></a>
                    </li>
                </ul>
            </div>
        </li>
    </ul>
    <div class="navigation-background"></div>
    <a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
</aside>
<!-- END: SideNav-->
