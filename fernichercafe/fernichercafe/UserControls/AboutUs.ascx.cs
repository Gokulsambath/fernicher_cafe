﻿using DairymanLibrary.Entity;
using fernicherLibrary.BizData;
using fernicherLibrary.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fernichercafe.UserControls
{
    public partial class AboutUs : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {

                Response.Redirect("Login.aspx");
            }
            if (!IsPostBack)
            {

                FillProfileValues();
            }

        }

        private void FillProfileValues()
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                BizDataUser obj = new BizDataUser();
                DataTable dt = obj.GetAboutUs();
                if (dt.Rows.Count > 0)
                {
                   txtbannerheader.Value =  dt.Rows[0]["BannerHeader"].ToString();
                   txtbannersubheader.Value = dt.Rows[0]["BannerSubHeader"].ToString();
                    txthistoryheader.Value = dt.Rows[0]["HistoryHeader"].ToString();
                    txthistorysubheader.Value = dt.Rows[0]["HsistorySubHeader"].ToString();
                    title1.Value = dt.Rows[0]["Title1"].ToString();
                    title1desc.Value = dt.Rows[0]["Title1Description"].ToString();
                    title2.Value = dt.Rows[0]["Title2"].ToString();
                    title2desc.Value = dt.Rows[0]["Title2Description"].ToString();
                    txtdirectorheader.Value = dt.Rows[0]["DirectorHeader"].ToString();
                    txtdirectorheaderdesc.Value = dt.Rows[0]["DirectorHeaderDescription"].ToString();
                    director1.Value = dt.Rows[0]["Director1"].ToString();
                    director1desc.Value = dt.Rows[0]["Director1Description"].ToString();
                    director2.Value = dt.Rows[0]["Director2"].ToString();
                    director2desc.Value = dt.Rows[0]["Director2Description"].ToString();
                    txtmissionheader.Value = dt.Rows[0]["MissionHeader"].ToString();
                    missiondesc.Value = dt.Rows[0]["MissionDescription"].ToString();
                    string final1 = "", finalresult1 = "";
                    string final2 = "", finalresult2 = "";
                    string final3 = "", finalresult3 = "";
                    string final4 = "", finalresult4 = "";
                    string final5 = "", finalresult5 = "";
                    string final6 = "", finalresult6 = "";
                    string yourString1 = dt.Rows[0]["Bannerphoto"].ToString();
                    string yourString2 = dt.Rows[0]["Title1photo"].ToString();
                    string yourString3 = dt.Rows[0]["Title2photo"].ToString();
                    string yourString4 = dt.Rows[0]["Director1photo"].ToString();
                    string yourString5 = dt.Rows[0]["Director2photo"].ToString();
                    string yourString6 = dt.Rows[0]["Missionphoto"].ToString();
                    String result1 = yourString1.Replace("~", "");
                    String result2 = yourString2.Replace("~", "");
                    String result3 = yourString3.Replace("~", "");
                    String result4 = yourString4.Replace("~", "");
                    String result5 = yourString5.Replace("~", "");
                    String result6 = yourString6.Replace("~", "");
                    if (result1 != "")
                    {
                        final1 = result1.Replace("\\", "/");
                        finalresult1 = final1.Remove(0, 1);
                    }
                    BannerFile.Attributes.Add("value", dt.Rows[0]["Bannerphoto"].ToString());
                    filepathedit1.Value = dt.Rows[0]["Bannerphoto"].ToString();
                    filenamelabel1.InnerText = finalresult1;
                    if (result2 != "")
                    {
                        final2 = result2.Replace("\\", "/");
                        finalresult2 = final2.Remove(0, 1);
                    }
                    title1file.Attributes.Add("value", dt.Rows[0]["Title1photo"].ToString());
                    filepathedit2.Value = dt.Rows[0]["Title1photo"].ToString();
                    filenamelabel2.InnerText = finalresult2;
                    if (result3 != "")
                    {
                        final3 = result3.Replace("\\", "/");
                        finalresult3 = final3.Remove(0, 1);
                    }
                    title2file.Attributes.Add("value", dt.Rows[0]["Title2photo"].ToString());
                    filepathedit3.Value = dt.Rows[0]["Title2photo"].ToString();
                    filenamelabel3.InnerText = finalresult3;
                    if (result4 != "")
                    {
                        final4 = result4.Replace("\\", "/");
                        finalresult4 = final4.Remove(0, 1);
                    }
                    director1file.Attributes.Add("value", dt.Rows[0]["Director1photo"].ToString());
                    filepathedit4.Value = dt.Rows[0]["Director1photo"].ToString();
                    filenamelabel4.InnerText = finalresult4;
                    if (result5 != "")
                    {
                        final5 = result5.Replace("\\", "/");
                        finalresult5 = final5.Remove(0, 1);
                    }
                    director2file.Attributes.Add("value", dt.Rows[0]["Director2photo"].ToString());
                    filepathedit5.Value = dt.Rows[0]["Director2photo"].ToString();
                    filenamelabel5.InnerText = finalresult5;
                    if (result6 != "")
                    {
                        final6 = result6.Replace("\\", "/");
                        finalresult6 = final6.Remove(0, 1);
                    }
                    missionfile.Attributes.Add("value", dt.Rows[0]["Missionphoto"].ToString());
                    filepathedit6.Value = dt.Rows[0]["Missionphoto"].ToString();
                    filenamelabel6.InnerText = finalresult6;
                }

            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {
                Response.Redirect("Login.aspx");
            }
            else
            {

                GeneralEntitie sourcelead = new GeneralEntitie();
                sourcelead.bannerheader = txtbannerheader.Value;
                sourcelead.bannersubheader = txtbannersubheader.Value;
                sourcelead.historyheader = txthistoryheader.Value;
                sourcelead.historysubheader = txthistorysubheader.Value;
                sourcelead.title1 = title1.Value;
                sourcelead.title1desc = title1desc.Value;
                sourcelead.title2 = title2.Value;
                sourcelead.title2desc = title2desc.Value;
                sourcelead.directorheader = txtdirectorheader.Value;
                sourcelead.directorheaddesc = txtdirectorheaderdesc.Value;
                sourcelead.director1 = director1.Value;
                sourcelead.director1desc = director1desc.Value;
                sourcelead.director2 = director2.Value;
                sourcelead.director2desc = director2desc.Value;
                sourcelead.missionheader = txtmissionheader.Value;
                sourcelead.missiondesc = missiondesc.Value;
                string fileName1 = "", fileName2 = "", fileName3 = "", fileName4 = "", fileName5 = "", fileName6 = "";
                if (BannerFile.HasFile)
                {
                    sourcelead.photo1 = BannerFile.PostedFile;
                    string FileName = Path.GetFileNameWithoutExtension(BannerFile.FileName);
                    string FileExtension = Path.GetExtension(BannerFile.FileName);
                    var guid = Guid.NewGuid().ToString();
                    FileName = DateTime.Now.ToString("yyyyMMdd") + "-" + guid + "-" + FileName.Trim() + FileExtension;
                    string UploadPath = ConfigurationManager.AppSettings["UserImagePath"].ToString();
                    fileName1 = UploadPath + FileName;
                    sourcelead.photo1.SaveAs(Server.MapPath(fileName1));
                }
                else
                {
                    fileName1 = filenamelabel1.InnerText;
                    fileName1 = "/" + fileName1;
                }
                sourcelead.bannerphoto = fileName1;
                if (title1file.HasFile)
                {
                    sourcelead.photo2 = title1file.PostedFile;
                    string FileName = Path.GetFileNameWithoutExtension(title1file.FileName);
                    string FileExtension = Path.GetExtension(title1file.FileName);
                    var guid = Guid.NewGuid().ToString();
                    FileName = DateTime.Now.ToString("yyyyMMdd") + "-" + guid + "-" + FileName.Trim() + FileExtension;
                    string UploadPath = ConfigurationManager.AppSettings["UserImagePath"].ToString();
                    fileName2 = UploadPath + FileName;
                    sourcelead.photo2.SaveAs(Server.MapPath(fileName2));
                }
                else
                {
                    fileName2 = filenamelabel2.InnerText;
                    fileName2 = "/" + fileName2;
                }
                sourcelead.title1photo = fileName2;
                if (title2file.HasFile)
                {
                    sourcelead.photo3 = title2file.PostedFile;
                    string FileName = Path.GetFileNameWithoutExtension(title2file.FileName);
                    string FileExtension = Path.GetExtension(title2file.FileName);
                    var guid = Guid.NewGuid().ToString();
                    FileName = DateTime.Now.ToString("yyyyMMdd") + "-" + guid + "-" + FileName.Trim() + FileExtension;
                    string UploadPath = ConfigurationManager.AppSettings["UserImagePath"].ToString();
                    fileName3 = UploadPath + FileName;
                    sourcelead.photo3.SaveAs(Server.MapPath(fileName3));
                }
                else
                {
                    fileName3 = filenamelabel3.InnerText;
                    fileName3 = "/" + fileName3;
                }
                sourcelead.title2photo = fileName3;
                if (director1file.HasFile)
                {
                    sourcelead.photo4 = director1file.PostedFile;
                    string FileName = Path.GetFileNameWithoutExtension(director1file.FileName);
                    string FileExtension = Path.GetExtension(director1file.FileName);
                    var guid = Guid.NewGuid().ToString();
                    FileName = DateTime.Now.ToString("yyyyMMdd") + "-" + guid + "-" + FileName.Trim() + FileExtension;
                    string UploadPath = ConfigurationManager.AppSettings["UserImagePath"].ToString();
                    fileName4 = UploadPath + FileName;
                    sourcelead.photo4.SaveAs(Server.MapPath(fileName4));
                }
                else
                {
                    fileName4 = filenamelabel4.InnerText;
                    fileName4 = "/" + fileName4;
                }
                sourcelead.director1photo = fileName4;
                if (director2file.HasFile)
                {
                    sourcelead.photo5 = director2file.PostedFile;
                    string FileName = Path.GetFileNameWithoutExtension(director2file.FileName);
                    string FileExtension = Path.GetExtension(director2file.FileName);
                    var guid = Guid.NewGuid().ToString();
                    FileName = DateTime.Now.ToString("yyyyMMdd") + "-" + guid + "-" + FileName.Trim() + FileExtension;
                    string UploadPath = ConfigurationManager.AppSettings["UserImagePath"].ToString();
                    fileName5 = UploadPath + FileName;
                    sourcelead.photo5.SaveAs(Server.MapPath(fileName5));
                }
                else
                {
                    fileName5 = filenamelabel5.InnerText;
                    fileName5 = "/" + fileName5;
                }
                sourcelead.director2photo = fileName5;
                if (missionfile.HasFile)
                {
                    sourcelead.photo6 = missionfile.PostedFile;
                    string FileName = Path.GetFileNameWithoutExtension(missionfile.FileName);
                    string FileExtension = Path.GetExtension(missionfile.FileName);
                    var guid = Guid.NewGuid().ToString();
                    FileName = DateTime.Now.ToString("yyyyMMdd") + "-" + guid + "-" + FileName.Trim() + FileExtension;
                    string UploadPath = ConfigurationManager.AppSettings["UserImagePath"].ToString();
                    fileName6 = UploadPath + FileName;
                    sourcelead.photo6.SaveAs(Server.MapPath(fileName6));
                }
                else
                {
                    fileName6 = filenamelabel6.InnerText;
                    fileName6 = "/" + fileName6;
                }
                sourcelead.missionphoto= fileName6;
                sourcelead.userid = Convert.ToInt32(sessionUserId);
                BizDataUser obj = new BizDataUser();
                obj.InsertAboutUs(sourcelead);
                Response.Redirect("Data.aspx?Action=AboutUs");

                Response.Redirect(Request.Url.AbsoluteUri);


            }
        }
    }
}