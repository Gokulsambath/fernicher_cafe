﻿using fernicherLibrary.BizData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fernichercafe.UserControls
{
    public partial class searchlist : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BuildHTMLProducts();
        }
        private void BuildHTMLProducts()
        {
           string searchkeyword = Request.QueryString["id"].ToString();            
            BizDataProducts biz = new BizDataProducts();
            DataTable dt = biz.GetProductDetailsBySearchList(searchkeyword);
            if (dt.Rows.Count > 0)
            {
               
                string html = "";
                string yourstring = dt.Rows[0]["subcategorybanner"].ToString();
                String resulttt = yourstring.Replace("~", "");
                if (resulttt != "")
                {
                    string result1 = resulttt.Replace("\\", "/");
                    string result2 = result1.Remove(0, 1);
                    result2 = result2.Replace(" ", "%20");
                    headerbanner.Attributes.CssStyle.Add("background-image", result2);
                }

                int a = 1;
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    if (i >= 6) { a = (i / 6) + 1; }
                    string yourString = dt.Rows[i]["profileimage"].ToString();
                    String result = yourString.Replace("~", "");
                    string resultfinal = "";
                    if (result != "")
                    {
                        string result1 = result.Replace("\\", "/");
                        string result2 = result1.Remove(0, 1);
                        resultfinal = result2.Replace(" ", "%20");

                    }
                   
                        
                     html += "<div class='col-xs-12 col-sm-6 col-md-6 col-lg-6 page page" + a + "'><article><a href='#' onclick=getenquiryproducts(this," + dt.Rows[i]["productid"].ToString() + "," + dt.Rows[i]["subcategoryid"].ToString().Trim() + "); id='enquiryproduct' class='btn btn-buy mfp-open'></a><div class='info'></div><a href='DataWeb.aspx?Action=ManageDetailsWeb&category=" + dt.Rows[i]["CategoryName"].ToString() + "&id=" + dt.Rows[i]["productid"].ToString() + "'><div class='btn btn-add'><i class='icon icon-eye'></i></div></a><div class='figure-list'><div class='image outer-div'><a href='DataWeb.aspx?Action=ManageDetailsWeb&category=" + dt.Rows[i]["CategoryName"].ToString() + "&id=" + dt.Rows[i]["productid"].ToString() + "' class='inner-div'><img src=" + resultfinal + " alt='' width='360' height='190' class='baby-img' /></a></div><div class='text'><h2 class='title h4'><a href='DataWeb.aspx?Action=ManageDetailsWeb&category=" + dt.Rows[i]["CategoryName"].ToString() + "&id=" + dt.Rows[i]["productid"].ToString() + "' > " + dt.Rows[i]["name"].ToString() + "</a></h2><!-- <sub>$ 1499,-</sub><sup>$ 1099,-</sup> --><span class='description clearfix'>" + dt.Rows[i]["description"].ToString() + "</span></div></div></article></div>";
                    

                }
                this.hiddenpaginationcount.Value = a.ToString();
                phldSearchlist.Controls.Add(new Literal { Text = html.ToString() });
            }
            else
            {
                Response.Redirect("DataWeb?Action=IndexWeb&searchlist=0");
                //phldSearchlist.Controls.Add(new Literal { Text = "No Items Found !!" });
            }
        }
    }
}