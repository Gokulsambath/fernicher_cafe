﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="productsoffer.ascx.cs" Inherits="fernichercafe.UserControls.productsoffer" %>


<style>
    label.radio-inline label {
        padding-left: 20px;
        display: inline-block;
    }

    .select-dropdown, svg {
        display: none !important;
    }

    select {
        display: block !important;
    }
</style>
<form id="Form1" runat="server">

    <div id="main">
        <div class="row">
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">
                    <div class="row">
                        <div class="col s10 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0">Product Offers</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <div class="section section-data-tables">

                        <div class="row">
                            <div class="col s12">
                                <div class="card">
                                    <div class="card-content">
                                        <div class="input-field col s6 col m4 col l4">
                                            <label class="active">Category</label>
                                            <asp:DropDownList class="form-control p-0" AutoPostBack="true" ID="ddlCategory1" OnSelectedIndexChanged="ddlCategory1_SelectedIndexChanged" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="input-field col s6 col m4 col l4">
                                            <label class="active">Sub Category</label>
                                            <asp:DropDownList class="form-control p-0" AutoPostBack="true" ID="ddlSubCategory1" OnSelectedIndexChanged="ddlSubCategory1_SelectedIndexChanged" runat="server">
                                            </asp:DropDownList>
                                        </div>

                                        <div class="row">
                                            <div class="col s12">
                                                <table id="page-length-option" class="display wrap" width="100%">
                                                    <thead>
                                                        <tr>

                                                            <th>Product Name</th>
                                                            <th>Offer Percentage</th>
                                                            <th>Image</th>
                                                            <th>Action</th>

                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <asp:PlaceHolder ID="pldProductAccessoriestable" runat="server"></asp:PlaceHolder>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                  


                </div>
               
                <div style="bottom: 50px; right: 19px;" id="adddproducts" runat="server" class="fixed-action-btn direction-top">
                    <a href="#add_offer" class="btn-floating btn-large gradient-45deg-light-blue-cyan gradient-shadow modal-trigger mb-2 mr-1">
                        <i class="material-icons">add</i>
                    </a>
                </div>
            </div>
        </div>
    </div>

    


    <div id="add_offer" class="modal">
        <div class="modal-content">
            <span class="modal-header right modal-close">
                <i class="material-icons right-align">clear</i>
            </span>
            <h4 runat="server" id="myModalLabel">Add Offer</h4>
            <div class="divider"></div>
            <div class="modal-body">

                <div class="row">
                    <input type="hidden" id="hiddenid" runat="server" />

                    <div class="input-field col s6 col m6 col l6">
                        <label class="active">Category Name <span class="red-text">*</span></label>
                        <select class="form-control p-0" id="ddlcat" runat="server">
                        </select>
                        <span class="highlight"></span><span class="bar"></span>


                    </div>
                    <div class="input-field col s6 col m6 col l6">
                        <label class="active">Sub Category Name <span class="red-text">*</span></label>
                        <select class="form-control p-0" id="ddlSubCategory" runat="server">
                        </select>
                        <input type="hidden" id="hidsub" runat="server" />
                        <span class="highlight"></span><span class="bar"></span>


                    </div>
                    <div class="input-field col s6 col m6 col l6" id="productnamediv">
                        <label class="active">Product Name <span class="red-text">*</span></label>
                        <select class="form-control p-0" id="ddlproductname" runat="server">
                        </select>
                        <input type="hidden" id="hidproduct" runat="server" />
                        <span class="highlight"></span><span class="bar"></span>

                    </div>
                    <%-- <div class="input-field col s6 col m6 col l6">
                        <label for="txtoffer">Offer Percentage</label>
                        <input type="text" class="form-control" id="txtoffer" runat="server" /><span class="highlight"></span> <span class="bar"></span>

                    </div>--%>
                    <div class="input-field col s12 col m12 col l12">
                        <div id="file-upload" class="section">
                            <div class="row section">
                                <div class="col s12 m4 l3">
                                    <p>Upload Photo</p>
                                </div>
                                <div class="col s6 m6 l6">
                                    <img id="image_upload_preview" />
                                    <label class="floatlabels" id="filenamelabel" runat="server"></label>
                                    <input type="hidden" id="filepathedit" runat="server" />
                                    <asp:FileUpload ID="inputFile" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <br />
                <div class="divider"></div>

                <div class="modal-footer">
                    <asp:HyperLink href="Data.aspx?Action=productsoffer" runat="server" class="modal-action modal-close waves-effect waves-red btn btn-danger ">Cancel</asp:HyperLink>
                    <asp:Button type="button" ID="btnsave" runat="server" OnClick="btnsave_Click" Text="save" class="modal-action waves-effect waves-green btn btn-info"></asp:Button>

                </div>
            </div>

        </div>
    </div>


</form>

<!-- Theme Customizer -->
<!-- BEGIN: Footer-->

<footer class="page-footer footer footer-static footer-dark gradient-45deg-indigo-purple gradient-shadow navbar-border navbar-shadow">
    <div class="footer-copyright">
        <div class="container"><span>&copy; Fernicher Cafe 2019</span></div>
    </div>
</footer>

<script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputFile").change(function () {
        readURL(this);
    });


    $("#MiddleCenterContent_ctl01_ddlproductname").change(function () {

        var id = $(this).find(":selected").val();
        //console.log(id);
        $("#MiddleCenterContent_ctl01_hidproduct").val(id);
    });
    $("#MiddleCenterContent_ctl01_ddlcat").change(function () {

        var id = $(this).find(":selected").val();

        $("#MiddleCenterContent_ctl01_ddlSubCategory").html("");
        if (id != 0) {
            $.ajax({
                type: 'POST',
                url: "userwebservice.asmx/populatesubcategorydropdown",
                data: "{ 'id': '" + id + "' }",
                async: false,
                contentType: 'application/json; charset-utf-8',

                success: function (response) {


                    var html = "<option value=0>choose your option</option>";
                    for (var i = 0; i < response.d.length; i++) {
                        html += "<option value=" + response.d[i].productid + ">" + response.d[i].productname + "</option>";
                    }
                    $("#MiddleCenterContent_ctl01_ddlSubCategory").append(html);


                }

            });
        }
    });


    $("#MiddleCenterContent_ctl01_ddlSubCategory").change(function () {

        var id = $(this).find(":selected").val();
        var selectedText = $("#MiddleCenterContent_ctl01_ddlcat option:selected").attr("id");
        $("#MiddleCenterContent_ctl01_hidsub").val(id);
        //  alert(selectedText);

        $("#MiddleCenterContent_ctl01_ddlproductname").html("");
        if (id != 0) {
            $.ajax({
                type: 'POST',
                url: "userwebservice.asmx/populateproductdropdown2",
                data: "{ 'id': '" + id + "','idname':'" + selectedText + "' }",
                async: false,
                contentType: 'application/json; charset-utf-8',

                success: function (response) {


                    var html = "<option value=0>choose your option</option>";
                    for (var i = 0; i < response.d.length; i++) {
                        html += "<option value=" + response.d[i].productid + ">" + response.d[i].productname + "</option>";
                    }
                    $("#MiddleCenterContent_ctl01_ddlproductname").append(html);


                }

            });
        }
    });

</script>

