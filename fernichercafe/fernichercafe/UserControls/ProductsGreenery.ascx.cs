﻿using fernicherLibrary.BizData;
using fernicherLibrary.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace fernichercafe.UserControls
{
    public partial class ProductsGreenery : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {
                Response.Redirect("Login.aspx");
            }
            if (!IsPostBack)
            {
                BuildHTMLTable(0,"Greenery");
                btnsave.Text = "Add";
                myModalLabel.InnerText = "Add Products";
                BizDataProducts bdl = new BizDataProducts();
                DataSet dsrole = bdl.GetAllSubCategoryForDropdown("Greenery");
                FillDropDown(ddlSubCategory, dsrole.Tables[0], "Choose your option");
                FillDropDowns(ddlSubCategory1, dsrole.Tables[0], "Choose your option");
                EditProductDetails();
                
            }

        }

        private void FillDropDowns(DropDownList ddlSubCategory1, DataTable dataTable, string v)
        {
            ddlSubCategory1.Items.Clear();            ListItem lstItem = new ListItem();            lstItem.Value = "0";            lstItem.Text = v;            ddlSubCategory1.Items.Add(lstItem);            for (int intCount1 = 0; intCount1 < dataTable.Rows.Count; intCount1++)            {                lstItem = new ListItem();                lstItem.Value = dataTable.Rows[intCount1][0].ToString().Trim();                lstItem.Text = dataTable.Rows[intCount1][1].ToString().Trim();                ddlSubCategory1.Items.Add(lstItem);            }
        }

        private void EditProductDetails()
        {
            BizDataProducts user = new BizDataProducts();
            string result1 = "", result2 = "", result4 = "", result5 = "", sometin = Request.QueryString["id"];
            int iddd = Convert.ToInt32(sometin);

            if (sometin != null)
            {

                DataSet dsCategoryid = user.EditProductDetails(iddd, "Greenery");
                if (dsCategoryid.Tables[0].Rows.Count > 0)
                {
                    string ss = ""; string value;
                    hiddenid.Value = dsCategoryid.Tables[0].Rows[0]["productgreeneryid"].ToString();
                    string yourString = dsCategoryid.Tables[0].Rows[0]["profileimage"].ToString();
                    String result = yourString.Replace("~", "");
                    if (result != "")
                    {
                        result1 = result.Replace("\\", "/");
                        result2 = result1.Remove(0, 1);
                    }
                    inputFile.Attributes.Add("value", dsCategoryid.Tables[0].Rows[0]["profileimage"].ToString());
                    filepathedit.Value = dsCategoryid.Tables[0].Rows[0]["profileimage"].ToString();
                    filenamelabel.InnerText = result2;
                    string yourString1 = dsCategoryid.Tables[0].Rows[0]["Bannerurl"].ToString();
                    String result3 = yourString1.Replace("~", "");
                    if (result3 != "")
                    {
                        result4 = result3.Replace("\\", "/");
                        result5 = result4.Remove(0, 1);
                    }
                    inputfile1.Attributes.Add("value", dsCategoryid.Tables[0].Rows[0]["Bannerurl"].ToString());
                    bannerpathedit.Value = dsCategoryid.Tables[0].Rows[0]["Bannerurl"].ToString();
                    txtvideo.Value = dsCategoryid.Tables[0].Rows[0]["videolink"].ToString();
                    bannername.InnerText = result5;
                    value = dsCategoryid.Tables[0].Rows[0]["status"].ToString();
                    if (value == "1") { active.Checked = true; } else { inactive.Checked = true; }
                    txtdesc.Value = dsCategoryid.Tables[0].Rows[0]["description"].ToString();
                    productname.Value = dsCategoryid.Tables[0].Rows[0]["name"].ToString();
                    ddlSubCategory.Value = dsCategoryid.Tables[0].Rows[0]["SubCategoryId"].ToString();

                    btnsave.Text = "Update";
                    myModalLabel.InnerText = "Edit Products";

                    ss = "<script language=JavaScript>$(document).ready(function(){ setTimeout(function () {   $('#add_products').modal('open') ;}, 200);});</script>";

                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CreatePopupOpen", ss);


                }
                btnsave.Text = "Update";
                myModalLabel.InnerText = "Edit Products";

            }

        }

        private void FillDropDown(HtmlSelect ddlSubCategory, DataTable dataTable, string v)
        {
            ddlSubCategory.Items.Clear();            ListItem lstItem = new ListItem();            lstItem.Value = "0";            lstItem.Text = v;            ddlSubCategory.Items.Add(lstItem);            for (int intCount1 = 0; intCount1 < dataTable.Rows.Count; intCount1++)            {                lstItem = new ListItem();                lstItem.Value = dataTable.Rows[intCount1][0].ToString().Trim();                lstItem.Text = dataTable.Rows[intCount1][1].ToString().Trim();                ddlSubCategory.Items.Add(lstItem);            }
        }

        private void BuildHTMLTable(int number,string value)
        {
            pldProductstable.Controls.Clear();
            BizDataProducts objBizDataUser = new BizDataProducts();
            DataSet dsUser = objBizDataUser.GetAllKidProducts(number,value);


            StringBuilder strAccount = new StringBuilder();
            if (dsUser.Tables[0].Rows.Count > 0)
            {
                for (int intCount = 0; intCount < dsUser.Tables[0].Rows.Count; intCount++)
                {
                    string id = dsUser.Tables[0].Rows[intCount]["productgreeneryid"].ToString();
                    string name = dsUser.Tables[0].Rows[intCount]["productname"].ToString();
                    strAccount.Append("<tr>");
                    strAccount.Append("<td><img class=listimg src='");
                    string yourString1 = dsUser.Tables[0].Rows[intCount]["bannerurl"].ToString();
                    String results1 = yourString1.Replace("~", "");
                    if (results1 != "")
                    {
                        string result2 = results1.Replace("\\", "/");
                        string result3 = result2.Remove(0, 1);
                        strAccount.Append(result3);
                    }
                    strAccount.Append("' /></td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["displayname"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["productname"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["subcategoryname"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["status"].ToString() == "1" ? "Active" : "In-Active");
                    strAccount.Append("</td>");
                    strAccount.Append("<td><img class=listimg src='");
                    string yourString = dsUser.Tables[0].Rows[intCount]["photourl"].ToString();
                    String result = yourString.Replace("~", "");
                    if (result != "")
                    {
                        string result1 = result.Replace("\\", "/");
                        string result2 = result1.Remove(0, 1);
                        strAccount.Append(result2);
                    }
                    strAccount.Append("' /></td>");

                    strAccount.Append("<td class='center-align'>");
                    strAccount.Append("<a href='Data.aspx?Action=productgreenerydetails&productgreeneryid=" + id + "&name=" + name + "' class='btn btn-info' style='padding: 3px 3px'>Manage details</a><a href='Data.aspx?Action=productsgreenery&id=" + id + "' style='padding-left:10px;padding-right: 10px'><i class='fa fa-edit'></i></a>");
                    strAccount.Append("</td>");


                    strAccount.Append("</tr>");


                }

                pldProductstable.Controls.Add(new Literal { Text = strAccount.ToString() });
            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (btnsave.Text == "Update")
            {
                UpdateProducts();
            }
            else
            {

                string sessionUserId = Session["UserID"] as string;

                if (string.IsNullOrEmpty(sessionUserId))
                {
                    Response.Redirect("Login.aspx");
                }
                else
                {
                    string fileName = "", fileName1 = "";
                ProductEntities sourcelead = new ProductEntities();
                sourcelead.productname = productname.Value;
                sourcelead.SubCategoryId = Convert.ToInt32(ddlSubCategory.Value);
                sourcelead.status = active.Checked == true ? "1" : "0";
                if (inputFile.HasFile)
                {
                    sourcelead.photo = inputFile.PostedFile;

                    string FileName = Path.GetFileNameWithoutExtension(inputFile.FileName);
                    string FileExtension = Path.GetExtension(inputFile.FileName);
                    var guid = Guid.NewGuid().ToString();
                    FileName = DateTime.Now.ToString("yyyyMMdd") + "-" + guid + "-" + FileName.Trim() + FileExtension;


                    string UploadPath = ConfigurationManager.AppSettings["UserImagePath"].ToString();


                    fileName = UploadPath + FileName;


                    sourcelead.photo.SaveAs(Server.MapPath(fileName));

                }

                if (inputfile1.HasFile)
                {
                    sourcelead.photo = inputfile1.PostedFile;

                    string FileName = Path.GetFileNameWithoutExtension(inputfile1.FileName);
                    string FileExtension = Path.GetExtension(inputfile1.FileName);

                    var guid = Guid.NewGuid().ToString();
                    FileName = DateTime.Now.ToString("yyyyMMdd") + "-" + guid + "-" + FileName.Trim() + FileExtension;

                    string UploadPath = ConfigurationManager.AppSettings["UserImagePath"].ToString();


                    fileName1 = UploadPath + FileName;


                    sourcelead.photo.SaveAs(Server.MapPath(fileName1));

                }

                sourcelead.photourl = fileName;
                sourcelead.videolink = txtvideo.Value;
                sourcelead.bannerurl = fileName1;
                sourcelead.description = txtdesc.Value;
                BizDataProducts obj = new BizDataProducts();
                obj.AddProducts(sourcelead,"Greenery");
                Response.Redirect("Data.aspx?Action=productsgreenery");

                Response.Redirect(Request.Url.AbsoluteUri);


                }
            }

        }

        private void UpdateProducts()
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
            string fileName = "", fileName1 = "";
            ProductEntities sourcelead = new ProductEntities();
            sourcelead.productname = productname.Value;
            sourcelead.SubCategoryId = Convert.ToInt32(ddlSubCategory.Value);
            sourcelead.status = active.Checked == true ? "1" : "0";
            if (inputFile.HasFile)
            {
                sourcelead.photo = inputFile.PostedFile;

                string FileName = Path.GetFileNameWithoutExtension(inputFile.FileName);
                string FileExtension = Path.GetExtension(inputFile.FileName);
                var guid = Guid.NewGuid().ToString();
                FileName = DateTime.Now.ToString("yyyyMMdd") + "-" + guid + "-" + FileName.Trim() + FileExtension;


                string UploadPath = ConfigurationManager.AppSettings["UserImagePath"].ToString();


                fileName = UploadPath + FileName;


                sourcelead.photo.SaveAs(Server.MapPath(fileName));

            }
            else
            {
                fileName = filenamelabel.InnerText;
                fileName = "/" + fileName;
            }
            if (inputfile1.HasFile)
            {
                sourcelead.photo = inputfile1.PostedFile;

                string FileName = Path.GetFileNameWithoutExtension(inputfile1.FileName);
                string FileExtension = Path.GetExtension(inputfile1.FileName);

                var guid = Guid.NewGuid().ToString();
                FileName = DateTime.Now.ToString("yyyyMMdd") + "-" + guid + "-" + FileName.Trim() + FileExtension;

                string UploadPath = ConfigurationManager.AppSettings["UserImagePath"].ToString();


                fileName1 = UploadPath + FileName;


                sourcelead.photo.SaveAs(Server.MapPath(fileName1));

            }
            else
            {
                fileName1 = bannername.InnerText;
                fileName1 = "/" + fileName1;
            }
            sourcelead.description = txtdesc.Value;
            sourcelead.videolink = txtdesc.Value;
            sourcelead.bannerurl = fileName1;
            sourcelead.photourl = fileName;
            sourcelead.productgreeneryid = Convert.ToInt32(hiddenid.Value);
            BizDataProducts obj = new BizDataProducts();
            obj.UpdateProducts(sourcelead,"Greenery");
            Response.Redirect("Data.aspx?Action=productsgreenery");

            Response.Redirect(Request.Url.AbsoluteUri);


            }
        }

        protected void ddlSubCategory1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string message = ddlSubCategory1.SelectedItem.Value;
            BuildHTMLTable(Convert.ToInt32(message),"Greenery");
        }
    }
}