﻿using fernicherLibrary.BizData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fernichercafe.UserControls
{
    public partial class GlobalHeaderLoggedIn : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataSet dsInternaluser = new BizDataUser().CheckvisitorCount();
            todaycount.InnerText ="Today Count :  " + dsInternaluser.Tables[0].Rows[0]["todaycount"].ToString();
            totalcount.InnerText ="Total Count :  " + dsInternaluser.Tables[1].Rows[0]["totalcount"].ToString();
        }
    }
}