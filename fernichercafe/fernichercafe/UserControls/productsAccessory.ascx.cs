﻿using fernicherLibrary.BizData;
using fernicherLibrary.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace fernichercafe.UserControls
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [ScriptService]
    public partial class productsAccessory : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {

                Response.Redirect("Login.aspx");
            }
            if (!IsPostBack)
            {
                BuildHTMLTable(0,0);
                btnsave.Text = "Add";
                myModalLabel.InnerText = "Add Products";
                BizDataProducts bdl = new BizDataProducts();
                DataSet dsrole = bdl.GetAllSubCategoryForDropdown("Kid");
                FillDropDown(ddlSubCategory, dsrole.Tables[0], "Choose your option");
                FillDropDowns(ddlSubCategory1, dsrole.Tables[0], "Choose your option");
                EditAccessoryDetails();
                

            }
        }
        
        private void EditAccessoryDetails()
        {
            BizDataProducts user = new BizDataProducts();
            string result1 = "", result2 = "", sometin = Request.QueryString["id"];
            int iddd = Convert.ToInt32(sometin);

            if (sometin != null)
            {

                DataSet dsCategoryid = user.EditAccessory(iddd, "Kid");
                if (dsCategoryid.Tables[0].Rows.Count > 0)
                {
                    string ss = ""; string value;
                    hiddenid.Value = dsCategoryid.Tables[0].Rows[0]["accessoryid"].ToString();
                    string yourString = dsCategoryid.Tables[0].Rows[0]["photourl"].ToString();
                    String result = yourString.Replace("~", "");
                    if (result != "")
                    {
                        result1 = result.Replace("\\", "/");
                        result2 = result1.Remove(0, 1);
                    }
                    inputFile.Attributes.Add("value", dsCategoryid.Tables[0].Rows[0]["photourl"].ToString());
                    filepathedit.Value = dsCategoryid.Tables[0].Rows[0]["photourl"].ToString();
                    filenamelabel.InnerText = result2;                   
                    
                    value = dsCategoryid.Tables[0].Rows[0]["status"].ToString();
                    if (value == "1") { active.Checked = true; } else { inactive.Checked = true; }
                    ddlSubCategory.Value = dsCategoryid.Tables[0].Rows[0]["subcategoryid"].ToString();
                    DataSet ds = user.GetAllKidProducts(Convert.ToInt32(dsCategoryid.Tables[0].Rows[0]["subcategoryid"].ToString()), "Kid");
                    populateproductdropdown(ds.Tables[0]);
                    ddlproductname.Value = dsCategoryid.Tables[0].Rows[0]["productid"].ToString();
                    ddlproductnamehidden.Value= dsCategoryid.Tables[0].Rows[0]["productid"].ToString();
                    txtaccessoryname.Value = dsCategoryid.Tables[0].Rows[0]["AccessoryName"].ToString();
                    txtmodelname.Value = dsCategoryid.Tables[0].Rows[0]["Model"].ToString();
                    //txtDepth.Value = dsCategoryid.Tables[0].Rows[0]["Depth"].ToString();
                    //txtHeight.Value = dsCategoryid.Tables[0].Rows[0]["height"].ToString();
                    //txtwidth.Value = dsCategoryid.Tables[0].Rows[0]["width"].ToString();
                    btnsave.Text = "Update";
                    myModalLabel.InnerText = "Edit Accessory";

                    ss = "<script language=JavaScript>$(document).ready(function(){ setTimeout(function () {   $('#add_accessory').modal('open');}, 200);});</script>";

                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CreatePopupOpen", ss);


                }
                btnsave.Text = "Update";
                myModalLabel.InnerText = "Edit Accessory";

            }
        }

        private void populateproductdropdown(DataTable dataTable)
        {
            ddlproductname.Items.Clear();
            ListItem lstItem = new ListItem();
            lstItem.Value = "0";
            lstItem.Text = "choose your option";
            ddlproductname.Items.Add(lstItem);
            for (int intCount1 = 0; intCount1 < dataTable.Rows.Count; intCount1++)
            {
                lstItem = new ListItem();
                lstItem.Value = dataTable.Rows[intCount1][0].ToString().Trim();
                lstItem.Text = dataTable.Rows[intCount1][1].ToString().Trim();
                ddlproductname.Items.Add(lstItem);
            }
        }
        private void populateproductdropdowns(DataTable dataTable)
        {
            ddlProduct.Items.Clear();
            ListItem lstItem = new ListItem();
            lstItem.Value = "0";
            lstItem.Text = "choose your option";
            ddlProduct.Items.Add(lstItem);
            for (int intCount1 = 0; intCount1 < dataTable.Rows.Count; intCount1++)
            {
                lstItem = new ListItem();
                lstItem.Value = dataTable.Rows[intCount1][0].ToString().Trim();
                lstItem.Text = dataTable.Rows[intCount1][1].ToString().Trim();
                ddlProduct.Items.Add(lstItem);
            }
        }

        private void FillDropDowns(DropDownList ddlSubCategory1, DataTable dataTable, string v)
        {
            ddlSubCategory1.Items.Clear();
            ListItem lstItem = new ListItem();
            lstItem.Value = "0";
            lstItem.Text = v;
            ddlSubCategory1.Items.Add(lstItem);
            for (int intCount1 = 0; intCount1 < dataTable.Rows.Count; intCount1++)
            {
                lstItem = new ListItem();
                lstItem.Value = dataTable.Rows[intCount1][0].ToString().Trim();
                lstItem.Text = dataTable.Rows[intCount1][1].ToString().Trim();
                ddlSubCategory1.Items.Add(lstItem);
            }
           
        }


        private void FillDropDown(HtmlSelect ddlSubCategory, DataTable dataTable, string v)
        {
            ddlSubCategory.Items.Clear();
            ListItem lstItem = new ListItem();
            lstItem.Value = "0";
            lstItem.Text = v;
            ddlSubCategory.Items.Add(lstItem);
            for (int intCount1 = 0; intCount1 < dataTable.Rows.Count; intCount1++)
            {
                lstItem = new ListItem();
                lstItem.Value = dataTable.Rows[intCount1][0].ToString().Trim();
                lstItem.Text = dataTable.Rows[intCount1][1].ToString().Trim();
                ddlSubCategory.Items.Add(lstItem);
            }
        }


        private void BuildHTMLTable(int subcategoryid, int productid)
        {
            pldProductAccessoriestable.Controls.Clear();
            BizDataProducts objBizDataUser = new BizDataProducts();
            DataSet dsUser = objBizDataUser.GetAllAccessories(subcategoryid,productid);


            StringBuilder strAccount = new StringBuilder();
            if (dsUser.Tables[0].Rows.Count > 0)
            {
                for (int intCount = 0; intCount < dsUser.Tables[0].Rows.Count; intCount++)
                {
                    string id = dsUser.Tables[0].Rows[intCount]["Accessoryid"].ToString();
                    string name = dsUser.Tables[0].Rows[intCount]["AccessoryName"].ToString();
                    strAccount.Append("<tr>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["subcategoryname"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["productname"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(name);
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["height"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["width"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["Depth"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["status"].ToString() == "1" ? "Active" : "In-Active");
                    strAccount.Append("</td>");
                    strAccount.Append("<td><img class=listimg src='");
                    string yourString = dsUser.Tables[0].Rows[intCount]["photourl"].ToString();
                    String result = yourString.Replace("~", "");
                    if (result != "")
                    {
                        string result1 = result.Replace("\\", "/");
                        string result2 = result1.Remove(0, 1);
                        strAccount.Append(result2);
                    }
                    strAccount.Append("' /></td>");

                    strAccount.Append("<td class='center-align'>");
                    strAccount.Append("<a href='Data.aspx?Action=productsAccessory&id=" + id + "' style='padding-left:10px;padding-right: 10px'><i class='fa fa-edit'></i></a>");
                    strAccount.Append("</td>");


                    strAccount.Append("</tr>");

                }

                pldProductAccessoriestable.Controls.Add(new Literal { Text = strAccount.ToString() });
            }
        }

        protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            string subcategory = ddlSubCategory1.SelectedItem.Value;
            string productid = ddlProduct.SelectedItem.Value;
            BuildHTMLTable(Convert.ToInt32(subcategory), Convert.ToInt32(productid));
        }

        protected void ddlSubCategory1_SelectedIndexChanged(object sender, EventArgs e)
        {
            BizDataProducts user = new BizDataProducts();
            string subcategory = ddlSubCategory1.SelectedItem.Value;
            ddlProduct.Items.Clear();
            if (subcategory != "0")
            {
                DataSet ds = user.GetAllKidProducts(Convert.ToInt32(subcategory), "Kid");
                populateproductdropdowns(ds.Tables[0]);
                BuildHTMLTable(Convert.ToInt32(subcategory), 0);
            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (btnsave.Text == "Update")
            {
                UpdateAccessory();
            }
            else
            {

                string sessionUserId = Session["UserID"] as string;

                if (string.IsNullOrEmpty(sessionUserId))
                {
                    Response.Redirect("Login.aspx");
                }
                else
                {
                    string fileName = "";
                    AccessoryEntitie sourcelead = new AccessoryEntitie();
                    sourcelead.subcategoryid = Convert.ToInt32(ddlSubCategory.Value);
                    sourcelead.productid = Convert.ToInt32(ddlproductnamehidden.Value);
                    sourcelead.AccessoryName = txtaccessoryname.Value;
                    sourcelead.Model = txtmodelname.Value;
                    sourcelead.width = txtwidth.Value;
                    sourcelead.height = txtHeight.Value;
                    sourcelead.Depth =txtDepth.Value;
                    sourcelead.status = active.Checked == true ? "1" : "0";
                    if (inputFile.HasFile)
                    {
                        sourcelead.photo = inputFile.PostedFile;

                        string FileName = Path.GetFileNameWithoutExtension(inputFile.FileName);
                        string FileExtension = Path.GetExtension(inputFile.FileName);
                        var guid = Guid.NewGuid().ToString();
                        FileName = DateTime.Now.ToString("yyyyMMdd") + "-" + guid + "-" + FileName.Trim() + FileExtension;


                        string UploadPath = ConfigurationManager.AppSettings["UserImagePath"].ToString();


                        fileName = UploadPath + FileName;


                        sourcelead.photo.SaveAs(Server.MapPath(fileName));

                    }
                    sourcelead.photoname = fileName;
                    BizDataProducts obj = new BizDataProducts();
                    obj.AddAccessory(sourcelead, "Kid");
                    Response.Redirect("Data.aspx?Action=productsAccessory");

                    Response.Redirect(Request.Url.AbsoluteUri);


                }
            }

        }

        private void UpdateAccessory()
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                string fileName = "";
                AccessoryEntitie sourcelead = new AccessoryEntitie();
                sourcelead.subcategoryid = Convert.ToInt32(ddlSubCategory.Value);
                sourcelead.productid = Convert.ToInt32(ddlproductnamehidden.Value);
                sourcelead.AccessoryName = txtaccessoryname.Value;
                sourcelead.Model = txtmodelname.Value;
                sourcelead.width = txtwidth.Value;
                sourcelead.height =txtHeight.Value;
                sourcelead.Depth = txtDepth.Value;
                sourcelead.status = active.Checked == true ? "1" : "0";
                if (inputFile.HasFile)
                {
                    sourcelead.photo = inputFile.PostedFile;

                    string FileName = Path.GetFileNameWithoutExtension(inputFile.FileName);
                    string FileExtension = Path.GetExtension(inputFile.FileName);
                    var guid = Guid.NewGuid().ToString();
                    FileName = DateTime.Now.ToString("yyyyMMdd") + "-" + guid + "-" + FileName.Trim() + FileExtension;


                    string UploadPath = ConfigurationManager.AppSettings["UserImagePath"].ToString();


                    fileName = UploadPath + FileName;


                    sourcelead.photo.SaveAs(Server.MapPath(fileName));

                }
                else
                {
                    fileName = filenamelabel.InnerText;
                    fileName = "/" + fileName;
                }
                sourcelead.photoname = fileName;
                sourcelead.accessoryid = Convert.ToInt32(hiddenid.Value);
                BizDataProducts obj = new BizDataProducts();
                obj.UpdateAccessory(sourcelead, "Kid");
                Response.Redirect("Data.aspx?Action=productsAccessory");

                Response.Redirect(Request.Url.AbsoluteUri);


            }
        }
    }
}