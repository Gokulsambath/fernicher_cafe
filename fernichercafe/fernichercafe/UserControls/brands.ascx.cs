﻿using fernicherLibrary.BizData;
using fernicherLibrary.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fernichercafe.UserControls
{
    public partial class brands : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {
                Response.Redirect("Login.aspx");
            }
            BuildHTMLTable();
            if (!IsPostBack)
            {

                btnsave.Text = "Add";
                myModalLabel.InnerText = "ADD OUR BRAND PARTNERS";                                

                EditBrandDetails();



            }
        }

        private void EditBrandDetails()
        {

            BizDataUser user = new BizDataUser();
            string result1 = "", result2 = "",  sometin = Request.QueryString["id"];
            int iddd = Convert.ToInt32(sometin);

            if (sometin != null)
            {

                DataSet dsCategoryid = user.GetAllBrands(iddd);
                if (dsCategoryid.Tables[0].Rows.Count > 0)
                {
                    string ss = ""; string value;
                    hiddenid.Value = dsCategoryid.Tables[0].Rows[0]["brandid"].ToString();
                    brandname.Value= dsCategoryid.Tables[0].Rows[0]["brandname"].ToString();
                    sitelink.Value = dsCategoryid.Tables[0].Rows[0]["brandlink"].ToString();
                    string yourString = dsCategoryid.Tables[0].Rows[0]["photourl"].ToString();
                    String result = yourString.Replace("~", "");
                    if (result != "")
                    {
                        result1 = result.Replace("\\", "/");
                        result2 = result1.Remove(0, 1);
                    }
                    inputFile.Attributes.Add("value", dsCategoryid.Tables[0].Rows[0]["photourl"].ToString());
                    filepathedit.Value = dsCategoryid.Tables[0].Rows[0]["photourl"].ToString();
                    filenamelabel.InnerText = result2;                    
                    value = dsCategoryid.Tables[0].Rows[0]["flgActive"].ToString();
                    if (value == "1") { active.Checked = true; } else { inactive.Checked = true; }
                    
                    btnsave.Text = "Update";
                    myModalLabel.InnerText = "Edit ";

                    ss = "<script language=JavaScript>$(document).ready(function(){ setTimeout(function () {   $('#add_brand_partner').modal('open');}, 200);});</script>";

                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CreatePopupOpen", ss);


                }
                btnsave.Text = "Update";
                myModalLabel.InnerText = "EDIT OUR BRAND PARTNERS";

            }

        }

        private void BuildHTMLTable()
        {
            phldbrandtable.Controls.Clear();
            BizDataUser objBizDataUser = new BizDataUser();
            DataSet dsUser = objBizDataUser.GetAllBrands(0);


            StringBuilder strAccount = new StringBuilder();
            if (dsUser.Tables[0].Rows.Count > 0)
            {
                for (int intCount = 0; intCount < dsUser.Tables[0].Rows.Count; intCount++)
                {
                    string id = dsUser.Tables[0].Rows[intCount]["brandid"].ToString();
                    strAccount.Append("<tr>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["brandname"].ToString());
                    strAccount.Append("</td>");
                   
                    strAccount.Append("<td><img class=listimg src='");
                    string yourString = dsUser.Tables[0].Rows[intCount]["photourl"].ToString();
                    String result = yourString.Replace("~", "");
                    if (result != "")
                    {
                        string result1 = result.Replace("\\", "/");
                        string result2 = result1.Remove(0, 1);
                        strAccount.Append(result2);
                    }
                    strAccount.Append("' /></td>");
                    strAccount.Append("<td>");
                    strAccount.Append("<a href="+dsUser.Tables[0].Rows[intCount]["brandlink"].ToString()+" target=_blank>"+ dsUser.Tables[0].Rows[intCount]["brandlink"].ToString() + "</a>");
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["flgActive"].ToString() == "1" ? "Active" : "In-Active");
                    strAccount.Append("</td>");

                    strAccount.Append("<td class='center-align'>");
                    strAccount.Append("<a class='modal-trigger' href='Data.aspx?Action=Brands&id=" + id + "' style='padding-right: 10px'><i class='fa fa-edit'></i></a>");
                    strAccount.Append("</td>");


                    strAccount.Append("</tr>");


                }

                phldbrandtable.Controls.Add(new Literal { Text = strAccount.ToString() });
            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (btnsave.Text == "Update")
            {
                UpdateBrands();
            }
            else
            {

                string sessionUserId = Session["UserID"] as string;

                if (string.IsNullOrEmpty(sessionUserId))
                {
                    Response.Redirect("Login.aspx");
                }
                else
                {
                    string fileName = "";
                    GeneralEntitie sourcelead = new GeneralEntitie();
                    sourcelead.brandname = brandname.Value;
                    sourcelead.brandlink = sitelink.Value;
                    sourcelead.active = active.Checked == true ? "1" : "0";
                    if (inputFile.HasFile)
                    {
                        //sourcelead.userid = Convert.ToInt32(sessionUserId);
                        sourcelead.photourl = inputFile.PostedFile;

                        string FileName = Path.GetFileNameWithoutExtension(inputFile.FileName);
                        string FileExtension = Path.GetExtension(inputFile.FileName);

                        var guid = Guid.NewGuid().ToString();
                        FileName = DateTime.Now.ToString("yyyyMMdd") + "-" + guid + "-" + FileName.Trim() + FileExtension;

                        string UploadPath = ConfigurationManager.AppSettings["UserImagePath"].ToString();


                        fileName = UploadPath + FileName;


                        sourcelead.photourl.SaveAs(Server.MapPath(fileName));

                    }
                    

                    sourcelead.photoname = fileName;
                    BizDataUser obj = new BizDataUser();
                    obj.AddBrands(sourcelead);
                    Response.Redirect("Data.aspx?Action=Brands");

                    Response.Redirect(Request.Url.AbsoluteUri);


                }
            }
        }

        private void UpdateBrands()
        {
            string sessionUserId = Session["UserID"] as string;

            if (string.IsNullOrEmpty(sessionUserId))
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                string fileName = "";
                GeneralEntitie sourcelead = new GeneralEntitie();
                sourcelead.active = active.Checked == true ? "1" : "0";
                sourcelead.brandname = brandname.Value;
                sourcelead.brandlink = sitelink.Value;
                if (inputFile.HasFile)
                {
                    sourcelead.photourl = inputFile.PostedFile;

                    string FileName = Path.GetFileNameWithoutExtension(inputFile.FileName);
                    string FileExtension = Path.GetExtension(inputFile.FileName);
                    var guid = Guid.NewGuid().ToString();
                    FileName = DateTime.Now.ToString("yyyyMMdd") + "-" + guid + "-" + FileName.Trim() + FileExtension;


                    string UploadPath = ConfigurationManager.AppSettings["UserImagePath"].ToString();


                    fileName = UploadPath + FileName;


                    sourcelead.photourl.SaveAs(Server.MapPath(fileName));

                }
                else
                {
                    fileName = filenamelabel.InnerText;
                    fileName = "/" + fileName;
                }
               
                
                sourcelead.photoname = fileName;
                sourcelead.brandid = Convert.ToInt32(hiddenid.Value);
                BizDataUser obj = new BizDataUser();
                obj.UpdateBrands(sourcelead);
                Response.Redirect("Data.aspx?Action=Brands");

                Response.Redirect(Request.Url.AbsoluteUri);


            }
        }
    }
}