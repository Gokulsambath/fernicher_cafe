﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AboutUs.ascx.cs" Inherits="fernichercafe.UserControls.AboutUs" %>
<!-- BEGIN: Page Main-->
<div id="main">
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s10 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0">About Us</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="container">
                <div class="section section-data-tables">
                    <form runat="server">
                        <div class="row">
                            <div class="col s12">
                                <div class="card">
                                    <div class="card-content">

                                        <div class="row">
                                            <div class="col x12 s12 m12 l12">
                                                <h5>Banner Section:</h5>
                                            </div>
                                            <div class="input-field col x12 s6 m6 l6">
                                                <input id="txtbannerheader" runat="server" type="text" class="validate">
                                                <label for="banner-header">Banner Header</label>
                                            </div>
                                            <div class="input-field col x12 s6 m6 l6">
                                                <input id="txtbannersubheader" runat="server" type="text" class="validate">
                                                <label for="banner-sub-header">Banner Sub Header</label>
                                            </div>
                                            <div class="section">
                                                <div class="section">
                                                    <div class="col s12 m4 l3">
                                                        <p>Upload Banner Photo</p>
                                                    </div>
                                                    <div class="col s12 m8 l9">
                                                        <img id="Img5" runat="server" />
                                                        <label class="floatlabels" id="filenamelabel1" runat="server"></label>
                                                        <input type="hidden" id="filepathedit1" runat="server" />
                                                        <asp:FileUpload ID="BannerFile" class="dropify" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="divider"></div>

                                        <div class="row">
                                            <div class="col x12 s12 m12 l12">
                                                <h5>History Section:</h5>
                                            </div>
                                            <div class="input-field col x12 s6 m6 l6">
                                                <input id="txthistoryheader" runat="server" type="text" class="validate">
                                                <label for="history-header">History Header</label>
                                            </div>
                                            <div class="input-field col x12 s6 m6 l6">
                                                <input id="txthistorysubheader" runat="server" type="text" class="validate">
                                                <label for="history-sub-header">History Sub Header</label>
                                            </div>


                                            <div class="input-field col x12 s6 m6 l6">
                                                <input id="title1" runat="server" type="text" class="validate">
                                                <label for="radherhyam-title">Radhe Shyam Title</label>
                                            </div>
                                            <div class="input-field col x12 s6 m6 l6">
                                                <textarea id="title1desc" runat="server" class="materialize-textarea"></textarea>
                                                <label for="radherhyam-description">Radhe Shyam Description</label>
                                            </div>
                                            <div class="section">
                                                <div class="section">
                                                    <div class="col s12 m4 l3">
                                                        <p>Upload Radhe Shyam Profile Photo</p>
                                                    </div>
                                                    <div class="col s12 m8 l9">
                                                        <img id="Img4" runat="server" />
                                                        <label class="floatlabels" id="filenamelabel2" runat="server"></label>
                                                        <input type="hidden" id="filepathedit2" runat="server" />
                                                        <asp:FileUpload ID="title1file" class="dropify" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="input-field col x12 s6 m6 l6">
                                                <input id="title2" runat="server" type="text" class="validate">
                                                <label for="fernicher-title">Fernicher Cafe Title</label>
                                            </div>
                                            <div class="input-field col x12 s6 m6 l6">
                                                <textarea id="title2desc" runat="server" class="materialize-textarea"></textarea>
                                                <label for="fernicher-description">Fernicher Cafe Description</label>
                                            </div>
                                            <div class="section">
                                                <div class="section">
                                                    <div class="col s12 m4 l3">
                                                        <p>Upload Fernicher Cafe Profile Photo</p>
                                                    </div>
                                                    <div class="col s12 m8 l9">
                                                        <img id="Img3" runat="server" />
                                                        <label class="floatlabels" id="filenamelabel3" runat="server"></label>
                                                        <input type="hidden" id="filepathedit3" runat="server" />
                                                        <asp:FileUpload ID="title2file" class="dropify" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <br>
                                        <div class="divider"></div>


                                        <div class="row">
                                            <div class="col x12 s12 m12 l12">
                                                <h5>From The Desk Of Directors:</h5>
                                            </div>
                                            <div class="input-field col x12 s6 m6 l6">
                                                <input id="txtdirectorheader" runat="server" type="text" class="validate">
                                                <label for="directors-header">Directors Header</label>
                                            </div>
                                            <div class="input-field col x12 s6 m6 l6">
                                                <input id="txtdirectorheaderdesc" runat="server" type="text" class="validate">
                                                <label for="directors-sub-header">Directors Sub Header</label>
                                            </div>


                                            <div class="input-field col x12 s6 m6 l6">
                                                <input id="director1" runat="server" type="text" class="validate">
                                                <label for="amit-title">Amit Poddar Title</label>
                                            </div>
                                            <div class="input-field col x12 s6 m6 l6">
                                                <textarea id="director1desc" runat="server" class="materialize-textarea"></textarea>
                                                <label for="amit-description">Amit Poddar Description</label>
                                            </div>
                                            <div class="section">
                                                <div class="section">
                                                    <div class="col s12 m4 l3">
                                                        <p>Upload Amit Poddar Profile Photo</p>
                                                    </div>
                                                    <div class="col s12 m8 l9">
                                                        <img id="Img2" runat="server" />
                                                        <label class="floatlabels" id="filenamelabel4" runat="server"></label>
                                                        <input type="hidden" id="filepathedit4" runat="server" />
                                                        <asp:FileUpload ID="director1file" class="dropify" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="input-field col x12 s6 m6 l6">
                                                <input id="director2" runat="server" type="text" class="validate">
                                                <label for="bikash-title">Bikash Poddar Title</label>
                                            </div>
                                            <div class="input-field col x12 s6 m6 l6">
                                                <textarea id="director2desc" runat="server" class="materialize-textarea"></textarea>
                                                <label for="bikash-description">Bikash Poddar Description</label>
                                            </div>
                                            <div class="section">
                                                <div class="section">
                                                    <div class="col s12 m4 l3">
                                                        <p>Upload Bikash Poddar Profile Photo</p>
                                                    </div>
                                                    <div class="col s12 m8 l9">
                                                        <img id="Img1" runat="server" />
                                                        <label class="floatlabels" id="filenamelabel5" runat="server"></label>
                                                        <input type="hidden" id="filepathedit5" runat="server" />
                                                        <asp:FileUpload ID="director2file" class="dropify" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <br>
                                        <div class="divider"></div>


                                        <div class="row">
                                            <div class="col x12 s12 m12 l12">
                                                <h5>Mission & Vission Section:</h5>
                                            </div>
                                            <div class="input-field col x12 s6 m6 l6">
                                                <input id="txtmissionheader" runat="server" type="text" class="validate">
                                                <label for="mission-header">Mission Header</label>
                                            </div>
                                            <div class="input-field col x12 s6 m6 l6">
                                                <input id="missiondesc" runat="server" type="text" class="validate">
                                                <label for="mission-sub-header">Mission Sub Header</label>
                                            </div>
                                            <div class="section">
                                                <div class="section">
                                                    <div class="col s12 m4 l3">
                                                        <p>Upload Mission Banner Photo</p>
                                                    </div>
                                                    <div class="col s12 m8 l9">
                                                        <img id="image_upload_preview" runat="server" />
                                                        <label class="floatlabels" id="filenamelabel6" runat="server"></label>
                                                        <input type="hidden" id="filepathedit6" runat="server" />
                                                        <asp:FileUpload ID="missionfile" class="dropify" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <asp:Button type="button" ID="btnsave" runat="server" OnClick="btnsave_Click" Text="save" class="modal-action waves-effect waves-green btn btn-info"></asp:Button>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>



            </div>
            <!-- START RIGHT SIDEBAR NAV -->
        </div>
    </div>
</div>


<!-- END: Page Main-->

<!-- Theme Customizer -->
<!-- BEGIN: Footer-->

<footer class="page-footer footer footer-static footer-dark gradient-45deg-indigo-purple gradient-shadow navbar-border navbar-shadow">
    <div class="footer-copyright">
        <div class="container"><span>&copy; 2019</span></div>
    </div>
</footer>

