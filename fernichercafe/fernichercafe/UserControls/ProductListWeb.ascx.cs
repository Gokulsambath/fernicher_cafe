﻿using fernicherLibrary.BizData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fernichercafeWebsite.UserControls
{
    public partial class ProductList : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            BuildHTMLProducts();
        }



        private void BuildHTMLProducts()
        {
            phldProductlist.Controls.Clear();
            string category = Request.QueryString["category"];
            string sometin = Request.QueryString["id"];
            string maplevel = Request.QueryString["maplevel"];

            BizDataProducts biz = new BizDataProducts();
            DataTable dtt = biz.GetProductDetailsById(category, sometin,"0");
            if (dtt.Rows.Count > 0)
            {
                navtitle1.Attributes["href"] = "~/DataWeb.aspx?Action=SubCategoryWeb&id=" + dtt.Rows[0]["CategoryId"].ToString() + "";
                string html = "",otherimage="";
                string yourstring = dtt.Rows[0]["subcategorybanner"].ToString();
                String resulttt = yourstring.Replace("~", "");
                if (resulttt != "")
                {
                    string result1 = resulttt.Replace("\\", "/");
                    string result2 = result1.Remove(0, 1);
                    result2 = result2.Replace(" ", "%20");
                    otherimage = result2;
                    headerbanner.Attributes.CssStyle.Add("background-image", result2);
                }
                titleh1.InnerText = dtt.Rows[0]["displayname"].ToString();
                navtitle1.InnerText = dtt.Rows[0]["displayname"].ToString();
                navtitle2.InnerText = dtt.Rows[0]["subcategoryname"].ToString();
                int a = 1;
                if (maplevel.ToString() == "null")
                {
                    DataTable dts = biz.GetMapLevelDetailsById(category, sometin);                    
                    for (int i = 0; i < dts.Rows.Count; i++)
                    {
                        string yourString = dts.Rows[i]["photourl"].ToString();
                        String result = yourString.Replace("~", "");
                        if (result != "")
                        {
                            string result1 = result.Replace("\\", "/");
                            string result2 = result1.Remove(0, 1);
                            result2 = result2.Replace(" ", "%20");
                            html += "<div class='col-xs-12 col-sm-6 col-md-4 col-lg-4'><article class='card'><div class='info'><span class='add-favorite added'></span><span></span></div><a href='DataWeb.aspx?Action=ProductListWeb&category=" + dts.Rows[i]["CategoryName"].ToString() + "&id=" + dts.Rows[i]["subcategoryid"].ToString() + "&maplevel=" + dts.Rows[i]["mapid"].ToString() + "'><div class='btn btn-add'><i class='icon icon-eye'></i></div></a><div class='figure-grid outer-div'><div class='image inner-div'><a href='DataWeb.aspx?Action=ProductListWeb&category=" + dts.Rows[i]["CategoryName"].ToString() + "&id=" + dts.Rows[i]["subcategoryid"].ToString() + "&maplevel=" + dts.Rows[i]["mapid"].ToString() + "'><img src=" + result2 + " alt='' widtsh='360' height='241' class='img-hover-zoom--colorize' /></a></div><div class='text'><h2 class='title h4'><a href='DataWeb.aspx?Action=ProductListWeb&category=" + dts.Rows[i]["CategoryName"].ToString() + "&id=" + dts.Rows[i]["subcategoryid"].ToString() + "&maplevel=" + dts.Rows[i]["mapid"].ToString() + "' >" + dts.Rows[i]["mapname"].ToString() + "</a></h2><span class='description clearfix'>Gubergren amet dolor ea diam takimata consetetur facilisis blandit et aliquyam lorem ea duo labore diam sit et consetetur nulla</span></div></div></article></div>";
                        }
                    }
                    //html += "<div class='col-xs-12 col-sm-6 col-md-4 col-lg-4'><article class='card'><div class='info'><span class='add-favorite added'></span><span></span></div><a href='DataWeb.aspx?Action=ProductListWeb&category=" + dtt.Rows[0]["CategoryName"].ToString() + "&id=" + dtt.Rows[0]["subcategoryid"].ToString() + "&maplevel=other'><div class='btn btn-add'><i class='icon icon-eye'></i></div></a><div class='figure-grid outer-div'><div class='image inner-div'><a href='DataWeb.aspx?Action=ProductListWeb&category=" + dtt.Rows[0]["CategoryName"].ToString() + "&id=" + dtt.Rows[0]["subcategoryid"].ToString() + "&maplevel=other'><img src=" + otherimage + " alt='' widtsh='360' height='241' class='img-hover-zoom--colorize' /></a></div><div class='text'><h2 class='title h4'><a href='DataWeb.aspx?Action=ProductListWeb&category=" + dtt.Rows[0]["CategoryName"].ToString() + "&id=" + dtt.Rows[0]["subcategoryid"].ToString() + "&maplevel=other'>Other</a></h2><span class='description clearfix'>Gubergren amet dolor ea diam takimata consetetur facilisis blandit et aliquyam lorem ea duo labore diam sit et consetetur nulla</span></div></div></article></div>";
                    phldProductlist.Controls.Clear();
                    phldProductlist.Controls.Add(new Literal { Text = html.ToString() });
                }
                else
                {
                    DataTable dt = biz.GetProductDetailsById(category, sometin,maplevel);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (i >= 6) { a = (i / 6) + 1; }
                        string yourString = dt.Rows[i]["profileimage"].ToString();
                        String result = yourString.Replace("~", "");
                        string resultfinal = "";
                        if (result != "")
                        {
                            string result1 = result.Replace("\\", "/");
                            string result2 = result1.Remove(0, 1);
                            resultfinal = result2.Replace(" ", "%20");

                        }
                        if (category == "Kid")
                        {

                            html += "<div class='col-xs-12 col-sm-12 col-md-6 col-lg-6 page page" + a + "'><article><a href='#' onclick=getenquiryproducts(this," + dt.Rows[i]["productkidid"].ToString() + "," + dt.Rows[i]["subcategoryid"].ToString().Trim() + "); id='enquiryproduct' class='btn btn-buy mfp-open'></a><div class='info'></div><a href='DataWeb.aspx?Action=ManageDetailsWeb&category=" + dt.Rows[i]["CategoryName"].ToString() + "&id=" + dt.Rows[i]["productkidid"].ToString() + "'><div class='btn btn-add'><i class='icon icon-eye'></i></div></a><div class='figure-list'><div class='image outer-div'><a href='DataWeb.aspx?Action=ManageDetailsWeb&category=" + dt.Rows[i]["CategoryName"].ToString() + "&id=" + dt.Rows[i]["productkidid"].ToString() + "' class='inner-div'><img src=" + resultfinal + " alt='' width='360' height='160' class='baby-img' /></a></div><div class='text'><h2 class='title h4'><a href='DataWeb.aspx?Action=ManageDetailsWeb&category=" + dt.Rows[i]["CategoryName"].ToString() + "&id=" + dt.Rows[i]["productkidid"].ToString() + "' > " + dt.Rows[i]["name"].ToString() + "</a></h2><!-- <sub>$ 1499,-</sub><sup>$ 1099,-</sup> --><span class='description clearfix'>" + dt.Rows[i]["description"].ToString() + "</span></div></div></article></div>";
                        }
                        else if (category == "Livingroom")
                        {

                            html += "<div class='col-xs-12 col-sm-12 col-md-6 col-lg-6 page page" + a + "'><article><a href='#' onclick=getenquiryproducts(this," + dt.Rows[i]["productlivingroomid"].ToString() + "," + dt.Rows[i]["subcategoryid"].ToString().Trim() + "); id='enquiryproduct' class='btn btn-buy mfp-open'></a><div class='info'></div><a href='DataWeb.aspx?Action=ManageDetailsWeb&category=" + dt.Rows[i]["CategoryName"].ToString() + "&id=" + dt.Rows[i]["productlivingroomid"].ToString() + "'><div class='btn btn-add'><i class='icon icon-eye'></i></div></a><div class='figure-list'><div class='image outer-div'><a href='DataWeb.aspx?Action=ManageDetailsWeb&category=" + dt.Rows[i]["CategoryName"].ToString() + "&id=" + dt.Rows[i]["productlivingroomid"].ToString() + "' class='inner-div'><img src=" + resultfinal + " alt='' width='360' height='160' class='baby-img' /></a></div><div class='text'><h2 class='title h4'><a href='DataWeb.aspx?Action=ManageDetailsWeb&category=" + dt.Rows[i]["CategoryName"].ToString() + "&id=" + dt.Rows[i]["productlivingroomid"].ToString() + "' > " + dt.Rows[i]["name"].ToString() + "</a></h2><!-- <sub>$ 1499,-</sub><sup>$ 1099,-</sup> --><span class='description clearfix'>" + dt.Rows[i]["description"].ToString() + "</span></div></div></article></div>";
                        }
                        else if (category == "Greenery")
                        {

                            html += "<div class='col-xs-12 col-sm-12 col-md-6 col-lg-6 page page" + a + "'><article><a href='#' onclick=getenquiryproducts(this," + dt.Rows[i]["productgreeneryid"].ToString() + "," + dt.Rows[i]["subcategoryid"].ToString().Trim() + "); id='enquiryproduct' class='btn btn-buy mfp-open'></a><div class='info'></div><a href='DataWeb.aspx?Action=ManageDetailsWeb&category=" + dt.Rows[i]["CategoryName"].ToString() + "&id=" + dt.Rows[i]["productgreeneryid"].ToString() + "'><div class='btn btn-add'><i class='icon icon-eye'></i></div></a><div class='figure-list'><div class='image outer-div'><a href='DataWeb.aspx?Action=ManageDetailsWeb&category=" + dt.Rows[i]["CategoryName"].ToString() + "&id=" + dt.Rows[i]["productgreeneryid"].ToString() + "' class='inner-div'><img src=" + resultfinal + " alt='' width='360' height='160' class='baby-img' /></a></div><div class='text'><h2 class='title h4'><a href='DataWeb.aspx?Action=ManageDetailsWeb&category=" + dt.Rows[i]["CategoryName"].ToString() + "&id=" + dt.Rows[i]["productgreeneryid"].ToString() + "' > " + dt.Rows[i]["name"].ToString() + "</a></h2><!-- <sub>$ 1499,-</sub><sup>$ 1099,-</sup> --><span class='description clearfix'>" + dt.Rows[i]["description"].ToString() + "</span></div></div></article></div>";
                        }

                    }
                    this.hiddenpaginationcount.Value = a.ToString();
                    phldProductlist.Controls.Add(new Literal { Text = html.ToString() });
                }
            }

        }
    }
}