﻿using DairymanLibrary.Entity;
using fernicherLibrary.BizData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fernichercafe
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            string strEmail = Email.Value.Trim();
            string strPassword = password.Value.Trim();
            UserEntities objInternaluserEntitie = new UserEntities();
            objInternaluserEntitie.Email = strEmail;
            objInternaluserEntitie.password = strPassword;

            DataSet dsInternaluser = new BizDataUser().CheckUserLogin(objInternaluserEntitie);
            if (dsInternaluser.Tables[0].Rows.Count > 0)
            {
                Session["LoggedIn"] = "Y";
                Session["UserType"] = dsInternaluser.Tables[0].Rows[0]["roleName"].ToString(); ;
                Session["UserRoleID"] = dsInternaluser.Tables[0].Rows[0]["roleId"].ToString();
                Session["UserName"] = dsInternaluser.Tables[0].Rows[0]["email"].ToString();
                Session["UserEmail"] = dsInternaluser.Tables[0].Rows[0]["email"].ToString();
                Session["UserID"] = dsInternaluser.Tables[0].Rows[0]["userId"].ToString();
                Session["FullName"] = dsInternaluser.Tables[0].Rows[0]["Name"].ToString();
                if (Session["UserRoleID"].ToString() == "1")
                {
                    Response.Redirect("Data.aspx?Action=Category");
                }
                else if(Session["UserRoleID"].ToString() == "2")
                {
                    Response.Redirect("DataWeb.aspx?Action=IndexWeb");
                }

            }
            else
            {
                lblLoginError.Visible = true;
                lblLoginError.Text = "User Id or Password is wrong";
            }
        }
    }
}