﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DairymanLibrary.Entity;
using DairymanLibrary.Gateway;
using fernicherLibrary.Entity;

namespace fernicherLibrary.BizData
{
    public class BizDataUser
    {
        public DataSet CheckUserLogin(UserEntities objInternaluserEntitie)
        {            GatewayUser objGatewayInternaluser = new GatewayUser();            return objGatewayInternaluser.CheckUserLogin(objInternaluserEntitie);
        }

        public DataSet CheckvisitorCount()
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();            return objGatewayInternaluser.CheckvisitorCount();
        }

        public void InsertEnquiry(GeneralEntitie objEnttitie)
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();            objGatewayInternaluser.InsertEnquiry(objEnttitie);
        }

        public DataSet GetAllHomeBanner(int i)
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();            return objGatewayInternaluser.GetAllHomeBanner(i);
        }

        public DataSet GetAllNewsLetter()
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();            return objGatewayInternaluser.GetAllNewsLetter();
        }

        public DataTable Getbrandpage()
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();            return objGatewayInternaluser.Getbrandpage();
        }

        public DataTable GetLabelControls()
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();            return objGatewayInternaluser.GetLabelControls();
        }

        public void InsertVisitorDetails(string ipv4, string ipv6,string tempip,string session)
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();            objGatewayInternaluser.InsertVisitorDetails(ipv4,ipv6,tempip,session);
        }

        public DataTable GetAboutUs()
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();            return objGatewayInternaluser.GetAboutUs();
        }

        public DataTable GetCompanyInfo()
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();            return objGatewayInternaluser.GetCompanyInfo();
        }

        public DataSet GetAllActiveBrands()
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();            return objGatewayInternaluser.GetAllActiveBrands();
        }

        public DataTable GetAllRating(string id,string category)
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();            return objGatewayInternaluser.GetAllRating(id,category);
        }

        public void InsertBrandpage(UserEntities sourcelead)
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();            objGatewayInternaluser.InsertBrandpage(sourcelead);
        }

        public DataSet GetAllBrands(int idd)
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();            return objGatewayInternaluser.GetAllBrands(idd);
        }

        public void InsertLabelControls(GeneralEntitie sourcelead)
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();            objGatewayInternaluser.InsertLabelControls(sourcelead);
        }

        public void InsertAboutCompany(UserEntities sourcelead)
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();            objGatewayInternaluser.InsertAboutCompany(sourcelead);
        }

        public DataSet GetAllcallback()
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();            return objGatewayInternaluser.GetAllcallback();
        }

        public DataSet GetAllEnquiry()
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();            return objGatewayInternaluser.GetAllEnquiry();
        }

        public void InsertCallBack(GeneralEntitie objEnttitie)
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();            objGatewayInternaluser.InsertCallBack(objEnttitie);
        }

        public void AddRating(GeneralEntitie ge)
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();            objGatewayInternaluser.AddRating(ge);
        }

        public void AddBrands(GeneralEntitie sourcelead)
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();            objGatewayInternaluser.AddBrands(sourcelead);
        }

        public void UpdateBrands(GeneralEntitie sourcelead)
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();            objGatewayInternaluser.UpdateBrands(sourcelead);
        }

        public List<Enquiry> getEnquiryImages(RootObject result)
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();            List<Enquiry> productlist = objGatewayInternaluser.getEnquiryImages(result);
            return productlist;
        }

        public void InsertAboutUs(GeneralEntitie sourcelead)
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();            objGatewayInternaluser.InsertAboutUs(sourcelead);
        }

        public void AddSubscription(string email)
        {
            GatewayUser gatewayUser = new GatewayUser();
            gatewayUser.AddSubscription(email);
        }

        public int checksubscribedUser(string email)
        {
            GatewayUser gatewayUser = new GatewayUser();
            return gatewayUser.checksubscribedUser(email);
        }
    }
   
}
