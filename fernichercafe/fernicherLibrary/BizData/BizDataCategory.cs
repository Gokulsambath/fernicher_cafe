﻿using fernicherLibrary.Entity;
using fernicherLibrary.Gateway;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fernicherLibrary.BizData
{
    public class BizDataCategory
    {
        public DataSet GetAllDetails()
        {

            GatewayCategory objGatewayInternaluser = new GatewayCategory();
            return objGatewayInternaluser.GetAllDetails();
        }

        public DataSet GetAllCategory()
        {
            GatewayCategory objGatewayInternaluser = new GatewayCategory();
            return objGatewayInternaluser.GetAllCategory();
        }

        public DataTable GetAllSubCategoryById(string sometin)
        {
            GatewayCategory objGatewayInternaluser = new GatewayCategory();
            return objGatewayInternaluser.GetAllSubCategoryById(Convert.ToInt64(sometin));
        }

        public DataSet GetAllActiveProductConcepts()
        {
            GatewayCategory objGatewayInternaluser = new GatewayCategory();
            return objGatewayInternaluser.GetAllActiveProductConcepts();
        }

        public DataSet GetAllActiveBanners()
        {
            GatewayCategory objGatewayInternaluser = new GatewayCategory();
            return objGatewayInternaluser.GetAllActiveBanners();
        }

        public DataSet GetAllDetailsOffers()
        {
            //throw new NotImplementedException();
            GatewayCategory objGatewayInternaluser = new GatewayCategory();
            return objGatewayInternaluser.GetAllDetailsOffers();
        }

        public DataSet EditCategoryDetails(int iddd)
        {
            GatewayCategory objGatewayInternaluser = new GatewayCategory();
            return objGatewayInternaluser.EditCategoryDetails(iddd);
        }

        public void AddCategory(CategoryEntitie sourcelead)
        {
            GatewayCategory objGatewayInternaluser = new GatewayCategory();
            objGatewayInternaluser.AddCategory(sourcelead);
        }
        public void UpdateCategory(CategoryEntitie sourcelead)
        {
            GatewayCategory objGatewayInternaluser = new GatewayCategory();
            objGatewayInternaluser.UpdateCategory(sourcelead);
        }

        public void UpdatesubCategory(CategoryEntitie sourcelead)
        {
            GatewayCategory objGatewayInternaluser = new GatewayCategory();
            objGatewayInternaluser.UpdatesubCategory(sourcelead);
        }

        public void AddsubCategory(CategoryEntitie sourcelead)
        {
            GatewayCategory objGatewayInternaluser = new GatewayCategory();
            objGatewayInternaluser.AddsubCategory(sourcelead);
        }


        public DataSet GetAllSubCategory()
        {
            GatewayCategory objGatewayInternaluser = new GatewayCategory();
            return objGatewayInternaluser.GetAllSubCategory();
        }

        public DataSet EditsubCategoryDetails(int iddd)
        {
            GatewayCategory objGatewayInternaluser = new GatewayCategory();
            return objGatewayInternaluser.EditsubCategoryDetails(iddd);
        }

        public DataSet GetAllDetailsAdmin()
        {

            GatewayCategory objGatewayInternaluser = new GatewayCategory();
            return objGatewayInternaluser.GetAllDetailsAdmin();
        }
    }
}
