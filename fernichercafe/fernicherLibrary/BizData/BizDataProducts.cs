﻿using fernicherLibrary.Entity;
using fernicherLibrary.Gateway;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fernicherLibrary.BizData
{
    public class BizDataProducts
    {
        public DataSet GetAllSubCategoryForDropdown(string v)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            return objGatewayInternaluser.GetAllSubCategoryForDropdown(v);
        }
        public DataSet GetAllKidProducts(int number,string value)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            return objGatewayInternaluser.GetAllKidProducts(number,value);
        }

        public DataTable GetMapLevelDetailsById(string category, string sometin)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            return objGatewayInternaluser.GetMapLevelDetailsById(category, sometin);
        }

        public DataTable GetProductDetailsBySearchList(string searchkeyword)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            return objGatewayInternaluser.GetProductDetailsBySearchList(searchkeyword);
        }

        public DataTable GetProductDetailsById(string category,string sometin, string v)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            return objGatewayInternaluser.GetProductDetailsById(category, Convert.ToInt64(sometin),v);
        }

        public DataSet GetAllProductDetails(string value,int number)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            return objGatewayInternaluser.GetAllProductDetails(number, value);
        }

        public DataSet GetAllProducts(int categoryid, int subcategoryid)
        {
            // throw new NotImplementedException();
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            return objGatewayInternaluser.GetAllProducts(categoryid, subcategoryid);
        }

        public DataSet EditMapping(int iddd)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            return objGatewayInternaluser.EditMapping(iddd);
        }

        public DataSet EditProductConcepts(int iddd)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            return objGatewayInternaluser.EditProductConcepts(iddd);
        }

        public DataSet EditOffers(int iddd)
        {
            // throw new NotImplementedException();
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            return objGatewayInternaluser.EditOffers(iddd);
        }

        public void AddProducts(ProductEntities sourcelead,string value)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            objGatewayInternaluser.AddProducts(sourcelead,value);
        }

        public void UpdateProducts(ProductEntities sourcelead,string value)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            objGatewayInternaluser.UpdateProducts(sourcelead,value);
        }

        public DataSet EditAccessory(int iddd, string v)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            return objGatewayInternaluser.EditAccessory(iddd, v);
        }

        public DataSet EditProductDetails(int iddd,string value)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            return objGatewayInternaluser.EditProductDetails(iddd,value);
        }

        public void InsertProductdetails(ProductEntities pe)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            objGatewayInternaluser.InsertProductdetails(pe);
        }

        public DataSet fillsubcategorybycategoryid(string id)
        {
            // throw new NotImplementedException();
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            return objGatewayInternaluser.fillsubcategorybycategoryid(id);

        }

        public DataSet gettheproducts(string id, string idname)
        {
            //throw new NotImplementedException();
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            return objGatewayInternaluser.gettheproducts(id,idname);
        }

        public string getimagepath(long imageid,string events)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            DataSet pathset = objGatewayInternaluser.getimagepath(imageid, events);
            string path = pathset.Tables[0].Rows[0]["imagename"].ToString();
            return path;
        }

        public void deleteimagepath(long imageid,string value)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            objGatewayInternaluser.deleteimagepath(imageid,value);
        }

        public void InsertLivingProductdetails(ProductEntities pe)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            objGatewayInternaluser.InsertLivingProductdetails(pe);
        }

        public DataSet GetAllProductConcepts(int categoryid, int subcategoryid)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            return objGatewayInternaluser.GetAllProductConcepts(categoryid, subcategoryid);
        }

        public DataSet GetAllMappedItems()
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            return objGatewayInternaluser.GetAllMappedItems();
        }

        public DataSet GetAllMappingLevel()
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            return objGatewayInternaluser.GetAllMappingLevel();
        }

        public DataSet GetAllAccessories(int subcategoryid, int productid)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            return objGatewayInternaluser.GetAllAccessories(subcategoryid,productid);
        }

        public void InsertGreeneryProductdetails(ProductEntities pe)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            objGatewayInternaluser.InsertGreeneryProductdetails(pe);
        }

        public void AddAccessory(AccessoryEntitie sourcelead, string v)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            objGatewayInternaluser.AddAccessory(sourcelead,v);
        }

        public void UpdateAccessory(AccessoryEntitie sourcelead, string v)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            objGatewayInternaluser.UpdateAccessory(sourcelead,v);
        }

        public DataSet GetAllKidProductsfordropdown(int number, string value)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            return objGatewayInternaluser.GetAllKidProductsfordropdown(number, value);
        }

        public DataSet GetAllMaplevelfordropdown(int v, int idname)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            return objGatewayInternaluser.GetAllMaplevelfordropdown(v, idname);
        }

        public void updateimagepath(long imageid, string colorcode, string v)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            objGatewayInternaluser.updateimagepath(imageid,colorcode, v);
        }

        public void AddOffer(OfferEntity ent)
        {
            //throw new NotImplementedException();
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            objGatewayInternaluser.AddOffer(ent);
        }

        public void UpdateOffer(OfferEntity ent)
        {
            // throw new NotImplementedException();
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            objGatewayInternaluser.UpdateOffer(ent);
        }

        public DataSet GetAllKidProductsN(int v, string idname)
        {
            //  throw new NotImplementedException();
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            return objGatewayInternaluser.GetAllKidProductsN(v, idname);
        }

        public void AddHomeBanner(HomeEntity ent)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            objGatewayInternaluser.AddHomeBanner(ent);
        }

        public void UpdateHomeBanner(HomeEntity sourcelead)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            objGatewayInternaluser.UpdateHomeBanner(sourcelead);
        }

        public void AddProductConcept(OfferEntity ent)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            objGatewayInternaluser.AddProductConcept(ent);
        }

        public void Updateproductconcept(OfferEntity ent)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            objGatewayInternaluser.Updateproductconcept(ent);
        }

        public DataSet GetAllKidProductsmap(int v, string idname)
        {

            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            return objGatewayInternaluser.GetAllKidProductsmap(v, idname);
        }

        public void AddMappingLevel(MappingEntity ent)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            objGatewayInternaluser.AddMappingLevel(ent);
        }

        public void AddMapping(MappingEntity ent)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            objGatewayInternaluser.AddMapping(ent);
        }

        public void UpdateMapping(MappingEntity ent)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            objGatewayInternaluser.UpdateMapping(ent);
        }

        public void UpdateMappingLevel(MappingEntity ent)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            objGatewayInternaluser.UpdateMappingLevel(ent);
        }

        public void deletemaplevel(int v1, int v2, int v3)
        {
            GatewayProducts objGatewayInternaluser = new GatewayProducts();
            objGatewayInternaluser.deletemaplevel(v1,v2,v3);
        }
    }
}
