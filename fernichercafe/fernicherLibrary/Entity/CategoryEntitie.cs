﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace fernicherLibrary.Entity
{
    public class CategoryEntitie
    {
        public int CategoryId { get; set; }
        public int SubCategoryId { get; set; }
        public int sortby { get; set; }
        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }
        public string status { get; set; }
        public string photourl { get; set; }
        public string bannerurl { get; set; }
        public HttpPostedFile  photo { get; set; }
        public int userid { get; set; }
        public string description { get; set; }
        public string displayname { get; set; }
    }
}
