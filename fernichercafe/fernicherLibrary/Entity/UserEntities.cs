﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DairymanLibrary.Entity
{
    public class UserEntities
    {
        public string brandpageheader { get; set; }

        public string brandpagesubheader { get; set; }

        public int? userid { get; set; }

        public string firstName { get; set; }

        public string lastName { get; set; }

        public string mobileNo { get; set; }

        public string dob { get; set; }

        public string Email { get; set; }

        public string alternateNo { get; set; }

        public string password { get; set; }

        public string confirmpassword { get; set; }

        public string mobileCountryCode { get; set; }

        public char ApproverFarmer { get; set; }

        public char ApproverVet { get; set; }
        public char ApproverParavet { get; set; }

        public char ApproverAIT { get; set; }
        public string address { get; set; }

        public string village { get; set; }

        public string hamlet { get; set; }

        public string taluka { get; set; }

        public string district { get; set; }

        public string pincode { get; set; }

        public int stateId { get; set; }

        public string voterId { get; set; }

        public string panNo { get; set; }

        public string aadharNo { get; set; }

        public int roleId { get; set; }

        public string photoUrl { get; set; }

        public string voterUrl { get; set; }

        public string eduUrl { get; set; }

        public string panUrl { get; set; }

        public string comments { get; set; }

        public string eduDegree { get; set; }

        public string eduAwardedBy { get; set; }

        public string dateofCertification { get; set; }

        public string practisingSince { get; set; }

        public string practiseLicenseNo { get; set; }

        public string Intro { get; set; }

        public string workinghours { get; set; }

        public string LocationEmbed { get; set; }


    }
}
