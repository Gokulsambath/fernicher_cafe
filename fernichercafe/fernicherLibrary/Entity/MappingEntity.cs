﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace fernicherLibrary.Entity
{
    public class MappingEntity
    {
        public int mapid { get; set; }
        public string mapname { get; set; }
        public string subcategoryname { get; set; }
        public string categoryname { get; set; }
        public int subcategoryid { get; set; }
        public int categoryid { get; set; }
        public string flgActive { get; set; }
        public int userid { get; set; }
        public int productid { get; set; }
        public HttpPostedFile photo { get; set; }
        public string photoname { get; set; }
    }
}
