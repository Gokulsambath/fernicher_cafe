﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace fernicherLibrary.Entity
{
    public class GeneralEntitie
    {
        
        public string productid { get; set; }
        public string categoryid { get; set; }
        public string subcategoryid { get; set; }
        public string enquiryname { get; set; }
        public string enquirycomments { get; set; }
        public string enquirymobile { get; set; }
        public string enquiryemail { get; set; }
        public string enquiryid { get; set; }
        public float rating { get; set; }
        public string categoryname { get; set; }
        public int brandid { get; set; }
        public string brandname { get; set; }
        public HttpPostedFile photourl { get; set; }
        public string active { get; set; }
        public string brandlink { get; set; }
        public string photoname { get; set; }
        public int userid { get; set; }

        public string bannerheader { get; set; }
        public string bannersubheader { get; set; }
        public string historyheader { get; set; }
        public string historysubheader { get; set; }
        public string title1 { get; set; }
        public string title1desc { get; set; }
        public string title2 { get; set; }
        public string title2desc { get; set; }
        public string directorheader { get; set; }
        public string directorheaddesc { get; set; }
        public string director1 { get; set; }
        public string director1desc { get; set; }
        public string director2 { get; set; }
        public string director2desc { get; set; }
        public string missionheader { get; set; }
        public string missiondesc { get; set; }
        public string bannerphoto { get; set; }
        public string title1photo { get; set; }
        public string title2photo { get; set; }
        public string director1photo { get; set; }
        public string director2photo { get; set; }
        public string missionphoto { get; set; }
        public HttpPostedFile photo1 { get; set; }
        public HttpPostedFile photo2 { get; set; }
        public HttpPostedFile photo3 { get; set; }
        public HttpPostedFile photo4 { get; set; }
        public HttpPostedFile photo5 { get; set; }
        public HttpPostedFile photo6 { get; set; }


        public string lblaboutusheader { get; set; }
        public string lblaboutusdesc { get; set; }
        public string lblproductcategoriesheader { get; set; }
        public string lblproductcategoriesdesc { get; set; }
        public string lblofferheader { get; set; }
        public string lblofferdesc { get; set; }
        public string lblproductconceptheader { get; set; }
        public string lblproductconceptdesc { get; set; }
    }
}
