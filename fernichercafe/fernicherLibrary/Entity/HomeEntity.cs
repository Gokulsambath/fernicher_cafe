﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace fernicherLibrary.Entity
{
    public class HomeEntity
    {
        public int bannerid { get; set; }
        public string bannerheader { get; set; }
        public string bannersubheader { get; set; }
        public HttpPostedFile Photourl { get; set; }
        public string photoname { get; set; }
        public string Active { get; set; }
        public int userid { get; set; }
        public int categoryid { get; set; }
        public int subcategoryid { get; set; }
        public int productid { get; set; }
    }
}
