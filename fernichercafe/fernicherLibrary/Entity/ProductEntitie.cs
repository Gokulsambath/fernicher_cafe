﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace fernicherLibrary.Entity
{
    


    public class ProductEntitie
    {
        public ProductEntitie()
        {
            Product = new ProductEntities();
            ProductList = new List<ProductEntities>();
        }
        ProductEntities Product = new ProductEntities();
        List<ProductEntities> ProductList = new List<ProductEntities>();
    }  
    public class ProductEntities
    {
        public string videolink { get; set; }
        public int productid { get; set; }
        public string area { get; set; }
        public string size { get; set; }
        public string modelname { get; set; }
        public int productlivingroomid { get; set; }
        public string targetgroup { get; set; }
        public string bannerurl { get; set; }
        public string segment { get; set; }
        public string agegroup { get; set; }
        public string theme { get; set; }
        public string certificates { get; set; }
        public string manufacture { get; set; }
        public string materials { get; set; }
        public string features { get; set; }
        public bool availability { get; set; }
        public int productinteriorid { get; set; }
        public int productkidid { get; set; }
        public int productkiddetailid { get; set; }
        public int SubCategoryId { get; set; }
        public string productname { get; set; }
        public string description { get; set; }
        public string status { get; set; }
        public string categoryname { get; set; }
        public HttpPostedFile photo { get; set; }
        public string photourl { get; set; }
        public List<string> photourlarray { get; set; }
        public List<string> photonamearray { get; set; }
        public int userid { get; set; }
        public List<string> Technicaldetails { get; set; }
        public int productgreeneryid { get; set; }
    }
}
