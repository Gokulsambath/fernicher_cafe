﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace fernicherLibrary.Entity
{
    public class AccessoryEntitie
    {
        public string AccessoryName { get; set; }
        public int accessoryid { get; set; }
        public string Model { get; set; }
        public string width { get; set; }
        public string height { get; set; }
        public string Depth { get; set; }
        public int productid { get; set; }
        public int subcategoryid { get; set; }
        public int categoryid { get; set; }
        public HttpPostedFile photo { get; set; }
        public string photoname { get; set; }
        public string status { get; set; }
    }
}
