﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace fernicherLibrary.Entity
{
    public class OfferEntity
    {
        public Int32 offerid { get; set; }
        public string offername { get; set; }
        public string productname { get; set; }
        public string subcategoryname { get; set; }
        public string categoryname { get; set; }
        public Int32 productid { get; set; }
        public Int32 subcategoryid { get; set; }
        public Int32 categoryid { get; set; }
        public string imagesrc { get; set; }
        public int productconceptid { get; set; }
        public string active { get; set; }
        public HttpPostedFile photo { get; set; }
        public string photoname { get; set; }
    }
}
