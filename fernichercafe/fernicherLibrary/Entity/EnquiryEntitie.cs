﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fernicherLibrary.Entity
{
    public class EnquiryEntitie
    {

    }
    public class RootObject
    {
        public List<Enquiry> ProductList { get; set; }
    }
    public class Enquiry
    {
        public int subcategoryid { get; set; }
        public string categoryname { get; set; }
        public int productid { get; set; }
        public string name { get; set; }
        public string Image { get; set; }
        public int mobile { get; set; }
        public string email { get; set; }
        public string comments { get; set; }
    }
}
