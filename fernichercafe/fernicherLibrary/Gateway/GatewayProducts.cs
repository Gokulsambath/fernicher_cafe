﻿using DairymanLibrary;
using fernicherLibrary.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fernicherLibrary.Gateway
{
    public class GatewayProducts
    {
        String strConnectionString;

        public GatewayProducts()
        {
            strConnectionString = GatewayHelper.GetConnectionString();
        }
        public DataSet GetAllSubCategoryForDropdown(string v)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllSubCategoryForDropdown", objSqlConnection);
            DataSet objDataSet = new DataSet();
            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@id", SqlDbType.NVarChar);
            objSqlDataAdapter.SelectCommand.Parameters["@id"].Value = v;
            objSqlDataAdapter.Fill(objDataSet);
            return objDataSet;
        }

        internal DataTable GetMapLevelDetailsById(string number, string value)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetMapLevelDetailsById", objSqlConnection);
            DataTable objDataSet = new DataTable();
            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@category", SqlDbType.NVarChar);
            objSqlDataAdapter.SelectCommand.Parameters["@category"].Value = number;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@id", SqlDbType.BigInt);
            objSqlDataAdapter.SelectCommand.Parameters["@id"].Value = value;
            objSqlDataAdapter.Fill(objDataSet);
            return objDataSet;
        }

        public DataTable GetProductDetailsBySearchList(string searchkeyword)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetProductDetailsBySearchList", objSqlConnection);
            DataTable objDataSet = new DataTable();
            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@search", SqlDbType.NVarChar);
            objSqlDataAdapter.SelectCommand.Parameters["@search"].Value = searchkeyword;
            objSqlDataAdapter.Fill(objDataSet);
            return objDataSet;
        }

        public DataSet GetAllProducts(int categoryid, int subcategoryid)
        {
            // throw new NotImplementedException();
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllProductsOffers", objSqlConnection);
            DataSet objDataSet = new DataSet();
            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@categoryid", SqlDbType.Int);
            objSqlDataAdapter.SelectCommand.Parameters["@categoryid"].Value = categoryid;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@subcategoryid", SqlDbType.Int);
            objSqlDataAdapter.SelectCommand.Parameters["@subcategoryid"].Value = subcategoryid;
            objSqlDataAdapter.Fill(objDataSet);
            return objDataSet;
        }

        public DataSet EditMapping(int iddd)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("EditMapping", objSqlConnection);
            DataSet objDataSet = new DataSet();
            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@id", SqlDbType.Int);
            objSqlDataAdapter.SelectCommand.Parameters["@id"].Value = iddd;

            objSqlDataAdapter.Fill(objDataSet);
            return objDataSet;
        }

        public DataSet EditProductConcepts(int iddd)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("EditProductConcepts", objSqlConnection);
            DataSet objDataSet = new DataSet();
            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@id", SqlDbType.Int);
            objSqlDataAdapter.SelectCommand.Parameters["@id"].Value = iddd;

            objSqlDataAdapter.Fill(objDataSet);
            return objDataSet;
        }

        public DataSet EditOffers(int iddd)
        {
            // throw new NotImplementedException();
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("EditOffers", objSqlConnection);
            DataSet objDataSet = new DataSet();
            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@id", SqlDbType.Int);
            objSqlDataAdapter.SelectCommand.Parameters["@id"].Value = iddd;
            
            objSqlDataAdapter.Fill(objDataSet);
            return objDataSet;
        }

        public DataTable GetProductDetailsById(string s,long v, string v1)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetProductDetailsById", objSqlConnection);
            DataTable objDataSet = new DataTable();
            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@category", SqlDbType.NVarChar);
            objSqlDataAdapter.SelectCommand.Parameters["@category"].Value = s;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@id", SqlDbType.BigInt);
            objSqlDataAdapter.SelectCommand.Parameters["@id"].Value = v;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@maplevel", SqlDbType.NVarChar);
            objSqlDataAdapter.SelectCommand.Parameters["@maplevel"].Value = v1;
            objSqlDataAdapter.Fill(objDataSet);
            return objDataSet;
        }

        public DataSet GetAllMappedItems()
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllMappedItems", objSqlConnection);
            DataSet objDataSet = new DataSet();
            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;            
            objSqlDataAdapter.Fill(objDataSet);
            return objDataSet;
        }

        public DataSet EditAccessory(int iddd, string v)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("EditAccessory", objSqlConnection);
            DataSet objDataSet = new DataSet(); 
            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@id", SqlDbType.BigInt);
            objSqlDataAdapter.SelectCommand.Parameters["@id"].Value = iddd;
            objSqlDataAdapter.Fill(objDataSet);
            return objDataSet;
        }

        public DataSet GetAllMappingLevel()
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllMappingLevel", objSqlConnection);
            DataSet objDataSet = new DataSet();
            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.Fill(objDataSet);
            return objDataSet;
        }

        public DataSet GetAllProductConcepts(int categoryid, int subcategoryid)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllProductsConcepts", objSqlConnection);
            DataSet objDataSet = new DataSet();
            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@categoryid", SqlDbType.Int);
            objSqlDataAdapter.SelectCommand.Parameters["@categoryid"].Value = categoryid;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@subcategoryid", SqlDbType.Int);
            objSqlDataAdapter.SelectCommand.Parameters["@subcategoryid"].Value = subcategoryid;
            objSqlDataAdapter.Fill(objDataSet);
            return objDataSet;
        }

        public DataSet GetAllMaplevelfordropdown(int v, int idname)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllMaplevelfordropdown", objSqlConnection);
            DataSet objDataSet = new DataSet();
            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@sub", SqlDbType.Int);
            objSqlDataAdapter.SelectCommand.Parameters["@sub"].Value = v;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@cat", SqlDbType.Int);
            objSqlDataAdapter.SelectCommand.Parameters["@cat"].Value = idname;
            objSqlDataAdapter.Fill(objDataSet);
            return objDataSet;
        }

        public DataSet GetAllKidProductsfordropdown(int number, string value)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllKidProductsfordropdown", objSqlConnection);
            DataSet objDataSet = new DataSet();
            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@id", SqlDbType.Int);
            objSqlDataAdapter.SelectCommand.Parameters["@id"].Value = number;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@value", SqlDbType.NVarChar);
            objSqlDataAdapter.SelectCommand.Parameters["@value"].Value = value;
            objSqlDataAdapter.Fill(objDataSet);
            return objDataSet;
        }

        public DataSet GetAllKidProductsmap(int v, string idname)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllKidProductsmap", objSqlConnection);
            DataSet objDataSet = new DataSet();
            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@id", SqlDbType.Int);
            objSqlDataAdapter.SelectCommand.Parameters["@id"].Value = v;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@value", SqlDbType.NVarChar);
            objSqlDataAdapter.SelectCommand.Parameters["@value"].Value = idname;
            objSqlDataAdapter.Fill(objDataSet);
            return objDataSet;
        }

        public void deletemaplevel(int v1, int v2, int v3)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("deletemaplevel", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;

            objSqlCommand.Parameters.Add("@productid", SqlDbType.Int);
            objSqlCommand.Parameters["@productid"].Value = v1;

            objSqlCommand.Parameters.Add("@subcategoryid", SqlDbType.Int);
            objSqlCommand.Parameters["@subcategoryid"].Value = v2;

            objSqlCommand.Parameters.Add("@categoryid", SqlDbType.Int);
            objSqlCommand.Parameters["@categoryid"].Value = v3;

            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public DataSet gettheproducts(string id, string idname)
        {
            // throw new NotImplementedException();
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("gettheproducts", objSqlConnection);
            DataSet objDataSet = new DataSet();
            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@id", SqlDbType.BigInt);
            objSqlDataAdapter.SelectCommand.Parameters["@id"].Value = id;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@idname", SqlDbType.VarChar);
            objSqlDataAdapter.SelectCommand.Parameters["@idname"].Value = idname;


            objSqlDataAdapter.Fill(objDataSet);
            return objDataSet;
        }

        internal void AddMappingLevel(MappingEntity ent)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("AddMappingLevel", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@cat", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@cat"].Value = ent.categoryid;

            objSqlCommand.Parameters.Add("@mapid", SqlDbType.Int);
            objSqlCommand.Parameters["@mapid"].Value = ent.mapid;

            objSqlCommand.Parameters.Add("@productid", SqlDbType.Int);
            objSqlCommand.Parameters["@productid"].Value = ent.productid;

            objSqlCommand.Parameters.Add("@subid", SqlDbType.Int);
            objSqlCommand.Parameters["@subid"].Value = ent.subcategoryid;

            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public DataSet fillsubcategorybycategoryid(string id)
        {
            //throw new NotImplementedException();
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("fillsubcategorybycategoryid", objSqlConnection);
            DataSet objDataSet = new DataSet();
            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@id", SqlDbType.Int);
            objSqlDataAdapter.SelectCommand.Parameters["@id"].Value = Convert.ToInt32(id);
            objSqlDataAdapter.Fill(objDataSet);
            return objDataSet;
        }

        public void UpdateMappingLevel(MappingEntity ent)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("UpdateMappingLevel", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@mapname", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@mapname"].Value = ent.mapname;

            objSqlCommand.Parameters.Add("@mapid", SqlDbType.Int);
            objSqlCommand.Parameters["@mapid"].Value = ent.mapid;

            objSqlCommand.Parameters.Add("@subcategoryid", SqlDbType.Int);
            objSqlCommand.Parameters["@subcategoryid"].Value = ent.subcategoryid;
            objSqlCommand.Parameters.Add("@categoryid", SqlDbType.Int);
            objSqlCommand.Parameters["@categoryid"].Value = ent.categoryid;
            objSqlCommand.Parameters.Add("@flgActive", SqlDbType.Int);
            objSqlCommand.Parameters["@flgActive"].Value = Convert.ToInt32(ent.flgActive);

            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public void UpdateMapping(MappingEntity ent)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("UpdateMapping", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@mapname", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@mapname"].Value = ent.mapname;

            objSqlCommand.Parameters.Add("@mapid", SqlDbType.Int);
            objSqlCommand.Parameters["@mapid"].Value = ent.mapid;

            objSqlCommand.Parameters.Add("@photo", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@photo"].Value = ent.photoname;

            objSqlCommand.Parameters.Add("@subcategoryid", SqlDbType.Int);
            objSqlCommand.Parameters["@subcategoryid"].Value = ent.subcategoryid;
            objSqlCommand.Parameters.Add("@categoryid", SqlDbType.Int);
            objSqlCommand.Parameters["@categoryid"].Value = ent.categoryid;
            objSqlCommand.Parameters.Add("@flgActive", SqlDbType.Int);
            objSqlCommand.Parameters["@flgActive"].Value = Convert.ToInt32(ent.flgActive);

            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public void AddMapping(MappingEntity ent)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("AddMapping", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@mapname", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@mapname"].Value =ent.mapname;

            objSqlCommand.Parameters.Add("@photo", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@photo"].Value = ent.photoname;

            objSqlCommand.Parameters.Add("@subcategoryid", SqlDbType.Int);
            objSqlCommand.Parameters["@subcategoryid"].Value = ent.subcategoryid;
            objSqlCommand.Parameters.Add("@categoryid", SqlDbType.Int);
            objSqlCommand.Parameters["@categoryid"].Value = ent.categoryid;
            objSqlCommand.Parameters.Add("@flgActive", SqlDbType.Int);
            objSqlCommand.Parameters["@flgActive"].Value = Convert.ToInt32(ent.flgActive);

            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public void Updateproductconcept(OfferEntity ent)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("UpdateProductconcept", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@active", SqlDbType.Int);
            objSqlCommand.Parameters["@active"].Value = Convert.ToInt32(ent.active);
            objSqlCommand.Parameters.Add("@productid", SqlDbType.Int);
            objSqlCommand.Parameters["@productid"].Value = ent.productid;
            objSqlCommand.Parameters.Add("@subcategoryid", SqlDbType.Int);
            objSqlCommand.Parameters["@subcategoryid"].Value = ent.subcategoryid;
            objSqlCommand.Parameters.Add("@categoryid", SqlDbType.Int);
            objSqlCommand.Parameters["@categoryid"].Value = ent.categoryid;
            objSqlCommand.Parameters.Add("@productconceptid", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@productconceptid"].Value = ent.productconceptid;

            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public void AddProductConcept(OfferEntity ent)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("InsertProductConcept", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@active", SqlDbType.Int);
            objSqlCommand.Parameters["@active"].Value = Convert.ToInt32(ent.active);
            objSqlCommand.Parameters.Add("@productid", SqlDbType.Int);
            objSqlCommand.Parameters["@productid"].Value = ent.productid;
            objSqlCommand.Parameters.Add("@subcategoryid", SqlDbType.Int);
            objSqlCommand.Parameters["@subcategoryid"].Value = ent.subcategoryid;
            objSqlCommand.Parameters.Add("@categoryid", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@categoryid"].Value = ent.categoryid;

            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public void UpdateOffer(OfferEntity ent)
        {
            //throw new NotImplementedException();
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("Updateoffer", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@offername", SqlDbType.VarChar);
            objSqlCommand.Parameters["@offername"].Value = ent.offername;
            objSqlCommand.Parameters.Add("@productid", SqlDbType.Int);
            objSqlCommand.Parameters["@productid"].Value = ent.productid;
            objSqlCommand.Parameters.Add("@subcategoryid", SqlDbType.Int);
            objSqlCommand.Parameters["@subcategoryid"].Value = ent.subcategoryid;
            objSqlCommand.Parameters.Add("@categoryid", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@categoryid"].Value = ent.categoryid;
            objSqlCommand.Parameters.Add("@offerid", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@offerid"].Value = ent.offerid;
            objSqlCommand.Parameters.Add("@photoname", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@photoname"].Value = ent.photoname;

            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();

        }

        public void UpdateHomeBanner(HomeEntity ent)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("UpdateHomeBanner", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@bannerheader", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@bannerheader"].Value = ent.bannerheader;
            objSqlCommand.Parameters.Add("@bannersubheader", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@bannersubheader"].Value = ent.bannersubheader;
            objSqlCommand.Parameters.Add("@photourl", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@photourl"].Value = ent.photoname;
            objSqlCommand.Parameters.Add("@userid", SqlDbType.Int);
            objSqlCommand.Parameters["@userid"].Value = ent.userid;
            objSqlCommand.Parameters.Add("@active", SqlDbType.Int);
            objSqlCommand.Parameters["@active"].Value = Convert.ToInt32(ent.Active);

            objSqlCommand.Parameters.Add("@productid", SqlDbType.Int);
            objSqlCommand.Parameters["@productid"].Value = ent.productid;
            objSqlCommand.Parameters.Add("@subcategoryid", SqlDbType.Int);
            objSqlCommand.Parameters["@subcategoryid"].Value = ent.subcategoryid;
            objSqlCommand.Parameters.Add("@categoryid", SqlDbType.Int);
            objSqlCommand.Parameters["@categoryid"].Value = ent.categoryid;
            objSqlCommand.Parameters.Add("@bannerid", SqlDbType.Int);
            objSqlCommand.Parameters["@bannerid"].Value = ent.bannerid;
            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public void AddHomeBanner(HomeEntity ent)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("AddHomeBanner", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@bannerheader", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@bannerheader"].Value = ent.bannerheader;
            objSqlCommand.Parameters.Add("@bannersubheader", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@bannersubheader"].Value = ent.bannersubheader;
            objSqlCommand.Parameters.Add("@photourl", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@photourl"].Value = ent.photoname;
            objSqlCommand.Parameters.Add("@userid", SqlDbType.Int);
            objSqlCommand.Parameters["@userid"].Value = ent.userid;
            objSqlCommand.Parameters.Add("@productid", SqlDbType.Int);
            objSqlCommand.Parameters["@productid"].Value = ent.productid;
            objSqlCommand.Parameters.Add("@subcategoryid", SqlDbType.Int);
            objSqlCommand.Parameters["@subcategoryid"].Value = ent.subcategoryid;
            objSqlCommand.Parameters.Add("@categoryid", SqlDbType.Int);
            objSqlCommand.Parameters["@categoryid"].Value = ent.categoryid;
            objSqlCommand.Parameters.Add("@active", SqlDbType.Int);
            objSqlCommand.Parameters["@active"].Value = Convert.ToInt32(ent.Active);

            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public DataSet GetAllProductDetails(int number, string value)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllProductDetails", objSqlConnection);
            DataSet objDataSet = new DataSet();
            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@id", SqlDbType.Int);
            objSqlDataAdapter.SelectCommand.Parameters["@id"].Value = number;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@value", SqlDbType.NVarChar);
            objSqlDataAdapter.SelectCommand.Parameters["@value"].Value = value;
            objSqlDataAdapter.Fill(objDataSet);
            return objDataSet;
        }

        public void AddOffer(OfferEntity ent)
        {
            // throw new NotImplementedException();
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("Insertoffer", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@offername", SqlDbType.VarChar);
            objSqlCommand.Parameters["@offername"].Value = ent.offername;
            objSqlCommand.Parameters.Add("@productid", SqlDbType.Int);
            objSqlCommand.Parameters["@productid"].Value = ent.productid;
            objSqlCommand.Parameters.Add("@subcategoryid", SqlDbType.Int);
            objSqlCommand.Parameters["@subcategoryid"].Value = ent.subcategoryid;
            objSqlCommand.Parameters.Add("@categoryid", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@categoryid"].Value = ent.categoryid;
            objSqlCommand.Parameters.Add("@photoname", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@photoname"].Value = ent.photoname;

            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();

        }

        public void AddProducts(ProductEntities sourcelead,string value)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("InsertProducts", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@productname", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@productname"].Value = sourcelead.productname;
            objSqlCommand.Parameters.Add("@photourl", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@photourl"].Value = sourcelead.photourl;
            objSqlCommand.Parameters.Add("@bannerurl", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@bannerurl"].Value = sourcelead.bannerurl;
            objSqlCommand.Parameters.Add("@desc", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@desc"].Value = sourcelead.description;
            objSqlCommand.Parameters.Add("@userid", SqlDbType.Int);
            objSqlCommand.Parameters["@userid"].Value = sourcelead.userid;
            objSqlCommand.Parameters.Add("@subcategoryid", SqlDbType.BigInt);
            objSqlCommand.Parameters["@subcategoryid"].Value = sourcelead.SubCategoryId;
            objSqlCommand.Parameters.Add("@status", SqlDbType.Int);
            objSqlCommand.Parameters["@status"].Value = sourcelead.status;
            objSqlCommand.Parameters.Add("@value", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@value"].Value = value;
            objSqlCommand.Parameters.Add("@videolink", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@videolink"].Value = sourcelead.videolink;
            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public void updateimagepath(long imageid, string colorcode, string value)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("updateimagecolorcode", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@id", SqlDbType.BigInt);
            objSqlCommand.Parameters["@id"].Value = imageid;
            objSqlCommand.Parameters.Add("@colorcode", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@colorcode"].Value = colorcode;
            objSqlCommand.Parameters.Add("@value", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@value"].Value = value;
            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public void UpdateAccessory(AccessoryEntitie sourcelead, string v)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("UpdateAccessory", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@accessoryname", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@accessoryname"].Value = sourcelead.AccessoryName;

            objSqlCommand.Parameters.Add("@model", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@model"].Value = sourcelead.Model;

            objSqlCommand.Parameters.Add("@width", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@width"].Value = sourcelead.width;

            objSqlCommand.Parameters.Add("@height", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@height"].Value = sourcelead.height;

            objSqlCommand.Parameters.Add("@depth", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@depth"].Value = sourcelead.Depth;

            objSqlCommand.Parameters.Add("@productid", SqlDbType.BigInt);
            objSqlCommand.Parameters["@productid"].Value = sourcelead.productid;

            objSqlCommand.Parameters.Add("@subcategoryid", SqlDbType.BigInt);
            objSqlCommand.Parameters["@subcategoryid"].Value = sourcelead.subcategoryid;

            objSqlCommand.Parameters.Add("@accessoryid", SqlDbType.BigInt);
            objSqlCommand.Parameters["@accessoryid"].Value = sourcelead.accessoryid;


            objSqlCommand.Parameters.Add("@status", SqlDbType.Int);
            objSqlCommand.Parameters["@status"].Value = Convert.ToInt32(sourcelead.status);

            objSqlCommand.Parameters.Add("@photourl", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@photourl"].Value = sourcelead.photoname;
            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public void AddAccessory(AccessoryEntitie sourcelead, string v)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("AddAccessory", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@accessoryname", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@accessoryname"].Value = sourcelead.AccessoryName;

            objSqlCommand.Parameters.Add("@status", SqlDbType.Int);
            objSqlCommand.Parameters["@status"].Value = Convert.ToInt32(sourcelead.status);

            objSqlCommand.Parameters.Add("@model", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@model"].Value = sourcelead.Model;

            objSqlCommand.Parameters.Add("@width", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@width"].Value = sourcelead.width;

            objSqlCommand.Parameters.Add("@height", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@height"].Value = sourcelead.height;

            objSqlCommand.Parameters.Add("@depth", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@depth"].Value = sourcelead.Depth;

            objSqlCommand.Parameters.Add("@productid", SqlDbType.BigInt);
            objSqlCommand.Parameters["@productid"].Value = sourcelead.productid;

            objSqlCommand.Parameters.Add("@subcategoryid", SqlDbType.BigInt);
            objSqlCommand.Parameters["@subcategoryid"].Value = sourcelead.subcategoryid;

            objSqlCommand.Parameters.Add("@photourl", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@photourl"].Value = sourcelead.photoname;
            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public DataSet GetAllAccessories(int subcategoryid, int productid)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllAccessories", objSqlConnection);
            DataSet objDataSet = new DataSet();
            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@subcategoryid", SqlDbType.Int);
            objSqlDataAdapter.SelectCommand.Parameters["@subcategoryid"].Value = subcategoryid;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@productid", SqlDbType.Int);
            objSqlDataAdapter.SelectCommand.Parameters["@productid"].Value = productid;
            objSqlDataAdapter.Fill(objDataSet);
            return objDataSet;
        }

        public void InsertGreeneryProductdetails(ProductEntities sourcelead)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("UpdateGreeneryfurnituremanagedetails", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@productgreeneryid", SqlDbType.BigInt);
            objSqlCommand.Parameters["@productgreeneryid"].Value = sourcelead.productgreeneryid;
            objSqlCommand.Parameters.Add("@area", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@area"].Value = sourcelead.area;
            objSqlCommand.Parameters.Add("@size", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@size"].Value = sourcelead.size;
            objSqlCommand.Parameters.Add("@modelname", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@modelname"].Value = sourcelead.modelname;
            //objSqlCommand.Parameters.Add("@features", SqlDbType.NVarChar);
            //objSqlCommand.Parameters["@features"].Value = sourcelead.features;
            //objSqlCommand.Parameters.Add("@agegroup", SqlDbType.NVarChar);
            //objSqlCommand.Parameters["@agegroup"].Value = sourcelead.agegroup;
            //objSqlCommand.Parameters.Add("@manufacture", SqlDbType.NVarChar);
            //objSqlCommand.Parameters["@manufacture"].Value = sourcelead.manufacture;
            //objSqlCommand.Parameters.Add("@materials", SqlDbType.NVarChar);
            //objSqlCommand.Parameters["@materials"].Value = sourcelead.materials;
            //objSqlCommand.Parameters.Add("@certificates", SqlDbType.NVarChar);
            //objSqlCommand.Parameters["@certificates"].Value = sourcelead.certificates;
            //objSqlCommand.Parameters.Add("@availability", SqlDbType.NVarChar);
            //objSqlCommand.Parameters["@availability"].Value = sourcelead.availability;
            objSqlCommand.Parameters.Add("@userid", SqlDbType.Int);
            objSqlCommand.Parameters["@userid"].Value = sourcelead.userid;
            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
            Insertgreenerytechnicaldetails(sourcelead);
            InsertgreeneryImages(sourcelead);
        }

        private void InsertgreeneryImages(ProductEntities sourcelead)
        {
            if (sourcelead.photourlarray.Count > 0)
            {
                for (int i = 0; i < sourcelead.photourlarray.Count; i++)
                {
                    SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
                    SqlCommand objSqlCommand = new SqlCommand("Insertgreeneryfurnituremanagedetailsimages", objSqlConnection);
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Parameters.Add("@productgreeneryid", SqlDbType.BigInt);
                    objSqlCommand.Parameters["@productgreeneryid"].Value = sourcelead.productgreeneryid;
                    objSqlCommand.Parameters.Add("@imagename", SqlDbType.NVarChar);
                    objSqlCommand.Parameters["@imagename"].Value = sourcelead.photourlarray[i];
                    objSqlCommand.Parameters.Add("@url", SqlDbType.NVarChar);
                    objSqlCommand.Parameters["@url"].Value = sourcelead.photonamearray[i];
                    objSqlCommand.Parameters.Add("@userid", SqlDbType.Int);
                    objSqlCommand.Parameters["@userid"].Value = sourcelead.userid;
                    objSqlConnection.Open();
                    objSqlCommand.ExecuteNonQuery();
                    objSqlConnection.Close();
                }
            }
        }

        private void Insertgreenerytechnicaldetails(ProductEntities sourcelead)
        {
            if (sourcelead.Technicaldetails.Count > 0)
            {
                for (int i = 0; i < sourcelead.Technicaldetails.Count; i++)
                {
                    SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
                    SqlCommand objSqlCommand = new SqlCommand("Insertgreeneryfurnituremanagetechnicaldetails", objSqlConnection);
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Parameters.Add("@productgreeneryid", SqlDbType.BigInt);
                    objSqlCommand.Parameters["@productgreeneryid"].Value = sourcelead.productgreeneryid;
                    objSqlCommand.Parameters.Add("@name", SqlDbType.NVarChar);
                    objSqlCommand.Parameters["@name"].Value = sourcelead.Technicaldetails[i];
                    objSqlCommand.Parameters.Add("@status", SqlDbType.NVarChar);
                    objSqlCommand.Parameters["@status"].Value = 'Y';
                    objSqlCommand.Parameters.Add("@userid", SqlDbType.Int);
                    objSqlCommand.Parameters["@userid"].Value = sourcelead.userid;
                    objSqlConnection.Open();
                    objSqlCommand.ExecuteNonQuery();
                    objSqlConnection.Close();
                }
            }
        }

        public void deleteimagepath(long imageid,string value)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("deleteimagepath", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@id", SqlDbType.BigInt);
            objSqlCommand.Parameters["@id"].Value = imageid;
            objSqlCommand.Parameters.Add("@value", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@value"].Value = value;
            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public DataSet getimagepath(long imageid,string events)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("getimagepath", objSqlConnection);
            DataSet objDataSet = new DataSet();
            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@id", SqlDbType.BigInt);
            objSqlDataAdapter.SelectCommand.Parameters["@id"].Value = imageid;

            objSqlDataAdapter.SelectCommand.Parameters.Add("@event", SqlDbType.NVarChar);
            objSqlDataAdapter.SelectCommand.Parameters["@event"].Value = events;
            objSqlDataAdapter.Fill(objDataSet);
            return objDataSet;
        }

        public void InsertProductdetails(ProductEntities sourcelead)
        { 
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("Updatekidfurnituremanagedetails", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@productkidid", SqlDbType.BigInt);
            objSqlCommand.Parameters["@productkidid"].Value = sourcelead.productkidid;
            objSqlCommand.Parameters.Add("@target", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@target"].Value = sourcelead.targetgroup;
            objSqlCommand.Parameters.Add("@theme", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@theme"].Value = sourcelead.theme;
            objSqlCommand.Parameters.Add("@segment", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@segment"].Value = sourcelead.segment;
            objSqlCommand.Parameters.Add("@features", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@features"].Value = sourcelead.features;
            objSqlCommand.Parameters.Add("@agegroup", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@agegroup"].Value = sourcelead.agegroup;
            objSqlCommand.Parameters.Add("@manufacture", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@manufacture"].Value = sourcelead.manufacture;
            objSqlCommand.Parameters.Add("@materials", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@materials"].Value = sourcelead.materials;
            objSqlCommand.Parameters.Add("@certificates", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@certificates"].Value = sourcelead.certificates;
            objSqlCommand.Parameters.Add("@availability", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@availability"].Value = sourcelead.availability;
            objSqlCommand.Parameters.Add("@userid", SqlDbType.Int);
            objSqlCommand.Parameters["@userid"].Value = sourcelead.userid;
            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
            Inserttechnicaldetails(sourcelead);
            InsertImages(sourcelead);
        }

        private void InsertImages(ProductEntities sourcelead)
        {
            if (sourcelead.photourlarray.Count > 0)
            {
                for (int i = 0; i < sourcelead.photourlarray.Count; i++)
                {
                    SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
                    SqlCommand objSqlCommand = new SqlCommand("Insertkidfurnituremanagedetailsimages", objSqlConnection);
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Parameters.Add("@productkidid", SqlDbType.BigInt);
                    objSqlCommand.Parameters["@productkidid"].Value = sourcelead.productkidid;
                    objSqlCommand.Parameters.Add("@imagename", SqlDbType.NVarChar);
                    objSqlCommand.Parameters["@imagename"].Value = sourcelead.photourlarray[i];
                    objSqlCommand.Parameters.Add("@url", SqlDbType.NVarChar);
                    objSqlCommand.Parameters["@url"].Value = sourcelead.photonamearray[i];
                    objSqlCommand.Parameters.Add("@userid", SqlDbType.Int);
                    objSqlCommand.Parameters["@userid"].Value = sourcelead.userid;
                    objSqlConnection.Open();
                    objSqlCommand.ExecuteNonQuery();
                    objSqlConnection.Close();
                }
            }
        }

        private void Inserttechnicaldetails(ProductEntities sourcelead)
        {
            if (sourcelead.Technicaldetails.Count > 0)
            {
                for (int i = 0; i < sourcelead.Technicaldetails.Count; i++)
                {
                    SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
                    SqlCommand objSqlCommand = new SqlCommand("Insertkidfurnituremanagetechnicaldetails", objSqlConnection);
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Parameters.Add("@productkidid", SqlDbType.BigInt);
                    objSqlCommand.Parameters["@productkidid"].Value = sourcelead.productkidid;
                    objSqlCommand.Parameters.Add("@name", SqlDbType.NVarChar);
                    objSqlCommand.Parameters["@name"].Value = sourcelead.Technicaldetails[i];
                    objSqlCommand.Parameters.Add("@status", SqlDbType.NVarChar);
                    objSqlCommand.Parameters["@status"].Value = 'Y';
                    objSqlCommand.Parameters.Add("@userid", SqlDbType.Int);
                    objSqlCommand.Parameters["@userid"].Value = sourcelead.userid;
                    objSqlConnection.Open();
                    objSqlCommand.ExecuteNonQuery();
                    objSqlConnection.Close();
                }
            }
        }

        public void UpdateProducts(ProductEntities sourcelead, string value)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("UpdateProducts", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@productname", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@productname"].Value = sourcelead.productname;
            objSqlCommand.Parameters.Add("@photourl", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@photourl"].Value = sourcelead.photourl;
            objSqlCommand.Parameters.Add("@bannerurl", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@bannerurl"].Value = sourcelead.bannerurl;
            objSqlCommand.Parameters.Add("@desc", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@desc"].Value = sourcelead.description;
            objSqlCommand.Parameters.Add("@userid", SqlDbType.Int);
            objSqlCommand.Parameters["@userid"].Value = sourcelead.userid;
            objSqlCommand.Parameters.Add("@subcategoryid", SqlDbType.BigInt);
            objSqlCommand.Parameters["@subcategoryid"].Value = sourcelead.SubCategoryId;
            objSqlCommand.Parameters.Add("@status", SqlDbType.Int);
            objSqlCommand.Parameters["@status"].Value = sourcelead.status;
            objSqlCommand.Parameters.Add("@value", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@value"].Value = value;
            objSqlCommand.Parameters.Add("@videolink", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@videolink"].Value = sourcelead.videolink;
            switch (value)
            {
                case "Kid":
                    objSqlCommand.Parameters.Add("@id", SqlDbType.BigInt);
                    objSqlCommand.Parameters["@id"].Value = sourcelead.productkidid;
                    break;
                case "Interior":
                    objSqlCommand.Parameters.Add("@id", SqlDbType.BigInt);
                    objSqlCommand.Parameters["@id"].Value = sourcelead.productinteriorid;
                    break;
                case "Livingroom":
                    objSqlCommand.Parameters.Add("@id", SqlDbType.BigInt);
                    objSqlCommand.Parameters["@id"].Value = sourcelead.productlivingroomid;
                    break;
                case "Greenery":
                    objSqlCommand.Parameters.Add("@id", SqlDbType.BigInt);
                    objSqlCommand.Parameters["@id"].Value = sourcelead.productgreeneryid;
                    break;
            }
           
            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public DataSet GetAllKidProducts(int number,string value)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllKidProducts", objSqlConnection);
            DataSet objDataSet = new DataSet();
            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@id", SqlDbType.Int);
            objSqlDataAdapter.SelectCommand.Parameters["@id"].Value = number;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@value", SqlDbType.NVarChar);
            objSqlDataAdapter.SelectCommand.Parameters["@value"].Value = value;
            objSqlDataAdapter.Fill(objDataSet);
            return objDataSet;
        }
        public DataSet GetAllKidProductsN(int v, string idname)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllKidProductsN", objSqlConnection);
            DataSet objDataSet = new DataSet();
            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@id", SqlDbType.Int);
            objSqlDataAdapter.SelectCommand.Parameters["@id"].Value = v;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@value", SqlDbType.NVarChar);
            objSqlDataAdapter.SelectCommand.Parameters["@value"].Value = idname;
            objSqlDataAdapter.Fill(objDataSet);
            return objDataSet;
        }

        public DataSet EditProductDetails(int iddd,string value)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("EditProductDetails", objSqlConnection);
            DataSet objDataSet = new DataSet();
            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@id", SqlDbType.BigInt);
            objSqlDataAdapter.SelectCommand.Parameters["@id"].Value = iddd;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@value", SqlDbType.NVarChar);
            objSqlDataAdapter.SelectCommand.Parameters["@value"].Value = value;
            objSqlDataAdapter.Fill(objDataSet);
            return objDataSet;
        }

        public void InsertLivingProductdetails(ProductEntities sourcelead)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("UpdateLivingfurnituremanagedetails", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@productlivingroomid", SqlDbType.BigInt);
            objSqlCommand.Parameters["@productlivingroomid"].Value = sourcelead.productlivingroomid;
            objSqlCommand.Parameters.Add("@features", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@features"].Value = sourcelead.features;
            objSqlCommand.Parameters.Add("@manufacture", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@manufacture"].Value = sourcelead.manufacture;
            objSqlCommand.Parameters.Add("@materials", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@materials"].Value = sourcelead.materials;
            objSqlCommand.Parameters.Add("@availability", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@availability"].Value = sourcelead.availability;
            objSqlCommand.Parameters.Add("@userid", SqlDbType.Int);
            objSqlCommand.Parameters["@userid"].Value = sourcelead.userid;
            objSqlCommand.Parameters.Add("@modelname", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@modelname"].Value = sourcelead.modelname;
            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
            InsertLivingtechnicaldetails(sourcelead);
            InsertLivingImages(sourcelead);
        }

        private void InsertLivingImages(ProductEntities sourcelead)
        {
            if (sourcelead.photourlarray.Count > 0)
            {
                for (int i = 0; i < sourcelead.photourlarray.Count; i++)
                {
                    SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
                    SqlCommand objSqlCommand = new SqlCommand("InsertLivingfurnituremanagedetailsimages", objSqlConnection);
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Parameters.Add("@productlivingroomid", SqlDbType.BigInt);
                    objSqlCommand.Parameters["@productlivingroomid"].Value = sourcelead.productlivingroomid;
                    objSqlCommand.Parameters.Add("@imagename", SqlDbType.NVarChar);
                    objSqlCommand.Parameters["@imagename"].Value = sourcelead.photourlarray[i];
                    objSqlCommand.Parameters.Add("@url", SqlDbType.NVarChar);
                    objSqlCommand.Parameters["@url"].Value = sourcelead.photonamearray[i];
                    objSqlCommand.Parameters.Add("@userid", SqlDbType.Int);
                    objSqlCommand.Parameters["@userid"].Value = sourcelead.userid;
                    objSqlConnection.Open();
                    objSqlCommand.ExecuteNonQuery();
                    objSqlConnection.Close();
                }
            }
        }

        private void InsertLivingtechnicaldetails(ProductEntities sourcelead)
        {
            if (sourcelead.Technicaldetails.Count > 0)
            {
                for (int i = 0; i < sourcelead.Technicaldetails.Count; i++)
                {
                    SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
                    SqlCommand objSqlCommand = new SqlCommand("InsertLivingfurnituremanagetechnicaldetails", objSqlConnection);
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Parameters.Add("@productlivingroomid", SqlDbType.BigInt);
                    objSqlCommand.Parameters["@productlivingroomid"].Value = sourcelead.productlivingroomid;
                    objSqlCommand.Parameters.Add("@name", SqlDbType.NVarChar);
                    objSqlCommand.Parameters["@name"].Value = sourcelead.Technicaldetails[i];
                    objSqlCommand.Parameters.Add("@status", SqlDbType.NVarChar);
                    objSqlCommand.Parameters["@status"].Value = 'Y';
                    objSqlCommand.Parameters.Add("@userid", SqlDbType.Int);
                    objSqlCommand.Parameters["@userid"].Value = sourcelead.userid;
                    objSqlConnection.Open();
                    objSqlCommand.ExecuteNonQuery();
                    objSqlConnection.Close();
                }
            }
        }

    }
}
