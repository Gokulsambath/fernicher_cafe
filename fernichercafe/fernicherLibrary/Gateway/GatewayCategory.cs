﻿using DairymanLibrary;
using fernicherLibrary.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fernicherLibrary.Gateway
{
    
    public class GatewayCategory
    {
        String strConnectionString;

        public GatewayCategory()
        {
            strConnectionString = GatewayHelper.GetConnectionString();
        }

        public DataSet GetAllDetailsAdmin()
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllCategoryAdmin", objSqlConnection);            DataSet objDataSet = new DataSet();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.Fill(objDataSet);            return objDataSet;
        }
        public DataSet GetAllDetails()
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllCategory", objSqlConnection);            DataSet objDataSet = new DataSet();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.Fill(objDataSet);            return objDataSet;
        }

        public DataSet GetAllActiveProductConcepts()
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllActiveProductConcepts", objSqlConnection);            DataSet objDataSet = new DataSet();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.Fill(objDataSet);            return objDataSet;
        }

        public DataSet GetAllActiveBanners()
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllActiveHomeBanner", objSqlConnection);            DataSet objDataSet = new DataSet();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.Fill(objDataSet);            return objDataSet;
        }

        public DataTable GetAllSubCategoryById(long v)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllSubCategoryById", objSqlConnection);            DataTable objDataSet = new DataTable();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@id", SqlDbType.BigInt);            objSqlDataAdapter.SelectCommand.Parameters["@id"].Value = v;
            objSqlDataAdapter.Fill(objDataSet);            return objDataSet;
        }

        public DataSet GetAllDetailsOffers()
        {
            // throw new NotImplementedException();
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("listofoffers", objSqlConnection);            DataSet objDataSet = new DataSet();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
           
            objSqlDataAdapter.Fill(objDataSet);            return objDataSet;
        }

        public DataSet EditCategoryDetails(int idd)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("EditCategoryDetails", objSqlConnection);            DataSet objDataSet = new DataSet();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@id", SqlDbType.BigInt);            objSqlDataAdapter.SelectCommand.Parameters["@id"].Value = idd;
            objSqlDataAdapter.Fill(objDataSet);            return objDataSet;
        }

        public void UpdatesubCategory(CategoryEntitie sourcelead)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("UpdateSubCategory", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@subcategoryid", SqlDbType.BigInt);
            objSqlCommand.Parameters["@subcategoryid"].Value = sourcelead.SubCategoryId;

            objSqlCommand.Parameters.Add("@CategoryId", SqlDbType.BigInt);
            objSqlCommand.Parameters["@CategoryId"].Value = sourcelead.CategoryId;
            objSqlCommand.Parameters.Add("@subcategoryname", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@subcategoryname"].Value = sourcelead.SubCategoryName;
            objSqlCommand.Parameters.Add("@status", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@status"].Value = sourcelead.status;

            objSqlCommand.Parameters.Add("@sortby", SqlDbType.Int);
            objSqlCommand.Parameters["@sortby"].Value = sourcelead.sortby;
            objSqlCommand.Parameters.Add("@userid", SqlDbType.Int);
            objSqlCommand.Parameters["@userid"].Value = sourcelead.userid;
            objSqlCommand.Parameters.Add("@photo", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@photo"].Value = sourcelead.photourl;
            objSqlCommand.Parameters.Add("@bannerurl", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@bannerurl"].Value = sourcelead.bannerurl;
            objSqlCommand.Parameters.Add("@description", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@description"].Value = sourcelead.description;
            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public void AddCategory(CategoryEntitie sourcelead)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("InsertCategory", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            //objSqlCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar);
            //objSqlCommand.Parameters["@CategoryName"].Value = sourcelead.CategoryName;
            objSqlCommand.Parameters.Add("@status", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@status"].Value = sourcelead.status;
            objSqlCommand.Parameters.Add("@displayname", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@displayname"].Value = sourcelead.displayname;
            objSqlCommand.Parameters.Add("@userid", SqlDbType.Int);
            objSqlCommand.Parameters["@userid"].Value = sourcelead.userid;
            objSqlCommand.Parameters.Add("@photo", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@photo"].Value = sourcelead.photourl;

            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public DataSet EditsubCategoryDetails(int iddd)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("EditsubCategoryDetails", objSqlConnection);            DataSet objDataSet = new DataSet();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.SelectCommand.Parameters.Add("@id", SqlDbType.BigInt);            objSqlDataAdapter.SelectCommand.Parameters["@id"].Value = iddd;
            objSqlDataAdapter.Fill(objDataSet);            return objDataSet;
        }
        public DataSet GetAllSubCategory()
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllSubCategory", objSqlConnection);            DataSet objDataSet = new DataSet();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.Fill(objDataSet);            return objDataSet;
        }
        public void AddsubCategory(CategoryEntitie sourcelead)
        {

            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("InsertSubCategory", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@CategoryId", SqlDbType.BigInt);
            objSqlCommand.Parameters["@CategoryId"].Value = sourcelead.CategoryId;
            objSqlCommand.Parameters.Add("@subcategoryname", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@subcategoryname"].Value = sourcelead.SubCategoryName;

            objSqlCommand.Parameters.Add("@status", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@status"].Value = sourcelead.status;
            objSqlCommand.Parameters.Add("@createdBy", SqlDbType.Int);
            objSqlCommand.Parameters["@createdBy"].Value = sourcelead.userid;
            objSqlCommand.Parameters.Add("@sortby", SqlDbType.Int);
            objSqlCommand.Parameters["@sortby"].Value = sourcelead.sortby;

            objSqlCommand.Parameters.Add("@photourl", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@photourl"].Value = sourcelead.photourl;
            objSqlCommand.Parameters.Add("@bannerurl", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@bannerurl"].Value = sourcelead.bannerurl;
            objSqlCommand.Parameters.Add("@description", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@description"].Value = sourcelead.description;
            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

      
        public void UpdateCategory(CategoryEntitie sourcelead)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("UpdateCategory", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;

            objSqlCommand.Parameters.Add("@CategoryId", SqlDbType.BigInt);
            objSqlCommand.Parameters["@CategoryId"].Value = sourcelead.CategoryId;
            //objSqlCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar);
            //objSqlCommand.Parameters["@CategoryName"].Value = sourcelead.CategoryName;
            objSqlCommand.Parameters.Add("@status", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@status"].Value = sourcelead.status;
            objSqlCommand.Parameters.Add("@displayname", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@displayname"].Value = sourcelead.displayname;

            objSqlCommand.Parameters.Add("@userid", SqlDbType.Int);
            objSqlCommand.Parameters["@userid"].Value = sourcelead.userid;
            objSqlCommand.Parameters.Add("@photo", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@photo"].Value = sourcelead.photourl;

            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public DataSet GetAllCategory()
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllCategorydropdown", objSqlConnection);            DataSet objDataSet = new DataSet();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.Fill(objDataSet);            return objDataSet;
        }
    }
}
