﻿using DairymanLibrary.Entity;
using fernicherLibrary.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DairymanLibrary.Gateway
{
   public  class GatewayUser
    {
        String strConnectionString;
        public GatewayUser()
        {
            strConnectionString = GatewayHelper.GetConnectionString();
        }
        public DataSet CheckUserLogin(UserEntities objInternaluserEntitie)        {            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("CheckUserLogin", objSqlConnection);            DataSet objDataSet = new DataSet();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;            objSqlDataAdapter.SelectCommand.Parameters.Add("@email", SqlDbType.NVarChar);            objSqlDataAdapter.SelectCommand.Parameters["@email"].Value = objInternaluserEntitie.Email;            objSqlDataAdapter.SelectCommand.Parameters.Add("@password", SqlDbType.NVarChar);
            objInternaluserEntitie.password = EncDec.Encrypt(objInternaluserEntitie.password, "passhide", "santosh", "SHA1", 4, "info1234hind5678", 192);
            objSqlDataAdapter.SelectCommand.Parameters["@password"].Value = objInternaluserEntitie.password;            objSqlDataAdapter.Fill(objDataSet);            return objDataSet;        }

        public DataSet CheckvisitorCount()
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("CheckvisitorCount", objSqlConnection);            DataSet objDataSet = new DataSet();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;            objSqlDataAdapter.Fill(objDataSet);            return objDataSet;
        }

        public DataTable Getbrandpage()
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("Getbrandpage", objSqlConnection);            DataTable objDataSet = new DataTable();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;            objSqlDataAdapter.Fill(objDataSet);            return objDataSet;
        }

        public void InsertVisitorDetails(string ipv4, string ipv6,string tempip,string session)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("InsertVisitors", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@ipv4", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@ipv4"].Value = ipv4;
            objSqlCommand.Parameters.Add("@ipv6", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@ipv6"].Value = ipv6;
            objSqlCommand.Parameters.Add("@tempip", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@tempip"].Value = tempip;
            objSqlCommand.Parameters.Add("@session", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@session"].Value = session;
            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public DataTable GetLabelControls()
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetLabelControls", objSqlConnection);            DataTable objDataSet = new DataTable();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;            objSqlDataAdapter.Fill(objDataSet);            return objDataSet;
        }

        public DataTable GetAboutUs()
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAboutUs", objSqlConnection);            DataTable objDataSet = new DataTable();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;            objSqlDataAdapter.Fill(objDataSet);            return objDataSet;
        }

        public void InsertBrandpage(UserEntities sourcelead)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("InsertBrandpage", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@brandheader", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@brandheader"].Value = sourcelead.brandpageheader;
            objSqlCommand.Parameters.Add("@brandsubheader", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@brandsubheader"].Value = sourcelead.brandpagesubheader;
            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public DataTable GetCompanyInfo()
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetCompanyInfo", objSqlConnection);            DataTable objDataSet = new DataTable();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;            objSqlDataAdapter.Fill(objDataSet);            return objDataSet;
        }

        public void InsertLabelControls(GeneralEntitie sourcelead)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("AddLabelControls", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@aboutusheader", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@aboutusheader"].Value = sourcelead.lblaboutusheader;
            objSqlCommand.Parameters.Add("@aboutussubheader", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@aboutussubheader"].Value = sourcelead.lblaboutusdesc;
            objSqlCommand.Parameters.Add("@productcategoriessubheader", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@productcategoriessubheader"].Value = sourcelead.lblproductcategoriesdesc;
            objSqlCommand.Parameters.Add("@productcategoriesheader", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@productcategoriesheader"].Value = sourcelead.lblproductcategoriesheader;
            objSqlCommand.Parameters.Add("@offerheader", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@offerheader"].Value = sourcelead.lblofferheader;
            objSqlCommand.Parameters.Add("@offersubheader", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@offersubheader"].Value = sourcelead.lblofferdesc;
            objSqlCommand.Parameters.Add("@productconceptheader", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@productconceptheader"].Value = sourcelead.lblproductconceptheader;
            objSqlCommand.Parameters.Add("@productconceptsubheader", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@productconceptsubheader"].Value = sourcelead.lblproductconceptdesc;
            objSqlCommand.Parameters.Add("@userid", SqlDbType.Int);
            objSqlCommand.Parameters["@userid"].Value = sourcelead.userid;
            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public DataSet GetAllHomeBanner(int i)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllHomeBanner", objSqlConnection);            DataSet objDataSet = new DataSet();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            objSqlDataAdapter.SelectCommand.Parameters.Add("@id", SqlDbType.Int);            objSqlDataAdapter.SelectCommand.Parameters["@id"].Value = i;            objSqlDataAdapter.Fill(objDataSet);            return objDataSet;
        }

        public void InsertAboutCompany(UserEntities sourcelead)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("InsertCompanyProfile", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@name", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@name"].Value = sourcelead.firstName;
            objSqlCommand.Parameters.Add("@mobile", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@mobile"].Value = sourcelead.mobileNo;
            objSqlCommand.Parameters.Add("@email", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@email"].Value = sourcelead.Email;
            objSqlCommand.Parameters.Add("@intro", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@intro"].Value = sourcelead.Intro;
            objSqlCommand.Parameters.Add("@address", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@address"].Value = sourcelead.address;
            objSqlCommand.Parameters.Add("@maplocation", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@maplocation"].Value = sourcelead.LocationEmbed;
            objSqlCommand.Parameters.Add("@workinghours", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@workinghours"].Value = sourcelead.workinghours;
            objSqlCommand.Parameters.Add("@userid", SqlDbType.Int);
            objSqlCommand.Parameters["@userid"].Value = sourcelead.userid;
            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public void InsertAboutUs(GeneralEntitie sourcelead)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("InsertAboutUs", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@bannerheader", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@bannerheader"].Value = sourcelead.bannerheader;
            objSqlCommand.Parameters.Add("@bannersubheader", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@bannersubheader"].Value = sourcelead.bannersubheader;
            objSqlCommand.Parameters.Add("@historyheader", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@historyheader"].Value = sourcelead.historyheader;
            objSqlCommand.Parameters.Add("@historysubheader", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@historysubheader"].Value = sourcelead.historysubheader;
            objSqlCommand.Parameters.Add("@title1", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@title1"].Value = sourcelead.title1;
            objSqlCommand.Parameters.Add("@title1desc", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@title1desc"].Value = sourcelead.title1desc;
            objSqlCommand.Parameters.Add("@title2", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@title2"].Value = sourcelead.title2;
            objSqlCommand.Parameters.Add("@title2desc", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@title2desc"].Value = sourcelead.title2desc;
            objSqlCommand.Parameters.Add("@directorheader", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@directorheader"].Value = sourcelead.directorheader;
            objSqlCommand.Parameters.Add("@directorheaddesc", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@directorheaddesc"].Value = sourcelead.directorheaddesc;
            objSqlCommand.Parameters.Add("@director1", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@director1"].Value = sourcelead.director1;
            objSqlCommand.Parameters.Add("@director1desc", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@director1desc"].Value = sourcelead.director1desc;
            objSqlCommand.Parameters.Add("@director2", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@director2"].Value = sourcelead.director2;
            objSqlCommand.Parameters.Add("@director2desc", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@director2desc"].Value = sourcelead.director2desc;
            objSqlCommand.Parameters.Add("@missionheader", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@missionheader"].Value = sourcelead.missionheader;
            objSqlCommand.Parameters.Add("@missiondesc", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@missiondesc"].Value = sourcelead.missiondesc;
            objSqlCommand.Parameters.Add("@bannerphoto", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@bannerphoto"].Value = sourcelead.bannerphoto;
            objSqlCommand.Parameters.Add("@title1photo", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@title1photo"].Value = sourcelead.title1photo;
            objSqlCommand.Parameters.Add("@title2photo", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@title2photo"].Value = sourcelead.title2photo;
            objSqlCommand.Parameters.Add("@director1photo", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@director1photo"].Value = sourcelead.director1photo;
            objSqlCommand.Parameters.Add("@director2photo", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@director2photo"].Value = sourcelead.director2photo;
            objSqlCommand.Parameters.Add("@missionphoto", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@missionphoto"].Value = sourcelead.missionphoto;
            objSqlCommand.Parameters.Add("@userid", SqlDbType.Int);
            objSqlCommand.Parameters["@userid"].Value = sourcelead.userid;
            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public DataSet GetAllNewsLetter()
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllNewsLetter", objSqlConnection);            DataSet objDataSet = new DataSet();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.Fill(objDataSet);            return objDataSet;
        }

        public DataSet GetAllActiveBrands()
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllActiveBrands", objSqlConnection);            DataSet objDataSet = new DataSet();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.Fill(objDataSet);            return objDataSet;
        }

        public DataSet GetAllBrands(int idd)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllBrands", objSqlConnection);            DataSet objDataSet = new DataSet();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            objSqlDataAdapter.SelectCommand.Parameters.Add("@id", SqlDbType.NVarChar);            objSqlDataAdapter.SelectCommand.Parameters["@id"].Value = idd;            objSqlDataAdapter.Fill(objDataSet);            return objDataSet;
        }

        public DataTable GetAllRating(string id,string category)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllRating", objSqlConnection);            DataTable objDataSet = new DataTable();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            objSqlDataAdapter.SelectCommand.Parameters.Add("@id", SqlDbType.BigInt);            objSqlDataAdapter.SelectCommand.Parameters["@id"].Value = Convert.ToInt64(id);


            objSqlDataAdapter.SelectCommand.Parameters.Add("@category", SqlDbType.NVarChar);            objSqlDataAdapter.SelectCommand.Parameters["@category"].Value = category;            objSqlDataAdapter.Fill(objDataSet);            return objDataSet;
        }

        public int checksubscribedUser(string email)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("checksubscribedUser", objSqlConnection);            DataTable objDataSet = new DataTable();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            objSqlDataAdapter.SelectCommand.Parameters.Add("@email", SqlDbType.NVarChar);            objSqlDataAdapter.SelectCommand.Parameters["@email"].Value = email;
                        objSqlDataAdapter.Fill(objDataSet);            return objDataSet.Rows.Count;
        }

        public void AddSubscription(string email)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("AddSubscription", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@email", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@email"].Value = email;
            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public List<Enquiry> getEnquiryImages(RootObject result)
        {
            
            List<Enquiry> productslist = new List<Enquiry>();
            for(int i = 0; i < result.ProductList.Count; i++)
            {
                SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
                SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("getEnquiryImages", objSqlConnection);
                DataTable objDataTable = new DataTable();
                objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                objSqlDataAdapter.SelectCommand.Parameters.Add("@id", SqlDbType.BigInt);
                objSqlDataAdapter.SelectCommand.Parameters["@id"].Value = Convert.ToInt64(result.ProductList[i].productid);


                objSqlDataAdapter.SelectCommand.Parameters.Add("@subcategoryid", SqlDbType.BigInt);
                objSqlDataAdapter.SelectCommand.Parameters["@subcategoryid"].Value = result.ProductList[i].subcategoryid;
                objSqlDataAdapter.Fill(objDataTable);
                if (objDataTable.Rows.Count > 0)
                {
                    for (int j = 0; j < objDataTable.Rows.Count; j++) {
                        string results = "";
                        Enquiry product = new Enquiry();
                        product.productid = Convert.ToInt32(objDataTable.Rows[j]["productid"]);
                        product.name = objDataTable.Rows[j]["name"].ToString();
                        product.subcategoryid =Convert.ToInt32(objDataTable.Rows[j]["SubCategoryId"]);
                        string yourString = objDataTable.Rows[j]["profileimage"].ToString();
                        String resultss = yourString.Replace("~", "");
                        if (resultss != "")
                        {
                            string result1 = resultss.Replace("\\", "/");
                            string result2 = result1.Remove(0, 1);
                            results = result2;
                        }
                        product.Image = results;
                        productslist.Add(product);
                    }
                }
            }
            return productslist;
        }

        public void UpdateBrands(GeneralEntitie ge)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("UpdateBrands", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@brandname", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@brandname"].Value = ge.brandname;
            objSqlCommand.Parameters.Add("@brandlink", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@brandlink"].Value = ge.brandlink;
            objSqlCommand.Parameters.Add("@photourl", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@photourl"].Value = ge.photoname;
            objSqlCommand.Parameters.Add("@userid", SqlDbType.Int);
            objSqlCommand.Parameters["@userid"].Value = ge.userid;
            objSqlCommand.Parameters.Add("@isactive", SqlDbType.Int);
            objSqlCommand.Parameters["@isactive"].Value = Convert.ToInt32(ge.active);
            objSqlCommand.Parameters.Add("@brandid", SqlDbType.Int);
            objSqlCommand.Parameters["@brandid"].Value = Convert.ToInt32(ge.brandid);
            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public void AddBrands(GeneralEntitie ge)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("InsertBrands", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@brandname", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@brandname"].Value = ge.brandname;
            objSqlCommand.Parameters.Add("@brandlink", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@brandlink"].Value = ge.brandlink;
            objSqlCommand.Parameters.Add("@photourl", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@photourl"].Value = ge.photoname;
            objSqlCommand.Parameters.Add("@userid", SqlDbType.Int);
            objSqlCommand.Parameters["@userid"].Value = ge.userid;
            objSqlCommand.Parameters.Add("@isactive", SqlDbType.Int);
            objSqlCommand.Parameters["@isactive"].Value = Convert.ToInt32(ge.active);
            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public DataSet GetAllcallback()
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllcallback", objSqlConnection);            DataSet objDataSet = new DataSet();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;            objSqlDataAdapter.Fill(objDataSet);            return objDataSet;
        }

        public void AddRating(GeneralEntitie ge)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("AddRating", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@name", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@name"].Value = ge.enquiryname;
            objSqlCommand.Parameters.Add("@email", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@email"].Value = ge.enquiryemail;
            objSqlCommand.Parameters.Add("@comments", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@comments"].Value = ge.enquirycomments;
            objSqlCommand.Parameters.Add("@rating", SqlDbType.Float);
            objSqlCommand.Parameters["@rating"].Value = ge.rating;
            objSqlCommand.Parameters.Add("@productid", SqlDbType.BigInt);
            objSqlCommand.Parameters["@productid"].Value = ge.productid;
            objSqlCommand.Parameters.Add("@category", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@category"].Value = ge.categoryname;
            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public void InsertCallBack(GeneralEntitie objEnttitie)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("InsertCallBack", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@name", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@name"].Value = objEnttitie.enquiryname;
            objSqlCommand.Parameters.Add("@email", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@email"].Value = objEnttitie.enquiryemail;
            objSqlCommand.Parameters.Add("@mobile", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@mobile"].Value = objEnttitie.enquirymobile;
            objSqlCommand.Parameters.Add("@comments", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@comments"].Value = objEnttitie.enquirycomments;
            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public DataSet GetAllEnquiry()
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllEnquiry", objSqlConnection);            DataSet objDataSet = new DataSet();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;            objSqlDataAdapter.Fill(objDataSet);            return objDataSet;
        }

        public void InsertEnquiry(GeneralEntitie objEnttitie)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("InsertEnquiry", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@productid", SqlDbType.BigInt);
            objSqlCommand.Parameters["@productid"].Value = Convert.ToInt64(objEnttitie.productid);
            objSqlCommand.Parameters.Add("@name", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@name"].Value = objEnttitie.categoryname;
            objSqlCommand.Parameters.Add("@username", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@username"].Value = objEnttitie.enquiryname;
            objSqlCommand.Parameters.Add("@email", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@email"].Value = objEnttitie.enquiryemail;
            objSqlCommand.Parameters.Add("@subcategoryid", SqlDbType.BigInt);
            objSqlCommand.Parameters["@subcategoryid"].Value = Convert.ToInt32(objEnttitie.subcategoryid);
            objSqlCommand.Parameters.Add("@mobile", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@mobile"].Value = objEnttitie.enquirymobile;
            objSqlCommand.Parameters.Add("@comments", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@comments"].Value = objEnttitie.enquirycomments;
            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }
    }
}
